# SPDX-License-Identifier: LGPL-2.1-or-later
# SPDX-FileCopyrightText: 2022 CERNimport MFipBitRate

import pytest
import time

from PyMFip.MFip import MFipBitRate, MFipHwCfg, MFipDataDir, MFipMCycleDef
from PyMFip.MFip import MFipDataDef, MFipPerWindDef, MFipMsgWindDef
from PyMFip.MFip import MFipWaitWindDef, MFipAperWindDef, MFipMCycle


class TestMFipbasic():
    """
    """

    def _print_structure_fields(self, struct):
        print()
        for field_name, field_type in struct._fields_:
            field_value = getattr(struct, field_name)
            print(f"{field_name}: {field_value}")

    def _check_report(self, report):
        for field_name, field_type in report._fields_:
            field_value = getattr(report, field_name)
            if field_name == 'ba_state':  # macro-cyle running
                if field_value != 2:  # BA not running uselless to check anything
                    assert True
                    return
                assert field_value == 2, f'{field_name} should be 2'
            if field_name.endswith('_err') and not field_name.endswith('mps_status_err'):
                assert field_value == 0
            if field_name == 'cycles':  # cycle counter
                assert field_value != 0, f'{field_name} should not be 0'
            if field_name == 'ext_sync_pulse_missed_count':
                assert field_value == 0, f'{field_name} should be 0'
        assert report.cycles == report.int_sync_pulse_count, "'cycles' and 'int_sync_pulse_count' should be equal"

    def test_open(self, mfip_dev):
        assert mfip_dev is not None

    def test_bus_speed(self, mfip_dev):
        speed = mfip_dev.get_hw_speed()
        assert speed == MFipBitRate.KB_2500.value

    def test_diag_report(self, mfip_dev):
        report = mfip_dev.get_report()
        # if pytest is launched with -s,or an assert fails,
        # report is dumped to the console
        self._print_structure_fields(report)
        self._check_report(report)


# Test1 specific Macro cycle programmation
@pytest.fixture(name="mcycle_test1", scope="class")
def mcycle_test1_fixture(mfip_dev):
    class MCycleTest1(MFipMCycle):

        mcycle_cfg = MFipMCycleDef(
            [  # list of windows composing the macro cycle
                MFipPerWindDef(  # Periodic window
                    [
                        MFipDataDef(0x057F, 2, MFipDataDir.PROD, None),
                        MFipDataDef(0x067F, 2, MFipDataDir.CONS, None),
                    ]
                ),
                MFipWaitWindDef(  # Wait window (mandatory to end macrocycle)
                    500,
                    1,
                ),
            ],
        )

        def per_var_cons_cb(self, mfip_hdl, mfip_data, mfip_irq):
            print()
            for var in self.prod_per_var_list:
                buf = var.contents.buffer
                print(f'Prod var {var.contents.id} next payload byte0: {buf[0]}')
                # Increment payload and write it
                self.per_var_payload += 1
                if self.per_var_payload == 256:
                    self.per_var_payload = 0
                buf[0] = self.per_var_payload
                self.write_var(var)
            for var in self.cons_per_var_list:
                # Update MFip var
                self.read_var(var)
                # Check if payload bytes equal current payload
                buf = var.contents.buffer
                print(f'Cons var {var.contents.id} payload byte0: {buf[0]}')

        def __init__(self):
            # Install a callback to
            for wind in self.mcycle_cfg.winds:
                if isinstance(wind, MFipPerWindDef):
                    last_var = wind.data[-1]._asdict()
                    last_var['cb'] = self.per_var_cons_cb
                    wind.data[-1] = MFipDataDef(**last_var)
                    self.per_var_payload = 0  # initial payload byte 0
            super().__init__(mfip_dev, self.mcycle_cfg)

    return MCycleTest1()


# Test2 specific Macro cycle programmation
@pytest.fixture(name="mcycle_test2", scope="class")
def mcycle_test2_fixture(mfip_dev):
    class MCycleTest2(MFipMCycle):

        mcycle_cfg = MFipMCycleDef(
            [  # list of windows composing the macro cycle
                MFipAperWindDef(  # Aperiodic window
                    18000,
                    1,
                    0,
                    0,
                    0,
                ),
                MFipWaitWindDef(  # Wait window (mandatory to end macrocycle)
                    20000,
                    1,
                ),
            ],
        )

        def __init__(self):
            super().__init__(mfip_dev, self.mcycle_cfg)

    return MCycleTest2()


class TestMFipMCycle():
    """
    """

    def test_macro_cycle_1(self, mcycle_test1):
        mcycle_test1.load()
        mcycle_test1.start()
        time.sleep(2)
        mcycle_test1.stop()
        mcycle_test1.reset()

    def test_macro_cycle_2(self, mcycle_test2):
        mcycle_test2.load()
        mcycle_test2.start()
        time.sleep(10)
        mcycle_test2.stop()
        mcycle_test2.reset()
