# SPDX-License-Identifier: LGPL-2.1-or-later
# SPDX-FileCopyrightText: 2022 CERN

import pytest
import logging

from PyMFip.MFip import MFipDev


@pytest.fixture(scope="session")
def mfip_dev(mfip_devid):
    with MFipDev(int(mfip_devid[0], 16)) as mfip_dev:
        yield mfip_dev

@pytest.fixture(scope='session', autouse=True)
def mfip_devid(request):
    return request.config.getoption("--devid")


def pytest_addoption(parser):
    parser.addoption("--devid", type=str, action="append",
                     required=True, help="A masterFip board devid")
