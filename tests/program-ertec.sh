#!/bin/bash

# SPDX-FileCopyrightText: 2024 CERN (home.cern)
#
# SPDX-License-Identifier: LGPL-2.1-or-later

. /usr/local/drivers/scripts/environment.sh

# program Ertec FW
sleep 5
/acc/local/L867/drv/masterfip/3.0.3/bin/pf_ertec_reprog -f ${FIRMWARE_PATH}/../svec-profip/profip-ertec-v3.0.3.bin
