#!/bin/bash
#
# SPDX-FileCopyrightText: 2024 CERN (home.cern)
#
# SPDX-License-Identifier: LGPL-2.1-or-later

. /usr/local/drivers/scripts/environment.sh

BITSTREAM_NAME="profip-v3.0.3"

modprobe -d "$MODPROBE_ROOT" mockturtle || exit 1

card_slots_fpga_configure "FMC-SVEC-A24" $BITSTREAM_NAME || exit 1

BUS=$(crate_get_bus_type)

if [ "$BUS" != "VME" ]
then
    logger -p user.error "$0 Unknown BUS type: $BUS"
    exit 1
fi

insmod /usr/local/drivers/svec-profip/wb_uart.ko || exit 1
insmod /usr/local/drivers/svec-profip/profip-svec.ko || exit 1

# Restart all masterfip mockturtle CPUs
for DEV in $(/usr/local/bin/lsmockturtle)
do
    APPID=$(cat "/sys/class/mockturtle/${DEV}/application_id")
    if [ "${APPID}" = "0x53564d42" ]
    then
        DEV_ID=0x$(echo "${DEV}" | cut -d "-" -f 2)
        /usr/local/bin/mockturtle-loader -D "${DEV_ID}" -i 0 -f "${FIRMWARE_PATH}/../svec-profip/profip-rt-ba-v3.0.3.bin"
        /usr/local/bin/mockturtle-loader -D "${DEV_ID}" -i 1 -f "${FIRMWARE_PATH}/../svec-profip/profip-rt-cmd-v3.0.3.bin"
        /usr/local/bin/mockturtle-cpu-restart -D "${DEV_ID}" -i 0 -i 1

        ln -s "/dev/mockturtle/${DEV}" /dev/masterfip.0
    fi
done

# Create masterFIP SHM before FIP diamon agent is launched
# This program create masterFIP SHM and exit.
/usr/local/bin/masterfip-diag-shm

#run profip diag tool

#change rw rights for tty
chmod a+rw /dev/ttywbu-pf-0
