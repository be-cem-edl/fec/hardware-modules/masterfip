files = [
    "spec_masterfip_mt.ucf",
    "spec_masterfip_mt_urv.vhd",
    "../../../dependencies/nanofip-gateware/src/wf_crc.vhd",
    "../../../dependencies/nanofip-gateware/src/wf_decr_counter.vhd",
    "../../../dependencies/nanofip-gateware/src/wf_incr_counter.vhd",
    "../../../dependencies/nanofip-gateware/src/wf_rx_deglitcher.vhd",
    "../../../dependencies/nanofip-gateware/src/wf_rx_deserializer.vhd",
    "../../../dependencies/nanofip-gateware/src/wf_rx_osc.vhd",
    "../../../dependencies/nanofip-gateware/src/wf_tx_osc.vhd",
    "../../../dependencies/nanofip-gateware/src/wf_tx_serializer.vhd"
]

modules = {
    "local" : [
        "../../rtl",
        "../../../dependencies/general-cores",
        "../../../dependencies/urv-core",
        "../../../dependencies/mockturtle",
        "../../../dependencies/gn4124-core",
        "../../../dependencies/spec",
        "../../../dependencies/wr-cores",
        "../../../dependencies/ddr3-sp6-core"
    ],
}


