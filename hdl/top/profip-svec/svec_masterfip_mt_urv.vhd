---------------------------------------------------------------------------------------------------
-- SPDX-FileCopyrightText: 2022 CERN (home.cern)                                                  |
--                                                                                                |
-- SPDX-License-Identifier: CERN-OHL-W-2.0+                                                       |
---------------------------------------------------------------------------------------------------
--_________________________________________________________________________________________________
--                                                                                                |
--                                        |SVEC masterFIP|                                        |
--                                                                                                |
--                                         CERN, BE/CO-HT                                         |
--________________________________________________________________________________________________|

---------------------------------------------------------------------------------------------------
--                                                                                                |
--                                        svec_masterfip_mt_urv                                   |
--                                                                                                |
---------------------------------------------------------------------------------------------------
-- File         svec_masterfip_mt_urv.vhd                                                         |
--                                                                                                |
-- Description  Top level of the masterFIP design with Mock Turtle on a SVEC carrier.             |
--                                                                                                |
--              Figure 1 shows the architecture and main components of the design.                |
--                ______________________________________________________________________          |
--               |                                                                      |         |
--               |                         _________________________________________    |         |
--               |                        |                             MOCK TURTLE |   |         |
--         _     |                        |                                _____    |   |         |
--        | |    |                        |                         ___   |     |   |   |         |
--        |F|    |                     . .| . . . . . . . . . . . >|   |  |     |   |   |         |
--        |I|    |   _____             .  |                        |   |  |     |   |   |         |
--        |E|    |  |     |            .  |        . . . . . . . .>|   |  |     |   |   |         |
--        |L| <--|  |     |            .  |        .         HMQs  |   |  |     |   |   |         |
--        |D|    |  |  F  |            .  |        .               |   |  |     |   |   |         |
--        |R|    |  |  M  |            .  |     ______             |   |  |     |   |   |         |
--        |I| -->|  |  C  |            .  | DP |      |            |   |  |     |   |   |         |
--        |V|    |  |     |            . .|. .>| CPU0 |   _____    | X |  |  G  |   |   |         |
--        |E|    |  |  M  |     ____   .  |    |______|  |     |   | b |  |  N  |   |   | <-PCIe->|
--        |_|    |  |  A  |    |    |  .  |              | SH. |   | a |  |  4  |   |   |   host  |
--               |  |  S  |. . |Xbar|. .  |     ______   | MEM |   | r |  |  1  |   |   |         |
-- ext pulse --> |  |  T  |    |____|  .  |    |      |  |_____|   |   |  |  2  |   |   |         |
--               |  |  E  |            .  | DP | CPU1 |            |   |  |  4  |   |   |         |
--               |  |  R  |            . .|. .>|______|            |   |  |     |   |   |         |
-- FMC 1wire <-->|  |  F  |               |       .                |   |  |     |   |   |         |
--               |  |  I  |               |       .           HMQs |   |  |     |   |   |         |
--               |  |  P  |               |       . . . . . . . . >|___|  |     |   |   |         |
--   FMC LEDs <--|  |     |               |                               |_____|   |   |         |
--               |  |_____|               |                                 _^_     |   |         |
--               |                        |                                |   |    |   |         |
--               |                        |                                |VIC|    |   |         |
--               |                        |                                |___|    |   |         |
--               |                        |_________________________________________|   |         |
--               |______________________________________________________________________|         |
--                              Figure 1: svec_masterfip_mt_urv architecture                      |
--                                                                                                |
--                                                                                                |
--              FMC MASTERFIP CORE:                                                               |
--              On one side the FMC MASTERFIP CORE is the interface to the FMC hardware (i.e.     |
--              FielDrive chip, external pulse LEMO, 1-wire DS18B20 chip, LEDs) on the other side |
--              it provides a wbgen2 WISHBONE where a set of control and status registers have    |
--              been defined to interface with the MOCK TURTLE.                                   |
--              The core ignores the notion of the WorldFIP frame type (ID_DAT/RT_DAT/..etc),     |
--              or the macrocycle sequence and macrocycle timing; the sw running on the Mock      |
--              Turtle CPUs is responsible for managing these aspects and for providing to this   |
--              core all the payload bytes (coming from the host) that have to be serializedand,  |
--              together with a serialization startup trigger, or for enabling the deserializer   |
--              and then providing to the host the deserialized bytes.                            |
--              Figure 2 shows the structure of a WorldFIP frame. The core is internally          |
--              generating (in the case of serialization) or validating (in the case of           |
--              deserialization) only the FSS, CRC and FES bytes; the rest of the bytes are       |
--              retrieved from or provided to the MOCK TURTLE. The core also encodes/decodes all  |
--              the bytes to/from the Manchester2 code (as specified by the WorldFIP protocol) and|
--              controls/monitors all the FielDrive signals.                                      |
--               _____________________________________________________________________________    |
--              |_____FSS_____|__Ctrl__|_____________Payload_____________|_____CRC____|__FES__|   |
--                                                                                                |
--                                     Figure 2: WorldFIP frame structure                         |
--                                                                                                |
--              MOCK TURTLE:                                                                      |
--              Instead of having a big FSM in HDL that would be executing the WorldFIP           |
--              macrocycle, we have software running on an embedded CPU, in order to add          |
--              flexibility and ease the implementation of the design. Mock Turtle is the         |
--              generic core that offers multi-CPU processing and all the infrastructure around.  |
--              The interface between the CPUs and the PCIe host is though HostMessageQueues(HMQ).|
--              The interface between the CPUs with the FMC MASTERFIP CORE is a set of wbgen2-    |
--              generated registers.                                                              |
--              In this design MT is configured with 2 CPUs:                                      |
--              - CPU0 is the heart of the design; it is "playing" the WorldFIP macrocycle.       |
--                For example,it initiates the delivery of a WorldFIP question frame, by providing|
--                the frame bytes to the FMC MASTERFIP CORE, and then awaits for the reception of |
--                the response frame.It retrieves these consumed data from the FMC MASTERFIP CORE,|
--                packs them in the corresponding HMQ (according to the frame type) and can notify|
--                the host through an IRQ.                                                        |
--              - CPU1 is mainly polling the host to retrieve new payload bytes for production.   |
--                When new data is received from the host through a dedicated HMQ, CPU1 puts them |
--                into the Shared Memory for CPU0 to retrieve them and provide them to the        |
--                FMC MASTERFIP CORE for serialization.                                           |
--                CPU1 does not need access to the FMC MASTERFIP CORE; however access is possible |
--                for debugging purposes.                                                         |
--                                                                                                |
--              XBAR:                                                                             |
--              The crossbar between the FMC MASTERFIP CORE and MOCK TURTLE is used so that       |
--              CPU0, CPU1 and to the PCIe host can access directly the wbgen2-defined regs       |
--              in the FMC MASTERFIP CORE.                                                        |
--              Note that to give access to the FMC MASTERFIP CORE to both CPU0 and CPU1, we      |
--              could have used the Shared Port of MT, instead of using the Dedicated Ports (DP)  |
--              and this crossbar; this though would have also affected (potentially slowed down) |
--              the accesses to the MT Shared Memory.                                             |
--              Note also that as mentioned above CPU1 is only accessing the FMC MASTERFIP CORE   |
--              for debugging purposes; the same goes also for the PCIe host.                     |
--                                                                                                |
--              CLK, RST:                                                                         |
--              There is only one clock domain of 62.5 MHz, in the whole design. The clock is     |
--              generated inside the MOCK TURTLE, from the 125 MHz SVEC PLL IC6 output clock      |
--              (clk_125m_pllref_p_i,clk_125m_pllref_n_i) and it is used by both MOCK TURTLE CPUs,|
--              by the FMC MASTERFIP CORE and the XBAR. A PCIe reset signal, synchronous to       |
--              the 62.5 MHz clock is also provided by MOCK TURTLE.                               |
--                                                                                                |
--              MEMORY MAP AS SEEN FROM PCIe:                                                     |
--              0x00000000 (size: 4 bytes)      : SDB signature                                   |
--              0x00002000 (size: 64 bytes)     : VIC                                             |
--              0x00010000 (size: 644 bytes)    : Host access to the FMC MASTERFIP CORE           |
--              0x00020000 (size: 128 kB)       : MOCK TURTLE                                     |
--                |-- 0x00020000                : HMQ Global Control Registers                    |
--                |-- 0x00024000                : HMQ incoming slots (Host->CPUs)                 |
--                |-- 0x00028000                : HMQ outgoing slots (CPUs->Host)                 |
--                |-- 0x0002c000                : CPU Control/Status Registers                    |
--                |-- 0x00030000                : Shared Memory (64 KB)                           |
--                                                                                                |
-- Authors      Evangelia Gousiou (Evangelia.Gousiou@cern.ch)                                     |
--              Eva Calvo Giraldo (Eva.Calvo.Giraldo@cern.ch)                                     |
--              Tomasz Wlostowski (Tomasz.Wlostowski@cern.ch)                                     |
--                                                                                                |
---------------------------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;       -- std_logic definitions
use IEEE.numeric_std.all;          -- conversion functions

library work;
use work.gencores_pkg.all;         -- for the functions used
use work.wishbone_pkg.all;         -- for the wb_crossbar
use work.mt_mqueue_pkg.all;        -- for the HMQ
use work.mock_turtle_pkg.all;      -- for the Mockturtle
use work.masterFIP_pkg.all;        -- for the fmc_masterfip_core definition
use work.masterfip_wbgen2_pkg.all; -- for the masterfip_wbgen2_csr records
use work.streamers_pkg.all;        -- for the SVEC board
use work.wr_board_pkg.all;         -- for the SVEC board
use work.buildinfo_pkg.all;        -- for the SVEC board
use work.sourceid_svec_masterfip_mt_urv_pkg;

library unisim;
use unisim.vcomponents.all;

entity svec_masterfip_mt_urv is
  generic (
    -- Reduces some timeouts to speed up simulations
    g_SIMULATION : integer := 0
  );
  port (
    -- Carrier signals
    rst_n_i             : in  std_logic; -- Reset from system FPGA
    --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --
    -- Clock
    clk_125m_pllref_p_i : in  std_logic; -- 125 MHz PLL reference,
    clk_125m_pllref_n_i : in  std_logic; -- used in MT to generate 100 MHz

    -- VME interface
    vme_write_n_i       : in    std_logic;
    vme_sysreset_n_i    : in    std_logic;
    vme_retry_oe_o      : out   std_logic;
    vme_retry_n_o       : out   std_logic;
    vme_lword_n_b       : inout std_logic;
    vme_iackout_n_o     : out   std_logic;
    vme_iackin_n_i      : in    std_logic;
    vme_iack_n_i        : in    std_logic;
    vme_gap_i           : in    std_logic;
    vme_dtack_oe_o      : out   std_logic;
    vme_dtack_n_o       : out   std_logic;
    vme_ds_n_i          : in    std_logic_vector(1 downto 0);
    vme_data_oe_n_o     : out   std_logic;
    vme_data_dir_o      : out   std_logic;
    vme_berr_o          : out   std_logic;
    vme_as_n_i          : in    std_logic;
    vme_addr_oe_n_o     : out   std_logic;
    vme_addr_dir_o      : out   std_logic;
    vme_irq_o           : out   std_logic_vector(7 downto 1);
    vme_ga_i            : in    std_logic_vector(4 downto 0);
    vme_data_b          : inout std_logic_vector(31 downto 0);
    vme_am_i            : in    std_logic_vector(5 downto 0);
    vme_addr_b          : inout std_logic_vector(31 downto 1);

    -- SVEC LEDs
    led_green_o         : out   std_logic; -- blinking with clk_62m5_sys
    led_red_o           : out   std_logic; -- active during a PCIe rst, gn_rst_n_i

    -- Profinet LEDs
    led_profinet_y_o    : out   std_logic;
    led_profinet_g_o    : out   std_logic;
    led_profinet_r_o    : out   std_logic;

    -- ERTEC GPIOs used for the Profinet LEDs
    ertec_gpio_16_i     : in    std_logic;
    ertec_gpio_17_i     : in    std_logic;
    ertec_gpio_18_i     : in    std_logic;
    -- FMC signals
    --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --
    -- FMC presence
    fmc_prsnt_m2c_n_i   : in    std_logic; -- FMC presence (used by MT)

    -- FMC 1-wire
    fmc_onewire_b       : inout std_logic; -- temper and unique id

    -- WorldFIP bus speed                  -- 31K25bps: speed_b1=0, speed_b0=0
    speed_b0_i          : in    std_logic; -- 1Mbps   : speed_b1=0, speed_b0=1
    speed_b1_i          : in    std_logic; -- 2M5bps  : speed_b1=1, speed_b0=0
                                           -- 5Mbps   : speed_b1=1, speed_b0=1
    -- WorldFIP FielDrive
    fd_rstn_o           : out   std_logic; -- reset
    fd_rxcdn_i          : in    std_logic; -- rx carrier detect
    fd_rxd_i            : in    std_logic; -- rx data
    fd_txer_i           : in    std_logic; -- tx error
    fd_wdgn_i           : in    std_logic; -- tx watchdog
    fd_txck_o           : out   std_logic; -- tx clk
    fd_txd_o            : out   std_logic; -- tx data
    fd_txena_o          : out   std_logic; -- tx enable

    -- External synchronisation pulse (input signal and transceiver control)
    ext_sync_term_en_o  : out   std_logic; -- enable 50 Ohm termin of the pulse
    ext_sync_dir_o      : out   std_logic := '0'; -- direction fixed B -> A
    ext_sync_oe_n_o     : out   std_logic; -- transceiver output enable
    ext_sync_i          : in    std_logic; -- input sync pulse

    -- FMC Front panel LEDs: controlled by the MT firmware, updated every macrocycle
    led_rx_act_n_o      : out   std_logic;
    led_rx_err_n_o      : out   std_logic;
    led_tx_act_n_o      : out   std_logic;
    led_tx_err_n_o      : out   std_logic;
    led_sync_act_n_o    : out   std_logic; -- stays OFF when ext_sync is not used
    led_sync_err_n_o    : out   std_logic; -- stays OFF when ext_sync is not used

    -- Test points
    tp1_o               : out   std_logic; -- connected to fd_rxd
    tp2_o               : out   std_logic; -- connected to fd_txd
    tp3_o               : out   std_logic; -- connected to MT led&dbg reg bit 8
    tp4_o               : out   std_logic; -- connected to MT led&dbg reg bit 9

    -- UART
    uart_txd_o          : out   std_logic;
    uart_rxd_i          : in    std_logic;
    ertec_uart_tx_1_i   : in    std_logic; -- connected to ERTEC UART TX
    ertec_uart_rx_1_o   : out   std_logic; -- connected to ERTEC UART RX
    ertec_uart_rx_2_i   : in    std_logic; -- connected to ERTEC UART TX
    ertec_uart_tx_2_o   : out   std_logic; -- connected to ERTEC UART RX

    -- SPI
    ertec_spi_mosi_i    : in    std_logic; -- ERTEC SPI master out slave in
    ertec_spi_miso_o    : out   std_logic; -- ERTEC SPI master in slave out
    ertec_spi_clk_i     : in    std_logic; -- ERTEC SPI clock
    ertec_spi_cs_n_i    : in    std_logic; -- ERTEC SPI chip select

    -- ERTEC Misc signals
    ertec_rmq_status_o  : out   std_logic_vector(6 downto 0);
    fmc_profinet_rst_n_o: out   std_logic; -- From FPGA to Profinet FMC - ERTEC
    ertec_rst_n_i       : in    std_logic; -- ERTEC reset gpio to reset the translator logic
    ertec_rst_cpu_n_i   : in    std_logic; -- ERTEC reset gpio to reset the rest of the logic

    ----------------------------------------------------
    -- Carrier front panel IOs
    ----------------------------------------------------
    fp_gpio1_b          : out   std_logic;
    fp_gpio2_b          : out   std_logic;
    fp_gpio3_b          : out   std_logic;
    fp_gpio4_b          : out   std_logic;
    fp_term_en_o        : out   std_logic_vector(4 downto 1);
    fp_gpio1_a2b_o      : out   std_logic;
    fp_gpio2_a2b_o      : out   std_logic;
    fp_gpio34_a2b_o     : out   std_logic;

    -- Signals added to remove synthesis errors due to
    -- the usage of SVEC convention. They are not used
    -- in this design
    button1_n_i         : in    std_logic;
    pcbrev_i            : in    std_logic_vector(4 downto 0);
    fmc0_prsnt_m2c_n_i  : in    std_logic;
    fmc1_prsnt_m2c_n_i  : in    std_logic;
    fmc0_scl_b          : inout std_logic;
    fmc0_sda_b          : inout std_logic;
    fmc1_scl_b          : inout std_logic;
    fmc1_sda_b          : inout std_logic;
    spi_ncs_o           : out   std_logic;
    spi_sclk_o          : out   std_logic;
    spi_mosi_o          : out   std_logic;
    spi_miso_i          : in    std_logic;
    onewire_b           : inout std_logic;
    carrier_scl_b       : inout std_logic;
    carrier_sda_b       : inout std_logic

    -- synthesis translate_off
    ;
    sim_wb_i : in t_wishbone_slave_in := cc_dummy_slave_in;
    sim_wb_o : out t_wishbone_slave_out
    -- synthesis translate_on
  );
end svec_masterfip_mt_urv;
--=================================================================================================
--                                    architecture declaration
--=================================================================================================
architecture rtl of svec_masterfip_mt_urv is

  ---------------------------------------------------------------------------------------------------
  --                                     MOCK TURTLE CONSTANTS                                     --
  ---------------------------------------------------------------------------------------------------
  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --
  -- HMQ: It total 7 HMQs have been defined. Each HMQ has 4 entries of 128 x 32 bits.

  --   6 bi-directional HMQs for core 0
  --     - 0: HMQ from CPU0 with the WorldFIP payloads from periodic consumed variables
  --     - 1: HMQ from CPU0 with the WorldFIP payloads from aperiodic consumed variables
  --         (only for the case of identif variable, scheduled as periodic variable, by radMon app)
  --     - 2: HMQ from CPU0 with the WorldFIP payloads from aperiodic consumed messages
  --     - 3: HMQ from CPU0 with the WorldFIP payloads from periodic consumed diagnostic variables
  --         (only for the case of the FIPdiag variable 0x067F)
  --     - 4: HMQ from CPU0 with the WorldFIP payloads from aperiodic consumed diagnostic variables
  --         (aperiodic presence and identification)
  --     - 5: HMQ towards CPU0 with commands for the bus config, used only at startup (e.g.: HW_RESET,
  --          PROGRAM_BA, BA_START, BA_RUNNING) and for the responses of CPU0 to the commands of the host

  --   1 bi-directional HMQ for core 1
  --     - 0: HMQ towards CPU1 with the payloads for produced WorldFIP frames (variables and messages;
  --          CPU1 then puts this data into the Shared Memory for CPU0 to access and put them in the
  --          bus) as well as requests for report data, requests for the scheduling of aperiodic traffic
  --          (presence/identification) etc (CPU1 again passes these requests into the Shared Memory)
  --          and for the responses of CPU1 to the commands of the host

  -- Device ID or application (masterFIP) ID needed in MT
  -- SVEC + MasterFIP
  constant c_MSTRFIP_APP_ID : std_logic_vector(31 downto 0) := x"53564D42";

  constant C_NODE_CONFIG : t_mt_config :=
  (app_id => c_MSTRFIP_APP_ID,
  cpu_count => 2,
  cpu_config => (0 => (memsize => 24576, -- the size should be enough for the storage of the RT sw running on CPU0 and for the macrocycle configuration
                       hmq_config => (6, (others => (2, 7, 2, x"0000_0000", true))),  -- 4  entries, 128 wide, 2 header bits
                       rmq_config => (6, (others => (4, 7, 3, x"0000_0000", true)))), -- 16 entries, 128 wide, 3 header bits
                 1 => (memsize => 2048,
                       hmq_config => (1, (others => (2, 7, 2, x"0000_0000", true))),  -- 4  entries, 128 wide, 2 header bits
                       rmq_config => (1, (others => (4, 7, 3, x"0000_0000", true)))), -- 16 entries, 128 wide, 3 header bits
  others => (0, c_MT_DEFAULT_MQUEUE_CONFIG, c_MT_DEFAULT_MQUEUE_CONFIG)),
  shared_mem_size => 8192); -- 32768 in bytes = 8192 words (divided by 4, as the new MT config requires)

  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --
  -- masterFIP crossbar constants
  constant C_SLAVE_ADDR : t_wishbone_address_array(0 downto 0) := (0 => x"00000000");
  constant C_SLAVE_MASK : t_wishbone_address_array(0 downto 0) := (0 => x"00000000");
  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --
  -- mockturtle interconnect crossbar constants
  constant c_NUM_WB_MASTERS : integer := 6; -- MT, Metadata, FMC0, UART, fmc profinet reset
  constant c_NUM_WB_SLAVES  : integer := 1; -- VME interface

  constant c_MASTER_GENNUM  : integer := 0;

  constant c_SLAVE_METADATA : integer := 5;
  constant c_SLAVE_MT       : integer := 4;
  constant c_SLAVE_FMC0     : integer := 3;
  constant c_SLAVE_UART_1   : integer := 2;
  constant c_SLAVE_UART_2   : integer := 1;
  constant c_SLAVE_RST_PRFIP: integer := 0;

  -- Convention metadata base address
  constant c_METADATA_ADDR    : t_wishbone_address := x"0000_4000";
  constant c_MT_ADDR          : t_wishbone_address := x"0004_0000";
  constant c_FMC0_ADDR        : t_wishbone_address := x"0003_0000";
  constant c_UART_1_ADDR      : t_wishbone_address := x"0001_0000";
  constant c_UART_2_ADDR      : t_wishbone_address := x"0001_2000";
  constant c_RST_ADDR         : t_wishbone_address := x"0001_1000";

  constant c_INTER_ADDR : t_wishbone_address_array(c_NUM_WB_MASTERS - 1 downto 0) :=
    (c_METADATA_ADDR,
    c_MT_ADDR,
    c_FMC0_ADDR,
    c_UART_1_ADDR,
    c_UART_2_ADDR,
    c_RST_ADDR);

  constant c_INTER_MASK : t_wishbone_address_array(c_NUM_WB_MASTERS - 1 downto 0) :=
    (x"ffff_c000",
    x"fffe_0000",
    x"ffff_0000",
    x"ffff_f000",
    x"ffff_f000",
    x"ffff_f000");

  constant c_XBAR_ADDRESS : t_wishbone_address := x"00000000";

  -- Wishbone bus(es) from masters attached to crossbar
  signal cnx_master_out  : t_wishbone_master_out_array(c_NUM_WB_MASTERS - 1 downto 0);
  signal cnx_master_in   : t_wishbone_master_in_array(c_NUM_WB_MASTERS - 1 downto 0);

  -- Wishbone bus(es) to slaves attached to crossbar
  signal cnx_slave_out   : t_wishbone_slave_out_array(c_NUM_WB_SLAVES - 1 downto 0);
  signal cnx_slave_in    : t_wishbone_slave_in_array(c_NUM_WB_SLAVES - 1 downto 0);

  -- Primary Wishbone slave(s) offsets
  constant c_WB_SLAVE_METADATA : integer := 0;

  ---------------------------------------------------------------------------------------------------
  --                                            Signals                                            --
  ---------------------------------------------------------------------------------------------------
  signal clk_62m5_sys     : std_logic;
  signal rst_sys_62m5     : std_logic; -- Coming from SVEC
  signal rst_n_sys        : std_logic; -- Coming from SVEC, provided to all
  signal s_rst_n          : std_logic; -- Combination of SVEC reset and ERTEC
  signal clk_125m_sys     : std_logic; -- for CDC in SPI
  signal cpu_ext_rst_n    : std_logic_vector(C_NODE_CONFIG.cpu_count-1 downto 0);
  signal s_rmq_id         : std_logic_vector(7 downto 0);
  signal console_irq      : std_logic;
  signal hmq_in_irq       : std_logic;
  signal hmq_out_irq      : std_logic;
  signal notify_irq       : std_logic;
  signal s_irq_uart_1     : std_logic;
  signal s_irq_uart_2     : std_logic;
  -- Mock Turtle
  signal fmc_core_wb_out  : t_wishbone_master_out_array(0 to 1);
  signal fmc_core_wb_in   : t_wishbone_master_in_array(0 to 1);
  signal fmc_wb_muxed_out : t_wishbone_master_out;
  signal fmc_wb_muxed_in  : t_wishbone_master_in;
  signal uart_wb_1_o      : t_wishbone_master_out;
  signal uart_wb_1_i      : t_wishbone_master_in;
  signal uart_wb_2_o      : t_wishbone_master_out;
  signal uart_wb_2_i      : t_wishbone_master_in;
  signal gpio_wb_i        : t_wishbone_master_in;
  signal gpio_wb_o        : t_wishbone_master_out;
  -- UART
  signal uart_tx          : std_logic;
  signal uart_rx          : std_logic;
  -- GPIO PORTS
  signal gpio_in          : std_logic_vector(0 downto 0);
  signal gpio_out         : std_logic_vector(0 downto 0);
  signal gpio_oen         : std_logic_vector(0 downto 0);
  -- MT endpoints
  signal rmq_endpoint_out : t_mt_rmq_endpoint_iface_out;
  signal rmq_endpoint_in  : t_mt_rmq_endpoint_iface_in;
  signal rmq_src_in       : t_mt_stream_source_in;
  signal rmq_src_out      : t_mt_stream_source_out;
  signal rmq_src_cfg_in   : t_mt_stream_config_in;
  signal rmq_src_cfg_out  : t_mt_stream_config_out;
  signal rmq_snk_in       : t_mt_stream_sink_in_array2d;
  signal rmq_snk_out      : t_mt_stream_sink_out_array2d;
  signal rmq_snk_cfg_in   : t_mt_stream_config_in;
  signal rmq_snk_cfg_out  : t_mt_stream_config_out;
  signal s_rmq_status     : std_logic_vector(6 downto 0);
  -- SVEC LEDs
  signal led_divider      : unsigned(22 downto 0);
  signal leds             : std_logic_vector(31 downto 0);
  signal svec_led         : std_logic_vector(15 downto 0);
  signal fd_txd           : std_logic;
  -- Simulation Wishbone signals
  signal sim_wb_in        : t_wishbone_slave_in := cc_dummy_slave_in;
  signal sim_wb_out       : t_wishbone_slave_out;
  -- used for connecting port from entity to the front panel of SVEC, for debugging
  signal s_fd_tx_data_o   : std_logic;
  signal s_fd_rx_data_i   : std_logic;
  signal s_fd_txck_o      : std_logic;
  signal s_fd_txena_o     : std_logic;
  -- Signals used for setting pull-up
  signal profip_rst       : std_logic;
  signal cpu_rst          : std_logic;
  signal spi_cs_n         : std_logic;

  --=================================================================================================
  --                                       Architecture begin
  --=================================================================================================

begin

  ---------------------------------------------------------------------------------------------------
  --                                        FIXED SIGNALS                                          --
  ---------------------------------------------------------------------------------------------------
  ext_sync_dir_o <= '0'; -- Direction fixed to: B -> A

  -- Reset generated by SVEC and then transferred to the other cores
  s_rst_n <= rst_sys_62m5;

  gen_mt_cpu_rst : for i in 0 to C_NODE_CONFIG.cpu_count-1 generate
    cpu_ext_rst_n(i) <= ertec_rst_cpu_n_i;
  end generate;

    ----------------------------------------------------------------------------
    --! PROFIP TRANSLATOR
    cmp_mt_profip_translator : entity work.mt_profip_translator
    generic map(
        g_data_width        => 32,
        g_cpol              => 0,
        g_cpha              => 1,
        g_cdc_enable        => 0,
        g_input_fifo_depth  => 128,
        g_output_fifo_depth => 128
    )
    port map(
        clk_i               => clk_62m5_sys,
        rst_n_i             => s_rst_n,
        spi_sample_clk_i    => clk_62m5_sys,
        ertec_rst_n_i       => ertec_rst_n_i,
        rmq_status_o        => s_rmq_status,
        rmq_id_o            => s_rmq_id,
        rmq_src_i           => rmq_src_in,
        rmq_src_o           => rmq_src_out,
        rmq_src_config_i    => rmq_snk_cfg_out,
        rmq_src_config_o    => rmq_snk_cfg_in,
        rmq_snk_i           => rmq_snk_in,
        rmq_snk_o           => rmq_snk_out,
        rmq_snk_config_i    => rmq_src_cfg_out,
        rmq_snk_config_o    => rmq_src_cfg_in,
        spi_clk_i           => ertec_spi_clk_i,
        spi_cs_n_i          => ertec_spi_cs_n_i,
        spi_mosi_i          => ertec_spi_mosi_i,
        spi_miso_o          => ertec_spi_miso_o
    );

    --! Assigning the rmq status so that it can be exposed to ERTEC
    ertec_rmq_status_o <= s_rmq_status;

    --! Assigning the RMQ signals (multiple RMQ)
    --! core | slot | hex_number
    --!   0      0        0
    --!   0      1        1
    --!   0      2        2
    --!   0      3        3
    --!   0      4        4
    --!   0      5        5
    --!   1      0        6
    p_multiple_rmq_assign : process (rmq_endpoint_out, rmq_src_out, s_rmq_id)
    begin

        --! UNUSED
        l_unused_slots_core0: for w in 6 to 7 loop
            rmq_endpoint_in.snk_in(0)(w).data <= (others => '0');
            rmq_endpoint_in.snk_in(0)(w).hdr <= '0';
            rmq_endpoint_in.snk_in(0)(w).valid <= '0';
            rmq_endpoint_in.snk_in(0)(w).last <= '0';
            rmq_endpoint_in.snk_in(0)(w).error <= '0';
        end loop l_unused_slots_core0;

        l_unused_slots_core1: for k in 1 to 7 loop
            rmq_endpoint_in.snk_in(1)(k).data <= (others => '0');
            rmq_endpoint_in.snk_in(1)(k).hdr <= '0';
            rmq_endpoint_in.snk_in(1)(k).valid <= '0';
            rmq_endpoint_in.snk_in(1)(k).last <= '0';
            rmq_endpoint_in.snk_in(1)(k).error <= '0';
        end loop l_unused_slots_core1;

        l_unused_cores: for i in 2 to 7 loop
            l_unused_slots: for j in 0 to 7 loop
                rmq_endpoint_in.snk_in(i)(j).data <= (others => '0');
                rmq_endpoint_in.snk_in(i)(j).hdr <= '0';
                rmq_endpoint_in.snk_in(i)(j).valid <= '0';
                rmq_endpoint_in.snk_in(i)(j).last <= '0';
                rmq_endpoint_in.snk_in(i)(j).error <= '0';
            end loop l_unused_slots;
        end loop l_unused_cores;

        case s_rmq_id is
            when x"00" =>
                rmq_src_in <= rmq_endpoint_out.snk_out(0)(0);
                rmq_endpoint_in.snk_in(0)(0) <= rmq_src_out;

            when x"01" =>
                rmq_src_in <= rmq_endpoint_out.snk_out(0)(1);
                rmq_endpoint_in.snk_in(0)(1) <= rmq_src_out;

            when x"02" =>
                rmq_src_in <= rmq_endpoint_out.snk_out(0)(2);
                rmq_endpoint_in.snk_in(0)(2) <= rmq_src_out;

            when x"03" =>
                rmq_src_in <= rmq_endpoint_out.snk_out(0)(3);
                rmq_endpoint_in.snk_in(0)(3) <= rmq_src_out;

            when x"04" =>
                rmq_src_in <= rmq_endpoint_out.snk_out(0)(4);
                rmq_endpoint_in.snk_in(0)(4) <= rmq_src_out;

            when x"05" =>
                rmq_src_in <= rmq_endpoint_out.snk_out(0)(5);
                rmq_endpoint_in.snk_in(0)(5) <= rmq_src_out;

            when x"06" =>
                rmq_src_in <= rmq_endpoint_out.snk_out(1)(0);
                rmq_endpoint_in.snk_in(1)(0) <= rmq_src_out;

            when others =>
                rmq_src_in.pkt_ready <= '0';
                rmq_src_in.ready <= '0';

                l_init: for x in 0 to 5 loop
                    rmq_endpoint_in.snk_in(0)(x).data <= (others => '0');
                    rmq_endpoint_in.snk_in(0)(x).hdr <= '0';
                    rmq_endpoint_in.snk_in(0)(x).valid <= '0';
                    rmq_endpoint_in.snk_in(0)(x).last <= '0';
                    rmq_endpoint_in.snk_in(0)(x).error <= '0';
                end loop l_init;

                rmq_endpoint_in.snk_in(1)(0).data <= (others => '0');
                rmq_endpoint_in.snk_in(1)(0).hdr <= '0';
                rmq_endpoint_in.snk_in(1)(0).valid <= '0';
                rmq_endpoint_in.snk_in(1)(0).last <= '0';
                rmq_endpoint_in.snk_in(1)(0).error <= '0';

        end case;
    end process p_multiple_rmq_assign;

    --! RMQ signals coming from MT
    rmq_snk_in(0)(0) <= rmq_endpoint_out.src_out(0)(0);
    rmq_snk_in(0)(1) <= rmq_endpoint_out.src_out(0)(1);
    rmq_snk_in(0)(2) <= rmq_endpoint_out.src_out(0)(2);
    rmq_snk_in(0)(3) <= rmq_endpoint_out.src_out(0)(3);
    rmq_snk_in(0)(4) <= rmq_endpoint_out.src_out(0)(4);
    rmq_snk_in(0)(5) <= rmq_endpoint_out.src_out(0)(5);
    rmq_snk_in(1)(0) <= rmq_endpoint_out.src_out(1)(0);

    --! RMQ signals coming from MT
    rmq_endpoint_in.src_in(0)(0) <= rmq_snk_out(0)(0);
    rmq_endpoint_in.src_in(0)(1) <= rmq_snk_out(0)(1);
    rmq_endpoint_in.src_in(0)(2) <= rmq_snk_out(0)(2);
    rmq_endpoint_in.src_in(0)(3) <= rmq_snk_out(0)(3);
    rmq_endpoint_in.src_in(0)(4) <= rmq_snk_out(0)(4);
    rmq_endpoint_in.src_in(0)(5) <= rmq_snk_out(0)(5);
    rmq_endpoint_in.src_in(1)(0) <= rmq_snk_out(1)(0);

    --! Assigning default values to the RMQ configuration signals
    rmq_snk_cfg_out <= c_MT_DUMMY_EP_CONFIG_OUT;
    rmq_endpoint_in.snk_config_in(0)(0) <= c_MT_DUMMY_EP_CONFIG_IN;
    rmq_endpoint_in.snk_config_in(0)(1) <= c_MT_DUMMY_EP_CONFIG_IN;
    rmq_endpoint_in.snk_config_in(0)(2) <= c_MT_DUMMY_EP_CONFIG_IN;
    rmq_endpoint_in.snk_config_in(0)(3) <= c_MT_DUMMY_EP_CONFIG_IN;
    rmq_endpoint_in.snk_config_in(0)(4) <= c_MT_DUMMY_EP_CONFIG_IN;
    rmq_endpoint_in.snk_config_in(0)(5) <= c_MT_DUMMY_EP_CONFIG_IN;
    rmq_endpoint_in.snk_config_in(1)(0) <= c_MT_DUMMY_EP_CONFIG_IN;

    rmq_src_cfg_out <= c_MT_DUMMY_EP_CONFIG_OUT;
    rmq_endpoint_in.src_config_in(0)(0) <= c_MT_DUMMY_EP_CONFIG_IN;
    rmq_endpoint_in.src_config_in(0)(1) <= c_MT_DUMMY_EP_CONFIG_IN;
    rmq_endpoint_in.src_config_in(0)(2) <= c_MT_DUMMY_EP_CONFIG_IN;
    rmq_endpoint_in.src_config_in(0)(3) <= c_MT_DUMMY_EP_CONFIG_IN;
    rmq_endpoint_in.src_config_in(0)(4) <= c_MT_DUMMY_EP_CONFIG_IN;
    rmq_endpoint_in.src_config_in(0)(5) <= c_MT_DUMMY_EP_CONFIG_IN;
    rmq_endpoint_in.src_config_in(1)(0) <= c_MT_DUMMY_EP_CONFIG_IN;

  ---------------------------------------------------------------------------------------------------
  --                                 MAIN WISHBONE CROSSBAR                                        --
  ---------------------------------------------------------------------------------------------------
  cmp_mt_intercon : entity work.xwb_crossbar
    generic map(
      g_num_masters => c_NUM_WB_SLAVES,
      g_num_slaves  => c_NUM_WB_MASTERS,
      g_registered  => true,
      g_address     => c_INTER_ADDR,
      g_mask        => c_INTER_MASK,
      g_verbose     => True)
    port map(
      clk_sys_i     => clk_62m5_sys,
      rst_n_i       => s_rst_n,
      slave_i       => cnx_slave_in,
      slave_o       => cnx_slave_out,
      master_o      => cnx_master_out,
      master_i      => cnx_master_in
    );

  ---------------------------------------------------------------------------------------------------
  --                                      MOCK TURTLE CORE                                         --
  ---------------------------------------------------------------------------------------------------
  cmp_mock_turtle_urv : mock_turtle_core
  generic map(
    g_CONFIG            => C_NODE_CONFIG,
    g_SYSTEM_CLOCK_FREQ => 62500000,      -- both CPUs at 62,5 MHz
    g_WITH_WHITE_RABBIT => FALSE)         -- no WR support
  port map(
    clk_i               => clk_62m5_sys,  -- 62,5 MHz; one clk domain in the whole design
    rst_n_i             => s_rst_n,       -- SVEC rst, synced with clk_62m5_sys
    -- shared peripheral port: not used

    -- ERTEC to reset the MT CPUs
    cpu_ext_rst_n_i     => cpu_ext_rst_n,
    -- Endpoint Interface
    rmq_endpoint_o      => rmq_endpoint_out,
    rmq_endpoint_i      => rmq_endpoint_in,

    -- dedicated (per-cpu) peripheral port
    -- WISHBONE connection of the fmc_masterFIP_core to the MT CPUs
    dp_master_o         => fmc_core_wb_out, -- access from MT CPU0 and CPU1 at base address 0x100000
    dp_master_i         => fmc_core_wb_in,

    host_slave_i        => cnx_master_out(c_SLAVE_MT),
    host_slave_o        => cnx_master_in(c_SLAVE_MT),

    hmq_in_irq_o        => hmq_in_irq,
    hmq_out_irq_o       => hmq_out_irq,
    notify_irq_o        => notify_irq,
    console_irq_o       => console_irq);
  ---------------------------------------------------------------------------------------------------
  --                                             XBAR                                              --
  ---------------------------------------------------------------------------------------------------
  -- Crossbar to give access to the fmc_masterFIP_core to CPU0, CPU1 and directly to the PCIe host.
  -- Note that to give access to the fmc_masterFIP_core to both CPU0 and CPU1, the SP of MT could
  -- have been used instead of the DP and this crossbar; this though would have also affected
  -- (potentially slowed down) the accesses to the MT Shared Memory.
  -- Note that in the MT firmware the CPU1 is only accessing the masterfip_leds register for debugging
  -- purposes. The PCIe host is accessing the core directly only for testing purposes.
  cmp_wb_crossbar : entity work.xwb_crossbar
    generic map(
      g_num_masters => c_NUM_WB_MASTERS,
      g_num_slaves  => c_NUM_WB_SLAVES,
      g_registered  => true,
      g_address     => C_SLAVE_ADDR,
      g_mask        => C_SLAVE_MASK)
    port map(
      clk_sys_i     => clk_62m5_sys,
      rst_n_i       => s_rst_n,
      slave_i(0)    => fmc_core_wb_out(0),
      slave_i(1)    => fmc_core_wb_out(1),
      slave_i(2)    => cnx_master_out(c_SLAVE_FMC0),
      slave_i(3)    => uart_wb_1_o,
      slave_i(4)    => uart_wb_2_o,
      slave_i(5)    => gpio_wb_o,
      slave_o(0)    => fmc_core_wb_in(0),
      slave_o(1)    => fmc_core_wb_in(1),
      slave_o(2)    => cnx_master_in(c_SLAVE_FMC0),
      slave_o(3)    => uart_wb_1_i,
      slave_o(4)    => uart_wb_2_i,
      slave_o(5)    => gpio_wb_i,
      master_o(0)   => fmc_wb_muxed_out,
      master_i(0)   => fmc_wb_muxed_in
    );

  ---------------------------------------------------------------------------------------------------
  --                                Wishbone UART Controller 1                                     --
  ---------------------------------------------------------------------------------------------------
  cmp_xwb_uart_controller_1 : entity work.xwb_simple_uart
  generic map(
      g_WITH_VIRTUAL_UART       => FALSE,
      g_WITH_PHYSICAL_UART      => TRUE,
      g_WITH_PHYSICAL_UART_FIFO => TRUE,
      g_TX_FIFO_SIZE            => 64,
      g_RX_FIFO_SIZE            => 64,
      g_INTERFACE_MODE          => PIPELINED,
      g_ADDRESS_GRANULARITY     => BYTE,
      g_VUART_FIFO_SIZE         => 0,
      g_PRESET_BCR              => 966)
  port map(
      clk_sys_i   => clk_62m5_sys,
      rst_n_i     => s_rst_n,
      slave_i     => cnx_master_out(c_SLAVE_UART_1),
      slave_o     => cnx_master_in(c_SLAVE_UART_1),
      desc_o      => open,
      int_o       => s_irq_uart_1,
      uart_rxd_i  => ertec_uart_tx_1_i,
      uart_txd_o  => ertec_uart_rx_1_o
  );

  ---------------------------------------------------------------------------------------------------
  --                                Wishbone UART Controller 2                                     --
  ---------------------------------------------------------------------------------------------------
  cmp_xwb_uart_controller_2 : entity work.xwb_simple_uart
  generic map(
      g_WITH_VIRTUAL_UART       => FALSE,
      g_WITH_PHYSICAL_UART      => TRUE,
      g_WITH_PHYSICAL_UART_FIFO => TRUE,
      g_TX_FIFO_SIZE            => 64,
      g_RX_FIFO_SIZE            => 64,
      g_INTERFACE_MODE          => PIPELINED,
      g_ADDRESS_GRANULARITY     => BYTE,
      g_VUART_FIFO_SIZE         => 0,
      g_PRESET_BCR              => 966)
  port map(
      clk_sys_i   => clk_62m5_sys,
      rst_n_i     => s_rst_n,
      slave_i     => cnx_master_out(c_SLAVE_UART_2),
      slave_o     => cnx_master_in(c_SLAVE_UART_2),
      desc_o      => open,
      int_o       => s_irq_uart_2,
      uart_rxd_i  => ertec_uart_rx_2_i, -- ERTEC TX_O -> FPGA RX_I -> UART RX_I
      uart_txd_o  => ertec_uart_tx_2_o
  );

  ---------------------------------------------------------------------------------------------------
  --                                Wishbone GPIO Controller                                       --
  ---------------------------------------------------------------------------------------------------
  cmp_rst_gpio : entity work.xwb_gpio_port
  generic map (
      g_interface_mode         => PIPELINED,
      g_address_granularity    => BYTE,
      g_num_pins               => 1,
      g_with_builtin_sync      => true,
      g_with_builtin_tristates => false)
  port map (
      clk_sys_i   => clk_62m5_sys,
      rst_n_i     => s_rst_n,
      slave_i     => cnx_master_out(c_SLAVE_RST_PRFIP),
      slave_o     => cnx_master_in(c_SLAVE_RST_PRFIP),
      desc_o      => open, -- port not used
      gpio_b      => open, -- port not used
      gpio_out_o  => gpio_out,
      gpio_in_i   => gpio_in,
      gpio_oen_o  => gpio_oen
  );

  -- ERTEC is reseted when reset is '0'
  fmc_profinet_rst_n_o <= '1' when (gpio_oen(0)='0') else gpio_out(0);

  ---------------------------------------------------------------------------------------------------
  --                                            METADATA                                           --
  ---------------------------------------------------------------------------------------------------
  cmp_xwb_metadata : entity work.xwb_metadata
    generic map(
      g_VENDOR_ID    => x"0000_10DC",     -- taken from wiki
      g_DEVICE_ID    => c_MSTRFIP_APP_ID, -- ASCII is SPMA (SVEC+MasterFIP)
      g_VERSION      => sourceid_svec_masterfip_mt_urv_pkg.version,
      g_CAPABILITIES => x"0000_0000",
      g_COMMIT_ID    => sourceid_svec_masterfip_mt_urv_pkg.sourceid)
    port map(
      clk_i          => clk_62m5_sys,
      rst_n_i        => s_rst_n,
      wb_i           => cnx_master_out(c_SLAVE_METADATA),
      wb_o           => cnx_master_in(c_SLAVE_METADATA)
    );
  ---------------------------------------------------------------------------------------------------
  --                                           SVEC BASE                                           --
  ---------------------------------------------------------------------------------------------------
  inst_svec_base : entity work.svec_base_wr
    generic map(
      --  For the VME64x interface: if true, also consider AM in the decoder.
      g_DECODE_AM    => FALSE,
      --  If true, instantiate a VIC/ONEWIRE/SPI/WR/DDRAM+DMA.
      g_WITH_VIC     => TRUE,  -- use of xwb_vic
      g_WITH_ONEWIRE => FALSE, -- no use of onewire
      g_WITH_SPI     => FALSE, -- no use of SPI
      g_WITH_WR      => FALSE, -- no use of WR
      g_WITH_DDR4    => FALSE, -- no use of DDR4
      g_WITH_DDR5    => FALSE, -- no use of DDR5
      --  Address of the application meta-data.  0 if none.
      g_APP_OFFSET   => c_METADATA_ADDR,
      --  Number of user interrupts
      g_NUM_USER_IRQ => 6, -- hmq in/out, notify and console irq, uart interrupts
      -- Simulation-mode enable parameter. Set by default (synthesis) to 0, and
      -- changed to non-zero in the instantiation of the top level DUT in the testbench.
      -- Its purpose is to reduce some internal counters/timeouts to speed up simulations.
      g_SIMULATION   => g_SIMULATION,
      -- Increase information messages during simulation
      g_VERBOSE      => False
    )
    port map(
      ---------------------------------------------------------------------------
      -- Clocks/resets
      ---------------------------------------------------------------------------
      -- Reset from system fpga
      rst_n_i             => rst_n_i,
      -- 125 MHz PLL reference
      clk_125m_pllref_p_i => clk_125m_pllref_p_i,
      clk_125m_pllref_n_i => clk_125m_pllref_n_i,
      -- 20MHz VCXO clock (for WR)
      clk_20m_vcxo_i      => '0',
      -- 125 MHz GTP reference
      clk_125m_gtp_n_i    => '0',
      clk_125m_gtp_p_i    => '0',
      -- Aux clocks, which can be disciplined by the WR Core
      clk_aux_i           => (others => '0'),
      -- 10MHz ext ref clock input
      clk_10m_ext_i       => '0',
      -- External PPS input
      pps_ext_i           => '0',
      ---------------------------------------------------------------------------
      -- VME interface
      ---------------------------------------------------------------------------
      vme_write_n_i       => vme_write_n_i,
      vme_sysreset_n_i    => vme_sysreset_n_i,
      vme_retry_oe_o      => vme_retry_oe_o,
      vme_retry_n_o       => vme_retry_n_o,
      vme_lword_n_b       => vme_lword_n_b,
      vme_iackout_n_o     => vme_iackout_n_o,
      vme_iackin_n_i      => vme_iack_n_i,
      vme_iack_n_i        => vme_iack_n_i,
      vme_gap_i           => vme_gap_i,
      vme_dtack_oe_o      => vme_dtack_oe_o,
      vme_dtack_n_o       => vme_dtack_n_o,
      vme_ds_n_i          => vme_ds_n_i,
      vme_data_oe_n_o     => vme_data_oe_n_o,
      vme_data_dir_o      => vme_data_dir_o,
      vme_berr_o          => vme_berr_o,
      vme_as_n_i          => vme_as_n_i,
      vme_addr_oe_n_o     => vme_addr_oe_n_o,
      vme_addr_dir_o      => vme_addr_dir_o,
      vme_irq_o           => vme_irq_o,
      vme_ga_i            => vme_ga_i,
      vme_data_b          => vme_data_b,
      vme_am_i            => vme_am_i,
      vme_addr_b          => vme_addr_b,
      ---------------------------------------------------------------------------
      -- FMC interface
      ---------------------------------------------------------------------------
      -- I2C interface for accessing FMC EEPROM.
      fmc0_scl_b          => open,
      fmc0_sda_b          => open,
      fmc1_scl_b          => open,
      fmc1_sda_b          => open,
      -- Presence  (there is a pull-up)
      fmc0_prsnt_m2c_n_i  => '0',
      fmc1_prsnt_m2c_n_i  => '0',

      ---------------------------------------------------------------------------
      -- Carrier
      ---------------------------------------------------------------------------
      spi_sclk_o          => spi_sclk_o,
      spi_ncs_o           => spi_ncs_o,
      spi_mosi_o          => spi_mosi_o,
      spi_miso_i          => spi_miso_i,
      carrier_scl_b       => open,
      carrier_sda_b       => open,

      -- Onewire interface
      onewire_b           => open,
      -- PCB revision
      pcbrev_i            => (others => '0'),
      ------------------------------------------
      --  User part
      ------------------------------------------
      --  Clocks and reset.
      clk_sys_62m5_o      => clk_62m5_sys, -- we only need the 62.5MHz that svec provides
      rst_sys_62m5_n_o    => rst_sys_62m5,
      clk_ref_125m_o      => clk_125m_sys,
      rst_ref_125m_n_o    => open,
      --  Interrupts
      irq_user_i(6)       => hmq_in_irq,
      irq_user_i(7)       => hmq_out_irq,
      irq_user_i(8)       => notify_irq,
      irq_user_i(9)       => console_irq,
      irq_user_i(10)      => s_irq_uart_1,
      irq_user_i(11)      => s_irq_uart_2,
      -- UART, needed from Profinet
      uart_rxd_i          => '1',
      uart_txd_o          => open,
      -- The wishbone bus from the gennum/host to the application
      -- Addresses 0-0x1fff are not available (used by the carrier).
      -- This is a pipelined wishbone with byte granularity.
      app_wb_o            => cnx_slave_in(c_MASTER_GENNUM),
      app_wb_i            => cnx_slave_out(c_MASTER_GENNUM)
    );

  -- connecting ertec - svec
  uart_txd_o <= ertec_uart_tx_1_i;

  ---------------------------------------------------------------------------------------------------
  --                                      FMC MASTERFIP CORE                                       --
  ---------------------------------------------------------------------------------------------------
  cmp_masterFIP_core : entity work.fmc_masterFIP_core
    generic map
    (
      g_span       => 32,
      g_width      => 32,
      g_simulation => f_int2bool(g_simulation))
    port map
    (
      clk_i        => clk_62m5_sys,
      rst_n_i      => s_rst_n,
      -- FMC one-wire
      onewire_b    => fmc_onewire_b,
      -- WorldFIP speed
      speed_b0_i   => speed_b0_i,
      speed_b1_i   => speed_b1_i,
      -- FIELDRIVE
      fd_rxcdn_a_i => fd_rxcdn_i,
      fd_rxd_a_i   => fd_rxd_i,
      fd_txer_a_i  => fd_txer_i,
      fd_wdgn_a_i  => fd_wdgn_i,
      fd_rstn_o    => fd_rstn_o,
      fd_txck_o    => s_fd_txck_o, --fd_txck_o,
      fd_txd_o     => fd_txd,
      fd_txena_o   => s_fd_txena_o, --fd_txena_o,
      -- External Synch
      ext_sync_term_en_o => ext_sync_term_en_o,
      ext_sync_a_i => ext_sync_i,
      ext_sync_dir_o => open, -- hard-wired to '0'
      ext_sync_oe_n_o => ext_sync_oe_n_o,
      -- LEDs
      leds_o       => leds,
      -- WISHBONE interface with MT CPU0 and CPU1
      wb_adr_i     => fmc_wb_muxed_out.adr,
      wb_dat_i     => fmc_wb_muxed_out.dat,
      wb_stb_i     => fmc_wb_muxed_out.stb,
      wb_we_i      => fmc_wb_muxed_out.we,
      wb_cyc_i     => fmc_wb_muxed_out.cyc,
      wb_sel_i     => fmc_wb_muxed_out.sel,
      wb_dat_o     => fmc_wb_muxed_in.dat,
      wb_ack_o     => fmc_wb_muxed_in.ack,
      wb_stall_o   => fmc_wb_muxed_in.stall);

  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  -
  -- unused WISHBONE signals
  fmc_wb_muxed_in.err <= '0';
  fmc_wb_muxed_in.rty <= '0';

  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  -
  led_rx_act_n_o   <= leds(0); -- probe on R4
  led_rx_err_n_o   <= leds(1); -- probe on R8
  led_tx_act_n_o   <= leds(2); -- probe on R4
  led_tx_err_n_o   <= leds(3); -- probe on R7
  led_sync_act_n_o <= leds(4); -- probe on R1
  led_sync_err_n_o <= leds(5); -- probe on R6

  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  -
  fd_txd_o <= fd_txd;
  tp1_o    <= fd_rxd_i;
  tp2_o    <= fd_txd;
  tp3_o    <= leds(8);
  tp4_o    <= leds(9);
  ---------------------------------------------------------------------------------------------------
  --                                     SVEC front panel LEDs                                     --
  ---------------------------------------------------------------------------------------------------
  p_drive_svec_led_clk_sys : process (clk_62m5_sys)
  begin
    if rising_edge(clk_62m5_sys) then
      if (rst_n_sys = '0') then
        svec_led <= "0111111111111111";
        led_divider <= (others => '0');
      else
        led_divider <= led_divider + 1;
        if (led_divider = 0) then
          svec_led <= svec_led(14 downto 0) & svec_led(15);
        end if;
      end if;
    end if;
  end process;

  led_green_o <= svec_led(15);
  led_red_o   <= not rst_n_sys;

  -- ERTEC gpios are connected to the LED of the profinet
  led_profinet_y_o <= ertec_gpio_16_i;
  led_profinet_r_o <= ertec_gpio_17_i;
  led_profinet_g_o <= ertec_gpio_18_i;

  -- The following procedure is to show in the SVEC front panel LEDs
  -- some of the signals, just for debugging reasons. The signals from
  -- SVEC that needs to be assigned are the fp_gpio and fp_term_en_o.

  -- drive the ERTEC SPI signals to internal signals
  --  chip_select <= ertec_spi_cs_n_i;
  --  spi_clock   <= ertec_spi_clk_i;
  --  spi_mosi    <= ertec_spi_mosi_i;
  --  ertec_spi_miso_o <= spi_miso;

  -- Drive the fieldrive signals
  s_fd_rx_data_i <= fd_rxd_i;
  s_fd_tx_data_o <= fd_txd;
  fd_txena_o     <= s_fd_txena_o;
  fd_txck_o      <= s_fd_txck_o;
--  s_fd_txck_o    <= fd_txck_o;
--  s_fd_txena_o   <= fd_txena_o;

  fp_gpio1_b      <= s_fd_txck_o;
  fp_gpio2_b      <= s_fd_rx_data_i;
  fp_gpio3_b      <= s_fd_txena_o;
  fp_gpio4_b      <= s_fd_tx_data_o;
  fp_term_en_o    <= (others=>'0');
  fp_gpio1_a2b_o  <= '1'; -- 1 is for out, 0 is for in
  fp_gpio2_a2b_o  <= '0';
  fp_gpio34_a2b_o <= '1';


  -- Front panel IO configuration
  -- fp_gpio_b -> these can be either in, out, inout. it is us who decide the direction
  --  fp_gpio1_b      <= chip_select;
  --  fp_gpio2_b      <= spi_clock;
  --  fp_gpio3_b      <= spi_mosi;
  --  fp_gpio4_b      <= spi_miso;
  --  fp_term_en_o    <= (others=>'0'); -- all 0's since we will not what to see an in signal
  -- these specify the direction
  --  fp_gpio1_a2b_o  <= '1'; -- 1 is for out, 0 is for in
  --  fp_gpio2_a2b_o  <= '1';
  --  fp_gpio34_a2b_o <= '1';

end rtl;
--=================================================================================================
--                                        architecture end
--=================================================================================================
---------------------------------------------------------------------------------------------------
