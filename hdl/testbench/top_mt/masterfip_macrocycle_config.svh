/*
 * This program source code file is part of MasterFip project.
 *
 * Copyright (C) 2013-2017 CERN
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, you may find one here:
 * http://www.gnu.org/licenses/old-licenses/gpl-2.0.html
 * or you may search the http://www.gnu.org website for the version 2 license,
 * or you may write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 */

/* masterfip_macrocycle_config.svh - test macrocycle definition */

`ifndef __MASTERFIP_MACROCYCLE_CONFIG_INCLUDED
 `define __MASTERFIP_MACROCYCLE_CONFIG_INCLUDED

 `include "logger.svh"
 `include "masterfip_common.svh"

typedef class MasterfipDriver;

virtual class MasterfipMacrocycleConfig;
   pure virtual function string getMacrocycleName();
   pure virtual task configureMacrocycle( MasterfipDriver drv );
endclass // MasterfipMacrocycleConfig

class MasterfipTestMacrocycle extends MasterfipMacrocycleConfig;
   protected int m_speed;
   protected int m_variant;
   protected int m_disableProducedFrame;
   
   function new;
      m_speed = 2500000;
      m_variant = 0;
      m_disableProducedFrame = -1;
      
   endfunction // getMacrocycleName

   function string getMacrocycleName();
      return $sformatf("Default Macrocycle [variant %d]", m_variant);
   endfunction // getMacrocycleName

   task automatic setSpeed( int speed );
      m_speed = speed;
   endtask // setSpeed

   task automatic setVariant( int v );
      m_variant = v;
   endtask // setSpeed

   
   task automatic disableProducedVariable(int index);
      m_disableProducedFrame = index;
      
   endtask // disableProducedFrame
   
   
   
   task automatic configureMacrocycle( MasterfipDriver drv );
      automatic Logger l = Logger::get();
      automatic MockTurtleDriver mt = drv.MockTurtle();
      automatic MQueueHost hmq = mt.HMQ();
      automatic Serializable response = new;
      
      // mstrfip_hw_cfg_set (dir:input slot:0) : external trigger and default turn around time according to the bus speed 

      automatic MQueueMessage msg_hw_cfg_set_25m = new ( '{'h2, 'h0, 'h1, 'h0, 'h0, 'hbb8, 'h2d50, 'h28} );
      automatic MQueueMessage msg_hw_cfg_set_31k = new ( '{'h2 , 'h0 , 'h1 , 'h0 , 'h0 , 'hcfd0 , 'h69460 , 'hc04 } );
      automatic MQueueMessage msg_hw_cfg_set_1m = new ( '{'h2 , 'h0 , 'h1 , 'h0 , 'h0 , 'hc80 , 'h4588 , 'h64 } );
      

	 
	// mstrfip_varlist_send  (dir:input slot:0) : send definition of periodic var list according to the scenario

	automatic MQueueMessage msg_var_list_send = new ('{ 'h5, 'h0, 'h6, 'h101, 'h400001, 'h15f01, 'h20001, 
							    'h2b401, 'h7c0001, 'h102, 'h400082, 'h15f02, 'h20082, 'h2b402, 'h7c0082 } );

      // mstrfip_instr-send  (dir:input slot:0) : macro cycle definition according to the scenario
      automatic MQueueMessage msg_instr_list_send_25m = new ( '{'h4 , 'h0 , 'hf4240 , 'h5 , 'h1 , 'h0 , 'h3 , 'h0 , 
								'h0 , 'h0 , 'h0 , 'h1 , 'h3 , 'h6 , 'h0 , 'h0 , 'h0 , 'h0 , 
								'h2 , 'haae60 , 'h3 , 'h3 , 'h100 , 'h0 , 'h0 , 'h3 , 'hf4240 , 
								'h0 , 'h0 , 'h0 , 'h0 , 'h0 , 'h4 , 'hf4240 , 'h1 , 'h0 , 
								'h0 , 'h0 , 'h0 } );

      automatic MQueueMessage msg_instr_list_send_31k = new ( '{ 'h4 , 'h0 , 'h2faf080 , 'h5 , 'h1 , 'h0 , 'h3 , 'h0 , 'h0 , 'h0 , 'h0 , 'h1 , 'h3 , 'h6 , 'h0 , 'h0 , 'h0
								 , 'h0 , 'h2 , 'h2160ec0 , 'h3 , 'h3 , 'h100 , 'h0 , 'h0 , 'h3 , 'h2faf080 , 'h0 , 'h0 , 'h0 , 'h0 , 'h0
								 , 'h4 , 'h2faf080 , 'h1 , 'h0 , 'h0 , 'h0 , 'h0 } );
      

      automatic MQueueMessage msg_instr_list_send_1m = new ( '{ 'h4 , 'h0 , 'h16e360 , 'h5 , 'h1 , 'h0 , 'h3 , 'h0 , 'h0 , 'h0 , 'h0 , 'h1 , 'h3 , 'h6 , 'h0 , 'h0 , 'h0
								, 'h0 , 'h2 , 'h100590 , 'h3 , 'h3 , 'h100 , 'h0 , 'h0 , 'h3 , 'h16e360 , 'h0 , 'h0 , 'h0 , 'h0 , 'h0
								, 'h4 , 'h16e360 , 'h1 , 'h0 , 'h0 , 'h0 , 'h0 } );
      

      
      automatic int bitrate, status;
      
      l.msg(0, "Configure macrocycle 1");
      
      drv.getBitrate( bitrate, status );
      
      if(status)
	begin
	   l.fail("No response for GET_BITRATE message");
	   return;
	end

      l.msg(0, $sformatf("Bit rate: %d", bitrate) );

      if ( ( m_speed == 2500000 && bitrate != 2 )
	   && ( m_speed == 1000000 && bitrate != 1 )
	   && ( m_speed == 31250 && bitrate != 0 ) )
	begin
	   l.fail($sformatf("GET_BITRATE response (0x%x) doesn't match the selected bus speed (%d)", bitrate, m_speed) );
	   return;
	end
      
      
      l.msg(2,"Sending HW_CFG_SET");

      case ( m_speed )
	31250:
	  hmq.sendAndReceiveSync(`MSTRFIP_HMQ_I_CPU0_CMD, msg_hw_cfg_set_31k, `MSTRFIP_HMQ_O_RESP_CPU0_CMD, response, 100, status );
	1000000:
	  hmq.sendAndReceiveSync(`MSTRFIP_HMQ_I_CPU0_CMD, msg_hw_cfg_set_1m, `MSTRFIP_HMQ_O_RESP_CPU0_CMD, response, 100, status );
	2500000:
	  hmq.sendAndReceiveSync(`MSTRFIP_HMQ_I_CPU0_CMD, msg_hw_cfg_set_25m, `MSTRFIP_HMQ_O_RESP_CPU0_CMD, response, 100, status );
      endcase // case ( m_speed )
      
      if( status )
	begin
	   l.fail("No response for HW_CFG_SET message");
	   return;
	end
      l.msg(2,"Sending VAR_LIST_SEND");
      

      hmq.sendAndReceiveSync(`MSTRFIP_HMQ_I_CPU0_CMD, msg_var_list_send,  `MSTRFIP_HMQ_O_RESP_CPU0_CMD, response, 100, status );
      if( status )
	begin
	   l.fail("No response for VAR_LIST_SEND message");
	   return;
	end
      l.msg(2,"Sending INSTR_LIST_SEND");
      

      case ( m_speed )
	31250:
	  hmq.sendAndReceiveSync (`MSTRFIP_HMQ_I_CPU0_CMD, msg_instr_list_send_31k, `MSTRFIP_HMQ_O_RESP_CPU0_CMD, response, 2000, status );
	1000000:
	  hmq.sendAndReceiveSync (`MSTRFIP_HMQ_I_CPU0_CMD, msg_instr_list_send_1m, `MSTRFIP_HMQ_O_RESP_CPU0_CMD, response, 2000, status );
	2500000:
	  hmq.sendAndReceiveSync (`MSTRFIP_HMQ_I_CPU0_CMD, msg_instr_list_send_25m, `MSTRFIP_HMQ_O_RESP_CPU0_CMD, response, 2000, status );
      endcase // case ( m_speed )
      


      if( status )
	begin
	   l.fail("No response for INSTR_LIST_SEND message");
	   return;
	end

      
   endtask // configureMacrocycle

   task automatic setPeriodicVariables( MasterfipDriver drv );
      automatic Logger l = Logger::get();
      automatic MockTurtleDriver mt = drv.MockTurtle();
      automatic MQueueHost hmq = mt.HMQ();

      automatic MQueueMessage msg_var1_payload = new ( '{ 'h8 , 'h0 , 'h0 , 'h40 , 'h3020100 , 'h7060504 , 'hb0a0908 , 'hf0e0d0c , 'h13121110 
							  , 'h17161514 , 'h1b1a1918 , 'h1f1e1d1c , 'h23222120 , 'h27262524 , 'h2b2a2928 
							  , 'h2f2e2d2c , 'h33323130 , 'h37363534 , 'h3b3a3938 , 'h3f3e3d3c});


      automatic MQueueMessage msg_var2_payload = new ( '{ 'h8 , 'h0 , 'h1 , 'h2 , 'h0100 });
      

      automatic MQueueMessage msg_var3_payload = new ( '{ 'h8 , 'h0 , 'h2 , 'h7c , 'h3020100 , 'h7060504 , 'hb0a0908 , 'hf0e0d0c , 'h13121110 
							  , 'h17161514 , 'h1b1a1918 , 'h1f1e1d1c , 'h23222120 , 'h27262524 , 'h2b2a2928 
							  , 'h2f2e2d2c , 'h33323130 , 'h37363534 , 'h3b3a3938 , 'h3f3e3d3c , 'h43424140 
							  , 'h47464544 , 'h4b4a4948 , 'h4f4e4d4c , 'h53525150 , 'h57565554 , 'h5b5a5958 
							  , 'h5f5e5d5c , 'h63626160 , 'h67666564 , 'h6b6a6968 , 'h6f6e6d6c , 'h73727170 
							  , 'h77767574 , 'h7b7a7978 });

      if( m_disableProducedFrame != 1 )
	begin
	   l.msg(2,"Sending Variable 1 (0x1) payload");
	   hmq.sendMessage (`MSTRFIP_HMQ_I_CPU1_CMD, msg_var1_payload );
	end
      
      if( m_disableProducedFrame != 2 )
	begin
	   l.msg(2,"Sending Variable 2 (0x5f) payload");
	   hmq.sendMessage (`MSTRFIP_HMQ_I_CPU1_CMD, msg_var2_payload );
	end
      
      if( m_disableProducedFrame != 3 )
	begin	
	   l.msg(2,"Sending Variable 3 (0xb4) payload");
	   hmq.sendMessage (`MSTRFIP_HMQ_I_CPU1_CMD, msg_var3_payload );
end
      
   endtask // setPeriodicVariables

   task automatic setMessages( MasterfipDriver drv );
      automatic Logger l = Logger::get();
      automatic MockTurtleDriver mt = drv.MockTurtle();
      automatic MQueueHost hmq = mt.HMQ();

      automatic MQueueMessage msg_aper_request1 = new ( 
							'{ 'h9 , 'h0 , 'h01, 'h100 , 'h1 , 'h3020100 , 'h7060504 , 'hb0a0908 , 
							   'hf0e0d0c , 'h13121110 , 'h17161514 , 'h1b1a1918 , 'h1f1e1d1c , 'h23222120 , 
							   'h27262524 , 'h2b2a2928 , 'h2f2e2d2c , 'h33323130 , 'h37363534 , 'h3b3a3938 , 
							   'h3f3e3d3c , 'h43424140 , 'h47464544 , 'h4b4a4948 , 'h4f4e4d4c , 'h53525150 , 
							   'h57565554 , 'h5b5a5958 , 'h5f5e5d5c , 'h63626160 , 'h67666564 , 'h6b6a6968 , 
							   'h6f6e6d6c , 'h73727170 , 'h77767574 , 'h7b7a7978 , 'h7f7e7d7c , 'h83828180 , 
							   'h87868584 , 'h8b8a8988 , 'h8f8e8d8c , 'h93929190 , 'h97969594 , 'h9b9a9998 , 
							   'h9f9e9d9c , 'ha3a2a1a0 , 'ha7a6a5a4 , 'habaaa9a8 , 'hafaeadac , 'hb3b2b1b0 , 
							   'hb7b6b5b4 , 'hbbbab9b8 , 'hbfbebdbc , 'hc3c2c1c0 , 'hc7c6c5c4 , 'hcbcac9c8 , 
							   'hcfcecdcc , 'hd3d2d1d0 , 'hd7d6d5d4 , 'hdbdad9d8 , 'hdfdedddc , 'he3e2e1e0 , 
							   'he7e6e5e4 , 'hebeae9e8 , 'hefeeedec , 'hf3f2f1f0 , 'hf7f6f5f4 , 'hfbfaf9f8 , 
							   'hfffefdfc } );

      // ID_MSG(0001)
      automatic MQueueMessage msg_aper_request2 = new ( '{ 'h9 , 'h0 , 'h1 , 'h1,  'h1, 'h00, 'h00 } );

      l.msg(0,"Requesting message 1");
      hmq.sendMessage (`MSTRFIP_HMQ_I_CPU1_CMD, m_variant == 0 ? msg_aper_request1 : msg_aper_request2 );

      
   endtask // setMessages
endclass // MasterfipTestMacrocycle

`endif //  `ifndef __MASTERFIP_MACROCYCLE_CONFIG_INCLUDED
