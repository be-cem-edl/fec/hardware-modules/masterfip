`ifndef __FIP_MONITOR_DEVICE_SVH
 `define __FIP_MONITOR_DEVICE_SVH

 `include "fip_device.svh"

class FipMonitorDevice extends FipDevice;

   protected FipFrameList m_frames;
   
   function automatic FipFrameList getFrames();
      return m_frames;
   endfunction // getFrames
   
   
   virtual task automatic onReceive ( FipFrame frame );
      Logger l = Logger::get();
      m_frames.push_back( frame );
      l.msg(0, $sformatf("----[%.1f us] > %s", real'(frame.getStartTime()) / real'(1us), frame.str() ) );
//      m_phy.setVerbose(1);
      
   endtask // onReceive

   virtual task automatic clear();
      m_frames = '{};
   endtask // clear
   
endclass // FipMonitorDevice

`endif

