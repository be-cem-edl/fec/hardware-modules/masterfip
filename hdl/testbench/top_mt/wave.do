# ##############################################################################
#
#
#
# ##############################################################################

onerror {resume}
quietly WaveActivateNextPane {} 0

# Main Testbench
add wave -noupdate -expand -group TESTBENCH /main/mosi_data
add wave -noupdate -expand -group TESTBENCH /main/miso_data
add wave -noupdate -expand -group TESTBENCH -unsigned /main/posedge_cnt
add wave -noupdate -expand -group TESTBENCH /main/q_mosi_data
add wave -noupdate -expand -group TESTBENCH -unsigned /main/mosi_cnt
add wave -noupdate -expand -group TESTBENCH /main/q_miso_data
add wave -noupdate -expand -group TESTBENCH -unsigned /main/miso_cnt
add wave -noupdate -expand -group TESTBENCH /main/operation
add wave -noupdate -expand -group TESTBENCH -unsigned /main/data_len
add wave -noupdate -expand -group TESTBENCH /main/basic_info
add wave -noupdate -expand -group TESTBENCH /main/wr_rmq_id
add wave -noupdate -expand -group TESTBENCH /main/idx
add wave -noupdate -expand -group TESTBENCH /main/ertec_rst
add wave -noupdate -expand -group TESTBENCH /main/ertec_rst_cpu

# DUT signals
add wave -noupdate -expand -group DUT /main/DUT/s_rst_n
add wave -noupdate -expand -group DUT /main/DUT/ertec_rst_n_i
add wave -noupdate -expand -group DUT /main/DUT/ertec_rst_cpu_n_i
add wave -noupdate -expand -group DUT /main/DUT/rst_n_sys

# ERTEC
add wave -noupdate -expand -group ERTEC -color Orange /main/DUT/ertec_spi_clk_i
add wave -noupdate -expand -group ERTEC -color Orange /main/DUT/ertec_spi_cs_n_i
add wave -noupdate -expand -group ERTEC -color Orange /main/DUT/ertec_spi_mosi_i
add wave -noupdate -expand -group ERTEC -color Orange /main/DUT/ertec_spi_miso_o
add wave -noupdate -expand -group ERTEC -color Orange /main/DUT/ertec_rmq_status_o

# DUT RMQ Signals
add wave -noupdate -expand -group DUT_RMQ -color Orange /main/DUT/rmq_endpoint_out
add wave -noupdate -expand -group DUT_RMQ -color Orange /main/DUT/rmq_endpoint_in
add wave -noupdate -expand -group DUT_RMQ -color Orange /main/DUT/rmq_src_in
add wave -noupdate -expand -group DUT_RMQ -color Orange /main/DUT/rmq_src_out
add wave -noupdate -expand -group DUT_RMQ -color Orange /main/DUT/rmq_snk_in
add wave -noupdate -expand -group DUT_RMQ -color Orange /main/DUT/rmq_snk_out

# MT_PROFIP_TRANSLATOR
add wave -noupdate -expand -group MT_PROFIP_TRANSLATOR -color Green /main/DUT/cmp_mt_profip_translator/clk_i
add wave -noupdate -expand -group MT_PROFIP_TRANSLATOR -color Green /main/DUT/cmp_mt_profip_translator/rst_n_i
add wave -noupdate -expand -group MT_PROFIP_TRANSLATOR -color Green /main/DUT/cmp_mt_profip_translator/s_operation
add wave -noupdate -expand -group MT_PROFIP_TRANSLATOR -color Green -unsigned /main/DUT/cmp_mt_profip_translator/s_rmq_id
add wave -noupdate -expand -group MT_PROFIP_TRANSLATOR -color Green /main/DUT/cmp_mt_profip_translator/s_wr_state
add wave -noupdate -expand -group MT_PROFIP_TRANSLATOR -color Green /main/DUT/cmp_mt_profip_translator/s_mosi_data
add wave -noupdate -expand -group MT_PROFIP_TRANSLATOR -color Green /main/DUT/cmp_mt_profip_translator/s_wr_data_valid
add wave -noupdate -expand -group MT_PROFIP_TRANSLATOR -color Green -unsigned /main/DUT/cmp_mt_profip_translator/s_wr_rmq_id
add wave -noupdate -expand -group MT_PROFIP_TRANSLATOR -color Green -unsigned /main/DUT/cmp_mt_profip_translator/s_wr_data_len
add wave -noupdate -expand -group MT_PROFIP_TRANSLATOR -color Green -unsigned /main/DUT/cmp_mt_profip_translator/s_wr_data_cnt
add wave -noupdate -expand -group MT_PROFIP_TRANSLATOR -color Green -unsigned /main/DUT/cmp_mt_profip_translator/s_wr_header_cnt
add wave -noupdate -expand -group MT_PROFIP_TRANSLATOR -color Green /main/DUT/cmp_mt_profip_translator/s_wr_header
add wave -noupdate -expand -group MT_PROFIP_TRANSLATOR -color Green /main/DUT/cmp_mt_profip_translator/s_wr_data
add wave -noupdate -expand -group MT_PROFIP_TRANSLATOR -color Green /main/DUT/cmp_mt_profip_translator/s_rd_state
add wave -noupdate -expand -group MT_PROFIP_TRANSLATOR -color Green /main/DUT/cmp_mt_profip_translator/s_spi_miso_data
add wave -noupdate -expand -group MT_PROFIP_TRANSLATOR -color Green /main/DUT/cmp_mt_profip_translator/s_miso_data_reg
add wave -noupdate -expand -group MT_PROFIP_TRANSLATOR -color Green /main/DUT/cmp_mt_profip_translator/s_ready
add wave -noupdate -expand -group MT_PROFIP_TRANSLATOR -color Green /main/DUT/cmp_mt_profip_translator/s_data_valid
add wave -noupdate -expand -group MT_PROFIP_TRANSLATOR -color Green /main/DUT/cmp_mt_profip_translator/s_rd_en

# SPI SIGNALS
add wave -noupdate -expand -group SPI_SIGNALS -color Green /main/DUT/cmp_mt_profip_translator/clk_i
add wave -noupdate -expand -group SPI_SIGNALS -color Green /main/DUT/cmp_mt_profip_translator/rst_n_i
add wave -noupdate -expand -group SPI_SIGNALS -color Green /main/DUT/cmp_mt_profip_translator/s_spi_state
add wave -noupdate -expand -group SPI_SIGNALS -color Green /main/DUT/cmp_mt_profip_translator/s_sample_en
add wave -noupdate -expand -group SPI_SIGNALS -color Green /main/DUT/cmp_mt_profip_translator/s_shift_en
add wave -noupdate -expand -group SPI_SIGNALS -color Green /main/DUT/cmp_mt_profip_translator/spi_clk_i
add wave -noupdate -expand -group SPI_SIGNALS -color Green /main/DUT/cmp_mt_profip_translator/spi_cs_n_i
add wave -noupdate -expand -group SPI_SIGNALS -color Green /main/DUT/cmp_mt_profip_translator/spi_mosi_i
add wave -noupdate -expand -group SPI_SIGNALS -color Green /main/DUT/cmp_mt_profip_translator/spi_miso_o
add wave -noupdate -expand -group SPI_SIGNALS -color Green /main/DUT/cmp_mt_profip_translator/s_sample_en
add wave -noupdate -expand -group SPI_SIGNALS -color Green /main/DUT/cmp_mt_profip_translator/s_shift_en
add wave -noupdate -expand -group SPI_SIGNALS -color Green /main/DUT/cmp_mt_profip_translator/s_data_reg_o
add wave -noupdate -expand -group SPI_SIGNALS -color Green /main/DUT/cmp_mt_profip_translator/s_data_reg_i
add wave -noupdate -expand -group SPI_SIGNALS -color Green -unsigned /main/DUT/cmp_mt_profip_translator/s_mosi_cnt
add wave -noupdate -expand -group SPI_SIGNALS -color Green -unsigned /main/DUT/cmp_mt_profip_translator/s_miso_cnt

# FIFOS
add wave -noupdate -divider MOSI_FIFO
add wave -noupdate /main/DUT/cmp_mt_profip_translator/gen_sync_fifo/cmp_mosi_sync_fifo/rst_n_i
add wave -noupdate /main/DUT/cmp_mt_profip_translator/gen_sync_fifo/cmp_mosi_sync_fifo/clk_i
add wave -noupdate /main/DUT/cmp_mt_profip_translator/gen_sync_fifo/cmp_mosi_sync_fifo/d_i
add wave -noupdate /main/DUT/cmp_mt_profip_translator/gen_sync_fifo/cmp_mosi_sync_fifo/we_i
add wave -noupdate /main/DUT/cmp_mt_profip_translator/gen_sync_fifo/cmp_mosi_sync_fifo/q_o
add wave -noupdate /main/DUT/cmp_mt_profip_translator/gen_sync_fifo/cmp_mosi_sync_fifo/rd_i
add wave -noupdate /main/DUT/cmp_mt_profip_translator/gen_sync_fifo/cmp_mosi_sync_fifo/empty_o
add wave -noupdate /main/DUT/cmp_mt_profip_translator/gen_sync_fifo/cmp_mosi_sync_fifo/full_o
add wave -noupdate -divider FIFO_0
add wave -noupdate /main/DUT/cmp_mt_profip_translator/gen_sync_fifo/gen_sync_miso_fifo_per_cpu_per_slot(0)/cmp_miso_sync_fifo/rst_n_i
add wave -noupdate /main/DUT/cmp_mt_profip_translator/gen_sync_fifo/gen_sync_miso_fifo_per_cpu_per_slot(0)/cmp_miso_sync_fifo/clk_i
add wave -noupdate /main/DUT/cmp_mt_profip_translator/gen_sync_fifo/gen_sync_miso_fifo_per_cpu_per_slot(0)/cmp_miso_sync_fifo/d_i
add wave -noupdate /main/DUT/cmp_mt_profip_translator/gen_sync_fifo/gen_sync_miso_fifo_per_cpu_per_slot(0)/cmp_miso_sync_fifo/we_i
add wave -noupdate /main/DUT/cmp_mt_profip_translator/gen_sync_fifo/gen_sync_miso_fifo_per_cpu_per_slot(0)/cmp_miso_sync_fifo/q_o
add wave -noupdate /main/DUT/cmp_mt_profip_translator/gen_sync_fifo/gen_sync_miso_fifo_per_cpu_per_slot(0)/cmp_miso_sync_fifo/rd_i
add wave -noupdate /main/DUT/cmp_mt_profip_translator/gen_sync_fifo/gen_sync_miso_fifo_per_cpu_per_slot(0)/cmp_miso_sync_fifo/empty_o
add wave -noupdate /main/DUT/cmp_mt_profip_translator/gen_sync_fifo/gen_sync_miso_fifo_per_cpu_per_slot(0)/cmp_miso_sync_fifo/full_o
add wave -noupdate -divider FIFO_1
add wave -noupdate /main/DUT/cmp_mt_profip_translator/gen_sync_fifo/gen_sync_miso_fifo_per_cpu_per_slot(1)/cmp_miso_sync_fifo/rst_n_i
add wave -noupdate /main/DUT/cmp_mt_profip_translator/gen_sync_fifo/gen_sync_miso_fifo_per_cpu_per_slot(1)/cmp_miso_sync_fifo/clk_i
add wave -noupdate /main/DUT/cmp_mt_profip_translator/gen_sync_fifo/gen_sync_miso_fifo_per_cpu_per_slot(1)/cmp_miso_sync_fifo/d_i
add wave -noupdate /main/DUT/cmp_mt_profip_translator/gen_sync_fifo/gen_sync_miso_fifo_per_cpu_per_slot(1)/cmp_miso_sync_fifo/we_i
add wave -noupdate /main/DUT/cmp_mt_profip_translator/gen_sync_fifo/gen_sync_miso_fifo_per_cpu_per_slot(1)/cmp_miso_sync_fifo/q_o
add wave -noupdate /main/DUT/cmp_mt_profip_translator/gen_sync_fifo/gen_sync_miso_fifo_per_cpu_per_slot(1)/cmp_miso_sync_fifo/rd_i
add wave -noupdate /main/DUT/cmp_mt_profip_translator/gen_sync_fifo/gen_sync_miso_fifo_per_cpu_per_slot(1)/cmp_miso_sync_fifo/empty_o
add wave -noupdate /main/DUT/cmp_mt_profip_translator/gen_sync_fifo/gen_sync_miso_fifo_per_cpu_per_slot(1)/cmp_miso_sync_fifo/full_o
add wave -noupdate -divider FIFO_2
add wave -noupdate /main/DUT/cmp_mt_profip_translator/gen_sync_fifo/gen_sync_miso_fifo_per_cpu_per_slot(2)/cmp_miso_sync_fifo/rst_n_i
add wave -noupdate /main/DUT/cmp_mt_profip_translator/gen_sync_fifo/gen_sync_miso_fifo_per_cpu_per_slot(2)/cmp_miso_sync_fifo/clk_i
add wave -noupdate /main/DUT/cmp_mt_profip_translator/gen_sync_fifo/gen_sync_miso_fifo_per_cpu_per_slot(2)/cmp_miso_sync_fifo/d_i
add wave -noupdate /main/DUT/cmp_mt_profip_translator/gen_sync_fifo/gen_sync_miso_fifo_per_cpu_per_slot(2)/cmp_miso_sync_fifo/we_i
add wave -noupdate /main/DUT/cmp_mt_profip_translator/gen_sync_fifo/gen_sync_miso_fifo_per_cpu_per_slot(2)/cmp_miso_sync_fifo/q_o
add wave -noupdate /main/DUT/cmp_mt_profip_translator/gen_sync_fifo/gen_sync_miso_fifo_per_cpu_per_slot(2)/cmp_miso_sync_fifo/rd_i
add wave -noupdate /main/DUT/cmp_mt_profip_translator/gen_sync_fifo/gen_sync_miso_fifo_per_cpu_per_slot(2)/cmp_miso_sync_fifo/empty_o
add wave -noupdate /main/DUT/cmp_mt_profip_translator/gen_sync_fifo/gen_sync_miso_fifo_per_cpu_per_slot(2)/cmp_miso_sync_fifo/full_o
add wave -noupdate -divider FIFO_3
add wave -noupdate /main/DUT/cmp_mt_profip_translator/gen_sync_fifo/gen_sync_miso_fifo_per_cpu_per_slot(3)/cmp_miso_sync_fifo/rst_n_i
add wave -noupdate /main/DUT/cmp_mt_profip_translator/gen_sync_fifo/gen_sync_miso_fifo_per_cpu_per_slot(3)/cmp_miso_sync_fifo/clk_i
add wave -noupdate /main/DUT/cmp_mt_profip_translator/gen_sync_fifo/gen_sync_miso_fifo_per_cpu_per_slot(3)/cmp_miso_sync_fifo/d_i
add wave -noupdate /main/DUT/cmp_mt_profip_translator/gen_sync_fifo/gen_sync_miso_fifo_per_cpu_per_slot(3)/cmp_miso_sync_fifo/we_i
add wave -noupdate /main/DUT/cmp_mt_profip_translator/gen_sync_fifo/gen_sync_miso_fifo_per_cpu_per_slot(3)/cmp_miso_sync_fifo/q_o
add wave -noupdate /main/DUT/cmp_mt_profip_translator/gen_sync_fifo/gen_sync_miso_fifo_per_cpu_per_slot(3)/cmp_miso_sync_fifo/rd_i
add wave -noupdate /main/DUT/cmp_mt_profip_translator/gen_sync_fifo/gen_sync_miso_fifo_per_cpu_per_slot(3)/cmp_miso_sync_fifo/empty_o
add wave -noupdate /main/DUT/cmp_mt_profip_translator/gen_sync_fifo/gen_sync_miso_fifo_per_cpu_per_slot(3)/cmp_miso_sync_fifo/full_o
add wave -noupdate -divider FIFO_4
add wave -noupdate /main/DUT/cmp_mt_profip_translator/gen_sync_fifo/gen_sync_miso_fifo_per_cpu_per_slot(4)/cmp_miso_sync_fifo/rst_n_i
add wave -noupdate /main/DUT/cmp_mt_profip_translator/gen_sync_fifo/gen_sync_miso_fifo_per_cpu_per_slot(4)/cmp_miso_sync_fifo/clk_i
add wave -noupdate /main/DUT/cmp_mt_profip_translator/gen_sync_fifo/gen_sync_miso_fifo_per_cpu_per_slot(4)/cmp_miso_sync_fifo/d_i
add wave -noupdate /main/DUT/cmp_mt_profip_translator/gen_sync_fifo/gen_sync_miso_fifo_per_cpu_per_slot(4)/cmp_miso_sync_fifo/we_i
add wave -noupdate /main/DUT/cmp_mt_profip_translator/gen_sync_fifo/gen_sync_miso_fifo_per_cpu_per_slot(4)/cmp_miso_sync_fifo/q_o
add wave -noupdate /main/DUT/cmp_mt_profip_translator/gen_sync_fifo/gen_sync_miso_fifo_per_cpu_per_slot(4)/cmp_miso_sync_fifo/rd_i
add wave -noupdate /main/DUT/cmp_mt_profip_translator/gen_sync_fifo/gen_sync_miso_fifo_per_cpu_per_slot(4)/cmp_miso_sync_fifo/empty_o
add wave -noupdate /main/DUT/cmp_mt_profip_translator/gen_sync_fifo/gen_sync_miso_fifo_per_cpu_per_slot(4)/cmp_miso_sync_fifo/full_o
add wave -noupdate -divider FIFO_5
add wave -noupdate /main/DUT/cmp_mt_profip_translator/gen_sync_fifo/gen_sync_miso_fifo_per_cpu_per_slot(5)/cmp_miso_sync_fifo/rst_n_i
add wave -noupdate /main/DUT/cmp_mt_profip_translator/gen_sync_fifo/gen_sync_miso_fifo_per_cpu_per_slot(5)/cmp_miso_sync_fifo/clk_i
add wave -noupdate /main/DUT/cmp_mt_profip_translator/gen_sync_fifo/gen_sync_miso_fifo_per_cpu_per_slot(5)/cmp_miso_sync_fifo/d_i
add wave -noupdate /main/DUT/cmp_mt_profip_translator/gen_sync_fifo/gen_sync_miso_fifo_per_cpu_per_slot(5)/cmp_miso_sync_fifo/we_i
add wave -noupdate /main/DUT/cmp_mt_profip_translator/gen_sync_fifo/gen_sync_miso_fifo_per_cpu_per_slot(5)/cmp_miso_sync_fifo/q_o
add wave -noupdate /main/DUT/cmp_mt_profip_translator/gen_sync_fifo/gen_sync_miso_fifo_per_cpu_per_slot(5)/cmp_miso_sync_fifo/rd_i
add wave -noupdate /main/DUT/cmp_mt_profip_translator/gen_sync_fifo/gen_sync_miso_fifo_per_cpu_per_slot(5)/cmp_miso_sync_fifo/empty_o
add wave -noupdate /main/DUT/cmp_mt_profip_translator/gen_sync_fifo/gen_sync_miso_fifo_per_cpu_per_slot(5)/cmp_miso_sync_fifo/full_o
add wave -noupdate -divider FIFO_6
add wave -noupdate /main/DUT/cmp_mt_profip_translator/gen_sync_fifo/gen_sync_miso_fifo_per_cpu_per_slot(6)/cmp_miso_sync_fifo/rst_n_i
add wave -noupdate /main/DUT/cmp_mt_profip_translator/gen_sync_fifo/gen_sync_miso_fifo_per_cpu_per_slot(6)/cmp_miso_sync_fifo/clk_i
add wave -noupdate /main/DUT/cmp_mt_profip_translator/gen_sync_fifo/gen_sync_miso_fifo_per_cpu_per_slot(6)/cmp_miso_sync_fifo/d_i
add wave -noupdate /main/DUT/cmp_mt_profip_translator/gen_sync_fifo/gen_sync_miso_fifo_per_cpu_per_slot(6)/cmp_miso_sync_fifo/we_i
add wave -noupdate /main/DUT/cmp_mt_profip_translator/gen_sync_fifo/gen_sync_miso_fifo_per_cpu_per_slot(6)/cmp_miso_sync_fifo/q_o
add wave -noupdate /main/DUT/cmp_mt_profip_translator/gen_sync_fifo/gen_sync_miso_fifo_per_cpu_per_slot(6)/cmp_miso_sync_fifo/rd_i
add wave -noupdate /main/DUT/cmp_mt_profip_translator/gen_sync_fifo/gen_sync_miso_fifo_per_cpu_per_slot(6)/cmp_miso_sync_fifo/empty_o
add wave -noupdate /main/DUT/cmp_mt_profip_translator/gen_sync_fifo/gen_sync_miso_fifo_per_cpu_per_slot(6)/cmp_miso_sync_fifo/full_o

#
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {6499714200 ps} 0}
quietly wave cursor active 1
configure wave -namecolwidth 568
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 0
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ps
update
WaveRestoreZoom {0 ps} {12070281300 ps}
