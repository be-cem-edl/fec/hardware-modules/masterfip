/*
 * This program source code file is part of MasterFip project.
 *
 * Copyright (C) 2013-2017 CERN
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, you may find one here:
 * http://www.gnu.org/licenses/old-licenses/gpl-2.0.html
 * or you may search the http://www.gnu.org website for the version 2 license,
 * or you may write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 */

/* 
   fip_frame.svh - implementation of the FipFrame class, representing
   a single WorldFIP protocol frame
*/


`ifndef __FIP_FRAME_INCLUDED
`define __FIP_FRAME_INCLUDED

`include "serializable.svh"
`include "logger.svh"
`include "fip_frame.svh"

`define MAX_FRAME_SIZE 128

`define VIOL_PLUS 2
`define VIOL_MINUS 3

`define CRC16_RESIDUE 16'he394


// Types of FIP frames
typedef enum 
  {
   FT_ID_DAT = 'h3, // question: transaction of variables
   FT_ID_MSG = 'h5, // question: transaction of messages
   FT_RP_DAT = 'h2, // response: variable
   FT_RP_DAT_MSG = 'h6,
   FT_RP_MSG_NOACK = 'h4,
   FT_RP_FIN = 'h40, // end of msg transaction
   FT_RP_ACK_EVEN = 'h30,
   FT_RP_MSG_ACK_EVEN = 'h14,

   FT_RP_ACK_ODD = 'hb0,
   FT_RP_MSG_ACK_ODD = 'h94,

   FT_RP_DAT_RQ = 'h50,
   FT_UNDEFINED
   } FipFrameControl;


// updates a FIP-compatible 16-bit CRC checksum for byte B.
function automatic void crc16_update(ref int crc, input byte b, input int n_bits = 8);
   int i, j;
   reg [15:0] poly = 16'b0001110111001111;
   reg [15:0] crc_in, crc_out;
   bit 	      current_bit;
   
   crc_in = crc;

   for(j=0;j<n_bits;j++)
     begin
	current_bit = (b & (1<< (7-j) )) ? 1 : 0;
	crc_out[0] = current_bit ^ crc_in[15];
	
	for(i=1;i<16;i++)
	  crc_out[i] = crc_in[i-1] ^ ( poly[i] & ( current_bit ^ crc_in[15] ) );
	crc_in = crc_out;
     end
   
   crc = crc_out;
endfunction // crc16_update

// reverses bit count in a 16-bit integer, helper function for CRC packing
function automatic int revbits16(input int x);
   
   int rv = 0;
   int i;
   return x;

   for(i=0;i<16;i++)
     if(x & (1<<i))
       rv |= (1<<(15-i));
   return rv;
   
endfunction // revbits16

// returns a dynamic array containg a byte range from startR to endR (inclusive)
function automatic uint8_array_t makeRange (int startR, int endR);
   int 	     i;
   uint8_t rv[$];

   for(i=startR;i<=endR;i++)
     rv.push_back(i);

   return rv;
endfunction // makeRange


// class FipFrame
//
// Represents a single frame of a FIP protocol, including a variety of incorrect/damaged frames.
class FipFrame extends Serializable;

   // control value
   protected FipFrameControl m_control;
   // frame payload (address/control/PDU/CRC fields excluded)
   protected uint8_t      m_payload[$];
   // CRC value
   protected    bit [15:0] m_crc;
   // variable the frame is referring to (in case of ID_DAT)
   protected    bit [7:0]  m_variable;
   // address of the agent
   protected    bit [7:0]  m_address;
   
   protected    bit 	   m_crc_ok;
   protected    time 	   m_start_time, m_end_time;

   // raw frame payload, as received by the PHY
   protected ByteBuffer m_rawData;
   
   protected uint8_t m_mpsStatus;
   protected uint8_t m_pduType;
   protected uint32_t m_msgSourceAddr;
   protected uint32_t m_msgDestAddr;
   
   protected bit 	   m_forceControlEnabled;
   protected uint8_t 	   m_forceControl;
   protected    bit 	   m_forceCRCError;
   protected bit 	   m_forceFESError;
   protected bit 	   m_forceTruncatedFES;
   protected bit 	   m_forcePDUEnabled;
   protected uint8_t 	   m_forcePDU;
   protected bit 	   m_forceTxTimeOffset;
   protected bit 	   m_forcePrematureEnd;
   protected longint 	   m_txTimeOffset;
   protected int 	   m_forcePreambleError;
   protected int 	   m_preambleBitCount;
   protected int 	   m_FESBitCount;
   protected bit 	   m_forceTxDataBitCount;
   protected int 	   m_txDataBitCount;
   protected bit 	   m_forceLgthFieldValue;
   protected uint8_t m_lgthFieldValue;

   
   function automatic FipFrame copy();
      FipFrame f = new;

      f.m_forceControl = m_forceControl;
      f.m_forceControlEnabled = m_forceControlEnabled;
      f.m_forcePDU = m_forcePDU;
      f.m_forcePDUEnabled = m_forcePDUEnabled;
      
      f.m_control = m_control;
      f.m_payload=m_payload;
      f.m_crc=m_crc;
      f.m_variable=m_variable;
      f.m_address=m_address;
      f.m_forceCRCError=m_forceCRCError;
      f.m_forcePreambleError=m_forcePreambleError;
      f.m_crc_ok=m_crc_ok;
      f.m_start_time=m_start_time;
      f.m_end_time=m_end_time;
      f.m_rawData=m_rawData;
      f.m_mpsStatus=m_mpsStatus;
      f.m_pduType=m_pduType;
      

      return f;
     
   endfunction // copy

   // creates a typical FIP presence frame
   static function FipFrame makePresence( );
      return makeRaw( '{ 'h50, 'h05, 'h80, 'h03, 'h00, 'hf0, 'h00 } );
   endfunction

   // creates a frame based on a raw payload
   static function FipFrame makeRaw( uint8_t rawData[$] );
      automatic      FipFrame f = new;
      f.deserializeBytes( rawData );
      return f;
   endfunction // makeRaw

   // creates a typical RP_DAT/RP_DAT_MSG frame with a given payload and MPS status field
   static function FipFrame makeRP_DAT( FipFrameControl control, uint8_t payload[$], uint8_t mpsStatus = 'h5 );
      automatic      FipFrame f = new;
      automatic int i;

      f.m_control = control;
      f.m_mpsStatus = mpsStatus;
      f.m_payload = payload;
      f.m_pduType = 'h40;
      
      return f;
   endfunction // makeRaw

   static function FipFrame makeRP_MSG( FipFrameControl frameType, int sourceAddr, int destAddr, uint8_t payload[$] );
      automatic      FipFrame f = new;
      automatic int i;
      
      f.m_control = frameType;
      f.m_msgSourceAddr = sourceAddr;
      f.m_msgDestAddr= destAddr;
      
      for(i=0;i<payload.size();i++)
	f.m_payload.push_back(payload[i]);
      
      return f;
   endfunction // makeRaw

   static function FipFrame makeRP_ACK( FipFrameControl frameType);
      automatic      FipFrame f = new;
      automatic int i;
  
      f.m_control = frameType;
      f.m_payload = '{};
          
      return f;
   endfunction // makeRaw


   static function FipFrame makeRP_FIN();
      automatic      FipFrame f = new;
      f.m_control = FT_RP_FIN;
      f.m_payload = '{};
      return f;
   endfunction // makeRaw

   
   task automatic setStartTime( time t );
      m_start_time = t;
   endtask // setStartTime

   task automatic setMPSStatus( uint8_t st );
      m_mpsStatus = st;
   endtask // setMPSStatus
   
   function automatic string control2String(FipFrameControl t);
      case (t)
	FT_ID_DAT: 
	  return "ID_DAT";
	FT_ID_MSG:
	  return "ID_MSG";
	FT_RP_DAT:
	  return "RP_DAT";
	FT_RP_DAT_MSG:	
	  return "RP_DAT_MSG";
	FT_RP_MSG_NOACK:
	  return "RP_MSG_NOACK";
	FT_RP_FIN:
	  return "RP_FIN";
	FT_RP_ACK_EVEN:	
	  return "RP_ACK(e)";
	FT_RP_ACK_ODD:
	  return "RP_ACK(o)";
//	FT_RP_MSG_ACK_EVEN:
//	  return "RP_MSG_ACK(e)";
	FT_RP_MSG_ACK_ODD:
	  return "RP_MSG_ACK(o)";
	default:
	  return "<unknown>";
      endcase // case (t)
   endfunction // control2String
      
   function automatic string formatPayload();
      int    i;
      string s = "";

      for(i = 0; i < m_payload.size(); i++)
	begin
	   if ( (i % 16) == 0 )
             s = {s , $sformatf("%04x: ", i) };

	   s = {s, $sformatf("%02x ", m_payload[i]) };
	   
	   if ( (i % 16) == 15 )
	     s = {s, "\n"};
	end
      
      return s;
   endfunction // formatPayload

   function automatic uint8_array_t getPayload();
      return m_payload;
      
   endfunction // getPayload
   
   
   function automatic int getPayloadSize();
      return m_payload.size();
   endfunction // getPayloadSize

  // function automatic int getHeaderPayloadSize();
//      return m_payloadLength;
  // endfunction // getHeaderPayloadSize
   
   function automatic byte getPDUType();
      return m_pduType;
   endfunction // getPDUType

   function automatic byte getMPSStatus();
      return m_mpsStatus;
   endfunction // getMPSStatus


   
   
//   task automatic dump( Logger l, int logLevel );
  //    int i;

/*      string str = "RAW: ";

       if (m_data != null) begin
	   for (i=0;i<m_data.size();i++)
	     begin
		str = {str, $sformatf("'h%02x, ", m_data.at(i) ) };
	     end
	end else  if (m_is_raw)
	begin
	   for (i=0;i<m_payload.size();i++)
	     begin
		str = {str, $sformatf("'h%02x, ", m_payload[i] ) };
	     end
	end 
      
      
      l.msg(logLevel, str);*/
//   endtask // dump

   function time getStartTime();
      return m_start_time;
   endfunction // getStartTime

   function time getEndTime();
      return m_end_time;
   endfunction // getStartTime

   function time timeGapTo( FipFrame other );
      return m_start_time - other.m_end_time;
   endfunction // getStartTime
   

   function new ( FipFrameControl t = FT_UNDEFINED, uint8_t payload[$] = '{} );
//      m_is_raw = 0;
      m_control = t;
      m_payload = payload;
      m_crc_ok = 1;
      m_forceControlEnabled = 0;
      m_forcePDUEnabled = 0;
      m_forcePreambleError = 0;
      m_preambleBitCount = 8;
      m_FESBitCount = 8;
      m_forceTxTimeOffset = 0;
      m_forcePrematureEnd = 0;
      m_forceTxDataBitCount = 0;
      m_forceLgthFieldValue = 0;
      m_forceCRCError = 0;
   endfunction // new


   function automatic void forceLgthFieldValue( int enabled, uint8_t value );
      m_forceLgthFieldValue =enabled;
      m_lgthFieldValue = value;
   endfunction // forceLgthFieldValue
   
   
   function automatic void forcePrematureEnd( int enabled );
      m_forcePrematureEnd = enabled;
   endfunction // forcePrematureEnd

   function automatic bit getForceTxTimeOffset();
      return m_forceTxTimeOffset;
   endfunction // getForcePrematureStart
   
   function automatic time getTxTimeOffset();
      return m_txTimeOffset;
   endfunction // getPrematureStartAdvance
   
   function automatic bit getForcePrematureEnd();
      return m_forcePrematureEnd;
   endfunction // getForcePrematureStart

   function automatic void forceDataTxBitCount( int enabled, int count );
      m_forceTxDataBitCount = enabled;
      m_txDataBitCount = count;
   endfunction
   
   
   function automatic int getDataTxBitCount();
      return m_txDataBitCount;
   endfunction // getPrematureStartAdvance

   
   function automatic int getForceDataTxBitCount();
      return m_forceTxDataBitCount;
   endfunction // getPrematureStartAdvance

   
   function automatic void forceTxTimeOffset( int enabled, longint offset );
      m_forceTxTimeOffset = enabled;
      m_txTimeOffset = offset;
   endfunction // forcePrematureEnd

   function automatic void forceTruncatedFES( int enabled, int bits );
      m_forceTruncatedFES = enabled;
      m_FESBitCount = bits;
   endfunction // forcePrematureEnd
   
   function automatic int getForcePreambleError();
      return m_forcePreambleError;
   endfunction // getForcePreambleError 

   function automatic int getForceTruncatedFES();
      return m_forceTruncatedFES;
   endfunction // getForcePreambleError 

   function automatic int getForceFESError();
      return m_forceFESError;
   endfunction // getForceFESError
   
   function automatic int getPreambleBitCount();
      return m_preambleBitCount;
   endfunction // getForcePreambleError

   function automatic int getFESBitCount();
      return m_FESBitCount;
   endfunction // getForcePreambleError
   
   function automatic void forcePreambleError( int f );
     m_forcePreambleError = f; 
   endfunction // setForcePreambleError

   function automatic void forceFESError( int f );
     m_forceFESError = f; 
   endfunction // setForcePreambleError

   function automatic void setPreambleBitCount( int cnt );
      m_preambleBitCount = cnt ;
   endfunction // setPreambleBitCount

   function automatic void setFESBitCount( int cnt );
      m_FESBitCount = cnt ;
   endfunction // setFESBitCount

   function FipFrameControl getControl();
      return m_control;
   endfunction // getType

   function uint8_t getAddress();
      return m_address;
   endfunction // getType

   function uint8_t getVariable();
      return m_variable;
   endfunction // getVariable
   
      
   // forces the preamble to have a given length (in bits)
   task forcePreambleLength( int length );
   endtask // forcePreambleLength

   // forces the frame ending sequence to have a given length (in bits)
   task forceFESLength( int length );
   endtask // forceFESLength
   

   // forces the frame ending sequence to have a non-standard value
   task forceFESValue( int value );
   endtask // forceFESValue
   

   // forces the frame to be sent with an incorrect CRC value
   task forceIncorrectCRC( bit do_force );
      m_forceCRCError = do_force;
   endtask // forceIncorrectCRC


   function automatic bit getForceIncorrectCRC();
      return m_forceCRCError;
   endfunction // getForceIncorrectCRC
   
   
   // forces the frame to be prematurely terminated (truncated)
   // during sending. offset specifies the payload byte after which
   // the truncation will occur.
   task forcePrematureTermination( bit do_force, int offset );
   endtask // forcePrematureTermination
   
   function bit isCRCCorrect();
      return m_crc_ok;
   endfunction // isCRCCorrect

   function automatic int equals( FipFrame other );

      if (m_control != other.m_control )
	return 0;

      case (m_control)
	FT_ID_DAT:
	  return (m_variable == other.m_variable) && (m_address == other.m_address);
	FT_RP_DAT, FT_RP_DAT_MSG:
	begin
	   
	  return (m_pduType == other.m_pduType)
			    && (m_payload == other.m_payload)
			    && (m_mpsStatus == other.m_mpsStatus);
	end

	FT_RP_MSG_NOACK, FT_RP_MSG_ACK_ODD, FT_RP_MSG_ACK_EVEN:
   	  return (m_msgSourceAddr == other.m_msgSourceAddr)
  	    && (m_msgDestAddr == other.m_msgDestAddr) 
	      && (m_payload == other.m_payload);
	
      endcase // case (m_control)
      
      
   endfunction
       
      

   function automatic string str();
      case ( m_control )

	FT_ID_DAT:
	  return $sformatf("ID_DAT[var 0x%x agent 0x%x]", m_variable, m_address);
	FT_ID_MSG:
	  return $sformatf("ID_MSG[var 0x%x agent 0x%x]", m_variable, m_address);
	FT_RP_FIN:
	  return $sformatf("RP_FIN[]");
	FT_RP_ACK_ODD:
	  return $sformatf("RP_ACK(o)[]");
	FT_RP_ACK_EVEN:
	  return $sformatf("RP_ACK(e)[]");
	FT_RP_DAT:
	  return $sformatf("RP_DAT[PDU 0x%x length 0x%x MPS_status 0x%x]: %s", m_pduType, m_payload.size(), m_mpsStatus, array2str("%02x ", m_payload));
	FT_RP_DAT_MSG:
	  return $sformatf("RP_DAT_MSG[PDU 0x%x length 0x%x MPS_status 0x%x]: %s", m_pduType, m_payload.size(), m_mpsStatus, array2str("%02x ", m_payload));
	FT_RP_DAT_RQ:
	  return $sformatf("RP_DAT_RQ[length 0x%x]: %s",  m_payload.size(),  array2str("%02x ", m_payload));

	FT_RP_MSG_NOACK:
	  return $sformatf("RP_MSG_NOACK[src 0x%x dst 0x%x length 0x%x]: %s", m_msgSourceAddr, m_msgDestAddr, m_payload.size(), array2str("%02x ", m_payload));
	FT_RP_MSG_ACK_ODD:
	  return $sformatf("RP_MSG_ACK(o)[src 0x%x dst 0x%x length 0x%x]: %s", m_msgSourceAddr, m_msgDestAddr, m_payload.size(), array2str("%02x ", m_payload));


	
	default:
	  return $sformatf("UNKNOWN[ctrl 0x%x]: %s", m_control, array2str("%02x ", m_payload));

	
       endcase // case ( m_control )
      
   endfunction // str
   
   
   function automatic void deserialize ( ByteBuffer data );

      automatic int fcs = 'hffff;
            
      for ( int i = 0; i < data.size(); i++)
	begin
	   crc16_update(fcs, data.at(i) );
	end
      

      
      
      m_rawData = data.copy();
      m_control = FipFrameControl' ( data.getByte() );
      m_crc_ok = (fcs == `CRC16_RESIDUE) ? 1 : 0;
      
      
      case ( m_control )
	FT_ID_DAT:
	  begin
	     m_variable = data.getByte();
	     m_address = data.getByte();
	  end
	
       FT_RP_DAT, FT_RP_DAT_MSG:
	 begin
	    automatic int payloadLen;
	    
	    m_pduType = data.getByte();
	    payloadLen = data.getByte() - 1;
	    m_payload = data.getBytes( payloadLen );
	    m_mpsStatus = data.getByte();

	 end

	FT_ID_MSG:
	  begin
	     m_variable = data.getByte();
	     m_address = data.getByte();
	  end

	FT_RP_MSG_NOACK, FT_RP_MSG_ACK_ODD:
	  begin
	     m_msgSourceAddr = 0;
	     m_msgSourceAddr |= uint32_t'(data.getByte()) << 16;
	     m_msgSourceAddr |= uint32_t'(data.getByte()) << 8;
	     m_msgSourceAddr |= uint32_t'(data.getByte());
	     m_msgDestAddr = 0;
	     m_msgDestAddr |= uint32_t'(data.getByte()) << 16;
	     m_msgDestAddr |= uint32_t'(data.getByte()) << 8;
	     m_msgDestAddr |= uint32_t'(data.getByte());
	     m_payload = data.getBytes( data.size() - 7 );
	  end

	FT_RP_FIN, FT_RP_ACK_ODD, FT_RP_ACK_EVEN:
	  begin
	  end

	FT_RP_DAT_RQ:
	  begin
	     m_payload = data.getBytes ( data.size() - 1 );
	   end
	

	default:
	  begin
	   //  $error("Unknown CTL: %x", m_control);
	     m_payload = data.getBytes(data.size() - 1 );
//	     m_is_raw = 1;
	  end
	
      endcase // case ( m_type )

      m_crc[15:8] = data.getByte();
      m_crc[7:0] = data.getByte();

   endfunction
   

   function automatic int size();
      return m_payload.size();
      
   endfunction


   task automatic forceControlField( bit enabled, uint8_t cv );
      m_forceControlEnabled = enabled;
      m_forceControl = cv;
   endtask // setForceControlField
   task automatic forcePDUField( bit enabled, uint8_t cv );
      m_forcePDUEnabled = enabled;
      m_forcePDU = cv;
   endtask // setForceControlField
   
   
   function automatic void serialize ( ByteBuffer data );

      automatic uint8_t control = m_forceControlEnabled ? m_forceControl : m_control;
      automatic uint8_t pdu = m_forcePDUEnabled ? m_forcePDU : m_pduType;
      
      data.addByte ( control );

      case ( m_control )
	FT_RP_DAT, FT_RP_DAT_MSG:
	  begin
	     data.addByte ( pdu );

	     if ( m_forceLgthFieldValue )
	       data.addByte(m_lgthFieldValue);
	     else
	       data.addByte ( m_payload.size() + 1 );

	     data.addBytes ( m_payload );
	     data.addByte ( m_mpsStatus );
	  end

	FT_RP_MSG_NOACK, FT_RP_MSG_ACK_ODD:
	  begin
	     data.addByte ( ((m_msgSourceAddr) >> 16) & 'hff );
	     data.addByte ( ((m_msgSourceAddr) >> 8) & 'hff );
	     data.addByte ( ((m_msgSourceAddr) >> 0) & 'hff );
	     data.addByte ( ((m_msgDestAddr) >> 16) & 'hff );
	     data.addByte ( ((m_msgDestAddr) >> 8) & 'hff );
	     data.addByte ( ((m_msgDestAddr) >> 0) & 'hff );
	     data.addBytes ( m_payload );
	  end

	FT_RP_FIN:
	  begin
	   end

	FT_RP_DAT_RQ:
	  begin
	     data.addBytes ( m_payload );
	   end
  
	
	default:
	  begin
	  end
	
      endcase // case ( m_control )
   endfunction
endclass // FipFrame

typedef FipFrame FipFrameList[$];


`endif //  `ifndef __FIP_FRAME_INCLUDED

