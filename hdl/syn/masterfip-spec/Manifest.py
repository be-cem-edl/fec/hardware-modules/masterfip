action  = "synthesis"
target  = "xilinx"
board   = "spec"
fetchto = "../../../dependencies"

syn_device  = "xc6slx45t"
syn_grade   = "-3"
syn_package = "fgg484"
syn_top     = "spec_masterfip_mt_urv"
syn_project = "spec_masterfip_mt_urv.xise"
top_module  = "spec_masterfip_mt_urv"
syn_tool    = "ise"

files = ["buildinfo_pkg.vhd"] 

modules = {
    "local" : [
        "../../top/masterfip-spec",
    ],
}

# Do not fail during hdlmake fetch
try:
  exec(open(fetchto + "/general-cores/tools/gen_buildinfo.py").read())
except:
  pass

syn_post_project_cmd = "$(TCL_INTERPRETER) syn_extra_steps.tcl $(PROJECT_FILE)"

spec_base_ucf = []

ctrls = ["bank3_64b_32b"]
