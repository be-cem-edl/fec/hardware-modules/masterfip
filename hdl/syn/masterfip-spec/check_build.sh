#!/bin/bash

# A simple script which check in all of the 
# generated reports from Xilinx ISE if there
# is some error message

OUTPUT_BASE=spec_masterfip_mt_urv
FILE_SYR="$OUTPUT_BASE".syr
FILE_TWR="$OUTPUT_BASE".twr
FILE_MAP="$OUTPUT_BASE"_map.mrp
FILE_PAR="$OUTPUT_BASE".par


ret_error=0

grep "^Number of errors" "$FILE_SYR"
grep "^Timing errors:" "$FILE_TWR"
grep "^Number of errors:" "$FILE_MAP"
grep "^Number of error messages:" "$FILE_PAR"

GREP_OUT=`grep "Number of errors " "$FILE_SYR"`
# make sure there is one and only one line of output
echo $GREP_OUT | grep " 0 (" | grep " 0 filtered" | wc -l | grep ^1$ > /dev/null
if [ $? -ne 0 ]; then
    echo -e "Error in "$FILE_SYR"!! line:\n"$GREP_OUT
    ret_error=1
fi


GREP_OUT=`grep "Timing errors: 0" "$FILE_TWR"`
# make sure there is one and only one line of output
echo $GREP_OUT |  wc -l | grep ^1$ > /dev/null
if [ $? -ne 0 ]; then
    echo -e "Error in "$FILE_TWR"!! line:\n"$GREP_OUT
    ret_error=1
fi


GREP_OUT=`grep "^Number of errors" "$FILE_MAP"`
# make sure there is one and only one line of output
echo $GREP_OUT | grep ' 0$' | wc -l | grep ^1$ > /dev/null
if [ $? -ne 0 ]; then
    echo -e "Error in "$FILE_MAP"!! line:\n"$GREP_OUT
    ret_error=1
fi


GREP_OUT=`grep "^Number of error messages:" "$FILE_PAR"`
# make sure there is one and only one line of output
echo $GREP_OUT | grep ' 0$' | wc -l | grep ^1$ > /dev/null
if [ $? -ne 0 ]; then
    echo -e "Error in "$FILE_PAR"!! line:\n"$GREP_OUT
    ret_error=1
fi


if [ $ret_error -eq 1 ]; then
    exit 1
else
    exit 0
fi
