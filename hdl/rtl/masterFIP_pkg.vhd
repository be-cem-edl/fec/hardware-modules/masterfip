--_________________________________________________________________________________________________
--                                                                                                |
--                                       |masterFIP core|                                         |
--                                                                                                |
--                                         CERN,BE/CO-HT                                          |
--________________________________________________________________________________________________|

---------------------------------------------------------------------------------------------------
--                                                                                                |
--                                         masterFIP_pkg                                          |
--                                                                                                |
---------------------------------------------------------------------------------------------------
-- File         masterFIP_pkg.vhd                                                                 |
--                                                                                                |
-- Description  Definitions of constants, types, entities related to the interface between the    |
--              fmc_masterfip_core and the Mock Turtle.                                           |
--              Note that a different package, the wf_package, is used for the WorldFIP           |
--              specific constant, types, entities and for the clock constants.                   |
--                                                                                                |
-- Author       Evangelia Gousiou     (Evangelia.Gousiou@cern.ch)                                 |
--                                                                                                |
---------------------------------------------------------------------------------------------------

---------------------------------------------------------------------------------------------------
--                                      SOLDERPAD LICENSE                                         |
--                                   Copyright CERN 2014-2018                                     |
--                              ------------------------------------                              |
-- Copyright and related rights are licensed under the Solderpad Hardware License, Version 2.0    |
-- (the "License"); you may not use this file except in compliance with the License.              |
-- You may obtain a copy of the License at http://solderpad.org/licenses/SHL-2.0.                 |
-- Unless required by applicable law or agreed to in writing, software, hardware and materials    |
-- distributed under this License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR       |
-- CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language   |
-- governing permissions and limitations under the License.                                       |
---------------------------------------------------------------------------------------------------



--=================================================================================================
--                                      Libraries & Packages
--=================================================================================================

library IEEE;
use IEEE.STD_LOGIC_1164.all;       -- std_logic definitions
use IEEE.NUMERIC_STD.all;          -- conversion functions
use work.wf_package.all;
use work.masterfip_wbgen2_pkg.all; -- for the masterfip_wbgen2_csr records



--=================================================================================================
--                              Package declaration for masterFIP_pkg
--=================================================================================================
package masterFIP_pkg is


---------------------------------------------------------------------------------------------------
--                                       Interface with MT                                       --
--               Array of words with the WorldFIP produced/consumed PAYLOAD bytes                --
---------------------------------------------------------------------------------------------------

  constant C_BYTE_WIDTH : integer := 8;  -- 8-bit bytes
  constant C_WORD_WIDTH : integer := 32; -- 32-bit words

-- Declaration of a structure with 67 words of 32-bit each = 268 bytes which represent the max
-- length of a frame, including
--   FSS  (2 bytes),
--   CTRL (1 byte),
--   Data (up to 262 for a message),
--   CRC  (2 bytes) and
--   FES  (1 byte).

-- Note that the deserializer, is registering bytes one by one as they arrive, after the FSS and
-- until the FES detection; therefore the max amount of bytes expected to be counted by the
-- deserializer is 266. Upon the rx_fss_crc_fes_ok_p_o the processor needs to read the rx_ctrl_byte
-- (separate register, not included in the rx_frame structure) and rx_byte_index_o-4 bytes from the
-- rx_frame structure (minus the CTRL, 2x CRC and FES bytes).

-- Note that the serializer, is counting one by one the bytes that are serialized, after the FSS and
-- before the CRC; therefore the max amount of bytes expected to be counted by the serializer is
-- 263.

  constant C_MAX_FRAME_WORDS      : integer := 67;
  constant C_MFP_MAX_FRAME_BYTES  : integer := 266;
  constant C_FRAME_WORDS_CNT_LGTH : integer := 7; -- counter overflows after 128 words = 512 bytes
                                                  -- for normal rx operation it should not exceed
                                                  -- 67 words; for normal tx operation it should
                                                  -- not exceed 66 words

  constant C_FRAME_BYTES_CNT_LGTH : integer := 9; -- counter overflows after 128 words = 512 bytes
                                                  -- for normal rx operation it should not exceed
                                                  -- 266 bytes; for normal tx operation it should
                                                  -- not exceed 263 bytes

  subtype data_word is std_logic_vector(C_WORD_WIDTH-1 downto 0);

  type rx_frame_t   is array (C_MAX_FRAME_WORDS-1 downto 0) of data_word;

-- Note that the serializer is not provided with the CRC and FES; the processor needs to provide
-- the CTRL byte (in a separate register, not included in the rx_frame structure) and up to
-- 262 Data bytes. In principle 66 data_words would be sufficient, but for symmetry with rx we kept 67.
  type tx_frame_t   is array (C_MAX_FRAME_WORDS-1 downto 0) of data_word;


---------------------------------------------------------------------------------------------------
--                            Constant regarding the deglitch filters                            --
---------------------------------------------------------------------------------------------------
--  constant C_DEGLITCH_THRESHOLD : natural := 10; -- declared in the wf_package


---------------------------------------------------------------------------------------------------
--                                      Components Declarations:                                 --
---------------------------------------------------------------------------------------------------

 ---------------------------------------------------------------------------------------------------
  component fmc_masterFIP_core is
  generic
    (g_span             : integer := 32;
     g_width            : integer := 32;
     g_simul            : boolean := FALSE);
  port
    (clk_i              : in    std_logic;
     rst_n_i            : in    std_logic;
     speed_b0_i         : in    std_logic;
     speed_b1_i         : in    std_logic;
     onewire_b          : inout std_logic;
     fd_rxcdn_a_i       : in    std_logic;
     fd_rxd_a_i         : in    std_logic;
     fd_txer_a_i        : in    std_logic;
     fd_wdgn_a_i        : in    std_logic;
     fd_rstn_o          : out   std_logic;
     fd_txck_o          : out   std_logic;
     fd_txd_o           : out   std_logic;
     fd_txena_o         : out   std_logic;
     ext_sync_term_en_o : out   std_logic;
     ext_sync_dir_o     : out   std_logic;
     ext_sync_oe_n_o    : out   std_logic;
     ext_sync_a_i       : in    std_logic;
     leds_o             : out   std_logic_vector(g_width-1 downto 0);
     wb_adr_i           : in    std_logic_vector(g_span-1 downto 0);
     wb_dat_i           : in    std_logic_vector(g_width-1 downto 0);
     wb_stb_i           : in    std_logic;
     wb_sel_i           : in    std_logic_vector(3 downto 0);
     wb_we_i            : in    std_logic;
     wb_cyc_i           : in    std_logic;
     wb_ack_o           : out   std_logic;
     wb_stall_o         : out   std_logic;
     wb_dat_o           : out   std_logic_vector(g_width-1 downto 0));
  end component;


---------------------------------------------------------------------------------------------------
  component masterfip_wbgen2_csr is
  port
    (rst_n_i               : in     std_logic;
     clk_sys_i             : in     std_logic;
     wb_adr_i              : in     std_logic_vector(7 downto 0);
     wb_dat_i              : in     std_logic_vector(31 downto 0);
     wb_dat_o              : out    std_logic_vector(31 downto 0);
     wb_cyc_i              : in     std_logic;
     wb_sel_i              : in     std_logic_vector(3 downto 0);
     wb_stb_i              : in     std_logic;
     wb_we_i               : in     std_logic;
     wb_ack_o              : out    std_logic;
     wb_stall_o            : out    std_logic;
     regs_i                : in     t_masterfip_in_registers;
     regs_o                : out    t_masterfip_out_registers);
  end component;


----------------------------------------------------------------------------------------------------
  component masterfip_rx is
  port
    (clk_i                 : in std_logic;
     speed_i               : in std_logic_vector(1 downto 0);
     rx_d_a_i              : in std_logic;
     rst_i                 : in std_logic;
     rx_rst_i              : in std_logic;
     rx_byte_index_o       : out std_logic_vector(C_FRAME_BYTES_CNT_LGTH-1 downto 0);
     rx_word_index_o       : out std_logic_vector(C_FRAME_WORDS_CNT_LGTH-1 downto 0);
     rx_ctrl_byte_o        : out std_logic_vector(C_BYTE_WIDTH-1 downto 0);
     rx_ctrl_byte_ok_o     : out std_logic; 
     rx_frame_o            : out rx_frame_t;
     rx_byte_o             : out std_logic_vector(C_BYTE_WIDTH-1 downto 0);
     rx_byte_ready_p_o     : out std_logic;
     rx_fss_crc_fes_ok_p_o : out std_logic;
     rx_fss_received_p_o   : out std_logic;
     rx_crc_wrong_p_o      : out std_logic;
     rx_bytes_num_err_o    : out std_logic);
  end component masterfip_rx;


---------------------------------------------------------------------------------------------------
  component masterfip_tx is
  port
    (clk_i                 : in std_logic;
     speed_i               : in std_logic_vector(1 downto 0);
     rst_i                 : in std_logic;
     tx_start_p_i          : in std_logic;
     tx_bytes_num_i        : in std_logic_vector(C_FRAME_BYTES_CNT_LGTH-1 downto 0);
     tx_frame_i            : in tx_frame_t;
     tx_ctrl_byte_i        : in std_logic_vector(C_BYTE_WIDTH-1 downto 0);
     tx_byte_index_o       : out std_logic_vector(C_FRAME_BYTES_CNT_LGTH-1 downto 0);
     tx_end_p_o            : out std_logic;
     tx_d_o                : out std_logic;
     tx_ena_o              : out std_logic;
     tx_clk_o              : out std_logic);
  end component masterfip_tx;


---------------------------------------------------------------------------------------------------
  component decr_counter
  generic
    (width                 : integer := 32);
  port
    (clk_i                 : in std_logic;
     rst_i                 : in std_logic;
     counter_load_i        : in std_logic;
     counter_top_i         : in std_logic_vector(width-1 downto 0);
     counter_is_zero_o     : out std_logic;
     counter_o             : out std_logic_vector(width-1 downto 0));
  end component;


---------------------------------------------------------------------------------------------------
  component incr_counter is
  generic
    (g_counter_lgth        : natural := 4);
  port
    (clk_i                 : in std_logic;
     counter_incr_i        : in std_logic;
     counter_reinit_i      : in std_logic;
     counter_o             : out std_logic_vector(g_counter_lgth-1 downto 0);
     counter_is_full_o     : out std_logic);
  end component incr_counter;


--=================================================================================================
--                                         package end
--=================================================================================================
end masterFIP_pkg;
---------------------------------------------------------------------------------------------------
--                                      E N D   O F   F I L E
---------------------------------------------------------------------------------------------------
