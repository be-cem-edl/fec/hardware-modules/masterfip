-------------------------------------------------------------------------------
--! SPDX-FileCopyrightText: 2022 CERN (home.cern)
--
--! SPDX-License-Identifier: CERN-OHL-W-2.0+
-------------------------------------------------------------------------------
--! Title      : mt_profip_translator
--! Project    : ProFIP
-------------------------------------------------------------------------------
--! File       : mt_profip_translator.vhd
--! Author     : Konstantinos Blantos <konstantinos.blantos@cern.ch>
--! Company    : CERN (BE-CEM-EDL)
--! Created    : 22-02-2023
--! Last update:
--! Platform   : FPGA-generic
--! Standard   : VHDL'08
-------------------------------------------------------------------------------
-- Description: Responsible of received the MOSI data from ERTEC through SPI
--              and transmitting MISO data from Mockturtle. For communication
--              with MockTurtle, Remote Message Queues are used.
-------------------------------------------------------------------------------


--=============================================================================
--                            Libraries & Packages                           --
--=============================================================================

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use work.mock_turtle_pkg.all;
use work.gencores_pkg.all;
use work.mt_mqueue_pkg.all;

--=============================================================================
--               Entity declaration for mt_profip_translator                --
--=============================================================================
entity mt_profip_translator is
  generic (
    --! Data size
    g_data_width        : natural;
    --! SPI polarity and phase
    g_cpol              : natural range 0 to 1;
    g_cpha              : natural range 0 to 1;
    --! Crossing Clock Domain enable for 1
    g_cdc_enable        : natural range 0 to 1;
    --! RX FIFO
    g_input_fifo_depth  : natural := 4;
    --! TX FIFO
    g_output_fifo_depth : natural := 4);
  port (
    --! Clock, reset and user interface
    clk_i               : in  std_logic;                    --! MT clock
    rst_n_i             : in  std_logic;                    --! MT reset
    spi_sample_clk_i    : in  std_logic;                    --! Clock from SVEC's PLL, used for CDC
    ertec_rst_n_i         : in  std_logic;                    --! reset pin coming from ERTEC
    rmq_status_o        : out std_logic_vector(6 downto 0);
    rmq_id_o            : out std_logic_vector(7 downto 0);

    --! RX (MSTRFIP TRANSLATOR -> RMQ)
    rmq_src_i           : in  t_mt_stream_source_in;        --! (ready, pkt_ready)
    rmq_src_o           : out t_mt_stream_source_out;       --! (data, hdr, valid, last, error)
    rmq_src_config_i    : in  t_mt_stream_config_out;       --! (adr, dat, we)
    rmq_src_config_o    : out t_mt_stream_config_in;        --! (dat)

    --! TX (RMQ -> MSTRFIP_TRANSLATOR)
    rmq_snk_i           : in  t_mt_stream_sink_in_array2d;  -- (data, hdr, valid, last, error)
    rmq_snk_o           : out t_mt_stream_sink_out_array2d; -- (ready, pkt_ready)
    rmq_snk_config_i    : in  t_mt_stream_config_out;       -- (adr, dat, we)
    rmq_snk_config_o    : out t_mt_stream_config_in;        -- (dat)

    --! SPI interface
    spi_clk_i           : in  std_logic; -- SPI clock
    spi_cs_n_i          : in  std_logic; -- SPI chip select, active LOW
    spi_mosi_i          : in  std_logic; -- SPI serial data, master out slave in
    spi_miso_o          : out std_logic); -- SPI serial data, master in slave out
end entity mt_profip_translator;

--==============================================================================
--!                           Architecture declaration                        --
--==============================================================================
architecture rtl of mt_profip_translator is

    --! FSM encoding attribute, used for synthesis
    attribute fsm_encoding : string;

    --! FSM types and signals
    --! WR RMQ FSM
    type t_wr_state is (IDLE, BASIC_INFO, HEADER_1, HEADER_2, WAIT_HEADER,
                        DATA_1, DATA_2, WAIT_DATA, END_OF_FRAME);
    signal s_wr_state                    : t_wr_state;
    attribute fsm_encoding of s_wr_state : signal is "one-hot";

    --! RD RMQ FSM
    type t_rd_state is (IDLE, WAIT_HEADER, RD_HEADER, PRE_LOAD, LOAD_HWORD, LOAD_LWORD, DATA, ERROR_STATE, RD_INTERRUPT, END_OF_FRAME);
    signal s_rd_state                    : t_rd_state;
    attribute fsm_encoding of s_rd_state : signal is "one-hot";

    --! SPI FSM
    type t_spi_state is (SPI_BASIC_INFO, PROCESSING, WR_RMQ, RD_RMQ);
    signal s_spi_state                    : t_spi_state;
    attribute fsm_encoding of s_spi_state : signal is "one-hot";

    --! FIFO array
    type t_miso_fifo_data_array is array (0 to 6) of std_logic_vector(g_data_width+3 downto 0);
    type t_miso_fifo_flag_array is array (0 to 6) of std_logic;

    --! Internal signals
    signal s_rst_n              : std_logic;
    signal s_spi_mosi_data      : std_logic_vector(g_data_width - 1 downto 0);
    signal s_spi_miso_data      : std_logic_vector(g_data_width - 1 downto 0);
    signal s_mosi_data          : std_logic_vector(g_data_width - 1 downto 0);
    signal s_wr_rmq_id          : std_logic_vector(7 downto 0); --! stores the RMQ ID byte from MOSI
    signal s_wr_data_len        : std_logic_vector(7 downto 0); --! stores the LENGTH  byte from MOSI
    signal s_wr_data_cnt        : unsigned(7 downto 0); --! counts the number of data bytes from MOSI
    signal s_wr_header_cnt      : unsigned(1 downto 0); --! counts the number of header bytes from MOSI
    signal s_wr_header          : std_logic_vector(15 downto 0); --! header data in WR RMQ
    signal s_wr_data            : std_logic_vector(15 downto 0); --! payload data in WR RMQ
    signal s_ready              : std_logic;
    signal s_operation          : std_logic;
    signal s_valid              : std_logic;
    signal s_fail               : std_logic;
    signal s_rd_en              : std_logic; --t_miso_fifo_flag_array;
    signal s_miso_data_reg      : std_logic_vector(31 downto 0);
    signal s_miso_last          : std_logic;
    signal s_data_valid         : std_logic;
    signal s_rmq_id             : natural range 0 to 6;
    signal s_data_reg_o         : std_logic_vector(g_data_width-1 downto 0);
    signal s_data_reg_i         : std_logic_vector(g_data_width-1 downto 0);
    signal s_mosi_cnt           : unsigned(f_log2_ceil(g_data_width)-1 downto 0);
    signal s_miso_cnt           : unsigned(f_log2_ceil(g_data_width)-1 downto 0);
    signal s_wr_data_valid      : std_logic;
    signal s_spi_clk_redge      : std_logic;
    signal s_spi_clk_fedge      : std_logic;
    signal s_spi_cs_n           : std_logic;
    signal s_sample_en          : std_logic;
    signal s_shift_en           : std_logic;
    signal s_mosi               : std_logic;

    --! MOSI async FIFO signals
    signal s_mosi_fifo_data     : std_logic_vector(g_data_width - 1 downto 0);
    signal s_mosi_wr_en         : std_logic;
    signal s_mosi_rd_en         : std_logic;
    signal s_mosi_fifo_wr_full  : std_logic;
    signal s_mosi_fifo_rd_empty : std_logic;

    --! MISO async FIFO signals
    signal s_miso_fifo_data_in  : t_miso_fifo_data_array;
    signal s_miso_fifo_data_out : t_miso_fifo_data_array;
    signal s_miso_wr_en         : t_miso_fifo_flag_array;
    signal s_miso_rd_en         : t_miso_fifo_flag_array;
    signal s_miso_fifo_wr_full  : t_miso_fifo_flag_array;
    signal s_miso_fifo_rd_empty : t_miso_fifo_flag_array;

begin

    --! Combinational reset
    s_rst_n <= rst_n_i and ertec_rst_n_i;

    ----------------------------------------------------------------------------
    --! Sunchronizers for the incoming SPI signals
    ----------------------------------------------------------------------------

    --! Use of gc_sync_ffs module in order to synchronize and
    --! detect the negative and positive edges of clock
    cmp_spi_clk_sync_ffs : entity work.gc_sync_ffs
    generic map (
        g_SYNC_EDGE => "positive"
    )
    port map (
        clk_i    => spi_sample_clk_i, --! destination clock domain (MT)
        rst_n_i  => s_rst_n,          --! async reset
        data_i   => spi_clk_i,        --! async spi clock
        synced_o => open,             --! synchronized output
        npulse_o => s_spi_clk_fedge,  --! sync spi clock negative edge detect output
        ppulse_o => s_spi_clk_redge   --! sync spi clock positive edge detect output
    );

    --! Define the sampling and shifting based on the CPHA
    gen_pha_zero : if g_cpha = 0 generate
        s_sample_en <= s_spi_clk_redge;
        s_shift_en  <= s_spi_clk_fedge;
    end generate;

    gen_pha_one : if g_cpha = 1 generate
        s_sample_en <= s_spi_clk_fedge;
        s_shift_en  <= s_spi_clk_redge;
    end generate;

    --! Synchronize spi chip select in MT clock domain
    cmp_spi_cs_n_sync_ffs : entity work.gc_sync_ffs
    generic map (
        g_SYNC_EDGE => "positive"
    )
    port map (
        clk_i    => spi_sample_clk_i, --! destination clock domain (MT)
        rst_n_i  => s_rst_n,          --! async reset
        data_i   => spi_cs_n_i,       --! async spi chip select
        synced_o => s_spi_cs_n,       --! synchronized chip select output
        npulse_o => open,             --! negative edge detect output
        ppulse_o => open              --! positive edge detect output
    );

    --! Synchronize spi mosi signal in MT clock domain
    cmp_spi_mosi_sync_ffs : entity work.gc_sync_ffs
    generic map (
        g_SYNC_EDGE => "positive"
    )
    port map (
        clk_i    => spi_sample_clk_i, --! destination clock domain (MT)
        rst_n_i  => s_rst_n,          --! async reset
        data_i   => spi_mosi_i,       --! async spi chip select
        synced_o => s_mosi,           --! synchronized chip select output
        npulse_o => open,             --! negative edge detect output
        ppulse_o => open              --! positive edge detect output
    );

    --! SPI FSM: responsible for sampling and shifting the SPI MOSI and MISO
    --! In addition, it generates control signals which are used later for
    --! the RMQ communication protocol between ERTEC and MockTurtle
    p_spi_fsm : process(spi_sample_clk_i, s_rst_n)
    begin
        if s_rst_n = '0' then
            s_spi_state     <= SPI_BASIC_INFO;
            s_data_reg_o    <= (others=>'0');
            s_data_reg_i    <= (others=>'0');
            s_mosi_cnt      <= (others=>'0');
            s_miso_cnt      <= (others=>'0');
            s_operation     <= '0';
            s_ready         <= '0';
            s_wr_data_valid <= '0';
            s_wr_rmq_id     <= (others=>'0');
        elsif rising_edge(spi_sample_clk_i) then

            case s_spi_state is

                ----------------------------------------------------------------
                --! STATE SPI_BASIC_INFO
                when SPI_BASIC_INFO =>
                    s_ready         <= '0';
                    s_wr_data_valid <= '0';
                    if s_spi_cs_n = '0' then
                        if s_sample_en = '1' then
                            if s_mosi_cnt = 7 then
                              s_operation <= s_mosi;
                            end if;
                            s_data_reg_o <= s_data_reg_o(g_data_width-2 downto 0) & s_mosi;
                            if (s_mosi_cnt = g_data_width-1) then
                                s_mosi_cnt  <= (others=>'0');
                                s_spi_state <= PROCESSING;
                                s_wr_data_valid <= '1';
                            else
                                s_mosi_cnt  <= s_mosi_cnt + 1;
                                s_spi_state <= SPI_BASIC_INFO;
                            end if;
                        end if;
                    else
                        s_spi_state <= SPI_BASIC_INFO;
                        s_data_reg_o<= (others=>'0');
                        s_data_reg_i<= (others=>'0');
                        s_mosi_cnt  <= (others=>'0');
                        s_miso_cnt  <= (others=>'0');
                        s_operation <= '0';
                    end if;

                ----------------------------------------------------------------
                --! STATE PROCESSING
                when PROCESSING =>
                    s_wr_data_valid <= '0';
                    if s_spi_cs_n = '0' then
                        s_wr_rmq_id   <= s_data_reg_o(23 downto 16);
                        if s_operation = '0' then
                            s_spi_state <= WR_RMQ;
                        elsif s_operation = '1' then
                            s_spi_state <= RD_RMQ;
                            s_data_reg_i <= s_miso_data_reg;
                        else
                            s_spi_state <= PROCESSING;
                        end if;
                    else
                        s_spi_state <= SPI_BASIC_INFO;
                    end if;

                ----------------------------------------------------------------
                --! STATE WR_RMQ
                when WR_RMQ =>
                    if s_spi_cs_n = '0' then
                        s_wr_data_valid <= '0';
                        if s_sample_en = '1' then
                            s_data_reg_o <= s_data_reg_o(g_data_width-2 downto 0) & s_mosi;
                            if s_mosi_cnt = g_data_width-1 then
                                s_mosi_cnt      <= (others=>'0');
                                s_wr_data_valid <= '1';
                            else
                                s_mosi_cnt  <= s_mosi_cnt + 1;
                                s_spi_state <= WR_RMQ;
                            end if;
                        end if;
                    else
                        s_spi_state <= SPI_BASIC_INFO;
                        s_data_reg_o<= (others=>'0');
                        s_data_reg_i<= (others=>'0');
                        s_mosi_cnt  <= (others=>'0');
                        s_miso_cnt  <= (others=>'0');
                        s_operation <= '0';
                    end if;

                ----------------------------------------------------------------
                --! STATE RD_RMQ
                when RD_RMQ =>
                    if s_spi_cs_n = '0' then
                        if s_shift_en = '1' then
                            if g_cpha = 1 and s_miso_cnt = 0 then
                                s_data_reg_i <= s_miso_data_reg;
                                s_ready      <= '1';
                                s_miso_cnt   <= s_miso_cnt + 1;
                                s_spi_state  <= RD_RMQ;
                            else
                                s_data_reg_i <= s_data_reg_i(g_data_width-2 downto 0) & '0';
                                if s_miso_cnt = g_data_width-1 then
                                    s_miso_cnt <= (others=>'0');
                                else
                                    s_miso_cnt <= s_miso_cnt + 1;
                                end if;
                            end if;
                        else
                            s_ready <= '0';
                        end if;
                    else
                        s_spi_state <= SPI_BASIC_INFO;
                        s_operation <= '0';
                    end if;

                ----------------------------------------------------------------
                --! STATE OTHERS
                when others =>
                    s_spi_state <= SPI_BASIC_INFO;

            end case;
        end if;
    end process p_spi_fsm;

    --! SPI MISO output
    spi_miso_o <= s_data_reg_i(g_data_width-1);

    --! MOSI data that will enter in the MOSI FIFO
    s_spi_mosi_data <= s_data_reg_o;

    ----------------------------------------------------------------------------
    --! Sync / async FIFOs are used whether or not cross clock domain is enabled
    ----------------------------------------------------------------------------

    --! Synchronous FIFOs - Cross clock domain is not activated
    gen_sync_fifo : if g_cdc_enable = 0 generate
    begin

        --! MOSI FIFO
        cmp_mosi_sync_fifo : entity work.inferred_sync_fifo
        generic map (
            g_data_width             => g_data_width,
            g_size                   => g_input_fifo_depth,
            g_show_ahead             => true,
            g_show_ahead_legacy_mode => true,
            g_with_empty             => true,
            g_with_full              => true,
            g_with_almost_empty      => false,
            g_with_almost_full       => false,
            g_with_count             => false,
            g_almost_empty_threshold => 0,
            g_almost_full_threshold  => 0,
            g_register_flag_outputs  => false)
        port map (
            rst_n_i                  => s_rst_n,
            clk_i                    => clk_i,
            d_i                      => s_spi_mosi_data,
            we_i                     => s_mosi_wr_en,
            q_o                      => s_mosi_fifo_data,
            rd_i                     => s_mosi_rd_en,
            empty_o                  => s_mosi_fifo_rd_empty,
            full_o                   => s_mosi_fifo_wr_full,
            almost_empty_o           => open,
            almost_full_o            => open,
            count_o                  => open
        );

        --! MISO FIFO: Multiple Sync FIFOs are used because there are multiple inputs
        gen_sync_miso_fifo_per_cpu_per_slot : for i in 0 to 6 generate
        begin

            --! MISO synchronous FIFO
            cmp_miso_sync_fifo : entity work.inferred_sync_fifo
            generic map (
                g_data_width             => g_data_width + 4,
                g_size                   => g_output_fifo_depth,
                g_show_ahead             => true,
                g_show_ahead_legacy_mode => true,
                g_with_empty             => true,
                g_with_full              => true,
                g_with_almost_empty      => false,
                g_with_almost_full       => false,
                g_with_count             => false,
                g_almost_empty_threshold => 0,
                g_almost_full_threshold  => 0,
                g_register_flag_outputs  => true)
            port map (
                rst_n_i                  => s_rst_n,
                clk_i                    => clk_i,
                d_i                      => s_miso_fifo_data_in(i),
                we_i                     => s_miso_wr_en(i),
                q_o                      => s_miso_fifo_data_out(i),
                rd_i                     => s_miso_rd_en(i),
                empty_o                  => s_miso_fifo_rd_empty(i),
                full_o                   => s_miso_fifo_wr_full(i),
                almost_empty_o           => open,
                almost_full_o            => open,
                count_o                  => open
            );

        end generate gen_sync_miso_fifo_per_cpu_per_slot;

    end generate gen_sync_fifo;

    --! Asynchronous FIFOs - Cross clock domain is activated
    gen_async_fifo : if g_cdc_enable = 1 generate
    begin

        --! Asynchronous FIFO used for fixing the CDC between
        --! SPI Slave and Translator, because there are both
        --! working under different clock domains.

        --! MOSI FIFO
        cmp_mosi_async_fifo : entity work.inferred_async_fifo
        generic map(
            g_data_width             => g_data_width,
            g_size                   => g_input_fifo_depth,
            g_show_ahead             => TRUE,
            g_with_rd_empty          => TRUE,
            g_with_rd_full           => FALSE,
            g_with_rd_almost_empty   => FALSE,
            g_with_rd_almost_full    => FALSE,
            g_with_rd_count          => FALSE,
            g_with_wr_empty          => FALSE,
            g_with_wr_full           => TRUE,
            g_with_wr_almost_empty   => FALSE,
            g_with_wr_almost_full    => FALSE,
            g_with_wr_count          => FALSE,
            g_almost_empty_threshold => 0,
            g_almost_full_threshold  => 0
        )
        port map(
            rst_n_i                  => s_rst_n,
            clk_wr_i                 => spi_sample_clk_i,
            d_i                      => s_spi_mosi_data,
            we_i                     => s_mosi_wr_en,
            wr_empty_o               => open,
            wr_full_o                => s_mosi_fifo_wr_full,
            wr_almost_empty_o        => open,
            wr_almost_full_o         => open,
            wr_count_o               => open,
            clk_rd_i                 => clk_i,
            q_o                      => s_mosi_fifo_data,
            rd_i                     => s_mosi_rd_en,
            rd_empty_o               => s_mosi_fifo_rd_empty,
            rd_full_o                => open,
            rd_almost_empty_o        => open,
            rd_almost_full_o         => open,
            rd_count_o               => open
        );

        --! Multiple Async FIFOs are used because there are multiple RMQs
        gen_async_miso_fifo_per_cpu_per_slot : for i in 0 to 6 generate
        begin
            cmp_miso_async_fifo : entity work.inferred_async_fifo
            generic map(
                g_data_width             => g_data_width + 4,
                g_size                   => g_output_fifo_depth,
                g_show_ahead             => TRUE,
                g_with_rd_empty          => TRUE,
                g_with_rd_full           => FALSE,
                g_with_rd_almost_empty   => FALSE,
                g_with_rd_almost_full    => FALSE,
                g_with_rd_count          => FALSE,
                g_with_wr_empty          => FALSE,
                g_with_wr_full           => TRUE,
                g_with_wr_almost_empty   => FALSE,
                g_with_wr_almost_full    => FALSE,
                g_with_wr_count          => FALSE,
                g_almost_empty_threshold => 0,
                g_almost_full_threshold  => 0
            )
            port map(
                rst_n_i                  => s_rst_n,
                clk_wr_i                 => clk_i,
                d_i                      => s_miso_fifo_data_in(i),
                we_i                     => s_miso_wr_en(i),
                wr_empty_o               => open,
                wr_full_o                => s_miso_fifo_wr_full(i),
                wr_almost_empty_o        => open,
                wr_almost_full_o         => open,
                wr_count_o               => open,
                clk_rd_i                 => spi_sample_clk_i,
                q_o                      => s_miso_fifo_data_out(i),
                rd_i                     => s_miso_rd_en(i),
                rd_empty_o               => s_miso_fifo_rd_empty(i),
                rd_full_o                => open,
                rd_almost_empty_o        => open,
                rd_almost_full_o         => open,
                rd_count_o               => open
            );
        end generate gen_async_miso_fifo_per_cpu_per_slot;

    end generate gen_async_fifo;

    --! Indicates the status of the FIFOs that are used
    --! for Reading the messages from the multiple RMQs
    rmq_status_o(0) <= s_miso_fifo_rd_empty(0);
    rmq_status_o(1) <= s_miso_fifo_rd_empty(1);
    rmq_status_o(2) <= s_miso_fifo_rd_empty(2);
    rmq_status_o(3) <= s_miso_fifo_rd_empty(3);
    rmq_status_o(4) <= s_miso_fifo_rd_empty(4);
    rmq_status_o(5) <= s_miso_fifo_rd_empty(5);
    rmq_status_o(6) <= s_miso_fifo_rd_empty(6);

    --! t_mt_stream_config_in and out will not be used
    rmq_src_config_o <= c_MT_DUMMY_EP_CONFIG_IN;
    rmq_snk_config_o <= c_MT_DUMMY_EP_CONFIG_IN;

    --! Final and synchronized MOSI data that will go
    --! to RMQ is the data taken from MOSI FIFO when
    --! the read enable of MOSI FIFO is HIGH
    p_mosi_data : process (clk_i, s_rst_n)
    begin
        if s_rst_n = '0' then
            s_mosi_data <= (others => '0');
        elsif rising_edge(clk_i) then
            if s_mosi_rd_en = '1' then
                s_mosi_data <= s_mosi_fifo_data;
            else
                s_mosi_data <= s_mosi_data;
            end if;
        end if;
    end process;

    --! The format of the data is the following:
    --! 35   - hdr
    --! 34   - valid
    --! 33   - last
    --! 32   - error
    --! 31:0 - data
    --! Assignment of the input data for MISO FIFOs
    s_miso_fifo_data_in(0) <= (rmq_snk_i(0)(0).hdr & rmq_snk_i(0)(0).valid & rmq_snk_i(0)(0).last & rmq_snk_i(0)(0).error & rmq_snk_i(0)(0).data);
    s_miso_fifo_data_in(1) <= (rmq_snk_i(0)(1).hdr & rmq_snk_i(0)(1).valid & rmq_snk_i(0)(1).last & rmq_snk_i(0)(1).error & rmq_snk_i(0)(1).data);
    s_miso_fifo_data_in(2) <= (rmq_snk_i(0)(2).hdr & rmq_snk_i(0)(2).valid & rmq_snk_i(0)(2).last & rmq_snk_i(0)(2).error & rmq_snk_i(0)(2).data);
    s_miso_fifo_data_in(3) <= (rmq_snk_i(0)(3).hdr & rmq_snk_i(0)(3).valid & rmq_snk_i(0)(3).last & rmq_snk_i(0)(3).error & rmq_snk_i(0)(3).data);
    s_miso_fifo_data_in(4) <= (rmq_snk_i(0)(4).hdr & rmq_snk_i(0)(4).valid & rmq_snk_i(0)(4).last & rmq_snk_i(0)(4).error & rmq_snk_i(0)(4).data);
    s_miso_fifo_data_in(5) <= (rmq_snk_i(0)(5).hdr & rmq_snk_i(0)(5).valid & rmq_snk_i(0)(5).last & rmq_snk_i(0)(5).error & rmq_snk_i(0)(5).data);
    s_miso_fifo_data_in(6) <= (rmq_snk_i(1)(0).hdr & rmq_snk_i(1)(0).valid & rmq_snk_i(1)(0).last & rmq_snk_i(1)(0).error & rmq_snk_i(1)(0).data);

    ----------------------------------------------------------------------------
    --! RX:  MASTERFIP TRANSLATOR --> MT (RMQ)
    ----------------------------------------------------------------------------
    --! FSM for writing from ERTEC to RMQ. Exiting the IDLE state when source
    --! input is ready and there is valid data coming from MOSI FIFO. In the BASIC
    --! INFO state the received data consist of 3 bytes (1 control byte, 1 RMQ_ID
    --! byte and 1 byte containg the Number of Bytes that the data contains). The
    --! In the HEADER state the header bytes assigned to src_o.data (3x32-bits).
    --! 4th byte is unused. Depending on the data len, received in the BASIC INFO,
    --! there is a counter which counts the number of packets of 32-bit data that
    --! received and is to be sent to RMQ. Once it is stops transmitting, last
    --! is asserted. END_OF_FRAME state, is the state that FSM will go for 1 clock cycle,
    --! and error will be asserted. Then, either it is an error or not, FSM will IDLE.
    --! NOTE: the incoming mosi data has 32-bit width, same as rmq data bus, but
    --! rmq discard half of them everytime, so that's why valid data should be send
    --! as a packet of 16-bits every time.
    p_wr_rmq_fsm : process (clk_i, s_rst_n)
    begin
        if s_rst_n = '0' then
            s_wr_state      <= IDLE;
            s_wr_data_len   <= (others => '0');
            s_wr_header_cnt <= (others => '1');
            s_wr_data_cnt   <= (others => '0');
            s_fail          <= '0';
        elsif rising_edge(clk_i) then

            case s_wr_state is

                ----------------------------------------------------------------
                --! STATE IDLE
                when IDLE =>
                if (rmq_src_i.ready = '1' and s_mosi_rd_en = '1') then
                    s_wr_state      <= BASIC_INFO;
                    s_wr_data_len   <= s_mosi_fifo_data(15 downto 8);
                    s_wr_header_cnt <= (others => '1');
                else
                    s_wr_state      <= IDLE;
                    s_wr_data_len   <= (others => '0');
                    s_wr_header_cnt <= (others => '1');
                    s_wr_data_cnt   <= (others => '0');
                    s_fail          <= '0';
                end if;

                ----------------------------------------------------------------
                --! STATE BASIC_INFO
                when BASIC_INFO =>
                    if (rmq_src_i.ready = '1' and s_mosi_rd_en = '1' and s_operation = '0') then
                        s_wr_data_cnt <= unsigned(s_wr_data_len);
                        s_wr_state  <= HEADER_1;
                    elsif (s_spi_cs_n = '1' or s_operation = '1') then
                        s_wr_state <= END_OF_FRAME;
                        s_fail       <= '1';
                    else
                        s_wr_state <= BASIC_INFO;
                        s_fail       <= '0';
                    end if;

                ----------------------------------------------------------------
                --! STATE HEADER_1
                when HEADER_1 =>
                    if (rmq_src_i.ready = '1') then
                        if s_wr_header_cnt /= 0 then
                            s_wr_state <= HEADER_2;
                            s_wr_header_cnt <= s_wr_header_cnt - 1;
                        else
                            s_wr_state <= IDLE;
                        end if;
                    elsif (s_spi_cs_n = '1') then
                        s_fail <= '1';
                        s_wr_state <= END_OF_FRAME;
                    else
                        s_wr_state <= HEADER_1;
                        s_fail <= '0';
                    end if;

                ----------------------------------------------------------------
                --! STATE HEADER_2
                when HEADER_2 =>
                    if (rmq_src_i.ready = '1') then
                        s_wr_state <= WAIT_HEADER;
                    elsif (s_spi_cs_n = '1') then
                        s_fail <= '1';
                        s_wr_state <= END_OF_FRAME;
                    else
                        s_fail <= '0';
                        s_wr_state <= HEADER_2;
                    end if;

                ----------------------------------------------------------------
                --! STATE WAIT_HEADER
                when WAIT_HEADER =>
                    if (rmq_src_i.ready = '1' and s_mosi_rd_en = '1') then
                        if s_wr_header_cnt = 0 then
                            s_wr_state <= DATA_1;
                        else
                            s_wr_state <= HEADER_1;
                        end if;
                    elsif (s_spi_cs_n = '1') then
                        s_fail <= '1';
                        s_wr_state <= END_OF_FRAME;
                    else
                        s_fail <= '0';
                        s_wr_state <= WAIT_HEADER;
                    end if;

                ----------------------------------------------------------------
                --! STATE DATA_1
                when DATA_1 =>
                    if (rmq_src_i.ready = '1') then
                        if s_wr_data_cnt /= 0 then
                            s_wr_state <= DATA_2;
                            s_wr_data_cnt <= s_wr_data_cnt - 1;
                        else
                            s_wr_state <= IDLE;
                        end if;
                    elsif (s_spi_cs_n = '1') then
                        s_fail <= '1';
                        s_wr_state <= END_OF_FRAME;
                    else
                        s_wr_state <= DATA_1;
                        s_fail <= '0';
                    end if;

                ----------------------------------------------------------------
                --! STATE DATA_2
                when DATA_2 =>
                    if (rmq_src_i.ready = '1') then
                        s_wr_state <= WAIT_DATA;
                    elsif (s_spi_cs_n = '1') then
                        s_fail <= '1';
                        s_wr_state <= END_OF_FRAME;
                    else
                        s_fail <= '0';
                        s_wr_state <= DATA_2;
                    end if;

                ----------------------------------------------------------------
                --! STATE WAIT_DATA
                when WAIT_DATA =>
                    if (rmq_src_i.ready = '1' and s_mosi_rd_en = '1') then
                        if s_wr_data_cnt = 0 then
                            s_wr_state <= END_OF_FRAME;
                        else
                            s_wr_state <= DATA_1;
                        end if;
                    elsif (s_wr_data_cnt = 0) then
                        s_wr_state <= END_OF_FRAME;
                    else
                        s_fail <= '0';
                        s_wr_state <= WAIT_DATA;
                    end if;

                ----------------------------------------------------------------
                --! STATE END_OF_FRAME
                when END_OF_FRAME =>
                    s_fail <= '0';
                    if s_spi_cs_n = '1' then
                        s_wr_state <= IDLE;
                    else
                        s_wr_state <= END_OF_FRAME;
                    end if;

                ----------------------------------------------------------------
                --! STATE OTHERS
                when others =>
                    s_fail <= '0';
                    s_wr_state <= IDLE;

                ----------------------------------------------------------------
            end case;

        end if;
    end process p_wr_rmq_fsm;

    --! Vector that contains information about the core and slot of RMQ
    rmq_id_o <= s_wr_rmq_id;

    --! RMQ protocol, requires that we should devide the header and payload data into 2 parts
    s_wr_header     <= s_mosi_data(31 downto 16) when s_wr_state = HEADER_1 else
                       s_mosi_data(15 downto 0) when s_wr_state = HEADER_2 else (others => '0');

    s_wr_data       <= s_mosi_data(31 downto 16) when s_wr_state = DATA_1 else
                       s_mosi_data(15 downto 0) when s_wr_state = DATA_2 else (others => '0');

    rmq_src_o.data  <= (x"0000" & s_wr_header) when (s_wr_state = HEADER_1 or s_wr_state = HEADER_2) else
                       (x"0000" & s_wr_data) when (s_wr_state = DATA_1 or s_wr_state = DATA_2) else (others => '0');

    rmq_src_o.hdr   <= '1' when (s_wr_state = HEADER_1 or s_wr_state = HEADER_2) else '0';

    s_valid         <= '1' when ((s_wr_state = HEADER_1 or s_wr_state = HEADER_2) or (s_wr_state = DATA_1 or s_wr_state = DATA_2)) else '0';
    rmq_src_o.valid <= s_valid or s_fail; -- in case to catch the error, we need valid HIGH (!)
    rmq_src_o.last  <= '1' when (s_wr_data_cnt = 0 and s_valid = '1') else '0';

    --! Assert error signal when less header or payload data has been sent by ERTEC (and CS is HIGH)
    rmq_src_o.error <= s_fail;

    --! Write enable of the MOSI FIFO
    s_mosi_wr_en <= s_wr_data_valid and (not s_mosi_fifo_wr_full);

    --! Read enable of the MOSI FIFO
    s_mosi_rd_en <= rmq_src_i.ready and (not s_mosi_fifo_rd_empty);

    ----------------------------------------------------------------------------
    --! TX: MT (RMQ) --> MASTERFIP TRANSLATOR
    ----------------------------------------------------------------------------

    --! FSM for reading data from RMQ. The basic idea is that we read the data from
    --! the MISO FIFO that it is already stored. If it is a header data
    p_rd_rmq_fsm : process (spi_sample_clk_i, s_rst_n)
    begin
        if s_rst_n = '0' then
            s_rd_state      <= IDLE;
            s_miso_data_reg <= (others => '0');
            s_data_valid    <= '0';
            s_miso_last     <= '0';
            s_spi_miso_data <= (others=>'0');

        elsif rising_edge(spi_sample_clk_i) then

            case s_rd_state is

                ----------------------------------------------------------------
                --! STATE IDLE
                when IDLE =>
                    s_miso_last     <= '0';
                    s_data_valid    <= '0';
                    s_miso_data_reg <= (others => '0');
                    s_spi_miso_data <= (others => '0');
                    if s_spi_cs_n = '0' then
                        if s_operation = '1' then
                            s_rd_state <= WAIT_HEADER;
                        else
                            s_rd_state <= IDLE;
                        end if;
                    else
                        s_rd_state <= IDLE;
                    end if;

                ----------------------------------------------------------------
                --! STATE WAIT_HEADER
                when WAIT_HEADER =>
                    s_miso_data_reg <= s_miso_fifo_data_out(s_rmq_id)(31 downto 0);
                    if s_ready = '1' then 
                        if s_miso_fifo_data_out(s_rmq_id)(35 downto 32) = "1100" and s_miso_fifo_rd_empty(s_rmq_id) = '0' then
                            s_data_valid    <= '1';
                            s_rd_state      <= RD_HEADER;
                            s_spi_miso_data <= s_miso_data_reg;
                        elsif s_miso_fifo_rd_empty(s_rmq_id) = '1' then
                            s_data_valid <= '1';
                            s_rd_state   <= ERROR_STATE;
                        end if;
                    elsif s_spi_cs_n = '1' then
                        s_rd_state <= RD_INTERRUPT;
                    else
                        s_data_valid <= '0';
                        s_rd_state   <= WAIT_HEADER;
                    end if;

                ----------------------------------------------------------------
                --! STATE RD_HEADER
                when RD_HEADER =>
                    s_data_valid <= '0';
                    s_rd_state   <= PRE_LOAD;

                ----------------------------------------------------------------
                --! STATE PRE_LOAD
                when PRE_LOAD =>
                    s_data_valid <= '0';
                    if s_miso_fifo_rd_empty(s_rmq_id) = '0' then
                        s_rd_state <= LOAD_HWORD;
                    else
                        s_rd_state <= PRE_LOAD;
                    end if;

                ----------------------------------------------------------------
                --! STATE LOAD_HWORD
                when LOAD_HWORD =>
                    s_data_valid <= '0';
                    if s_miso_fifo_rd_empty(s_rmq_id) = '0' then
                        if s_miso_fifo_data_out(s_rmq_id)(34) = '1' and s_miso_fifo_data_out(s_rmq_id)(32) = '0' then -- valid & !error
                            s_miso_data_reg(31 downto 16) <= s_miso_fifo_data_out(s_rmq_id)(15 downto 0);
                            s_rd_state <= LOAD_LWORD;
                        else
                            s_rd_state <= ERROR_STATE;
                        end if;
                    else
                        s_rd_state <= LOAD_HWORD;
                    end if;

                ----------------------------------------------------------------
                --! STATE LOAD_LWORD
                when LOAD_LWORD =>
                    if s_miso_fifo_rd_empty(s_rmq_id) = '0' then
                        if s_miso_fifo_data_out(s_rmq_id)(34) = '1' and s_miso_fifo_data_out(s_rmq_id)(32) = '0' then -- valid & !error
                            s_miso_data_reg(15 downto 0) <= s_miso_fifo_data_out(s_rmq_id)(15 downto 0);
                            if s_miso_fifo_data_out(s_rmq_id)(33) = '1' then -- if last
                                s_miso_last  <= '1';
                                s_rd_state <= END_OF_FRAME;
                            end if;
                            s_rd_state <= DATA;
                        elsif s_spi_cs_n = '1' then
                            s_rd_state <= RD_INTERRUPT;
                        else
                            s_rd_state <= ERROR_STATE;
                        end if;
                    else
                        s_rd_state <= LOAD_LWORD;
                    end if;

                ----------------------------------------------------------------
                --! STATE DATA
                when DATA =>
                    if s_ready = '1' then
                        s_data_valid <= '1';
                        s_spi_miso_data <= s_miso_data_reg;
                        if s_miso_last = '1' then
                            s_rd_state <= END_OF_FRAME;
                        else
                            s_rd_state <= LOAD_HWORD;
                        end if;
                    elsif s_spi_cs_n = '1' then
                        s_rd_state <= RD_INTERRUPT;
                    else
                        s_rd_state <= DATA;
                    end if;

                ----------------------------------------------------------------
                --! STATE ERROR_STATE
                when ERROR_STATE =>
                    s_data_valid    <= '1';
                    s_miso_last     <= '0';
                    s_spi_miso_data <= x"DEADCAFE";
                    s_miso_data_reg <= x"DEADCAFE";
                    if s_spi_cs_n = '1' then
                        s_rd_state <= IDLE;
                    else
                        s_rd_state <= RD_INTERRUPT;
                    end if;

                ----------------------------------------------------------------
                --! STATE END_OF_FRAME
                when END_OF_FRAME =>
                    s_data_valid    <= '0';
                    s_miso_last     <= '0';
                    s_miso_data_reg <= (others => '0');
                    s_spi_miso_data <= (others => '0');
                    if s_spi_cs_n = '1' then
                        s_rd_state <= IDLE;
                    else
                        s_rd_state <= END_OF_FRAME;
                    end if;

                ----------------------------------------------------------------
                --! STATE RD_INTERRUPT
                when RD_INTERRUPT =>
                      s_data_valid <= '0';
                      if s_miso_fifo_rd_empty(s_rmq_id) = '1' and s_ready = '1' then
                          s_rd_state <= END_OF_FRAME;
                      elsif s_spi_cs_n = '1' then
                          s_rd_state <= IDLE;
                      else
                          s_rd_state <= RD_INTERRUPT;
                      end if;

                ----------------------------------------------------------------
                --! STATE DEFAULT
                when others =>
                    s_rd_state <= IDLE;

                ----------------------------------------------------------------
            end case;
        end if;
    end process p_rd_rmq_fsm;

    --! Process for the multiplexing the different RMQs
    p_rd_mux : process (s_miso_fifo_wr_full, s_miso_fifo_rd_empty, s_rd_en, s_miso_fifo_data_in)
    begin

        --! Ready and packet ready for the multiple RMQs
        rmq_snk_o_ready_assignment : for k in 0 to 5 loop
            rmq_snk_o(0)(k).ready     <= not s_miso_fifo_wr_full(k);
            rmq_snk_o(0)(k).pkt_ready <= not s_miso_fifo_wr_full(k);
        end loop;

        rmq_snk_o(1)(0).ready     <= not s_miso_fifo_wr_full(6);
        rmq_snk_o(1)(0).pkt_ready <= not s_miso_fifo_wr_full(6);

        --! Write enable of the MISO FIFOs
        miso_fifo_write_enable : for k in 0 to 6 loop
            s_miso_wr_en(k) <= s_miso_fifo_data_in(k)(34) and (not s_miso_fifo_wr_full(k));
        end loop;

     end process;

    --! Internal signal, used for miso fifo read enable
    s_rd_en <= '1' when (s_rd_state = RD_HEADER) else
               '1' when ((s_rd_state = LOAD_HWORD or s_rd_state = LOAD_LWORD) and s_miso_fifo_data_out(s_rmq_id)(34)='1') else
               '1' when s_rd_state = RD_INTERRUPT else
               '0';

    s_miso_rd_en(s_rmq_id) <= s_rd_en and (not s_miso_fifo_rd_empty(s_rmq_id)); 

    --! Convert RMQ_ID to an integer, to be used as an index
    s_rmq_id  <= to_integer(unsigned(s_wr_rmq_id));

end architecture rtl;
