--_________________________________________________________________________________________________
--                                                                                                |
--                                       |masterFIP core|                                         |
--                                                                                                |
--                                         CERN,BE/CO-HT                                          |
--________________________________________________________________________________________________|

---------------------------------------------------------------------------------------------------
--                                                                                                |
--                                         decr_counter                                           |
--                                                                                                |
---------------------------------------------------------------------------------------------------
-- File         decr_counter.vhd                                                                  |
--                                                                                                |
-- Description  Stop counter. Configurable "counter_top_i" and "width".                           |
--              "Current count value" and "counting done" signals available.                      |
--              "Counter done" signal asserted simultaneous to "current count value = 0".         |
--              Countdown is launched each time "counter_load_i" is asserted for one clock tick.  |
--                                                                                                |
-- Authors      Evangelia Gousiou (Evangelia.Gousiou@cern.ch)                                     |
--                                                                                                |
---------------------------------------------------------------------------------------------------

---------------------------------------------------------------------------------------------------
--                                      SOLDERPAD LICENSE                                         |
--                                   Copyright CERN 2014-2018                                     |
--                              ------------------------------------                              |
-- Copyright and related rights are licensed under the Solderpad Hardware License, Version 2.0    |
-- (the "License"); you may not use this file except in compliance with the License.              |
-- You may obtain a copy of the License at http://solderpad.org/licenses/SHL-2.0.                 |
-- Unless required by applicable law or agreed to in writing, software, hardware and materials    |
-- distributed under this License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR       |
-- CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language   |
-- governing permissions and limitations under the License.                                       |
---------------------------------------------------------------------------------------------------



--=================================================================================================
--                                       Libraries & Packages
--=================================================================================================

-- Standard library
library IEEE;
use IEEE.STD_LOGIC_1164.all; -- std_logic definitions
use IEEE.NUMERIC_STD.all;    -- conversion functions

--=================================================================================================
--                            Entity declaration for decr_counter
--=================================================================================================

entity decr_counter is

  generic
    (width             : integer := 32);                        -- default size
  port
  -- INPUTS
    (clk_i             : in std_logic;
     rst_i             : in std_logic;
     counter_load_i    : in std_logic;                          -- loads counter with counter_top_i
     counter_top_i     : in std_logic_vector(width-1 downto 0); -- counter start value

  -- OUTPUTS
     counter_o         : out std_logic_vector(width-1 downto 0);
     counter_is_zero_o : out std_logic);                        -- counter empty indication

end decr_counter;


--=================================================================================================
--                                    architecture declaration
--=================================================================================================

architecture rtl of decr_counter is

  constant zeroes : unsigned(width-1 downto 0) :=(others=>'0');
  signal one      : unsigned(width-1 downto 0);
  signal counter  : unsigned(width-1 downto 0) := (others=>'0'); -- init to avoid sim warnings

--=================================================================================================
--                                       architecture begin
--=================================================================================================
begin

  decr_counting: process (clk_i)
  begin
    if rising_edge (clk_i) then

      if rst_i = '1' then
        counter_is_zero_o <= '0';
        counter           <= zeroes;

      elsif counter_load_i = '1' then
        counter_is_zero_o <= '0';
        counter           <= unsigned(counter_top_i) - "1";

      elsif counter = zeroes then
        counter_is_zero_o <= '0';
        counter           <= zeroes;

      elsif counter = one then
        counter_is_zero_o <= '1';
        counter           <= counter - "1";

      else
        counter_is_zero_o <= '0';
        counter           <= counter - "1";
      end if;
    end if;
  end process;

  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --
  counter_o   <= std_logic_vector(counter);
  one         <= zeroes + "1";


end architecture rtl;
--=================================================================================================
--                                        architecture end
--=================================================================================================
---------------------------------------------------------------------------------------------------
--                                      E N D   O F   F I L E
---------------------------------------------------------------------------------------------------