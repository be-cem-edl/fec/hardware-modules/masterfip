# SPDX-FileCopyrightText: 2023 2023 CERN (home.cern)
#
# SPDX-License-Identifier: LGPL-2.1-or-later

"""
SPDX-License-Identifier: LGPL-2.1-or-later
SPDX-FileCopyrightText: 2020 CERN
"""

from collections import namedtuple
import os
import ctypes as ct
from enum import IntEnum


class MFipReport(ct.Structure):
    _fields_ = [
        ("tx_ok", ct.c_int),
        ("tx_err", ct.c_int),
        ("fd_tx_err", ct.c_int),
        ("fd_tx_err_hwtime", ct.c_int),
        ("fd_tx_err_cycle", ct.c_int),
        ("fd_cd", ct.c_int),
        ("fd_tx_watchdog", ct.c_int),
        ("fd_tx_watchdog_hwtime", ct.c_int),
        ("fd_tx_watchdog_cycle", ct.c_int),
        ("rx_ok", ct.c_int),
        ("rx_err", ct.c_int),
        ("rx_tmo", ct.c_int),
        ("rx_mps_status_err", ct.c_int),
        ("tx_mps_status_err", ct.c_int),
        ("cycles", ct.c_int),
        ("ext_sync_pulse_count", ct.c_int),
        ("ext_sync_pulse_missed_count", ct.c_int),
        ("int_sync_pulse_count", ct.c_int),
        ("ba_state", ct.c_int),
        ("cycle_state", ct.c_int),
        ("temp", ct.c_int),
    ]


class MFipBitRate(IntEnum):
    KB_31 = 0         # 31.25 kb/s
    KB_1000 = 1       # 1 Mb/s
    KB_2500 = 2       # 2.5 Mb/s
    KB_UNDEFINED = 3  # undefined speed


class MFipDataDir(IntEnum):
    PROD = 1  # data produced by MasterFip
    CONS = 2  # data consumed by MasterFip


class MFipData(ct.Structure):
    _fields_ = [
        ("type", ct.c_int),
        ("id", ct.c_int),
        ("buffer", ct.POINTER(ct.c_ubyte)),
        ("bsz", ct.c_int),
        ("status", ct.c_int),
        ("frame_error", ct.c_int),
        ("playload_error", ct.c_int),
        ("priv", ct.c_void_p),
    ]


# Define callback prototype
MFipDataCb = ct.CFUNCTYPE(None, ct.c_void_p, ct.POINTER(MFipData), ct.c_void_p)


class MFipDataCfg(ct.Structure):
    _fields_ = [
        ("id", ct.c_int),
        ("flags", ct.c_int),
        ("max_bsz", ct.c_int),
        ("mstrfip_data_handler", ct.POINTER(MFipDataCb)),
    ]


class MFipPerVarWindCfg(ct.Structure):
    _fields_ = [
        ("varlist", ct.POINTER(ct.POINTER(MFipData))),
        ("var_count", ct.c_int),
    ]


class MFipAperVarWindCfg(ct.Structure):
    # couples of fields are not used in the test and are not properly
    # initialized, in particular callbacks functions like ident_var_handler()
    _fields_ = [
        ("end_ustime", ct.c_int),
        ("enable_diag", ct.c_int),
        ("ident_var_list", ct.POINTER(ct.c_void_p)),  # unused
        ("ident_var_count", ct.c_int),  # unused
        ("mstrfip_ident_var_handler", ct.c_void_p),  # unused
    ]


class MFipHwCfg(ct.Structure):
    _fields_ = [
        ("enable_ext_trig", ct.c_int),
        ("enable_ext_trig_term", ct.c_int),
        ("enable_int_trig", ct.c_int),
        ("turn_around_ustime", ct.c_int),
    ]


# Define callback prototype
MFipErrorCb = ct.CFUNCTYPE(None, ct.c_void_p, ct.c_int)


class MFipSwCfg(ct.Structure):
    _fields_ = [
        ("irq_thread_prio", ct.c_int),
        ("diag_thread_prio", ct.c_int),
        ("event_ustimeout", ct.c_int),
        ("mstrfip_error_handler", ct.POINTER(MFipErrorCb))
    ]


class MFipDiagShm(ct.Structure):
    _fields_ = [
        ("deprecated", ct.c_ubyte * 80),
#        ("ext", MFipDiagShmExt),
    ]


# Couple of namedtuple to ease the definition of the data set defined in pytest
# to program a macrocycle

# MasterFip data configuration to create FIP data: per var, aper var, aper msg,..
# addr: 2 bytes hex value 'var_no << FF |agt addr' ex 0x0604 : var:0x06, agt:0x04
# sz: payloas size in bytes (hex value)
# dir: direction PROD or CONS by the master
# cb: callback handler. If not None, called when the data is scheduled on the bus
MFipDataDef = namedtuple('MFipDataDef', 'addr sz dir cb')

# Periodic Variable Window to append in the macrocycle
# data: list of MFipDataDef
MFipPerWindDef = namedtuple('MFipPerWindDef', 'data')

# Aperiodic Variable Window to append in the macrocycle
# end: time in us relative to the macrocycle start at which msg wind ends
# enable_diag: enable diagnostic
# ident_var_list: list of identification variables to be scheduled
# ident_var_count: ident_var_list size
# cb: callback handler. if any called at the end of the aper var window
MFipAperWindDef = namedtuple('MFipAperWindDef',
                            'end enable_diag ident_var_list ident_var_count cb')

# Aperiodic Message Window to append in the macrocycle
# end: time in us relative to the macrocycle start at which msg wind ends
# cons_fifo_sz: max number of pending requests send by agents
# prod_fifo_sz: max number of pending request send by application
# cons_cb: callback handler. if any called at the end of msg wind
# prod_cb: callback handler. if any called at the end of msg wind
MFipMsgWindDef = namedtuple('MFipMsgWindDef',
                            'end cons_fifo_sz prod_fifo_sz cons_cb prod_cb')

# Wait Window to append in the macrocycle
# end: time in us relative to the macrocycle start at which msg wind ends
# silent: =1 no traffic, =0: traffic  padding
MFipWaitWindDef = namedtuple('MFipWaitWindDef', 'end silent')

# MacroCycle data configuration
# winds: list of windows composing the macro cycle
MFipMCycleDef = namedtuple('MFipMCycleDef', 'winds')


class LibMFip():

    __libmfip = None  # singleton
    __lib_name = 'libmasterfip.so'

    @classmethod
    def __load_lib(cls):
        # It is expected to set LD_LIBRARY_PATH or to have the shared library in
        # standard place like /usr/lib
        try:
            cls.__libmfip = ct.CDLL(cls.__lib_name)
        except OSError as err:
            cls.__libmfip = None
            raise err

    @classmethod
    def __create_cfunc_objects(cls):
        def mfip_strerror(errno):
            """
            Return ADC errors

            :ivar err: error number
            :return: an error string
            """

            return LibMFip.mfip_strerror(errno).decode()

        def __mfip_errcheck_int(ret, func, args):
            if ret < 0:
                raise OSError(ct.get_errno(),
                              mfip_strerror(ct.get_errno()), "")
            else:
                return ret

        def __mfip_errcheck_pointer(ret, func, args):
            """Generic error handler for functions returning pointers"""
            if ret is None:
                raise OSError(ct.get_errno(),
                              mfip_strerror(ct.get_errno()), "")
            else:
                return ret

        cls.mfip_init = cls.__libmfip.mstrfip_init
        cls.mfip_exit = cls.__libmfip.mstrfip_exit

        cls.mfip_open_dev = cls.__libmfip.mstrfip_open_by_id
        cls.mfip_open_dev.argtypes = [ct.c_int]
        cls.mfip_open_dev.restype = ct.c_void_p
        cls.mfip_open_dev.errcheck = __mfip_errcheck_pointer

        cls.mfip_close_dev = cls.__libmfip.mstrfip_close
        cls.mfip_close_dev.argtypes = [ct.c_void_p]
        cls.mfip_close_dev.restype = None

        cls.mfip_rtapp_reset = cls.__libmfip.mstrfip_rtapp_reset
        cls.mfip_rtapp_reset.argtypes = [ct.c_void_p]
        cls.mfip_rtapp_reset.restype = ct.c_int
        cls.mfip_rtapp_reset.errcheck = __mfip_errcheck_int

        cls.mfip_report_get = cls.__libmfip.mstrfip_report_get
        cls.mfip_report_get.argtypes = [ct.c_void_p, ct.POINTER(MFipReport)]
        cls.mfip_report_get.restype = ct.c_int
        cls.mfip_report_get.errcheck = __mfip_errcheck_int

        cls.mfip_speed_get = cls.__libmfip.mstrfip_hw_speed_get
        cls.mfip_speed_get.argtypes = [ct.c_void_p, ct.POINTER(ct.c_int)]
        cls.mfip_speed_get.restype = ct.c_int
        cls.mfip_speed_get.errcheck = __mfip_errcheck_int

        cls.mfip_hw_cfg_set = cls.__libmfip.mstrfip_hw_cfg_set
        cls.mfip_hw_cfg_set.argtypes = [ct.c_void_p, ct.POINTER(MFipHwCfg)]
        cls.mfip_hw_cfg_set.restype = ct.c_int
        cls.mfip_hw_cfg_set.errcheck = __mfip_errcheck_int

        cls.mfip_sw_cfg_set = cls.__libmfip.mstrfip_sw_cfg_set
        cls.mfip_sw_cfg_set.argtypes = [ct.c_void_p, ct.POINTER(MFipSwCfg)]
        cls.mfip_sw_cfg_set.restype = ct.c_int
        cls.mfip_sw_cfg_set.errcheck = __mfip_errcheck_int

        cls.mfip_mcycle_create = cls.__libmfip.mstrfip_macrocycle_create
        cls.mfip_mcycle_create.argtypes = [ct.c_void_p]
        cls.mfip_mcycle_create.restype = ct.c_void_p
        cls.mfip_mcycle_create.errcheck = __mfip_errcheck_pointer

        cls.mfip_var_create = cls.__libmfip.mstrfip_var_create
        cls.mfip_var_create.argtypes = [ct.c_void_p, ct.POINTER(MFipDataCfg)]
        cls.mfip_var_create.restype = ct.POINTER(MFipData)
        cls.mfip_var_create.errcheck = __mfip_errcheck_pointer

        cls.mfip_var_write = cls.__libmfip.mstrfip_var_write
        cls.mfip_var_write.argtypes = [ct.c_void_p, ct.POINTER(MFipData)]
        cls.mfip_var_write.restype = ct.c_int
        cls.mfip_var_write.errcheck = __mfip_errcheck_int

        cls.mfip_var_read = cls.__libmfip.mstrfip_var_update
        cls.mfip_var_read.argtypes = [ct.c_void_p, ct.POINTER(MFipData)]
        cls.mfip_var_read.restype = None

        cls.mfip_per_var_wind_append = cls.__libmfip.mstrfip_per_var_wind_append
        cls.mfip_per_var_wind_append.argtypes = [ct.c_void_p,
                                                 ct.POINTER(MFipPerVarWindCfg)]
        cls.mfip_per_var_wind_append.restype = ct.c_int
        cls.mfip_per_var_wind_append.errcheck = __mfip_errcheck_int

        cls.mfip_aper_var_wind_append = cls.__libmfip.mstrfip_aper_var_wind_append
        cls.mfip_aper_var_wind_append.argtypes = [ct.c_void_p,
                                                 ct.POINTER(MFipAperVarWindCfg)]
        cls.mfip_aper_var_wind_append.restype = ct.c_int
        cls.mfip_aper_var_wind_append.errcheck = __mfip_errcheck_int

        cls.mfip_wait_wind_append = cls.__libmfip.mstrfip_wait_wind_append
        cls.mfip_wait_wind_append.argtypes = [ct.c_void_p, ct.c_int, ct.c_int]
        cls.mfip_wait_wind_append.restype = ct.c_int
        cls.mfip_wait_wind_append.errcheck = __mfip_errcheck_int

        cls.mfip_ba_load = cls.__libmfip.mstrfip_ba_load
        cls.mfip_ba_load.argtypes = [ct.c_void_p, ct.c_void_p]
        cls.mfip_ba_load.restype = ct.c_int
        cls.mfip_ba_load.errcheck = __mfip_errcheck_int

        cls.mfip_ba_start = cls.__libmfip.mstrfip_ba_start
        cls.mfip_ba_start.argtypes = [ct.c_void_p]
        cls.mfip_ba_start.restype = ct.c_int
        cls.mfip_ba_start.errcheck = __mfip_errcheck_int

        cls.mfip_ba_stop = cls.__libmfip.mstrfip_ba_stop
        cls.mfip_ba_stop.argtypes = [ct.c_void_p]
        cls.mfip_ba_stop.restype = ct.c_int
        cls.mfip_ba_stop.errcheck = __mfip_errcheck_int

        cls.mfip_ba_reset = cls.__libmfip.mstrfip_ba_reset
        cls.mfip_ba_reset.argtypes = [ct.c_void_p]
        cls.mfip_ba_reset.restype = ct.c_int
        cls.mfip_ba_reset.errcheck = __mfip_errcheck_int

        cls.mfip_diag_get = cls.__libmfip.mstrfip_diag_get
        cls.mfip_diag_get.argtypes = [ct.c_void_p]
        cls.mfip_diag_get.restype = ct.POINTER(MFipDiagShm)
        cls.mfip_diag_get.errcheck = __mfip_errcheck_pointer

        cls.mfip_strerror = cls.__libmfip.mstrfip_strerror
        cls.mfip_strerror.argtypes = [ct.c_int]
        cls.mfip_strerror.restype = ct.c_char_p
        cls.mfip_diag_get.errcheck = __mfip_errcheck_pointer

    @classmethod
    def setup(cls):
        if cls.__libmfip is not None:
            return
        cls.__load_lib()
        cls.__create_cfunc_objects()


class MFipDev():
    """
    Master Fip device instance
    """

    def __init__(self, devid):

        try:
            LibMFip.setup()
            LibMFip.mfip_init()
            self._mfip_hdl = self.__open(devid)
            # mandatory calls right after an open
            # Reset RT Application
            self._rt_app_reset()
            # Get HW speed (bus bit rate)
            speed = self.get_hw_speed()
            # set HW config: default is using internal trigger
            self.set_hw_cfg()
            # set sw config
            self.set_sw_cfg()
        except Exception as e:
            print(e)
            raise e

    def __del__(self):
        self.__close()
        LibMFip.mfip_exit()

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_value, exc_tb):
        self.__close()

    def get_hdl(self):
        return self._mfip_hdl

    def __open(self, devid):
        return LibMFip.mfip_open_dev(devid)

    def __close(self):
        """
        Close the device file handle and free attached resources.
        """
        if self._mfip_hdl is not None:
            LibMFip.mfip_close_dev(self._mfip_hdl)
            self._mfip_hdl = None

    def _error_callback(self, arg1, arg2):
        err_msg = LibMFip.mfip_strerror(arg2).decode()
        raise ValueError(err_msg)

    # General MFip error handler
    # arg1: masterFip device handle
    # arg2: error code
    def _rt_app_reset(self):
        LibMFip.mfip_rtapp_reset(self._mfip_hdl)

    def get_report(self):
        """
        Return diagnostic report.
        """
        report = MFipReport()
        LibMFip.mfip_report_get(self._mfip_hdl, ct.byref(report))
        return report

    def get_hw_speed(self):
        """
        Return MasterFip HW config.
        """
        speed = ct.c_int()
        LibMFip.mfip_speed_get(self._mfip_hdl, ct.byref(speed))
        return speed.value

    def set_hw_cfg(self):
        """
        Return MasterFip HW config.
        """
        hw_cfg = MFipHwCfg()
        hw_cfg.enable_int_trig = 1
        hw_cfg.turn_around_ustime = 13
        LibMFip.mfip_hw_cfg_set(self._mfip_hdl, ct.byref(hw_cfg))

    def set_sw_cfg(self):
        """
        Return MasterFip HW config.
        """
        sw_cfg = MFipSwCfg()
        sw_cfg.irq_thread_prio = 50
        sw_cfg.diag_thread_prio = 20
        cb_ptr = MFipErrorCb(self._error_callback)
        sw_cfg.mstrfip_error_handler = ct.cast(cb_ptr, ct.POINTER(MFipErrorCb))
        LibMFip.mfip_sw_cfg_set(self._mfip_hdl, ct.byref(sw_cfg))

    def get_presence_list(self):
        pass


class MFipMCycle():
    """
    Master Fip Macro Cycle
    """

    def _append_per_wind(self, wind):
        data = MFipDataCfg()
        wind_cfg = MFipPerVarWindCfg()
        cons_per_var_nb = prod_per_var_nb = 0
        for d in wind.data:
            if d.dir == MFipDataDir.CONS:
                cons_per_var_nb += 1
            else:
                prod_per_var_nb += 1
        self.per_vars = (ct.POINTER(MFipData) * len(wind.data))()
        self.cons_per_var_list = (ct.POINTER(MFipData) * cons_per_var_nb)()
        self.prod_per_var_list = (ct.POINTER(MFipData) * prod_per_var_nb)()
        cons_idx = prod_idx = 0
        for i, d in enumerate(wind.data):
            data.id = d.addr
            data.max_bsz = d.sz
            data.flags = d.dir
            if d.cb is not None:
                cb_ptr = MFipDataCb(d.cb)
                data.mstrfip_data_handler = ct.cast(cb_ptr, ct.POINTER(MFipDataCb))
            var = LibMFip.mfip_var_create(self._mcycle, data)
            self.per_vars[i] = var
            if d.dir == MFipDataDir.CONS:
                self.cons_per_var_list[cons_idx] = var
                cons_idx += 1
            else:
                self.prod_per_var_list[prod_idx] = var
                prod_idx += 1
        wind_cfg.varlist = self.per_vars
        wind_cfg.var_count = len(self.per_vars)
        LibMFip.mfip_per_var_wind_append(self._mcycle, wind_cfg)

    def _append_aper_wind(self, wind):
        aper_wind_cfg = MFipAperVarWindCfg()
        aper_wind_cfg.end_ustime = wind.end
        aper_wind_cfg.enable_diag = wind.enable_diag
        aper_wind_cfg.ident_var_list = None
        aper_wind_cfg.ident_var_count = wind.ident_var_count
        aper_wind_cfg.mstrfip_ident_var_handler = None

        LibMFip.mfip_aper_var_wind_append(self._mcycle, aper_wind_cfg)

    def _append_msg_wind(self, wind):
        pass

    def _append_wait_wind(self, wind):
        LibMFip.mfip_wait_wind_append(self._mcycle, wind.silent, wind.end)

    def __init__(self, mfip_dev, mcycle_cfg):
        self._mcycle = self.__create(mfip_dev.get_hdl())
        self.mfip_dev = mfip_dev
        self.prod_per_var_list = None
        self.cons_per_var_list = None
        for wind in mcycle_cfg.winds:
            if isinstance(wind, MFipPerWindDef):
                self._append_per_wind(wind)
            elif isinstance(wind, MFipAperWindDef):
                self._append_aper_wind(wind)
            elif isinstance(wind, MFipMsgWindDef):
                self._append_msg_wind(wind)
            elif isinstance(wind, MFipWaitWindDef):
                self._append_wait_wind(wind)
        pass

    def __create(self, mfip):
        return LibMFip.mfip_mcycle_create(mfip)

    def load(self):
        LibMFip.mfip_ba_load(self.mfip_dev.get_hdl(), self._mcycle)

    def start(self):
        LibMFip.mfip_ba_start(self.mfip_dev.get_hdl())

    def stop(self):
        LibMFip.mfip_ba_stop(self.mfip_dev.get_hdl())

    def reset(self):
        LibMFip.mfip_ba_reset(self.mfip_dev.get_hdl())

    def read_var(self, var):
        res = LibMFip.mfip_var_read(self.mfip_dev.get_hdl(), var)

    def write_var(self, var):
        LibMFip.mfip_var_write(self.mfip_dev.get_hdl(), var)
