/*
 * Copyright (C) 2016-2017 CERN (www.cern.ch)
 * Author: Michel Arruat <michel.arruat@cern.ch>
 *
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#ifndef __MASTERFIP_COMMON_PRIV_H
#define __MASTERFIP_COMMON_PRIV_H

#include "masterfip-common.h"

#define NSEC_PER_SEC 1000000000ULL
#define USEC_PER_SEC 1000000ULL

static inline uint32_t ticks_to_ns(uint32_t ticks, uint32_t cpu_speed_hz) {
	return (uint32_t)(((uint64_t)ticks * NSEC_PER_SEC) / cpu_speed_hz);
}

static inline uint32_t ticks_to_us(uint32_t ticks, uint32_t cpu_speed_hz) {
	return (uint32_t)(((uint64_t)ticks * USEC_PER_SEC) / cpu_speed_hz);
}

static inline uint32_t us_to_ticks(uint32_t us, uint32_t cpu_speed_hz) {
	return (uint32_t)(((uint64_t)us * cpu_speed_hz) / USEC_PER_SEC);
}

static inline uint32_t ns_to_ticks(uint32_t ns, uint32_t cpu_speed_hz) {
	return (uint32_t)(((uint64_t)ns * cpu_speed_hz) / NSEC_PER_SEC);
}

/* WR HMQ slots */
#define MSTRFIP_HMQ_O_UNDEFINED -1 /* undefined slot */

#define MSTRFIP_HMQ_O_APP_PER_VAR 0 /*CPU0 out slot: app.'s periodic var */
#define MSTRFIP_HMQ_O_APP_APER_VAR 1 /*CPU0 out slot: app.'s aperiodic var */
#define MSTRFIP_HMQ_O_APP_APER_MSG 2 /*CPU0 out slot: app.'s aperiodic msg */
#define MSTRFIP_HMQ_O_DIAG_PER_VAR 3 /*CPU0 out slot: diag.'s periodic var */
#define MSTRFIP_HMQ_O_DIAG_APER_VAR 4 /*CPU0 out slot: diag.'s aperiodic var */
#define MSTRFIP_HMQ_ACQ_SLOT_COUNT 5 /* number of HMQ acquisition slots */

#define MSTRFIP_HMQ_IO_CPU0_CMD 5 /* Core 0 input/output slot: send BA commands */
#define MSTRFIP_HMQ_IO_CPU1_CMD 0 /* Core 1 input/output slot: send new payloads */

/*
 * FIP frame encoding:
 * This should be left in the rt header file. Unfortunately byte operation to
 * skip some header and tail in the payload are not working properly in
 * mockturtle. Therefore some knowledge of the frame is exposed to the library.
 * frame encoding: |ctrl(1byte)|Payload(bsz)|CRC(2bytes)|
 * Payload encoding depends of the frame type:
 * MPS var payload: |PDU_type(1byte)|length(1byte)|DATA(bsz)|Status(1byte)|
 * SM_MMPS var paylaod: |PDU_type(1byte)|length(1byte)|data(bsz)|
 * APER MSG payload: |dest_addr(3bytes)|src_addr(3bytes)|data(bsz)|
 */
#define MSTRFIP_VAR_PAYLOAD_HDR_BSZ 2
#define MSTRFIP_VAR_MPS_STATUS_BSZ 1
#define MSTRFIP_MSG_PAYLOAD_HDR_BSZ 6
#define MSTRFIP_MSG_PAYLOAD_MAX_DATA_BSZ 256
#define MSTRFIP_MSG_PAYLOAD_MAX_WSZ 66

#define MSTRFIP_IDENT_VAR_PAYLOAD_NWORDS 3
#define MSTRFIP_PRESENCE_VAR_DATA_BSZ 5
#define MSTRFIP_PRESENCE_VAR_PAYLOAD_NWORDS 2

/**
 * MPS status definition
 * Any response frame got as last byte a status with two useful bits qualifying
 * in terms of refreshment and significance the response
 */
#define MSTRFIP_MPS_REFRESH_BIT 0x1
#define MSTRFIP_MPS_SIGNIFICANCE_BIT 0x4

#define MSTRFIP_DIAG_ADDR 0x7F
#define MSTRFIP_DIAG_PROD_VAR_ID ((0x05 << 8) | MSTRFIP_DIAG_ADDR)
#define MSTRFIP_DIAG_CONS_VAR_ID ((0x06 << 8) | MSTRFIP_DIAG_ADDR)
#define MSTRFIP_DIAG_VAR_DATA_BSZ 2

#define MSTRFIP_DIAG_IDENT_VAR_ID ((0x10 << 8) | MSTRFIP_DIAG_ADDR)
#define MSTRFIP_AGENT_IDENT_VAR_ID 0x1000
#define MSTRFIP_IDENT_VAR_DATA_BSZ 8

/*
 * Agent adress are in range of [0..255] (0: is the address of fipmaster.
 * 255/32 = 8 32 bits words and each bit is assigned to its corresponding agent
 * in order to register the demand for an identification variable
 * So raisng the bit [2][4] means schedule ident var of the agent address 0x64.
 * The same for presence variable
 */
#define MSTRFIP_PRESENCE_REQUEST_WSIZE 8
#define MSTRFIP_IDENT_REQUEST_WSIZE 8


/*
 * worldfip gateware defines the max payload size of MQ messages as
 * 128 32bits words. Unfortunately the last or two words are not
 * usable (reported to Tom). So we use only 126 words.
 * (-1, for RMQ, because 32 bits at the and are reserved for CRC)
 */
#define MSTRFIP_TRTLMSG_MAX_PAYLOAD_WSZ_HMQ (128 - 2)
#define MSTRFIP_TRTLMSG_MAX_PAYLOAD_WSZ_RMQ (128 - 2 - 1)

/*
 * FIP data attribute:
 * this flag complete the enum mstrfip_data_flags but is managed by the
 * library. If a callback has been registered with a specific FIP data, teh
 * library append this flags in order to register that an irq is required when
 * this FIP data circulates over the bus.
 */
#define MSTRFIP_DATA_FLAGS_IRQ (1 << 7)

/* MASTERFIP message's ID */
enum mstrfip_messages_id {
	/* commands */
	MSTRFIP_CMD_GET_BITRATE = 0x1,
	MSTRFIP_CMD_SET_HW_CFG,
	MSTRFIP_CMD_GET_RT_VERSION,
	MSTRFIP_CMD_SET_BA_CYCLE,
	MSTRFIP_CMD_SET_VAR_LIST,
	MSTRFIP_CMD_SET_APER_MSG,
	MSTRFIP_CMD_SET_BA_STATE,
	MSTRFIP_CMD_SET_VAR_PAYLOAD,
	MSTRFIP_CMD_SET_MSG_PAYLOAD,
	MSTRFIP_CMD_GET_RESPTIME,
	MSTRFIP_CMD_GET_PRESENT_LIST,
	MSTRFIP_CMD_GET_REPORT,
	MSTRFIP_CMD_REQ_IDENT_VAR,
	/* answers */
	MSTRFIP_REP_ACK, /* send back when the CMD succeed */
	MSTRFIP_REP_NACK, /* send back when the CMD failed */
	MSTRFIP_REP_BITRATE,
	MSTRFIP_REP_RT_VERSION,
};

enum mstrfip_ba_state_cmd {
	MSTRFIP_BA_STATE_CMD_START = 0x1,
	MSTRFIP_BA_STATE_CMD_STOP = 0x2,
	MSTRFIP_BA_STATE_CMD_RESET = 0x4,
};

///*
// * Parity bit: for MSG_ACK transaction, the control byte can be odd or even.
// * For instance the control byte of an even RP_MSG_ACK is 0x14 and for an
// * odd RP_MSG_ACK it is 0x14|0x80=0x94. This is important because if the
// * agent sent an odd RP_MSG_ACK, it expects from the master an odd RP_ACK
// */
//#define MSTRFIP_CTRL_BYTE_ODD_BIT 0x80
//#define MSTRFIP_CTRL_BYTE_ODD_BIT_MASK 0x7F
enum mstrfip_frame_ctrl_type {
	/* periodic variables frames */
	MSTRFIP_RP_UNKNOWN = 0x1, /**< unknown RP control byte */
	MSTRFIP_ID_DAT = 0x3, /**< question frame for periodic variable */
	MSTRFIP_RP_DAT = 0x2, /**< response frame for periodic variable */
	MSTRFIP_RP_DAT_MSG = 0x6, /**< response frame for periodic variable and
					request for a message*/
	/* messages frames */
	MSTRFIP_ID_MSG = 0x5, /**< question frame for messages */
	MSTRFIP_RP_MSG_NOACK = 0x4,

//	MSTRFIP_RP_MSG_ACK = 0x14,
//	MSTRFIP_RP_FIN = 0x40,
//	MSTRFIP_RP_ACK = 0x30, /* check RP_ACK+ RP_ACK- */


	/* TODO: this PAIR/IMPAIR need to be understood. We expect to have a
	 * single control byte MSTRFIP_RP_MSG_ACK */
	MSTRFIP_RP_MSG_ACK_EVEN = 0x14,
	MSTRFIP_RP_MSG_ACK_ODD = 0x94,
	MSTRFIP_RP_FIN = 0x40,
	MSTRFIP_RP_ACK_EVEN = 0x30, /* check RP_ACK+ RP_ACK- */
	MSTRFIP_RP_ACK_ODD = 0xB0, /* check RP_ACK+ RP_ACK- */
};

/*
 * Description of every ba instruction arguments
 */
/*
 * Arguments used for a periodic var window instruction
 */
struct mstrfip_ba_per_var_wind_args{
	uint32_t start_var_idx; /*sorted valist index of the first var to play*/
	uint32_t stop_var_idx; /*sorted varlist index of the last var to play*/
};

/*
 * Arguments used for an aperiodic var window instruction
 */
struct mstrfip_ba_aper_var_wind_args {
	/*
	 * time in cpu ticks, relative to the begin of the macro cycle
	 * where the aperiodic var window ends.
	 */
	uint32_t ticks_end_time;
};

/*
 * Arguments used for an aperiodic message window instruction
 */
struct mstrfip_ba_aper_msg_wind_args {
	/*
	 * time in cpu ticks, relative to the begin of the macro cycle
	 * where the aperiodic var window ends.
	 */
	uint32_t ticks_end_time;
	/*
	 * Define the max number of message requests sent by the agents
	 * during a periodic window in order to tell the master to schedule
	 * some message transaction during the nect aperiodic message window.
	 */
	uint32_t cons_msg_fifo_sz;
	/*
	 * Define the max number of messages sent by an application, which can
	 * be stored, waiting to be schedule in the next aperiodic message window.
	 */
	uint32_t prod_msg_fifo_sz;

	uint32_t prod_msg_max_bsz; /* max payload size of prod mesg (in byte) */
	/*
	 * Flags defining the behaviour of the system to report on messaging.
	 * If set, an irq is raised at the end of an aperiodic window to report
	 * on consumed or/and produced messages that occurred during this
	 * window.
	 */
	uint32_t cons_msg_global_irq;
	uint32_t prod_msg_global_irq;
};

/*
 * Arguments used for a wait window instruction
 */
struct mstrfip_ba_wait_wind_args {
	uint32_t ticks_end_time;
	uint32_t is_silent;
};

/**
 * @enum mstrfip_ba_instruction_set
 * The full instruction set supported by the master. It's a subset of teh
 * original FIP's instruction set
 */
enum mstrfip_ba_instr_code {
	MSTRFIP_BA_PER_VAR_WIND = 1, /**< process periodic variables */
	MSTRFIP_BA_APER_MSG_WIND, /**< process aperiodic messages */
	MSTRFIP_BA_APER_VAR_WIND, /**< process aperiodic SMMPS variables */
	MSTRFIP_BA_WAIT_WIND, /**< wait until an absolute time in the macro cycle*/
	MSTRFIP_BA_NEXT_MACRO, /**< start again the macro cycle execution */
};

/*
 * In order to program the macro cycle we use the concept of instruction. An
 * instruction is made of code and a set of arguments which are depending of
 * the instruction codde.
 */
struct mstrfip_ba_instr {
	enum mstrfip_ba_instr_code code;
	union {
		struct mstrfip_ba_per_var_wind_args per_var_wind;
		struct mstrfip_ba_aper_var_wind_args aper_var_wind;
		struct mstrfip_ba_aper_msg_wind_args aper_msg_wind;
		struct mstrfip_ba_wait_wind_args wait_wind;
	};
};

/*
 * Any mockturtle msg contains this header
 */
struct mstrfip_trtlmsg_hdr {
	uint32_t id; /* message id */
	uint32_t seq; /* sequence number */
};

/*
 * mturtle message header used to program the macro cycle.
 * This header is then followed by the payload which is a list of
 * struct_ba_instr entries. instr_count field gives the
 * number of instructions contained in the payload.
 *
 */
struct mstrfip_ba_instr_hdr_trtlmsg {
	struct mstrfip_trtlmsg_hdr trtl_hdr;
	uint32_t cycle_ticks_length; /* macro cycle duration in CPU ticks */
	uint32_t instr_count; /* number of instruction */
};
#define MSTRFIP_BA_INSTR_HDR_TRTLMSG_WSZ \
	sizeof(struct mstrfip_ba_instr_hdr_trtlmsg) / sizeof(uint32_t)

/*
 * mturtle message used to read HW speed
 */
struct mstrfip_hw_speed_trtlmsg {
	struct mstrfip_trtlmsg_hdr trtl_hdr;
	uint32_t bitrate; /* rx/tx bit rate */
};
#define MSTRFIP_HW_SPEED_TRTLMSG_WSZ \
		sizeof(struct mstrfip_hw_speed_trtlmsg) / sizeof(uint32_t)

/*
 * mturtle message used to get version
 */
struct mstrfip_rt_version_trtlmsg {
	struct mstrfip_trtlmsg_hdr trtl_hdr;
	uint32_t fpga_id;
	uint32_t rt_id;
	uint32_t rt_version;
	uint32_t fpga_version;
	uint32_t rt_git_version;
};
#define MSTRFIP_RT_VERSION_TRTLMSG_WSZ \
		sizeof(struct mstrfip_rt_version_trtlmsg) / sizeof(uint32_t)

/*
 * mturtle message used to configure HW
 */
struct mstrfip_hw_cfg_trtlmsg {
	struct mstrfip_trtlmsg_hdr trtl_hdr;
	uint32_t enb_ext_trig; /* enable external trigger */
	uint32_t enb_ext_trig_term; /* enable external trigger 50ohms term */
	uint32_t enb_int_trig; /* enable internal trigger */
	uint32_t tr_ticks; /* turn around time in CPU ticks */
	uint32_t ts_ticks; /* silence time in CPU ticks */
	uint32_t bit_ticks; /* bit tx/rx speed */
};
#define MSTRFIP_HW_CFG_TRTLMSG_WSZ \
		sizeof(struct mstrfip_hw_cfg_trtlmsg) / sizeof(uint32_t)

/*
 * mturtle message used to set ba state: START/STOP/RESET
 */

struct mstrfip_ba_state_trtlmsg {
	struct mstrfip_trtlmsg_hdr trtl_hdr;
	uint32_t state; /**< ba state to set: STRAT/STOP/RESET */
};

/*
 * mturtle message used to set var payload
 */
struct mstrfip_var_payload_trtlmsg {
	struct mstrfip_trtlmsg_hdr trtl_hdr;
	uint32_t key; /**< used to locate corresponding fip data object */
	uint32_t bsz;  /**< payload size in bytes */
};
#define MSTRFIP_VAR_PAYLOAD_HDR_TRTLMSG_WSZ \
		sizeof(struct mstrfip_var_payload_trtlmsg) / sizeof(uint32_t)

/*
 * mturtle message used to set msg payload
 */
struct mstrfip_msg_payload_trtlmsg {
	struct mstrfip_trtlmsg_hdr trtl_hdr;
	uint32_t dest_addr; /**< address of the remote agent */
	uint32_t bsz; /**< payload size in bytes */
	uint32_t flags; /**< direction + irq flags */
};
#define MSTRFIP_MSG_PAYLOAD_HDR_TRTLMSG_WSZ \
		sizeof(struct mstrfip_msg_payload_trtlmsg) / sizeof(uint32_t)

/*
 * mturtle message used to request identification var
 */
struct mstrfip_ident_request_trtlmsg {
	struct mstrfip_trtlmsg_hdr trtl_hdr;
	uint32_t ident_request[MSTRFIP_IDENT_REQUEST_WSIZE];
};
#define MSTRFIP_MSG_IDENT_REQUEST_TRTLMSG_WSZ \
		sizeof(struct mstrfip_ident_request_trtlmsg) / sizeof(uint32_t)

/*
 * mturtle message used to get agent response time
 * Agent's response time is used later on to validate macrocycle programation.
 */
struct mstrfip_response_time_trtlmsg {
	struct mstrfip_trtlmsg_hdr trtl_hdr;
	uint32_t cycle_nticks; /* macrocycle length to schedule pres var */
	uint32_t from_agt_addr; /* from agent address */
	uint32_t to_agt_addr; /* to agent address */
};
#define MSTRFIP_RESPONSE_TIME_TRTLMSG_WSZ \
		sizeof(struct mstrfip_response_time_trtlmsg) / sizeof(uint32_t)

/*
 * This struct is shared by the library and the RT
 * Note: basically, key is used for efficient lookup in the lib and in the
 * RT task to retrieve the appropriate data structure, through messages
 * exchanged between host and mock-turtle.
 * Alignment issue: because this struct is exchanged with the rt side the order
 * of fields respect the self-alignment rule. The sizeof struct mstrfip_var_desc
 * gives 8 bytes (2 32bits word)on an x86 processor
 */
struct mstrfip_data_hdr {
	uint16_t key; /**< used to locate efficiently fip data */
	uint16_t id; /**< var id 0xXXYY : XX=var number YY:agent adress */
	uint16_t max_bsz; /**< max payload size in bytes (max=256bytes) */
	uint8_t mq_slot; /**< acq mq slot */
	uint8_t flags; /**< direction + IRQ flag */
};

/*
 * Defines a record corresponding to FIP event which can be: frame, start
 * macrocyle, or start of a window(aper_var_wind, per_var_wind, aper_var_wind
 * or wait_wind)
 * A record is made of the nstime when it's start to be emitted on the bus and
 * a word describing the frame.
 * This two words define bitfields:
 * time: bit31-bit28:nb of sec | bit27-bit0: number of cpu ticks (10ns unit)
 * frame_desc:
 * 	Byte3-Byte2: varid
 * 	Byte1:ctrlByte
 * 	Byte0: bit6-bit4:event(window,frame,..) | bit3-bi1:status | bit0:direction
 */
#define MSTRFIP_EVTREC_TIMESEC_MASK 0xF0000000
#define MSTRFIP_EVTREC_TIMESEC_SHIFT 28
//#define MSTRFIP_EVTREC_TIMETICKS_MASK 0x0FFFFFFF
#define MSTRFIP_EVTREC_VARID_MASK 0xFFFF0000
#define MSTRFIP_EVTREC_VARID_SHIFT 16
#define MSTRFIP_EVTREC_CTRLBYTE_MASK 0x0FF00
#define MSTRFIP_EVTREC_CTRLBYTE_SHIFT 8
#define MSTRFIP_EVTREC_WIND_MASK 0x70
#define MSTRFIP_EVTREC_WIND_SHIFT 4
#define MSTRFIP_EVTREC_MCYCLE_MASK 0x80
#define MSTRFIP_EVTREC_MCYCLE_SHIFT 7
#define MSTRFIP_EVTREC_STATUS_MASK 0xE
#define MSTRFIP_EVTREC_STATUS_SHIFT 1
#define MSTRFIP_EVTREC_DIR_MASK 0x1

#define MSTRFIP_EVTREC_STATUS_OK 0
#define MSTRFIP_EVTREC_STATUS_TMO 1
#define MSTRFIP_EVTREC_STATUS_ERR 2
#define MSTRFIP_EVTREC_DIR_PROD 0
#define MSTRFIP_EVTREC_DIR_CONS 1

struct mstrfip_evt_record {
	uint32_t time; /* 10ns tick */
	uint32_t evt_desc;
};

struct mstrfip_record_cfg {
	uint32_t flg; /* enable/disable recording */
	uint32_t ctrltype_trigger; /* ctrl type event should match to start recording */
	uint32_t varid_trigger; /* varid event should match to start recording */
	uint32_t status_trigger; /* status event should match to start recording */
	uint32_t req_nentries; /* number of entries requested */
	uint32_t nentries; /* number of records */
	uint32_t stime; /* start recording time in seconds */
};

#define MSTRFIP_MAX_RECORD_COUNT 2000
struct smem_record_traffic {
	struct mstrfip_record_cfg cfg;
	struct mstrfip_evt_record buf[MSTRFIP_MAX_RECORD_COUNT]; /* record buffer */
};

struct smem_per_var {
	/*
	 * payload size: to simplify payload lookup the offset between payloads
	 * is aligned to the max payload size (in words).
	 */
	uint32_t payload_wsz;
	uint32_t prod_nvar;
	uint32_t payload_buf_addr; /* pointer to the payload list area */
	/*
	 * payload_state: each produced variable payload's has its
	 * corresponding bit in the payload_state array which is raised and
	 * lower respectively each time a new payload is received and consumed.
	 * The size of the payload_state array is deduced from nvar:
	 * nword = nvar / 32 + !!(nvar % 32)
	 */
	uint32_t payload_state_addr; /* pointer to the request payload state */
};

struct smem_aper_var {
	uint32_t presence_request[MSTRFIP_PRESENCE_REQUEST_WSIZE];
	uint32_t ident_request[MSTRFIP_IDENT_REQUEST_WSIZE];
	uint32_t ident_req;
};

/*
 * payload size is not knew in advance. To ease and optimize increment of read
 * and write pointers in the FIFO we use an intermediate table "entries" which
 * contains the pointers to the payload and read and write pointers are just
 * index in the entries table.
 */
struct smem_aper_msg {
	uint32_t payload_wsz; /* max message payload size in word */
	/*
	 * address where starts a circular buffer of ptr to payload.
	 * Each entry in this array is a pointer to the payload
	 * to be carry in the aper message
	 */
	uint32_t payload_buf_addr;
	uint32_t payload_buf_sz; /* size of the payload ptr array */
	uint32_t payload_count; /* current number of payload  */
	uint32_t r_idx; /* payload read index: pointer to get the payload */
	uint32_t w_idx; /* payload write index: pointer to write the payload */
};

/*
 * Defines SMEM layout data
 * This struct and the nested ones are also used by the diagnostic tool,
 * running on 64bits architecture. Therefore they shouldn't contain pointer or
 * long but only uint32_t.
 * If it is only uint32_t types, it's not necessary to use
 * '__attribute__((aligned(4), packed))' which is useless and which makes the
 * compiler generates slower and bigger code
 */
struct smem_data_cfg {
	uint32_t smem_wsz; /* shared memory current size in words*/
	uint32_t bss_wsz; /* bss current size in words*/
	uint32_t smem_mtx; /* SMEM mutex */
	uint32_t ba_state_cmd; /* ba state command */
	struct smem_aper_var aper_var; /* aperiodic produced var */
	struct smem_aper_msg aper_msg; /* aperiodic produced messages */
	struct smem_per_var per_var; /* periodic produced var */
	struct mstrfip_report report; /* report: errors, statistics... */
	/*
	 * "record" field has to be always the last entry in smem_data_cfg struct
	 * to not be reseted when an application starts. This way diagnostic
	 * tool can be lanched before the application, waiting for recording
	 * @see mstrfip_smem_partitioning function.
	 */
	struct smem_record_traffic record; /* traffic recording */
};

#endif //__MASTERFIP_COMMON_PRIV_H
