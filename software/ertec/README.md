# ProFIP

<!--
SPDX-FileCopyrightText: 2023 CERN (home.cern)

SPDX-License-Identifier: CC-BY-SA-4.0+
-->

ProFIP is a translator between PROFINET and MasterFIP networks. It allows to control the MasterFIP network (and NanoFIP nodes) via PROFINET.
ProFIP uses the same firmware and libraries as MasterFIP, but has different gateware (the current version of MasterFIP runs on SPEC, ProFIP requires SVEC),
and additional firmware for the ERTEC processor.
ProFIP is developed on the /profip_master branch.

## Documentation

The documentation is located in /doc. It consists of technical documentation (profip-td) and user manual (profip-um). It can be built using sphinx (make html) or downloaded from Gitblab CI.

## Building

The software for ERTEC can be built on both Linux and Windows (with Eclipse).\
It consists of the ECOS operating system '/software/ertec/ecos' and the application '/software/ertec/sources'

### build on Linux (ECOS + appliction)

#### requirements (for Ubuntu)

gcc-arm-none-eabi\
make\
libncurses5-dev\
libstdc++6\
build-essential\
libstdc++-9-dev\
libstdc++-arm-none-eabi-newlib\
python3-pip\
python3-sphinx\
doxygen\
graphviz\
git\
libstdc++5:i386\
tclsh

You can use the docker image to build: gitlab-registry.cern.ch/be-cem-edl/evergreen/gitlab-ci/ertec-fw-build

<code>cd software/ertec/ecos<br>
make -j12<br>
cd ../sources<br>
make -j12<br>
</code>

### build on Windows (ECOS + application)

see doc/ertec/SW/Guideline_EvalKit_ERTEC200P_V4.7.0.pdf
