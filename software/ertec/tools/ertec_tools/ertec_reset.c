// SPDX-FileCopyrightText: 2024 CERN (home.cern)
//
// SPDX-License-Identifier: LGPL-2.1-or-later

/**
 * @author piotr.klasa@cern.ch
 * 
 * Tool for resetting ERTEC
*/


#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <stdint.h>
#include <stdlib.h>
#include <vmebus/libvmebus.h>
#include <arpa/inet.h>

#define GPIO_OFFSET 0x11000

#define REG_CLEAR_OUTPUT    0x0
#define REG_SET_OUTPUT      0x4
#define REG_DATA_DIR        0x8
#define REG_PIN_STATE       0xC

void er_help(void)
{
        printf("Tool for resetting Ertec (for ProFIP)\n");
        printf(" -a, --address ADDR   board address\n");
        printf(" -h, help\n");
}

int do_ertec_reset(unsigned int vme_addr)
{
    struct vme_mapping map;
    unsigned offset = GPIO_OFFSET;
    unsigned pg = getpagesize();

    memset(&map, 0, sizeof(struct vme_mapping));
    map.am = 0x39;
    map.data_width = 32;
    map.sizel = pg;
    map.vme_addrl = vme_addr | (offset & ~(pg - 1));
    
    void *base_board = (void*) vme_map(&map, 1);
    
    printf("vme_addr    0x%x\n", vme_addr);
    printf("gpio_offset 0x%x\n", offset);

    if(base_board == NULL)
    {
        printf("Error\n");
        return 1;
    }

    uint32_t value = htonl(0x01);

    *(volatile uint32_t *)(base_board + REG_DATA_DIR ) = value; // set gpio direction as output
    *(volatile uint32_t *)(base_board + REG_CLEAR_OUTPUT ) = value; // clear output (RESET)
    usleep(40000); // wait 40 ms
    *(volatile uint32_t *)(base_board + REG_SET_OUTPUT ) = value; // set output (not RESET)

    printf("Success\n");

    vme_unmap(&map, 1);

    return 0;
}

int main(int argc, char *argv[])
{

    unsigned int vme_addr = 0x100000;
    char c;

	/* Parse specific args */
	while ((c = getopt (argc, argv, "ha:")) != -1) {
		switch (c) {
		case 'h':
			er_help();
			return 0;
		case 'a':
			vme_addr = strtol(optarg, NULL, 16);
			break;
		case '?':
			break;
		}
	}

    return do_ertec_reset(vme_addr);
}