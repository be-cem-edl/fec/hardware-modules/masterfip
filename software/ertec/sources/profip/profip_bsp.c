// SPDX-FileCopyrightText: 2023 CERN (home.cern)
//
// SPDX-License-Identifier: LGPL-2.1-or-later

#include "profip_bsp.h"
#include "profip_led.h"

void profip_bsp_start_led_blinking(uint32_t freq)
{
	profip_led_blink(PROFIP_LED_GREEN, PROFIP_LED_BLINK_SLOW);
	profip_led_blink(PROFIP_LED_YELLOW, PROFIP_LED_BLINK_SLOW);
	profip_led_blink(PROFIP_LED_RED, PROFIP_LED_BLINK_SLOW);
}

void profip_bsp_stop_led_blinking(void)
{
	profip_led_off(PROFIP_LED_GREEN);
	profip_led_off(PROFIP_LED_YELLOW);
	profip_led_off(PROFIP_LED_RED);
}
