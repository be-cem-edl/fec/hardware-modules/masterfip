// SPDX-FileCopyrightText: 2023 CERN (home.cern)
//
// SPDX-License-Identifier: LGPL-2.1-or-later

/**
 * @author Piotr Klasa <piotr.klasa@cern.ch>
 *
 * @file pf_cas.h
 * @name Profip Control & Status
 * @desc Modul for receiving and storing information from the PLC about
 *       the macrocycle configuration - such as the length of the macrocycle,
 *       the length of the window for aperiodic variables. At is also
 *       responsible for receiving commands from the PLC to control ProFIP
 *       - such as reset, stop the macrocycle - store them and make them
 *       available for other modules. The module also collectes and sends
 *       information about the device and macrocycle to the PLC, such as:
 *       real macrocycle length, device status, number of configured devices.
 */

#ifndef CAS_H
#define CAS_H

#include <stdint.h>

#include "pf_utils.h"

#define PF_CONTROL_SIZE 32
#define PF_STATUS_SIZE 32

/**
 * @name cas_init
 * @desc Initializes the module
 **/
void cas_init(void);

/**
 * @name cas_copy_to_pnStack
 * @param buff
 * @param size
 * @return iops
 * @brief Function responsible for copy data comming from Module (device
 *        statistics and informations aboud macrocycle) to the Profinet 
 *        stack (so later its send to PLC). It copies "size" bytes from
 *        CAS application to "buff". This funciton shall be called from
 *        PN stack ("read from appl" data callback).
 */
uint8_t cas_copy_to_pnStack(uint8_t* buff, uint32_t size);

/**
 * @name cas_copy_from_pnStack
 * @param buff
 * @param size
 * @return iocs
 * @brief Function responsible for coping data comming from Profinet stack 
 *        (PLC) to the CAS module. It copies "size" data from "buff" to CAS
 *        Application. This function shall be called from PN stack ("write
 *        to appl" data callback), 
 */
uint8_t cas_copy_from_pnStack(uint8_t* buff, uint32_t size);

/**
 * @name cas_write_record
 * @param index
 * @param buff
 * @param size
 * @return 0 on success
 * @brief Function responsible for coping record data from Profinet stack
 *        to CAS module. It shall be called from PN stack ("read param" 
 *        callback)
 */
int cas_write_record(uint32_t index, uint8_t* buff, uint32_t size);

/**
 * @brief Saves the given value of profip_status internally, 
 *        This value is then cyclically sent to the PLC via
 *        the CAS module.
*/
void cas_set_profip_status(uint8_t profip_status);
void cas_set_masterfip_status(uint8_t masterfip_status);
void cas_set_nbr_of_prod_variables(uint8_t nbr_of_prod_variables);
void cas_set_nbr_of_cons_variables(uint8_t nbr_of_cons_variables);
void cas_set_bus_speed(uint8_t bus_speed);
void cas_set_turnaround_time_us(uint16_t turnaround_time_us);
void cas_set_wait_wind_time_us(uint32_t wait_wind_time_us);
void cas_set_aper_var_wind_time_us(uint32_t aper_var_wind_time_us);
void cas_set_irq_thread_tick(uint8_t irq_thread_tick);
void cas_set_pf_thread_tick(uint8_t pf_thread_tick);
void cas_set_fip_cycle_cnt(uint16_t fip_cycle_cnt);
void cas_set_tcp_reprogramming_enabled(uint8_t tcp_reprogramming_enabled);
void cas_set_mfip_git_vers(uint32_t mfip_git_vers);


uint8_t cas_get_init_done(void);
uint16_t cas_get_turnaround_time_us(void);
uint32_t cas_get_wait_wind_time_us(void);
uint32_t cas_get_aper_var_wind_time_us(void);
uint8_t cas_get_diag_enabled(void);

uint8_t cas_init_params_changed(void);

uint8_t cas_cmd_reset(void);
uint8_t cas_cmd_reset_masterfip(void);
uint8_t cas_cmd_stop_macrocycle(void);
uint8_t cas_cmd_clear_faults(void);
uint8_t cas_cmd_clear_nodes_faults(void);
uint8_t cas_cmd_tcp_reprogramming(void);

#endif
