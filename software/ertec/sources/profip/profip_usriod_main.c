// SPDX-FileCopyrightText: 2023 CERN (home.cern)
//
// SPDX-License-Identifier: LGPL-2.1-or-later

#include <string.h>
#include "compiler.h"
#include "usriod_cfg.h"
#include "usrapp_cfg.h"   // example application spec. configuration settings

#include "version_dk.h"
#include "usriod_cfg.h"
#include "pniousrd.h"
#include "bspadapt.h"
#include "iodapi_event.h"
#include "os.h"
#include "os_taskprio.h"
#include "usriod_diag.h"
#include "usriod_utils.h"
#include "usriod_im_func.h"
#include "pnio_trace.h"
#include "iod_cfg1.h"
#include "iodapi_rema.h"
#include "nv_data.h"
#include "PnUsr_Api.h"
#include "logadapt.h"
#include "perform_measure.h"
#include "profip.h"
#include "profip_bsp.h"
#include "profip_wdg.h"
#include "pf_err.h"
#include "data_uart_util.h"
#include "pf_utils.h"
#include "profip_gpio.h"
#include "profip_led.h"
#include "profip_log.h"

#if ( (PNIOD_PLATFORM & PNIOD_PLATFORM_ECOS_EB200P) || (PNIOD_PLATFORM & PNIOD_PLATFORM_POSIX_EB200P) )
	//#include "hama_com.h"
	#include <cyg/hal/ertec200p_reg.h>
	#include <cyg/kernel/kapi.h>
	#include <cyg/hal/hama_timer.h>
#endif

#if ( (MODULE_ID_DAP_1PORT == MODULE_ID_DAP) && ((IOD_INCLUDE_MRPD==1) || (IOD_INCLUDE_MRP==1)) )
	#error ("DAP4 does not support MRP and MRPD features")
#endif

#define CMD_MAX_LEN 100
    /*===========================================================================*/

    // *------------ external functions  ----------*
    extern void TcpUtilFlashFirmware (void);
	extern void OsPrintMemInfo();
	extern void OsPrintThreads();

    static void PrintHelp (void);
    static void PrintLastApdu (void);

    // *------------ internal functions  ----------*

    // *------------ public data  ----------*


    // *------------ local data  ----------*
    static PNUSR_DEVICE_INSTANCE PnUsrDev;
    static PNIO_UINT32           LastRemoteApduStatus = 0;

    // *-------------------------------------------------------------------------------------------
    // * list of IO subslot (including also PDEV subslots), that shall be plugged during startup.
    // *
    // * list order:  DAP first, then PDEV, then IO submodules:
    // *    DAP                              (mandatory)
    // *    PDEF-Interface                   (mandatory)
    // *    PDEV port 1 .... PDEV port n     (mandatory)
    // *    IO submodules                    (optional)
    // *
    // *
    // * Note, that I&M0 support for the DAP is mandatory, but optional for all other submoduls.
    // * In this case they have to respond to an IM0 read request with the IM0 data of the DAP, that is
    // * a proxy for the device related IM0 data here.
    // *
    // * IO subslots can optionally be plugged or pulled later.
    // * DAP and PDEV subslots can not be pulled.
    // *
    // *
    // * RESTRICTIONS  (see also example IoSubList[] below)
    // * ============
    // * 1. exact one submodule must be the proxy for the device (must be INTERFACE subslot 0x8000)
    // * 2. IM1...4 is only supported for the proxy, all other submodules must have IM0.ImSupported = 0 !!
    // *
    // *
    // *-------------------------------------------------------------------------------------------
    static PNIO_SUB_LIST_ENTRY IoSubList []
    = {
		// Api  Slot	Subslot,    ModId,              SubId,                InLen,  OutLen, I&M0 support
		{  0,	0,      1,          MODULE_ID_DAP,  	SUBMOD_ID_DEFAULT,    0,      0,      PNIO_IM0_SUBMODULE + PNIO_IM0_DEVICE}
		, {  0,   0,      0x8000,     MODULE_ID_DAP,  	SUBMOD_ID_PDEV_IF,    0,      0,      PNIO_IM0_SUBMODULE} // PDEV interface
		, {  0,   0,      0x8001,     MODULE_ID_DAP,  	SUBMOD_ID_PDEV_PORT,  0,      0,      PNIO_IM0_SUBMODULE}   // PDEV port1
		#if (IOD_CFG_PDEV_NUMOF_PORTS >= 2)
		, {  0,   0,      0x8002,     MODULE_ID_DAP,  	SUBMOD_ID_PDEV_PORT,  0,      0,      PNIO_IM0_SUBMODULE}   // PDEV port2
		#endif
		, {  0,   1,           1,              0x90,  	                  1, 32,     32,      PNIO_IM0_SUBMODULE} // control & status
		, {  0,   2,           1,              0x91,  	                  1,  0,    180,      PNIO_IM0_SUBMODULE} // diagnostic
    };


    // * --------------------------------------------
    // * List of IM0 data
    // * ---------------------------
    // * Api, Slot, Subslot         in machine format
    // * Vendor ID, OrderID,..,...  in BIG ENDIAN FORMAT
    // * --------------------------------------------
    static PNIO_IM0_LIST_ENTRY Im0List []
        = { // * --------------------------------------------
            // * IM0 Data for the DAP
            // * (mandatory, because proxy for device)
            // * --------------------------------------------
           {0,                          // PNIO_UINT32     Api;                 // api number
            0,                          // PNIO_UINT32     Slot;                // slot number (1..0x7fff)
            1,                          // PNIO_UINT32     Subslot;             // subslot number (1..0x7fff)
               {IOD_CFG_VENDOR_ID,      // PNIO_UINT16     VendorId;            // VendorIDHigh, VendorIDLow
            	PROFIP_ORDERNUMBER,		// PNIO_UINT8      OrderId [20];        // Order_ID, visible, must be 20 bytes here (fill with blanks)
				IOD_CFG_IM0_SERIAL_NUMBER,  // PNIO_UINT8  SerNum  [16];        // IM_Serial_Number, visible string, must be 16 bytes here (fill with blanks)
				PROFIP_HW_REVISION,     // PNIO_UINT16     HwRevision;          // IM_Hardware_Revision
               {PROFIP_VERSION_PREFIX,  // PNIO_UINT8      SwRevPrefix;         // software revision prefix
                PROFIP_VERSION_MAJOR,   // PNIO_UINT8      SwRevFuncEnhanc;     // IM_SW_Revision_Funct_Enhancement
                PROFIP_VERSION_MINOR ,  // PNIO_UINT8      SwRevBugFix;         // IM_SW_Revision_Bug_Fix
                PROFIP_VERSION_PATCH},  // PNIO_UINT8      SwRevInternChange;   // IM_SW_Revision_Internal_Change
				0,                      // PNIO_UINT16     Revcnt;              // IM_Revision_Counter, notifies a hw modification
                0x00,                   // PNIO_UINT16     ProfId;              // IM_Profile_ID, see Profile Guideline I&M functions
                0x05,                   // PNIO_UINT16     ProfSpecTyp;         // IM_Profile_Spec_Type (e.g. 5="interface module")
                0x01,                   // PNIO_UINT8      VersMaj;             // IM_Version major
                0x01,                   // PNIO_UINT8      VersMin;             // IM_Version minor
#if IOD_INCLUDE_IM5
				PNIO_SUPPORT_IM12345    // PNIO_UINT16     ImSupported;         // IM_Supported, IM1...5 supported (bit list, bit 1...bit5, here: 0 ==> IM1..5  supported)
#else
				PNIO_SUPPORT_IM1234     // PNIO_UINT16     ImSupported;         // IM_Supported, IM1...4 supported (bit list, bit 1...bit4, here: 0 ==> IM1..4  supported)
#endif
			    }},
        // * --------------------------------------------
        // * IM0 Data for the slot 0 interface
        // * (optional)
        // * --------------------------------------------
		   {0,                          // PNIO_UINT32     Api;                 // api number
		    0,                          // PNIO_UINT32     Slot;                // slot number (1..0x7fff)
		    0x8000,                     // PNIO_UINT32     Subslot;             // subslot number (1..0x7fff)
		   	   {IOD_CFG_VENDOR_ID,      // PNIO_UINT16     VendorId;            // VendorIDHigh, VendorIDLow
		   		PROFIP_ORDERNUMBER,		// PNIO_UINT8      OrderId [20];        // Order_ID, visible, must be 20 bytes here (fill with blanks)
		   	   IOD_CFG_IM0_SERIAL_NUMBER, // PNIO_UINT8    SerNum  [16];        // IM_Serial_Number, visible string, must be 16 bytes here (fill with blanks)
			   PROFIP_HW_REVISION,      // PNIO_UINT16     HwRevision;          // IM_Hardware_Revision
			   {PROFIP_VERSION_PREFIX,  // PNIO_UINT8      SwRevPrefix;         // software revision prefix
			   PROFIP_VERSION_MAJOR,    // PNIO_UINT8      SwRevFuncEnhanc;     // IM_SW_Revision_Funct_Enhancement
			   PROFIP_VERSION_MINOR ,   // PNIO_UINT8      SwRevBugFix;         // IM_SW_Revision_Bug_Fix
			   PROFIP_VERSION_PATCH},   // PNIO_UINT8      SwRevInternChange;   // IM_SW_Revision_Internal_Change
			   0,                       // PNIO_UINT16     Revcnt;              // IM_Revision_Counter, notifies a hw modification
			   0x00,                    // PNIO_UINT16     ProfId;              // IM_Profile_ID, see Profile Guideline I&M functions
			   0x05,                    // PNIO_UINT16     ProfSpecTyp;         // IM_Profile_Spec_Type (e.g. 5="interface module")
			   0x01,                    // PNIO_UINT8      VersMaj;             // IM_Version major
			   0x01,                    // PNIO_UINT8      VersMin;             // IM_Version minor (must be 1, do not change !!)
#if IOD_INCLUDE_IM5
				PNIO_SUPPORT_IM12345    // PNIO_UINT16     ImSupported;         // IM_Supported, IM1...5 supported (bit list, bit 1...bit5, here: 0 ==> IM1..5  supported)
#else
				PNIO_SUPPORT_IM1234     // PNIO_UINT16     ImSupported;         // IM_Supported, IM1...4 supported (bit list, bit 1...bit4, here: 0 ==> IM1..4  supported)
#endif
			    }},
		// * --------------------------------------------
		// * IM0 Data for the slot 0 port 1
		// * (optional)
		// * --------------------------------------------
		   {0,                          // PNIO_UINT32     Api;                 // api number
			0,                          // PNIO_UINT32     Slot;                // slot number (1..0x7fff)
			0x8001,                     // PNIO_UINT32     Subslot;             // subslot number (1..0x7fff)
			   {IOD_CFG_VENDOR_ID,      // PNIO_UINT16     VendorId;            // VendorIDHigh, VendorIDLow
				PROFIP_ORDERNUMBER,		// PNIO_UINT8      OrderId [20];        // Order_ID, visible, must be 20 bytes here (fill with blanks)
			   IOD_CFG_IM0_SERIAL_NUMBER,  // PNIO_UINT8   SerNum  [16];        // IM_Serial_Number, visible string, must be 16 bytes here (fill with blanks)
			   PROFIP_HW_REVISION,      // PNIO_UINT16     HwRevision;          // IM_Hardware_Revision
			   {PROFIP_VERSION_PREFIX,  // PNIO_UINT8      SwRevPrefix;         // software revision prefix
			   PROFIP_VERSION_MAJOR,    // PNIO_UINT8      SwRevFuncEnhanc;     // IM_SW_Revision_Funct_Enhancement
			   PROFIP_VERSION_MINOR ,   // PNIO_UINT8      SwRevBugFix;         // IM_SW_Revision_Bug_Fix
			   PROFIP_VERSION_PATCH},   // PNIO_UINT8      SwRevInternChange;   // IM_SW_Revision_Internal_Change
			   0,                       // PNIO_UINT16     Revcnt;              // IM_Revision_Counter, notifies a hw modification
			   0x00,                    // PNIO_UINT16     ProfId;              // IM_Profile_ID, see Profile Guideline I&M functions
			   0x05,                    // PNIO_UINT16     ProfSpecTyp;         // IM_Profile_Spec_Type (e.g. 5="interface module")
			   0x01,                    // PNIO_UINT8      VersMaj;             // IM_Version major
			   0x01,                    // PNIO_UINT8      VersMin;             // IM_Version minor (must be 1, do not change !!)
#if IOD_INCLUDE_IM5
				PNIO_SUPPORT_IM12345    // PNIO_UINT16     ImSupported;         // IM_Supported, IM1...5 supported (bit list, bit 1...bit5, here: 0 ==> IM1..5  supported)
#else
				PNIO_SUPPORT_IM1234     // PNIO_UINT16     ImSupported;         // IM_Supported, IM1...4 supported (bit list, bit 1...bit4, here: 0 ==> IM1..4  supported)
#endif
			    }},
#if (IOD_CFG_PDEV_NUMOF_PORTS >= 2)
		// * --------------------------------------------
		// * IM0 Data for the slot 0 port 2
		// * (optional)
		// * --------------------------------------------
		   {0,                          // PNIO_UINT32     Api;                 // api number
			0,                          // PNIO_UINT32     Slot;                // slot number (1..0x7fff)
			0x8002,                     // PNIO_UINT32     Subslot;             // subslot number (1..0x7fff)
			   {IOD_CFG_VENDOR_ID,      // PNIO_UINT16     VendorId;            // VendorIDHigh, VendorIDLow
				PROFIP_ORDERNUMBER,		// PNIO_UINT8      OrderId [20];        // Order_ID, visible, must be 20 bytes here (fill with blanks)
			   IOD_CFG_IM0_SERIAL_NUMBER,  // PNIO_UINT8   SerNum  [16];        // IM_Serial_Number, visible string, must be 16 bytes here (fill with blanks)
			   PROFIP_HW_REVISION,      // PNIO_UINT16     HwRevision;          // IM_Hardware_Revision
			   {PROFIP_VERSION_PREFIX,  // PNIO_UINT8      SwRevPrefix;         // software revision prefix
			   PROFIP_VERSION_MAJOR,    // PNIO_UINT8      SwRevFuncEnhanc;     // IM_SW_Revision_Funct_Enhancement
			   PROFIP_VERSION_MINOR ,   // PNIO_UINT8      SwRevBugFix;         // IM_SW_Revision_Bug_Fix
			   PROFIP_VERSION_PATCH},   // PNIO_UINT8      SwRevInternChange;   // IM_SW_Revision_Internal_Change
			   0,                       // PNIO_UINT16     Revcnt;              // IM_Revision_Counter, notifies a hw modification
			   0x00,                    // PNIO_UINT16     ProfId;              // IM_Profile_ID, see Profile Guideline I&M functions
			   0x05,                    // PNIO_UINT16     ProfSpecTyp;         // IM_Profile_Spec_Type (e.g. 5="interface module")
			   0x01,                    // PNIO_UINT8      VersMaj;             // IM_Version major
			   0x01,                    // PNIO_UINT8      VersMin;             // IM_Version minor (must be 1, do not change !!)
#if IOD_INCLUDE_IM5
				PNIO_SUPPORT_IM12345    // PNIO_UINT16     ImSupported;         // IM_Supported, IM1...5 supported (bit list, bit 1...bit5, here: 0 ==> IM1..5  supported)
#else
				PNIO_SUPPORT_IM1234     // PNIO_UINT16     ImSupported;         // IM_Supported, IM1...4 supported (bit list, bit 1...bit4, here: 0 ==> IM1..4  supported)
#endif
			    }},
#endif
            // * --------------------------------------------
            // * IM0 Data for the slot 1 (CAS)
            // * (optional)
            // * --------------------------------------------
           {0,                          // PNIO_UINT32     Api;                 // api number
            1,                          // PNIO_UINT32     Slot;                // slot number (1..0x7fff)
            1,                          // PNIO_UINT32     Subslot;             // subslot number (1..0x7fff)
               {IOD_CFG_VENDOR_ID,      // PNIO_UINT16     VendorId;            // VendorIDHigh, VendorIDLow
            	PROFIP_ORDERNUMBER,		// PNIO_UINT8      OrderId [20];        // Order_ID, visible, must be 20 bytes here (fill with blanks)
                IOD_CFG_IM0_SERIAL_NUMBER,  // PNIO_UINT8  SerNum  [16];        // IM_Serial_Number, visible string, must be 16 bytes here (fill with blanks)
				PROFIP_HW_REVISION,     // PNIO_UINT16     HwRevision;          // IM_Hardware_Revision
               {PROFIP_VERSION_PREFIX,  // PNIO_UINT8      SwRevPrefix;         // software revision prefix
                PROFIP_VERSION_MAJOR,   // PNIO_UINT8      SwRevFuncEnhanc;     // IM_SW_Revision_Funct_Enhancement
				PROFIP_VERSION_MINOR,   // PNIO_UINT8      SwRevBugFix;         // IM_SW_Revision_Bug_Fix
				PROFIP_VERSION_PATCH},  // PNIO_UINT8      SwRevInternChange;   // IM_SW_Revision_Internal_Change
                0x01,                   // PNIO_UINT16     Revcnt;              // IM_Revision_Counter, notifies a hw modification
                0x00,                   // PNIO_UINT16     ProfId;              // IM_Profile_ID, see Profile Guideline I&M functions (default: 0)
                0x03,                   // PNIO_UINT16     ProfSpecTyp;         // IM_Profile_Spec_Type (e.g. 3="io module")
                0x01,                   // PNIO_UINT8      VersMaj;             // IM_Version major (must be 1, do not change !!)
                0x01,                   // PNIO_UINT8      VersMin;             // IM_Version minor (must be 1, do not change !!)
				PNIO_SUPPORT_IM1234     // PNIO_UINT16     ImSupported;         // IM_Supported, IM1...4 supported (bit list, bit 1...bit4, here: 0 ==> IM1..4  supported)
			    }},
           // * --------------------------------------------
           // * IM0 Data for the slot 2 (Diag)
           // * (optional)
           // * --------------------------------------------
         {0,                          // PNIO_UINT32     Api;                 // api number
           2,                          // PNIO_UINT32     Slot;                // slot number (1..0x7fff)
           1,                          // PNIO_UINT32     Subslot;             // subslot number (1..0x7fff)
              {IOD_CFG_VENDOR_ID,      // PNIO_UINT16     VendorId;            // VendorIDHigh, VendorIDLow
               PROFIP_ORDERNUMBER,     // PNIO_UINT8      OrderId [20];        // Order_ID, visible, must be 20 bytes here (fill with blanks)
               IOD_CFG_IM0_SERIAL_NUMBER, // PNIO_UINT8   SerNum  [16];        // IM_Serial_Number, visible string, must be 16 bytes here (fill with blanks)
			   PROFIP_HW_REVISION,     // PNIO_UINT16     HwRevision;          // IM_Hardware_Revision
              {PROFIP_VERSION_PREFIX,  // PNIO_UINT8      SwRevPrefix;         // software revision prefix
               PROFIP_VERSION_MAJOR,   // PNIO_UINT8      SwRevFuncEnhanc;     // IM_SW_Revision_Funct_Enhancement
			   PROFIP_VERSION_MINOR,   // PNIO_UINT8      SwRevBugFix;         // IM_SW_Revision_Bug_Fix
			   PROFIP_VERSION_PATCH},  // PNIO_UINT8      SwRevInternChange;   // IM_SW_Revision_Internal_Change
               0x01,                   // PNIO_UINT16     Revcnt;              // IM_Revision_Counter, notifies a hw modification
               0x00,                   // PNIO_UINT16     ProfId;              // IM_Profile_ID, see Profile Guideline I&M functions (default: 0)
               0x03,                   // PNIO_UINT16     ProfSpecTyp;         // IM_Profile_Spec_Type (e.g. 3="io module")
               0x01,                   // PNIO_UINT8      VersMaj;             // IM_Version major (must be 1, do not change !!)
               0x01,                   // PNIO_UINT8      VersMin;             // IM_Version minor (must be 1, do not change !!)
			   PNIO_SUPPORT_IM1234     // PNIO_UINT16     ImSupported;         // IM_Supported, IM1...4 supported (bit list, bit 1...bit4, here: 0 ==> IM1..4  supported)
			   }},
    };

    static void profip_print_devname(void) {
        PrintDeviceName();
    }

    struct profip_console_cmd_t
    {
    	char* name;
    	void (*action)();
    	char* desc;
    };

    static struct profip_console_cmd_t console_cmd_list[] =
    {
    	{
    		.name = "version",
    		.action = PrintDevkitVersion,
    		.desc = "print device version"
    	},
    	{
    		.name = "mfip version",
    		.action = profip_print_mfip_version,
    		.desc = "print masterfip version"
    	},
    	{
    		.name = "tcpflash",
    		.action = TcpUtilFlashFirmware,
    		.desc = "download firmware via TCP and store into flash"
    	},
    	{
    		.name = "submodules",
    		.action = PrintSubmodules,
    		.desc = "print plugged submodules"
    	},
    	{
    		.name = "name",
    		.action = profip_print_devname,
    		.desc = "print device name"
    	},
    	{
    		.name = "name set",
    		.action = InputAndStoreDeviceName,
    		.desc = "set device name and store in flash"
    	},
    	{
    		.name = "mac",
    		.action = PrintMacAddress,
    		.desc = "print MAC address"
    	},
    	{
    		.name = "mac set",
    		.action = InputAndStoreMacAddress,
    		.desc = "set MAC address and store in flash"
    	},
    	{
    		.name = "ip",
    		.action = PrintIpAddress,
    		.desc = "print IP address"
    	},
    	{
    		.name = "ip set",
    		.action = InputAndStoreIpAddress,
    		.desc = "set IP address and store into flash"
    	},
    	{
    		.name = "errors",
    		.action = pf_err_print,
    		.desc = "print ProFip errors",
    	},
    	{
    		.name = "status",
    		.action = profip_print_status,
    		.desc = "print ProFip status"
    	},
    	{
    		.name = "mcycle",
    		.action = profip_print_mcycle,
    		.desc = "print Macrocycle"
    	},
    	{
    		.name = "slots",
    		.action = profip_print_slots,
    		.desc = "print information about all slots"
    	},
    	{
    		.name = "nodes data",
    		.action = profip_print_nf_nodes,
    		.desc = "print data from all nf nodes",
    	},
    	{
    		.name = "presence",
    		.action = profip_print_presence,
    		.desc = "print presence list",
    	},
    	{
    		.name = "alarms",
    		.action = profip_print_alarms,
    		.desc = "print active alarms",
    	},
    	{
    		.name = "reset mcycle",
    		.action = profip_reset_masterFip,
    		.desc = "restart MasterFip"
    	},
    	{
    		.name = "trace buffer",
    		.action = SaveTraceBuffer,
    		.desc = "print memory trace buffer on console"
    	},
    	{
    		.name = "apdu",
    		.action = PrintLastApdu,
    		.desc = "print last remote APDU state"
    	},
    	{
    		.name = "performance",
    		.action = (void (*)())ExecutePerformanceMeasure,
    		.desc = "measure system performance"
    	},
    	{
    		.name = "meminfo",
    		.action = OsPrintMemInfo,
    		.desc = "print mem info"
    	},
    	{
    		.name = "threads",
    		.action = OsPrintThreads,
    		.desc = "print thread info"
    	},
    	{
    		.name = "reboot",
    		.action = OsReboot,
    		.desc = "perform System Reboot"
    	},
    	{
    		.name = "?",
    		.action = PrintHelp,
    		.desc = "print help"
    	},
    };

    static int pf_cmp(char *str1, char *str2, int size)
    {
    	int i, score=0;
    	for(i = 0; i<size; i++)
    	{
    		if(str1[i] == str2[i])
    		{
    			score++;
    		}
    	}

    	// min 60%
    	return (score * 5 > size *3);
    }

    static int aresimilar(char *str1, int str1size, char *str2, int str2size)
    {
    	int i;
    	char* smallerStr, *biggerStr;
    	int smallerSize, biggerSize;

    	if(str1size > str2size)
    	{
    		biggerStr = str1;
    		biggerSize = str1size;
    		smallerStr = str2;
    		smallerSize = str2size;
    	}
    	else
    	{
    		biggerStr = str2;
    		biggerSize = str2size;
    		smallerStr = str1;
    		smallerSize = str1size;
    	}

		for(i=0; i<biggerSize - smallerSize + 1; i++)
		{
			if(pf_cmp(smallerStr, biggerStr+i, smallerSize))
			{
				return 1;
			}
		}

		return 0;
    }

    // *----------------------------------------------------------------*
    // *
    // *  PrintHelp (void)
    // *
    // *----------------------------------------------------------------*
    static void PrintHelp (void)
    {
    	int i,j;
    	// *---------------------------------------
        // *      help
        // *---------------------------------------

        PROFIP_LOG ("\nCOMMAND LIST:\n");

		for(i = 0; i<ARRAYSIZE(console_cmd_list); i++)
		{
			PROFIP_LOG("%s", console_cmd_list[i].name);

			if(strlen(console_cmd_list[i].name) >= 20)
			{
				PROFIP_LOG(" ");
			}
			else
			{
				for(j=0; j<(20 - strlen(console_cmd_list[i].name)); j++)
				{
					PROFIP_LOG(" ");
				}
			}

			PROFIP_LOG("%s\n", console_cmd_list[i].desc);
		}
    }

    static void PrintLastApdu (void)
    {
    	PNIO_ConsolePrintf ("last remote APDU State (AR=1) = 0x%x\n", LastRemoteApduStatus);
    }

 // *----------------------------------------------------------------*
 // *
 // *  MainAppl(void)
 // *
 // *----------------------------------------------------------------*
 // *
 // *  main application function
 // *   - starts the pnio stack
 // *   - starts user interface task
 // *   - handles user inputs and starts the selected test functions
 // *
 // *  Input:    argc            not used yet
 // *            argv            not used yet
 // *  Output:   ----
 // *
 // *----------------------------------------------------------------*
    void MainAppl (void)

    {
        PNIO_UINT32 Status = PNIO_OK;
        PNIO_UINT32 exitAppl = PNIO_FALSE;
        uint8_t cmd[CMD_MAX_LEN];
        int i;

        // *-----------------------------------------------------
        // *     set startup parameter for the device
        // *     Note: in this simple example we suppose,
        // *     the DAP has no MRP capability.
        // *     If MPR shall be supported, the PN controller must
        // *     be capabable to send an MRP configuration record,
        // *     even if MRP is not activated.
        // *     More info to this topic see example App1_Standard,
        // *     file usriod_main.c
        // *-----------------------------------------------------
        PnUsrDev.VendorId            = IOD_CFG_VENDOR_ID;                       // Vendor ID, requested from PROFIBUS/PROFINET organization (PI)
        PnUsrDev.DeviceId            = IOD_CFG_DEVICE_ID;                       // Device ID, must be unique inside one Vendor ID
        PnUsrDev.MaxNumOfSubslots    = IOD_CFG_NUMOF_SUBSLOTS;          // maximum number of subslots per slot, managable by PN Stack
        PnUsrDev.pDevType            = IOD_CFG_DEVICE_TYPE;             // see also GSDML file, product family

        // **** startup the PN stack ****
        // *-----------------------------------------------------------
        // *     setup device stack and plug all io modules
        // *     (number of plugged modules <= IOD_CFG_NUMOF_SUBSLOTS)
        // *-----------------------------------------------------------
        //lint -e{866} Unusual use of '+' in argument to sizeof
		Status = PnUsr_DeviceSetup (&PnUsrDev,                                  // device specific configuration
                                    &IoSubList[0],                              // list of plugged submodules (real configuration), including PDEV
                                    sizeof (IoSubList) / sizeof (IoSubList[0]), // number of entries in IoSubList
                                    &Im0List[0],                                // list of IM0 data sets
                                    sizeof (Im0List) / sizeof (Im0List[0]));     // number of entries in Im0List

	    // *----------------------------------------------------------
	    // * print startup result message
	    // *----------------------------------------------------------
        if (Status == PNIO_OK)
        {
            PROFIP_LOG ("##SYSTEM STARTUP FINISHED, OK\n");
        }
        else
        	PROFIP_LOG ("##ERROR AT SYSTEM STARTUP\n");

        // Init wdg
        profip_wdg_init();

        // Set device name (if not set)
        if (PrintDeviceName() == 0) {
            ParamStoreDeviceName( (uint8_t*)"profip", strlen("profip") );
        }

        // Init all gpios
        profip_gpio_init();

        // Init LEDs
        profip_led_init();

        // Init data uart
        data_uart_util_init();

        // Start profip translator thread
        if(PNIO_OK != profip_init())
        {
        	PROFIP_LOG ("##ERROR AT SYSTEM STARTUP\n");
        }

        #if (INCLUDE_LOGADAPT == 1)
		    OsWait_ms (1000);
		    LogAdaptInit ();    // &&&2do  for LOGADAPT
        #endif

        // *----------------------------------------------------------
	    // *endless loop: wait on key pressed
	    // *----------------------------------------------------------
	    while (exitAppl == PNIO_FALSE)
	    {
		    // *-----------------------------------
		    // * wait on key pressed by the user
		    // *-----------------------------------

	    	int cmd_len = OsKeyScanString( "\n", cmd, CMD_MAX_LEN );
	    	int found = 0;

	    	if(cmd_len > 0)
	    	{

				for(i = 0; i<ARRAYSIZE(console_cmd_list); i++)
				{
					if(memcmp(cmd, console_cmd_list[i].name, MAX(strlen(console_cmd_list[i].name), cmd_len)) == 0)
					{
						console_cmd_list[i].action();
						found = 1;
						break;
					}
				}

				if(found == 0)
				{
					PROFIP_LOG("invalid command: '%s'\n", cmd);

					for(i = 0; i<ARRAYSIZE(console_cmd_list); i++)
					{
						if(aresimilar((char*)cmd, (int)cmd_len, (char*)console_cmd_list[i].name, (int)strlen(console_cmd_list[i].name)))
						{
							if(found == 0)
							{
								PROFIP_LOG("similar commands:", cmd);
							}

							PROFIP_LOG(" '%s'", console_cmd_list[i].name);
							found = 1;
						}
					}

					if(found == 1)
					{
						PROFIP_LOG("\n");
					}

					PROFIP_LOG("press '?' to print all available commands\n");
				}
	    	}
	    }
    }


    // *----------------------------------------------------------------*
    // *
    // *  PnUsr_cbf_IoDatXch (void)
    // *
    // *----------------------------------------------------------------*
    // *  cyclic exchange of IO data
    // *
    // *  This function performs the cyclic IO data exchange
    // *  Every IO data exchange (one data read and one data write)
    // *  it is called from the PNIO stack.
    // *
    // *----------------------------------------------------------------*
    // *  Input:    ----
    // *  Output:   ----
    // *
    // *----------------------------------------------------------------*
    PNUSR_CODE_FAST PNIO_BOOL PnUsr_cbf_IoDatXch (void)
    {
        volatile PNIO_UINT32 Status;

        // *---------------------------------------------------
        // *  read output data from PNIO controller
        // *---------------------------------------------------
        Status = PNIO_initiate_data_read  (PNIO_SINGLE_DEVICE_HNDL);                    // output data
        LastRemoteApduStatus = PNIO_get_last_apdu_status (PNIO_SINGLE_DEVICE_HNDL, 1);  // read last APDU state from first AR

        switch (Status)
        {
	        case PNIO_OK:
		         break;
	        case PNIO_NOT_OK:
		         break;
	        default:
	        	PROFIP_LOG ("Error 0x%x at PNIO_initiate_data_read()\n", Status);
		         break;
        }

        // *---------------------------------------------------
        // *  implement user software here
        // *  (e.g. write physical outputs)
        // *---------------------------------------------------

        // *---------------------------------------------------
        // *  send input data to PNIO controller
        // *---------------------------------------------------
        if (Status != PNIO_NOT_OK)
            Status = PNIO_initiate_data_write (PNIO_SINGLE_DEVICE_HNDL);        // input data
        switch (Status)
        {
	        case PNIO_OK:
		         break;
	        case PNIO_NOT_OK:
		         break;
	        default:
	        	PROFIP_LOG ("Error 0x%x at PNIO_initiate_data_write()\n", Status);
		         break;
        }


        return (Status);
    }
