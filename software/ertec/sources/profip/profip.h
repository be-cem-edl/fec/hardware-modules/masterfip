// SPDX-FileCopyrightText: 2023 CERN (home.cern)
//
// SPDX-License-Identifier: LGPL-2.1-or-later

/**
 * @file profip.h
 * @author Piotr Klasa (piotr.klasa@cerh.ch)
 * @brief ProFip Translator
 * @version 0.1
 * @date 2022-06-16
 * 
 * The ProFip Translator module implements:
 *  - receiving initial parameters for MasterFip network
 *    from PLC
 *  - main ProFIP state machine
 *  - MasterFip programming
 *  - deleting/sending variables from/to PLC from/to MasterFip
 *  - reporting alarms
 */

#ifndef PROFIP_H
#define PROFIP_H

#include <stdint.h>

#include "pnio_types.h"
#include "pnioerrx.h"

/**
 * @brief Initialize ProFip translator
 * 
 * @return PNIO_UINT32 
 */
PNIO_UINT32 profip_init(void);

/**
 * Notification to ProFip that all initial parameters from Profinet received
 * It shall be called from PN stack ("ready for input update" callback).
*/
void profip_set_wf_config_done(void);

/**
 * @brief Notification for ProFip that Profinet is now disconnected
*/
void profip_pn_disconnected(void);

/**
 * @brief copies data for subslot from ProFip translator to Profinet
 * @returns iops - I/O provider status
*/
uint8_t profip_copy_pf_to_pnStack(uint32_t slot, uint8_t* buff, uint32_t size);

/**
 * @brief copies data for subslot from Profinet to ProFip translator
 * @return iocs - I/O consumer status
*/
uint8_t profip_copy_pnStack_to_pf(uint32_t slot, uint8_t* buff, uint32_t size);

/**
 * @brief Initialize all ProFip static variables
*/
uint8_t profip_pn_var_init(void);

/**
 * @brief Writes record to ProFip
*/
int profip_write_record(uint32_t slot, uint32_t index, uint8_t* buff, uint32_t size);

/**
 * @brief Reads record from ProFIP
*/
int profip_read_record(uint32_t slot, uint32_t index, uint8_t* buff, uint32_t size);

/**
 * @brief Prints information about macrocycle
 */
void profip_print_mcycle(void);

/**
 * @brief Prints information about ProFip and MasterFip
 */
void profip_print_status(void);

/**
 * @brief Prints information about active alarms
 */
void profip_print_alarms(void);

/**
 * @brief Prints presence list
 */
void profip_print_presence(void);

/**
 * @brief Prints data from all nanoFIP nodes
 */
void profip_print_nf_nodes(void);

/**
 * @brief Prints information about slots
 */
void profip_print_slots(void);

/**
 * @brief Prints masterfip version
 */
void profip_print_mfip_version(void);

/**
 * @brief Resets masterFip
 */
void profip_reset_masterFip(void);

#endif
