// SPDX-FileCopyrightText: 2023 CERN (home.cern)
//
// SPDX-License-Identifier: LGPL-2.1-or-later

#include "profip_log.h"
#include "compiler.h"
#include "usriod_cfg.h"
#include "usrapp_cfg.h"   // example application spec. configuration settings

    #include "pniousrd.h"
    #include "iodapi_event.h"
    #include "os.h"
    #include "usriod_im_func.h"
    #include "nv_data.h"
    #include "bspadapt.h"
#include "cas.h"
    #include "PnUsr_Api.h"
    #include "usriod_PE.h"
    #include "usriod_utils.h"
	#include "profip.h"
	#include "profip_bsp.h"
	#include "pf_diag.h"
	#include "pf_err.h"
	#include "pf_alarm.h"

	// *=======================================================
    // *  defines
    // *=======================================================
    #define DEMO_RECORD  "ABCDEFGH"

    // *=======================================================
    // *  extern  functions
    // *=======================================================

    // *=======================================================
    // *  extern  data
    // *=======================================================

    // *=======================================================
    // *  static  data
    // *=======================================================

    // *=======================================================
    // *  public data
    // *=======================================================

    // ***** asynchronous record rd/wr only
    USR_REC_READ_RQ   PnioRecReadRq;
    USR_REC_WRITE_RQ  PnioRecWriteRq;

    // *** AR properties

    // *----------------------------------------------------------------*
    // *
    // *  PnUsr_cbf_iodapi_event_varinit (void)
    // *
    // *  initialize static variables (must be called first during startup )
    // *----------------------------------------------------------------*
    //lint -e{832, 578} Parameter 'Symbol' not explicitly declared, int assumed
    void PnUsr_cbf_iodapi_event_varinit (PNIO_SUB_LIST_ENTRY* pIoSubList,
    									 PNIO_UINT32          NumOfSubListEntries)
    {
        //init status & diagnostics
        cas_init();

        //init all profip modules
        profip_pn_var_init();
    }


    // *****************************************************************************************
    // *************************** read and write input data ***********************************
    // *****************************************************************************************

    // *----------------------------------------------------------------*
    // *
    // *  PNIO_cbf_data_write (...)
    // *
    // *----------------------------------------------------------------*
    // *
    // *  Passes the input data from the application to the stack.
    // *  The application reads the data from the specified input module
    // *  and handles them to the stack.
    // *  function UsrReadInputData() is called by the pnio stack.
    // *
    // *  Input:	   PNIO_UINT32    DevHndl,		// [in]  Device Handle
    // *			   PNIO_DEV_ADDR  *pAddr,		// [in]  location (slot, subslot)
    // *			   PNIO_UINT32    data_len,	    // [in] data length
    // *			   PNIO_UINT8     *pBuffer      // [in,out] pointer to the input data
    // *			   PNIO_UINT8     Iocs			// [in]  (io controller) consumer status
    // *
    // *	Output:	   return   io-provider-status  // PNIO_S_GOOD, PNIO_S_BAD
    // *
    // *
    // *----------------------------------------------------------------*
    PNIO_IOXS    PNIO_cbf_data_write
		    (PNIO_UINT32	DevHndl,		// [in]  device handle
		     PNIO_DEV_ADDR	*pAddr,			// geographical or logical address
		     PNIO_UINT32 	BufLen,			// [in]	 length of the submodule input data
		     PNIO_UINT8     *pBuffer, 		// [in,out] Ptr to data buffer to write to
		     PNIO_IOXS	    Iocs            // [in]  remote (io controller) consumer status
            )
    {
        PNIO_UINT32 slot_num	= pAddr->Geo.Slot;

	    PNIO_UNUSED_ARG (DevHndl);

        if(slot_num == 1) //control & status
        {
        	cas_copy_to_pnStack(pBuffer, BufLen);
        }
        else if(slot_num == 2) // diagnostics
        {
            pf_diag_copy_to_pnStack(pBuffer, BufLen);
        }
        else if(slot_num >= 3) // nanoFIP node
        {
            profip_copy_pf_to_pnStack(slot_num, pBuffer, BufLen);
        }

        return PNIO_S_GOOD;//Iops;	// return local provider state
	
    }

    // *----------------------------------------------------------------*
    // *
    // *  PNIO_cbf_data_read (...)
    // *
    // *----------------------------------------------------------------*
    // *
    // *  Passes the output data from the stack to the application.
    // *  The application takes the data and writes them to the specified
    // *  output module.
    // *  function UsrWriteOutputData() is called by the pnio stack.
    // *
    // *  Input:
    // *			   PNIO_UINT32    DevHndl       // [in] device handle
    // *			   PNIO_DEV_ADDR  *pAddr,		// [in]  location (slot, subslot)
    // *			   PNIO_UINT32    data_len,	    // [in] data length
    // *			   PNIO_UINT8*    pBuffer       // [in] pointer to the input data
    // *			   PNIO_UINT8     IoPs			// [in]  (io controller) provider status
    // *
    // *	Output:	   return   io-consumer-status  // IOCS_STATE_GOOD, IOCS_STATE_BAD
    // *
    // *
    // *----------------------------------------------------------------*
    PNIO_IOXS     PNIO_cbf_data_read
		    (PNIO_UINT32	DevHndl,		// [in]  device handle
		     PNIO_DEV_ADDR	*pAddr,			// [in]  geographical or logical address
		     PNIO_UINT32 	BufLen,			// [in]  length of the submodule input data
		     PNIO_UINT8*    pBuffer, 		// [in]  Ptr to data buffer to read from
		     PNIO_IOXS	    Iops            // [in]  (io controller) provider status
            )
    {

        PNIO_UINT32 slot_num	= pAddr->Geo.Slot;

        PNIO_UNUSED_ARG (DevHndl);

        if (slot_num == 1) //control & status
        {
        	cas_copy_from_pnStack(pBuffer, BufLen);
        }
        else if(slot_num == 2) //diagnostic
        {
        	//do nth
        }
        else if(slot_num >= 3) //nanoFIP nodes
        {
            profip_copy_pnStack_to_pf(slot_num, pBuffer, BufLen);
        }

        return PNIO_S_GOOD;//Iocs;	// consumer state (of local io device)
    }


	/**
	 *  @brief Update only IOxS for write
	 *
	 *  @param[in]      DevHndl         Device handle
	 *  @param[in]      *pAddr          Geographical or logical address
	 *  @param[in]      Iocs            Remote consumer status
	 *
	 *  @return                         Local provider status
	 *
	 *  Write without actual update of data
	 *
	 */
    PNIO_IOXS    PNIO_cbf_data_write_IOxS_only
		    (PNIO_UINT32	DevHndl,
		     PNIO_DEV_ADDR	*pAddr,
		     PNIO_IOXS	    Iocs
            )
    {
        PNIO_UNUSED_ARG (DevHndl);

        return PNIO_S_GOOD;	// provider state (of local io device)
    }


	/**
	 *  @brief Update only IOxS for read
	 *
	 *  @param[in]      DevHndl         Device handle
	 *  @param[in]      *pAddr          Geographical or logical address
	 *  @param[in]      Iops            Remote provider status
	 *
	 *  @return                         Local consumer status
	 *
	 *  Read without actual update of data
	 *
	 */
    PNIO_IOXS     PNIO_cbf_data_read_IOxS_only
		    (PNIO_UINT32	DevHndl,
		     PNIO_DEV_ADDR	*pAddr,
		     PNIO_IOXS	    Iops
            )
    {
        PNIO_UNUSED_ARG (DevHndl);

        return PNIO_S_GOOD;	// consumer state (of local io device)
    }


    // ****************************************************************************************
    // ******************* connect indication and ownership indication  ***********************
    // ****************************************************************************************


    // *----------------------------------------------------------------*
    // *
    // *  PNIO_cbf_ar_connect_ind (pAR)
    // *
    // *----------------------------------------------------------------*
    // *
    // *  The AR Info Indication of a module
    // *
    // *    Input:	   PNIO_UINT32    pAr;		// ID of the Application Relation
    // *    Output:	   ---
    // *
    // *
    // *----------------------------------------------------------------*
    void PNIO_cbf_ar_connect_ind
	    (
	        PNIO_UINT32	    DevHndl,	        // [in] handle for a multidevice
	        PNIO_AR_TYPE    ArType,             // [in] type of AR (see cm_ar_type_enum)
	        PNIO_UINT32     ArNum,              // [in] AR number  (device access: ArNumber = 3)
	        PNIO_UINT16     ArSessionKey,       // [in] AS session key
            PNIO_UINT16     SendClock,          // [in] sendclock
            PNIO_UINT16     RedRatioIocrIn,     // [in] reduction ratio of input IOCR
            PNIO_UINT16     RedRatioIocrOut,    // [in] reduction ratio of output IOCR
            PNIO_UINT32     HostIp              // [in] ip address of host ( PN-Controller )
        )
    {
        PNIO_UINT32 ReportHostIP = HostIp;
        PNIO_UINT8* u8ReportHostIP = (PNIO_UINT8*) &ReportHostIP;

	    PNIO_UNUSED_ARG (DevHndl);
        NumOfAr++;      // increment number of running ARs

	    PNIO_printf ( (PNIO_INT8*) "##CONNECT_IND AR=%d AR type=%d sendclock=%d, reduction_ratio_in=%d, reduction_ratio_out=%d, sessionKey=%d, hostIP=%03d.%03d.%03d.%03d\n",
                      ArNum,
			          ArType,
                      SendClock,
                      RedRatioIocrIn, RedRatioIocrOut,
                      ArSessionKey, 
                      *(u8ReportHostIP + 0), *(u8ReportHostIP + 1), *(u8ReportHostIP + 2), *(u8ReportHostIP + 3));
    }


    // *----------------------------------------------------------------*
    // *
    // *  PNIO_cbf_ar_ownership_ind (pAR)
    // *
    // *----------------------------------------------------------------*
    // *
    // *  The AR ownership Indication of a module
    // *
    // *  Input:   PNIO_UINT32 pAr;		                ID of the Application Relation
    // *
    // *  Output:  PNIO_EXP*   pOwnSub->OwnSessionKey   set to 0 only if ownership is rejected
    // *			                                    else keep unchanged
    // *                       pOwnSub->IsWrongSubmod   set to PNIO_TRUE only if wrong module,
    // *			                                    else keep unchanged
    // *
    // *----------------------------------------------------------------*
    PNIO_VOID   PNIO_cbf_ar_ownership_ind
		    (
			    PNIO_UINT32		DevHndl,
                PNIO_UINT32     ArNum,          // AR number 1....NumOfAR
			    PNIO_EXP*   	pOwnSub     	// [in] expected configuration in ownership indication
            )
    {
	    PNIO_UINT32 i, Status;
	    PNIO_DEV_ADDR Addr;

	    PNIO_UNUSED_ARG (DevHndl);

	    PNIO_printf ( (PNIO_INT8*) "##OWNERSHIP_IND AR = %d number of submodules = %d\n",
                     ArNum, pOwnSub->NumOfPluggedSub);

        for (i = 0; i < pOwnSub->NumOfPluggedSub; i++)
        {
            PNIO_printf ( (PNIO_INT8*) "  Api=%d Slot=%d Sub=%d ModID=%d SubID=%d OwnSessKey=%d isWrong=%d\n",
                 pOwnSub->Sub[i].ApiNum,
                 pOwnSub->Sub[i].SlotNum,
                 pOwnSub->Sub[i].SubNum,
                 pOwnSub->Sub[i].ModIdent,
                 pOwnSub->Sub[i].SubIdent,
                 pOwnSub->Sub[i].OwnSessionKey,
                 pOwnSub->Sub[i].IsWrongSubmod
                );

                /* Accept all submodules defined by PLC configuration*/
			   if(pOwnSub->Sub[i].IsWrongSubmod)
			   {

					Addr.Geo.Slot		= pOwnSub->Sub[i].SlotNum;			// slot 1
					Addr.Geo.Subslot	= pOwnSub->Sub[i].SubNum;			// subslot 1

					/* Calling PNIO_sub_pull in context of PNIO_cbf_ar_ownership_ind is allowed
					@see: Interface_Description_PN_IO_DevKits_V4.0.7.pdf:41 (3.3.2) */
					Status = PNIO_sub_pull (PNIO_SINGLE_DEVICE_HNDL,
										   PNIO_DEFAULT_API,
										&Addr);			// location (slot, subslot)

					PROFIP_LOG(__OK__"Plugged submodule %d of module %d. Data length: in[%d] out[%d]\n", \
							pOwnSub->Sub[i].SubIdent, pOwnSub->Sub[i].ModIdent, \
							pOwnSub->Sub[i].In.data_length, pOwnSub->Sub[i].Out.data_length);

					/* Calling PNIO_sub_plug in context of PNIO_cbf_ar_ownership_ind is allowed
					@see: Interface_Description_PN_IO_DevKits_V4.0.7.pdf:41 (3.3.2) */
					Status = PNIO_sub_plug (PNIO_SINGLE_DEVICE_HNDL,
										   PNIO_DEFAULT_API,
										   &Addr,			                 // location (slot, subslot)
										   pOwnSub->Sub[i].ModIdent,
										   pOwnSub->Sub[i].SubIdent,	     // submodule identifier
										   pOwnSub->Sub[i].In.data_length,   // submodule input data length
										   pOwnSub->Sub[i].Out.data_length,  // submodule output data length
										   PNIO_IM0_SUBMODULE,               // Submodule supports IM0
										   (IM0_DATA*)NULL,
										   PNIO_S_GOOD);                     // initial iops value, only for submodules without io data


					pOwnSub->Sub[i].IsWrongSubmod = PNIO_FALSE;

					if (Status != PNIO_OK)
					{
						PNIO_ConsolePrintf ("Error %x occured\n", PNIO_get_last_error ());
					}
			   }

        }  // end for (i = 0; i < pOwnSub->NumOfPluggedSub...
    }



    // *----------------------------------------------------------------*
    // *
    // *  PNIO_cbf_param_end_ind ()
    // *
    // *----------------------------------------------------------------*
    // *
    // *  The PNIO controller has sent an "PARAM END" event
    // *
    // *  Input:	DevHndl        Device handle
    // *            ArNum          AR number (1...N)
    // *            SessionKey     session key
    // *            Api            API (valid only, if SubslotNum <> 0)
    // *            SlotNum        SlotNum (valid only, if SubslotNum <> 0)
    // *            SubslotNum     == 0:    param end for all submodules,
    // *                           <> 0:    param end only for this submodule
    // *            MoreFollows    PNIO_TRUE: more param end ind follow, PNIO_FALSE: last one
    // *  Output:   return         PNIO_SUBMOD_STATE_RUN               module works properly and provides valid data, stack will create appl-ready now
    // *                           PNIO_SUBMOD_STATE_STOP              module has problem and can't provide valid data, stack will create appl-ready now (mod-diffblock entry)
    // *                           PNIO_SUBMOD_STATE_APPL_RDY_FOLLOWS  module parameterization is still running, application will notify appl-ready later to stack
    // *
    // *----------------------------------------------------------------*
    PNIO_SUBMOD_STATE  PNIO_cbf_param_end_ind (PNIO_UINT32 DevHndl,
                                         PNIO_UINT16 ArNum,
                                         PNIO_UINT16 SessionKey,
                                         PNIO_UINT32 Api,
                                         PNIO_UINT16 SlotNum,
                                         PNIO_UINT16 SubslotNum,
                                         PNIO_BOOL   MoreFollows)

    {
        return (PNIO_SUBMOD_STATE_RUN);	    // system generates automatically "application ready"-telegram
    }


    // *----------------------------------------------------------------*
    // *
    // *  PNIO_cbf_ready_for_input_update_ind ()
    // *
    // *----------------------------------------------------------------*
    // *
    // *  The PNIO controller has sent an "READY FOR INPUT UPDATE" event
    // *
    // *  NOTE:    Strongly avoid a reentrant call of UsrDbai_initiate_data_write()
    // *           or UsrDbai_initiate_data_read(), also PNIO_initiate_data_read()
    // *           or PNIO_initiate_data_write().
    // *
    // *  Input:	DevHndl        Device handle
    // *            ArNum          AR number (1...N)
    // *            InpUpdState    input update state (AR_STARTUP or AR_INDATA )
    // *  Output:   ---
    // *
    // *----------------------------------------------------------------*
    PNIO_VOID  PNIO_cbf_ready_for_input_update_ind (PNIO_UINT32 DevHndl,
                                                    PNIO_UINT16 ArNum,
                                                    PNIO_INP_UPDATE_STATE InpUpdState)
    {
        PNIO_UINT32 Status = PNIO_OK;


        profip_set_wf_config_done();
	    // *-----------------------------------------------------------------------
        // *    perform a first data exchange, to set IOPS and IOCS to STATE_GOOD
        // *    necessary to set IO controller state to RUN without error.
	    // *-----------------------------------------------------------------------
		Status =  PNIO_initiate_data_read (DevHndl);

		{ // Example: APDU status can read here
			PNIO_UINT32 ApduStatus = PNIO_get_last_apdu_status (PNIO_SINGLE_DEVICE_HNDL, ArNum);
			PNIO_UNUSED_ARG(ApduStatus);
		} /*lint !e438 last value assigned is not used */

		if (PNIO_OK == Status)
		{
			Status =  PNIO_initiate_data_write (DevHndl);
			if (PNIO_OK != Status)
			{
				PNIO_printf ( "##Error IO Data Exchange in PNIO_cbf_ready_for_input_update_ind()\n");
			}
		}

        PnUsr_ActivateIoDatXch();

	    // *-----------------------------------------------------------------------
        // *    notify, if first "readyForInputUpdate" after AR start or
        // *    because of replugging a submodule.
	    // *-----------------------------------------------------------------------
        if (InpUpdState == PNIO_AR_STARTUP)
        {
    		PNIO_printf ( "##READY FOR INPUT UPDATE DURING AR_STARTUP ARnum=%d\n", ArNum);
        }
        else
        {
    		PNIO_printf ( "##READY FOR INPUT UPDATE DURING AR INDATA  ARnum=%d\n", ArNum);
        }
    }


    // *----------------------------------------------------------------*
    // *
    // *  PNIO_cbf_ar_indata_ind ()
    // *
    // *----------------------------------------------------------------*
    // *
    // *  The pnio stack notifies the user, that after start of data
    // *  exchange  a first set of input data have been received. That means,
    // *  actual input data are valid.
    // *
    // *  Input:	DevHndl        Device handle
    // *            ArNum          AR number (1...N)
    // *            SessionKey     0  (not used here)
    // *  Output:   ---
    // *
    // *----------------------------------------------------------------*
    void PNIO_cbf_ar_indata_ind (PNIO_UINT32 DevHndl,
                                 PNIO_UINT16 ArNum,
                                 PNIO_UINT16 SessionKey)
    {
	    PNIO_UNUSED_ARG (DevHndl);
	    PNIO_printf ( (PNIO_INT8*) "##AR IN-Data event indication received, ArNum = %xh, Session = %xh\n",
				      ArNum, SessionKey);
    }



    // *----------------------------------------------------------------*
    // *
    // *  PNIO_cbf_ar_disconn_ind ()
    // *
    // *----------------------------------------------------------------*
    // *
    // *  The Device is offline. Reason code is provided by the context
    // *  manager of the pnio stack.
    // *
    // *  Input:	DevHndl        Device handle
    // *            ArNum          AR number (1...N)
    // *            SessionKey     0  (not used here)
    // *            ReasonCode	   reason code (see PNIO_AR_REASON)
    // *  Output:   ---
    // *
    // *----------------------------------------------------------------*
    void PNIO_cbf_ar_disconn_ind	 (PNIO_UINT32	 DevHndl,
                                      PNIO_UINT16    ArNum,
                                      PNIO_UINT16    SessionKey,
								      PNIO_AR_REASON ReasonCode)
    {
	    PNIO_UNUSED_ARG (DevHndl);
	    PNIO_printf ( (PNIO_INT8*) "##AR Offline indication ,ArNum = %xh, Session = %xh, Reason = %xh\n",
		              ArNum, SessionKey, ReasonCode);

        if (NumOfAr)
            NumOfAr--;      // decrement number of running ARs

        pf_alarm_ar_disconnected();
        profip_pn_disconnected();
        // if (NumOfAr == 0)
        //    done by stack      Bsp_EbSetLed (EB_LED_BUS_FAULT,  1);   // no more ARs running, set red LED,
    }



    // *****************************************************************************************
    // *************************** ALARM INDICATIONS  ******************************************
    // *****************************************************************************************
    void    PNIO_cbf_dev_alarm_ind (PNIO_UINT32			DevHndl,
								    PNIO_DEV_ALARM_DATA	*pAlarm)
    {
	    PNIO_printf ( (PNIO_INT8*) "##APDU alarm indication, Dev=%d, Slot=%d, Subslot=%d, AR=%d, Sess=%d\n",
                    DevHndl,
                    pAlarm->SlotNum,
                    pAlarm->SubslotNum,
                    pAlarm->ArNum,
                    pAlarm->SessionKey
                );

        PNIO_printf ( (PNIO_INT8*) "  Prio=%xh, Type=%xh, DiagCH = %d, DiagGen=%d, Tag=%d, DatLen=%d\n",
                    pAlarm->AlarmPriority,
                    pAlarm->AlarmType,
                    pAlarm->DiagChannelAvailable,
                    pAlarm->DiagGenericAvailable,
                    pAlarm->UserStructIdent,
                    pAlarm->UserAlarmDataLength
                );
    }


    // *****************************************************************************************
    // ************************  Request callback function  ************************
    // *****************************************************************************************
    void PNIO_cbf_async_req_done   (PNIO_UINT32     DevHndl,       // device handle
                                    PNIO_UINT32     ArNum,         // AR number (1...N)
                                    PNIO_ALARM_TYPE AlarmType,     // alarm type
                                    PNIO_UINT32     Api,           // API number
                                    PNIO_DEV_ADDR   *pAddr,        // location (slot, subslot)
                                    PNIO_UINT32     Status,        // status
									PNIO_UINT16		Diag_tag)
    {
       PNIO_UNUSED_ARG (DevHndl);
    	if( 0 == ArNum )
    	{
    		PNIO_printf( "Asynchronous request not sent - no existing AR. Alarmtype=%d Api=%d Slot=%d Subslot=%d\n",
    				AlarmType, Api, pAddr->Geo.Slot, pAddr->Geo.Subslot);
    	}
    	else
    	{
       PNIO_printf ( (PNIO_INT8*) "Asynchronous request ArNum=%d Alarmtype=%d Api=%d Slot=%d Subslot=%d Status = %x\n",
		              ArNum, AlarmType, Api, pAddr->Geo.Slot, pAddr->Geo.Subslot, Status);
    	}

       pf_alarm_asynq_req_completed();
    }

    // *****************************************************************************************
    // *************************** RECORD INDICATIONS  *****************************************
    // *****************************************************************************************


    // *----------------------------------------------------------------*
    // *
    // *  PnUsr_cbf_rec_read
    // *
    // *----------------------------------------------------------------*
    // *
    // *----------------------------------------------------------------*
    PNIO_UINT32  PnUsr_cbf_rec_read
	(
		PNIO_UINT32			DevHndl,        // device handle
		PNIO_UINT32			Api,            // application process identifier
        PNIO_UINT16         ArNum,			// ar - number
        PNIO_UINT16 		SessionKey,	    // ar session number
		PNIO_UINT32			SequenceNum,    // CLRPC sequence number
		PNIO_DEV_ADDR		*pAddr,			// geographical or logical address
		PNIO_UINT32			RecordIndex,    // record index
		PNIO_UINT32			*pBufLen,		// [in, out] in: length to read, out: length, read by user
		PNIO_UINT8			*pBuffer,		// [in] buffer pointer
		PNIO_ERR_STAT		*pPnioState		// [out] 4 byte PNIOStatus (ErrCode, ErrDecode, ErrCode1, ErrCode2), see IEC61158-6
	)
    {
	    PNIO_UNUSED_ARG (DevHndl);
	    PNIO_UNUSED_ARG (Api);
	    PNIO_UNUSED_ARG (SessionKey);
	    PNIO_UNUSED_ARG (SequenceNum);

	    if(pAddr->Geo.Slot == 1 || pAddr->Geo.Slot == 2) //control & status || diagnostics
	   	{
	    	*pBufLen = pf_diag_read_record(RecordIndex, pBuffer, *pBufLen);
	   	}
		else if(pAddr->Geo.Slot >= 3) //NanoFIP node
		{
			int ret = profip_read_record(pAddr->Geo.Slot, RecordIndex, pBuffer, *pBufLen);

			if (ret)
				*pBufLen = 0;
		}

	    return PNIO_OK;
    }


    // *----------------------------------------------------------------*
    // *
    // *  PnUsr_cbf_rec_write ()
    // *
    // *----------------------------------------------------------------*
    // *
    // *----------------------------------------------------------------*
    PNIO_UINT32  PnUsr_cbf_rec_write
	(
		PNIO_UINT32			DevHndl,        // device handle
		PNIO_UINT32			Api,            // application process identifier
        PNIO_UINT16         ArNum,          // ar - number
        PNIO_UINT16 		SessionKey,	    // ar session number
		PNIO_UINT32			SequenceNum,    // CLRPC sequence number
		PNIO_DEV_ADDR		*pAddr,			// geographical or logical address
		PNIO_UINT32			RecordIndex,    // record index
		PNIO_UINT32			*pBufLen,   	// [in, out] in: length to write, out: length, written by user
		PNIO_UINT8			*pBuffer,		// [in] buffer pointer
		PNIO_ERR_STAT		*pPnioState		// 4 byte PNIOStatus (ErrCode, ErrDecode, ErrCode1, ErrCode2), see IEC61158-6
	)

    {
	    PNIO_UINT32     Status = PNIO_OK;
        int ret;

	    PNIO_UNUSED_ARG (DevHndl);
	    PNIO_UNUSED_ARG (Api);
	    PNIO_UNUSED_ARG (ArNum);
	    PNIO_UNUSED_ARG (SessionKey);
	    PNIO_UNUSED_ARG (SequenceNum);

	    if(pAddr->Geo.Slot == 1) //control & status
	    {
	    	ret = cas_write_record(RecordIndex, pBuffer, *pBufLen);
	    	Status = (ret == 0) ? PNIO_OK : PNIO_NOT_OK;
	    }
	    else if(pAddr->Geo.Slot == 2) //diagnostic
	    {
	    	Status = PNIO_NOT_OK;
	    }
	    else if(pAddr->Geo.Slot > 2) //NanoFIP node
	    {
	    	ret = profip_write_record(pAddr->Geo.Slot, RecordIndex, pBuffer, *pBufLen);
	    	Status = (ret == 0) ? PNIO_OK : PNIO_NOT_OK;
	    }
	    else
	    {
	    	Status = PNIO_NOT_OK;
	    }

        if (Status != PNIO_OK)
	    {	// *** if an error occured, it must be specify  according to IEC 61158-6
            pPnioState->ErrCode   = 0xdf;  // IODWriteRes with ErrorDecode = PNIORW
            pPnioState->ErrDecode = 0x80;  // PNIORW
            pPnioState->ErrCode1  = 0xb6;  // example: Error Class 11 = Access, ErrorNr 6 = "access denied"
            pPnioState->ErrCode2  = 0;     // here dont care
            pPnioState->AddValue1 = 0;     // here dont care
            pPnioState->AddValue2 = 0;     // here dont care
	    }

        return (Status);   // OK: function executed, NOT_OK: error occured
    }





    // *----------------------------------------------------------------*
    // *
    // *  PNIO_cbf_save_station_name ()
    // *
    // *----------------------------------------------------------------*
    // *
    // *  The DCP package of the pnio stack has got a "set station name"
    // *  request from the pnio controller and notifies the user by this
    // *  function.
    // *  The user has to save this name in a non volatile memory (for example
    // *  a nv-ram), if the Remanent-parameter is <> 0.
    // *
    // *  manager of the pnio stack.
    // *
    // *  Input:	PNIO_UINT8      *pStationName   name string,
    // * 			PNIO_UINT16     NameLength	   length of the name string
    // * 			PNIO_UINT8      Remanent        <> 0: save remanent
    // *
    // *
    // *  Output:   return          PNIO_OK: function completed successfully
    // *	                        PNIO_NOT_OK: function could not be completed
    // *----------------------------------------------------------------*
    PNIO_UINT32  PNIO_cbf_save_station_name
                        (	PNIO_INT8    *pStationName,
						    PNIO_UINT16   NameLength,
						    PNIO_UINT8   Remanent )
    {
       if (Remanent)
       {
          PNIO_printf((PNIO_INT8*) "##Save remanent: Station Name = %.*s  received, Length=%d  Remanent = %d\n",
              NameLength, pStationName, NameLength, Remanent);

          // **** store data in non volatile memory ****
          Bsp_nv_data_store (PNIO_NVDATA_DEVICENAME,    // nv data type: device name
                             pStationName,              // source pointer to the devicename
                             NameLength);               // length of the device name
       }

       else
       {
           PNIO_printf ( (PNIO_INT8*) "Station Name = %.*s  received, Length=%d  Remanent = %d\n",
	                     NameLength, pStationName, NameLength, Remanent);

           Bsp_nv_data_store (PNIO_NVDATA_DEVICENAME,    // nv data type: device name
                              "",                        // source pointer to the devicename
                              0);               // length of the device name
       }

       // *-----------------------------------------------------------------*
       // * return PNIO_OK:     name modification is accepted by application
       // *        PNIO_NOT_OK: name modification is denied   by application
       // *-----------------------------------------------------------------*
       return (PNIO_OK);
    }



    // *----------------------------------------------------------------*
    // *
    // *  PNIO_cbf_save_ip_addr()
    // *
    // *----------------------------------------------------------------*
    // *
    // *  The DCP package of the pnio stack has got a "set ip address"
    // *  request from the pnio controller and notifies the user by this
    // *  function.
    // *  The user has to save the ip address, subnet mask and default router
    // *  address in a non volatile memory (for example a nv-ram), if the
    // *  Remanent-parameter is <> 0.
    // *
    // *  manager of the pnio stack.
    // *
    // *  Input:	PNIO_UINT32      NewIpAddr      new ip addres
    // * 			PNIO_UINT32      SubnetMask     Subnet mask
    // * 			PNIO_UINT32      DefRouterAddr  default gateway
    // * 			PNIO_UINT8       Remanent       <> 0: save remanent,
    // *                                            == 0: do not save remanent
    // *
    // *
    // *  Output:   return		    PNIO_OK: function completed successfully
    // *	                        PNIO_NOT_OK: function could not be completed
    // *
    // *----------------------------------------------------------------*
    PNIO_UINT32 PNIO_cbf_save_ip_addr
                                 (  PNIO_UINT32 NewIpAddr,
                                    PNIO_UINT32 SubnetMask,
                                    PNIO_UINT32 DefRouterAddr,
                                    PNIO_UINT8  Remanent)
    {
       NV_IP_SUITE  *IpSuite;

       OsAllocFX( (void **) &IpSuite, sizeof(NV_IP_SUITE), 0);

       if (Remanent)
       {
          PNIO_printf ( (PNIO_INT8*) "##Remanent address was stored \n");

          // **** save ip suite in non volatile memory ****
          IpSuite->IpAddr     = NewIpAddr;
          IpSuite->SubnetMask = SubnetMask;
          IpSuite->DefRouter  = DefRouterAddr;
          Bsp_nv_data_store (PNIO_NVDATA_IPSUITE,      // nv data type: ip suite
                             IpSuite,                  // source pointer
                             sizeof (NV_IP_SUITE));               // length of the device name

       }
       else
       {
            // *-----------------------------------------------------
            // *  according to the PNIO SPEC an already (remanent)
            // *  stored IP address must be deleted, if a new ip address
            // *  is set non remanent.
            // *-----------------------------------------------------
            IpSuite->IpAddr     = 0;
            IpSuite->SubnetMask = 0;
            IpSuite->DefRouter  = 0;
            Bsp_nv_data_store (PNIO_NVDATA_IPSUITE,       // nv data type: ip suite
                               IpSuite,                  // source pointer
                               sizeof (NV_IP_SUITE));     // length of the device name

            PNIO_printf ( (PNIO_INT8*) "Remanent address was deleted \n");
       }

       OsFree(IpSuite);

       return (PNIO_OK);

    }

    /**
     *  @brief there is new ip data
     *
     *
     *  information about new ip, mask and gateway
     *	only printed to console to show the data
     *
     */
    PNIO_UINT32 PNIO_cbf_report_new_ip_addr( PNIO_UINT32 NewIpAddr,
    								    	 PNIO_UINT32 SubnetMask,
											 PNIO_UINT32 DefRouterAddr)
    {
        PNIO_UINT32 ReportIpSuite = NewIpAddr;
        PNIO_UINT32 ReportSubnetMask = SubnetMask;
        PNIO_UINT32 ReportDefRouterAddr = DefRouterAddr;

        PNIO_UINT8* u8ReportIpSuite = (PNIO_UINT8*) &ReportIpSuite;
        PNIO_UINT8* u8ReportSubnetMask = (PNIO_UINT8*) &ReportSubnetMask;
        PNIO_UINT8* u8ReportDefRouterAddr = (PNIO_UINT8*) &ReportDefRouterAddr;

    	PNIO_printf("IP address = %03d.%03d.%03d.%03d, Subnet mask = %03d.%03d.%03d.%03d, Default router = %03d.%03d.%03d.%03d\n", 
            *(u8ReportIpSuite + 0), *(u8ReportIpSuite + 1), *(u8ReportIpSuite + 2), *(u8ReportIpSuite + 3), 
            *(u8ReportSubnetMask + 0), *(u8ReportSubnetMask + 1), *(u8ReportSubnetMask + 2), *(u8ReportSubnetMask + 3),
            *(u8ReportDefRouterAddr + 0), *(u8ReportDefRouterAddr + 1), *(u8ReportDefRouterAddr + 2), *(u8ReportDefRouterAddr + 3));

    	return (PNIO_OK);
    }

    // *----------------------------------------------------------------*
    // *
    // *  PNIO_cbf_start_led_blink ()
    // *
    // *----------------------------------------------------------------*
    // *
    // *  Start blinking with LED after DCP Request (received from the
    // *  engineering tool
    // *  function.
    // *  The user has to start blinking with the specified LED
    // *
    // *  Input:		PNIO_UINT32    DevHndl     Device handle
    // *                PNIO_UINT32    PortNum     port number  1..N
    // *                PNIO_UINT32    frequency   blinking frequency
    // *  Output:       return         must be PNIO_OK
    // *----------------------------------------------------------------*
    PNIO_UINT32 PNIO_cbf_start_led_blink (PNIO_UINT32 DevHndl,
                                          PNIO_UINT32 PortNum,
                                          PNIO_UINT32 frequency)
    {
        PNIO_UNUSED_ARG (DevHndl);
        PNIO_printf ( (PNIO_INT8*) "##LED Blink START, Port = %d, frequency = %d Hz \n", PortNum, frequency);
        profip_bsp_start_led_blinking(frequency);
        return (PNIO_OK);    // must be PNIO_OK, other's not possible
    }



    // *----------------------------------------------------------------*
    // *
    // *  PNIO_cbf_stop_led_blink ()
    // *
    // *----------------------------------------------------------------*
    // *  Stop blinking with LED (started after DCP Request)
    // *
    // *  Input:		PNIO_UINT32    DevHndl     Device handle
    // *                PNIO_UINT32    PortNum     port number  1..N
    // *  Output:       return         must be PNIO_OK
    // *----------------------------------------------------------------*
    PNIO_UINT32 PNIO_cbf_stop_led_blink (PNIO_UINT32 DevHndl,
                                         PNIO_UINT32 PortNum)
    {
        PNIO_UNUSED_ARG (DevHndl);
        PNIO_printf ( (PNIO_INT8*) "##LED Blink STOP, Port = %d\n", PortNum);
        profip_bsp_stop_led_blinking();
        return (PNIO_OK);    // must be PNIO_OK, other's not possible
    }


    // *----------------------------------------------------------------*
    // *
    // *  PNIO_cbf_reset_factory_settings ()
    // *
    // *----------------------------------------------------------------*
    // *  advises the application, to reset to factory settings
    // *
    // *  Input:	PNIO_UINT32     DevHndl     Device handle
    // *  Input:	PNIO_RTF_OPTION RtfOption   specifies, which data have
    // *                                        to be resetted
    // *  Output:   return          must be PNIO_OK
    // *----------------------------------------------------------------*
    PNIO_UINT32 PNIO_cbf_reset_factory_settings (PNIO_UINT32 DevHndl, PNIO_RTF_OPTION RtfOption)
    {
        PNIO_UNUSED_ARG (DevHndl);

        Bsp_nv_data_clear (RtfOption);

        return (PNIO_OK);    // must be PNIO_OK, other's not possible

    }


    #if IOD_INCLUDE_REC8028_8029
        // **** special case:   read record index 0x8029  (read output data)
        PNIO_IOXS    PNIO_cbf_substval_out_read  // read substitution values for output submodule
                (
                 PNIO_UINT32    DevHndl,            // [in] Handle for Multidevice
                 PNIO_DEV_ADDR  *pAddr,             // [in] geographical or logical address
                 PNIO_UINT32    BufLen,             // [in] length of the submodule output substitute data
                 PNIO_UINT8     *pBuffer,           // [in] Ptr to submodule output substitute data
                 PNIO_UINT16*   pSubstMode,         // [in, out - BIG ENDIAN] SubstitutionMode: 0=ZERO or inactive (default), 1:LastValue, 2:Replacement value SubstitutionMode: 0=ZERO or inactive, 1:LastValue, 2:Replacement value
                 PNIO_UINT16*   pSubstActive        // [in, out - BIG ENDIAN] SubstituteActiveFlag:  0=operation, 1=substitute. default value is 0: if (IOPS & IOCS = GOOD), else: 1
                )
        {

           // *** set the substitute data  ***
           OsMemSet (pBuffer, 0xab, BufLen);    // example: all substitute data are 0xab (the user will change that...)

           // *** *pSubstMode = 2;      // we accept the preset value from the stack (set mode to "replace"), so nothing to do here"
           //*pSubstMode = 2;

           // *** we accept the preset value from stack:  "0 = Operation", if (remoteIOPS == GOOD and localIOCS == GOOD),  else "1 = Substitute"
           //*pSubstActive = REC8029_SUBSTSTATE_OPERATION;

           // we assume, the output data set is valid
           return (PNIO_S_GOOD);
        }
    #endif

/**
 * @brief User functionality as reaction to new ARFSU
 *
 * @param[in]		  ARFSU_enabled      		PNIO_ARFSU_ENABLED, PNIO_ARFSU_DISABLED
 * @param[in]         ARFSU_changed        		PNIO_ARFSU_CHANGED, PNIO_ARFSU_NOT_CHANGED
 *
 * @return            void
 *
 * 	This function is called when ARFSU write record is recieved
 * 	Function is ready for user functionality
 * 	Informs user if ARFSU UUID was changed or not -> if parameterization was changed
 *
 */
PNIO_VOID PNIO_cbf_report_ARFSU_record(
		PNIO_UINT8			ARFSU_enabled,
		PNIO_UINT8			ARFSU_changed
		)
{
	if( PNIO_ARFSU_CHANGED == ARFSU_changed )
	{
		PNIO_printf( "New ARFSU UUID -> parameterization of device was changed\n" );
	}
}


// *----------------------------------------------------------------*
// *
// *  PNIO_cbf_new_plug_ind ()
// *
// *----------------------------------------------------------------*
// *
// *  The Plug Indication of a module
// *
// *    Input:	   PNIO_DEV_ADDR      *pAddr	        Module address
// *               PNIO_UINT32         InputDataLen	    submodule input data length
// *               PNIO_UINT32         OutputDataLen	submodule output data length
// *    Output:	   ---
// *
// *
// *----------------------------------------------------------------*
PNIO_VOID PNIO_cbf_new_plug_ind
(
    PNIO_DEV_ADDR       *pAddr,
    PNIO_UINT32          InputDataLen,
    PNIO_UINT32          OutputDataLen
)
{
    //TODO - inform applications
}

// *----------------------------------------------------------------*
// *
// *  PNIO_cbf_new_pull_ind ()
// *
// *----------------------------------------------------------------*
// *
// *  The Pull Indication of a module
// *
// *    Input:	   PNIO_DEV_ADDR      *pAddr	Module address
// *    Output:	   ---
// *
// *
// *----------------------------------------------------------------*
PNIO_VOID PNIO_cbf_new_pull_ind
(
    PNIO_DEV_ADDR       *pAddr         // [in] slot/subslot number
)
{
    //TODO - inform applications
}
