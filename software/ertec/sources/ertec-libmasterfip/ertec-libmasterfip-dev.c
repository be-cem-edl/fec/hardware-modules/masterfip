/*
 * Copyright (C) 2016-2017 CERN (www.cern.ch)
 * Author: Michel Arruat <michel.arruat@cern.ch>
 *
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <pthread.h>

#include "profip_log.h"
#include "masterfip-common-priv.h"
#include "ertec-libmasterfip-priv.h"
#include "ertec-libmasterfip.h"
#include "mqospi.h"
#include "mstrfip_ctrl.h"
#include "os.h"

static struct mstrfip_desc dev = {0};

struct mstrfip_dev *mstrfip_open(void)
{
    // init mqospi
    mqospi_init();

	/*
	 * Reference version for rt app. and fpga are injected at compile time
	 * and compred to ones get from HW to check any mismatch
	 * In case of mismatch the device is not opened.
	 */

	memset(&dev, 0, sizeof(dev));

    dev.ref_rt_version = MFIP_RT_VERSION;
    dev.ref_fpga_version = MFIP_FPGA_VERSION;

    /* init some fields */
    dev.ba.bitrate = MSTRFIP_BITRATE_UNDEFINED;
    dev.ba.state = MSTRFIP_FSM_INITIAL;

    PROFIP_LOG(__OK__"MasterFip opened\n");
    return (struct mstrfip_dev*) &dev;
}

void mstrfip_reset(void)
{
	int diag_started;
	int irq_started;

	mstrfip_stop_threads();
	mstrfip_ctrl_cpu_disable();

	diag_started = dev.ba.diag_started;
	irq_started = dev.ba.irq_started;

	memset(&dev, 0, sizeof(dev));

	dev.ba.diag_started = diag_started;
	dev.ba.irq_started = irq_started;

    dev.ref_rt_version = MFIP_RT_VERSION;
    dev.ref_fpga_version = MFIP_FPGA_VERSION;

    mstrfip_ctrl_cpu_enable();

	mqospi_reset();

    /* init some fields */
    dev.ba.bitrate = MSTRFIP_BITRATE_UNDEFINED;
    dev.ba.state = MSTRFIP_FSM_INITIAL;

    PROFIP_LOG(__OK__"MasterFip reseted\n");
}

void mstrfip_stop_threads(void)
{
	dev.ba.diag_run = 0U;
	dev.ba.irq_run = 0U;

	if(dev.ba.diag_started)
	{
		while(!dev.ba.diag_stopped)
		{
			OsWait_ms(10);
		}
		PROFIP_DEBUG("Diag thread stopped\n");
	}

	if(dev.ba.irq_started)
	{
		while(!dev.ba.irq_stopped)
		{
			OsWait_ms(10);
		}
		PROFIP_DEBUG("Irq thread stopped\n");
	}
}

struct mstrfip_desc *mstrfip_get(void)
{
    return &dev;
}

uint32_t mstrfip_get_turnaround_ustime(struct mstrfip_dev *dev)
{
	return ((struct mstrfip_desc*)dev)->ba.hw_cfg.turn_around_ustime;
}

static int mstrfip_print_pvw(int instr_idx)
{
	struct mstrfip_ba_instr* instr = &dev.mcycle.instrlist[instr_idx];
	int i=0;

	for(i=instr->per_var_wind.start_var_idx; i<instr->per_var_wind.stop_var_idx; i++)
	{
		struct mstrfip_data * var = dev.mcycle.per_vars.sorted_varlist[i];
		struct mstrfip_data_priv *pvar_priv = (struct mstrfip_data_priv *) var->priv;

		PROFIP_LOG("       |");

		if((pvar_priv->hdr.flags & 0x03) == 1)
		{
			PROFIP_LOG(" -> Prod");
		}
		else if((pvar_priv->hdr.flags & 0x03) == 2)
		{
			PROFIP_LOG(" <- Cons");
		}

		PROFIP_LOG(" Var 0x\e[0;32m%.2x\e[0;33m%.2x\e[0m", (var->id) >> 8, (var->id) & 0xFF);

		PROFIP_LOG(" %d bytes", pvar_priv->hdr.max_bsz);

		if((var->id) >> 8 == 127)
		{
			PROFIP_LOG(" DIAG");
		}

		if((pvar_priv->hdr.flags & 0x80) >> 7 != 0)
		{
			PROFIP_LOG(" +IRQ");
		}

		PROFIP_LOG("\n");
	}

	return compute_list_var_length(&dev.ba, &dev.mcycle, \
			instr->per_var_wind.start_var_idx, instr->per_var_wind.stop_var_idx);
}

void mstrfip_print(void)
{
	PROFIP_LOG("Macrocycle\n");
	uint32_t old_time, time = 0;

	for(int i=0; i< dev.mcycle.instr_count ; i++)
	{
		switch (dev.mcycle.instrlist[i].code)
		{
		case MSTRFIP_BA_PER_VAR_WIND:
			PROFIP_LOG("%6.3f +- Periodic Var Windows\n", 0.001 * time);
			old_time = time;
			time += mstrfip_print_pvw(i);
			PROFIP_LOG("       | %6.3f ms\n", 0.001 * (time - old_time));
			break;
		case MSTRFIP_BA_APER_VAR_WIND:
			PROFIP_LOG("%6.3f +- Aperiodic Var Windows\n", 0.001 * time);
			old_time = time;
			time = ticks_to_us(dev.mcycle.instrlist[i].aper_var_wind.ticks_end_time, MFIP_CPU_HZ);
			PROFIP_LOG("       | %6.3f ms\n", 0.001 * (time - old_time));
			break;
		case MSTRFIP_BA_WAIT_WIND:
			PROFIP_LOG("%6.3f +- Wait Windows\n", 0.001 * time);
			old_time = time;
			time = ticks_to_us(dev.mcycle.instrlist[i].wait_wind.ticks_end_time, MFIP_CPU_HZ);
			PROFIP_LOG("       | %6.3f ms\n", 0.001 * (time - old_time));
			break;
		default:
			break;
		}
	}
	PROFIP_LOG("%6.3f +- End\n", 0.001 * time);
}

extern void mstrfip_print_presence_list(void)
{
	int i,j,k;
	struct mstrfip_diag_shm* shm = mstrfip_diag_get();

	if(shm != NULL)
	{

		PROFIP_LOG ("     0  1  2  3  4  5  6  7  8  9  a  b  c  d  e  f\n");

		for(i=0; i<0x10; i++)
		{
			PROFIP_LOG("%3x ", i*16);

			for(k=0; k<2; k++)
			{
				for(j=0; j<8; j++)
				{
					if((shm->present_list[i*2+k] & (1 << j)) != 0)
					{
						PROFIP_LOG(" X ");
					}
					else
					{
						PROFIP_LOG(" . ");
					}
				}
			}
			PROFIP_LOG("\n");
		}
	}
}
