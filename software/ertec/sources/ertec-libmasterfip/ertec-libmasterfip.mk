# SPDX-FileCopyrightText: 2024 CERN (home.cern)
#
# SPDX-License-Identifier: LGPL-2.1-or-later

SRC_C += $(SRC_ROOT_DIR_APPL)/ertec-libmasterfip/ertec-libmasterfip-ba-config.c
SRC_C += $(SRC_ROOT_DIR_APPL)/ertec-libmasterfip/ertec-libmasterfip-ba-rules.c
SRC_C += $(SRC_ROOT_DIR_APPL)/ertec-libmasterfip/ertec-libmasterfip-ba-runtime-actions.c
SRC_C += $(SRC_ROOT_DIR_APPL)/ertec-libmasterfip/ertec-libmasterfip-dev.c
SRC_C += $(SRC_ROOT_DIR_APPL)/ertec-libmasterfip/ertec-libmasterfip-irq.c
SRC_C += $(SRC_ROOT_DIR_APPL)/ertec-libmasterfip/ertec-libmasterfip-macrocycle-actions.c
SRC_C += $(SRC_ROOT_DIR_APPL)/ertec-libmasterfip/ertec-libmasterfip-macrocycle-config.c
SRC_C += $(SRC_ROOT_DIR_APPL)/ertec-libmasterfip/ertec-libmasterfip-util.c
SRC_C += $(SRC_ROOT_DIR_APPL)/ertec-libmasterfip/ertec-libmasterfip-diag.c
	
INCD += -I$(SRC_ROOT_DIR_APPL)/ertec-libmasterfip/
