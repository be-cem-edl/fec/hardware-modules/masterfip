/*
 * Copyright (C) 2016-2017 CERN (www.cern.ch)
 * Author: Michel Arruat <michel.arruat@cern.ch>
 *
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#include <stdint.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <sched.h>
#include <sys/types.h>

#include "../profip/cas.h"
#include "profip_log.h"
#include "os.h"
#include "pnio_types.h"
#include "pnioerrx.h"
#include "os_taskprio.h"

#include "mqospi.h"

#include "ertec-libmasterfip-priv.h"
#include "ertec-libmasterfip.h"
#include "pf_err.h"
#include "profip_wdg.h"

/**
 * masterFIP IRQ handler type
 */
typedef int (*mstrfip_irq_handler_t)(struct mstrfip_desc *mstrfip,
				     struct mstrfip_irq_entry_desc *irq_entry,
				     struct mstrfip_irq *irq);

/**
 * It returns the number of messages in a masterFIP Mock Turtle buffer
 * @param[in] trtl masterFIP Mock turtle buffer
 * @return The total number of messages in the buffer
 */
static unsigned int mstrfip_trtl_buf_msg_counter(struct mstrfip_mqospi_buf *trtl)
{
	struct mstrfip_acq_trtlmsg_desc *acq_trtlmsg;
	unsigned int count = 0;
	int i;

	for (i = 0; i < trtl->msg_count; ++i) {
		acq_trtlmsg = (struct mstrfip_acq_trtlmsg_desc *)
				trtl->msglist[i].data;
		count += acq_trtlmsg->nentries;
	}

	return count;
}

static void mstrfip_handle_error(struct mstrfip_desc *mstrfip, int errcode)
{
	pf_err(PF_ERR_MODULE_LIBMASTERFIP, errcode, 0U, "Mstrfip_handle_error\n");

	if (mstrfip->ba.sw_cfg.mstrfip_error_handler == NULL)
	{
		return; /* client didn't specify an error handler */
	}
	//mstrfip->ba.sw_cfg.mstrfip_error_handler((struct mstrfip_dev *)mstrfip,
	//					errcode);
}

static void mstrfip_get_irq_entry(struct mstrfip_desc *mstrfip,
				  struct mstrfip_mqospi_buf *trtl,
				  struct mstrfip_irq_entry_desc **irq_entry,
				  struct mstrfip_irq *irq)
{
	struct mstrfip_acq_trtlmsg_desc *acq_trtlmsg;
	struct mqospi_msg *trtlmsg;

	trtlmsg = &trtl->msglist[trtl->msg_count - 1];
	acq_trtlmsg = (struct mstrfip_acq_trtlmsg_desc *)trtlmsg->data;
	/* Check if this message carries an irq */
	if (acq_trtlmsg->irq_woffset == 0) {
		*irq_entry = NULL; // No irq entry found: set it to NULL
		return; // keep the message till an irq is raised
	}
	// the message carries an irq: points to the irq entry
	*irq_entry = (struct mstrfip_irq_entry_desc *)
			((uint32_t *)acq_trtlmsg + acq_trtlmsg->irq_woffset);
	if (trtl->msg_count != (*irq_entry)->trtlmsg_count) {
		PROFIP_ERR("wrong msg count %d != %d\n", trtl->msg_count, (*irq_entry)->trtlmsg_count);
		mstrfip_handle_error(mstrfip,
				     MSTRFIP_TRTL_MSG_COUNT_INCONSISTENCY);
	}
	irq->hw_sec = (*irq_entry)->hw_sec;
	/* HW CPU ticks are translated into ns */
	irq->hw_ns = ticks_to_ns((*irq_entry)->hw_ticks, MFIP_CPU_HZ);
	irq->irq_count = (*irq_entry)->irq_count;
}

static void  mstrfip_purge_trtl_hmq(struct mstrfip_desc *mstrfip)
{
	int i;

	/*
	 * try to read a maximum of trtl messages in once: we don't know how
	 * many messages are accumulated in the driver, but we can't read more
	 * than the remaining space in the msglist
	 */

	for (i = 0; i < MSTRFIP_HMQ_ACQ_SLOT_COUNT; ++i)
	{
		mqospi_purge(MSTRFIP_CPU0, i);
	}
}

static int mstrfip_read_trtlmsg(struct mstrfip_desc *mstrfip, int mq_slot,
			struct mstrfip_mqospi_buf *trtl)
{
	struct mqospi_msg *trtlmsg;
	int res;

	if (trtl->msg_count >= MSTRFIP_TRTL_BUF_MSGLIST_SIZE) {
		PROFIP_ERR("Too many mockturtle msg accumulated (%d) (%d)\n", mq_slot, trtl->msg_count);
		mstrfip_handle_error(mstrfip, MSTRFIP_TRTL_MSG_COUNT_OVERFLOW);
		return -1;
	}

	trtlmsg = &trtl->msglist[trtl->msg_count];
	/*
	 * try to read a maximum of trtl messages in once: we don't know how
	 * many messages are accumulated in the driver, but we can't read more
	 * than the remaining space in the msglist
	 * FIX introduces in LIB version 1.1.3
	 * Read one message at once to avoid packing of two messages containing
	 * both an irq entry. When interrupt rate is very high (interrupt on
	 * each variable for instance), mockturtle drive starts to accumulate
	 * messages and receive_n function can return several messages.
	 * To avoid this problem messages are get one by one.
	 */
	res = mqospi_msg_async_recv(MSTRFIP_CPU0, mq_slot, trtlmsg, 1);

	if (res < 0) {
		PROFIP_ERR("msg recv failed\n");
		mstrfip_handle_error(mstrfip, MSTRFIP_TRTL_MSG_READ_ERR);
		return -1;
	}
	else if (res == 0) {
		PROFIP_ERR("no msg\n");
		mstrfip_handle_error(mstrfip, MSTRFIP_TRTL_MSG_READ_NULL);
		return -1;
	}
	/* no error, res is the number of trtl messages read */
	trtl->msg_count += res; // increment pending trtlmsg counter
	return 0;
}

static int mstrfip_handle_diag_per_var_irq(struct mstrfip_desc *mstrfip,
					   struct mstrfip_irq_entry_desc *irq_entry,
					   struct mstrfip_irq *irq)
{
	struct mstrfip_ba_diag *diag = &mstrfip->ba.mcycle->diag;
	struct mstrfip_data *pvar;

	irq->acq_count = 1; // diag var single acquisition
	/* locate the periodic var which raises the irq */
	pvar = (irq_entry->flags & MSTRFIP_DATA_FLAGS_PROD) ?
		diag->varlist[1] : diag->varlist[0];
	/* calls application's var handler */
	mstrfip_diag_var_handler((struct mstrfip_dev *)mstrfip, pvar, irq);
	/* trtlmsg have been consumed, reset trtlmsg count */
	diag->trtl.msg_count = 0;
	return 0;
}

static int mstrfip_handle_diag_aper_var_irq(struct mstrfip_desc *mstrfip,
					    struct mstrfip_irq_entry_desc *irq_entry,
					    struct mstrfip_irq *irq)
{
	struct mstrfip_ba_diag *diag = &mstrfip->ba.mcycle->diag;
	struct mstrfip_data *pvar = diag->ident_var;

	irq->acq_count = 1;// diag ident single acquisition
	/* calls application's var handler */
	mstrfip_diag_ident_var_handler((struct mstrfip_dev *)mstrfip,
				       pvar, irq);
	/* trtlmsg have been consumed, reset msg count */
	diag->trtl.msg_count = 0;

	return 0;
}

static int mstrfip_handle_app_aper_var_irq(struct mstrfip_desc *mstrfip,
				     struct mstrfip_irq_entry_desc *irq_entry,
				     struct mstrfip_irq *irq)
{
	struct mstrfip_ba_aper_var_set *aper_vars =
					&mstrfip->ba.mcycle->aper_vars;
	/* -1 to remove the IRQ message itself */
	irq->acq_count = mstrfip_trtl_buf_msg_counter(&aper_vars->trtl) - 1;

	/* calls application's var handler */
	if (aper_vars->mstrfip_ident_var_handler != NULL) {
		aper_vars->mstrfip_ident_var_handler((struct mstrfip_dev *)mstrfip,
						     irq);
	}
	else { // no callback registered
		PROFIP_ERR("no user callback registered to process (aper) %d %d\n", irq_entry->type, irq_entry->key);
		mstrfip_handle_error(mstrfip, MSTRFIP_BA_APER_VAR_NO_CB);
	}
	/* trtlmsg have been consumed in the callback, reset wrnmsg count */
	aper_vars->trtl.msg_count = 0;
	return 0;
}

static int mstrfip_handle_app_per_var_irq(struct mstrfip_desc *mstrfip,
					  struct mstrfip_irq_entry_desc *irq_entry,
					  struct mstrfip_irq *irq)
{
	struct mstrfip_ba_per_var_set *per_vars = &mstrfip->ba.mcycle->per_vars;
	struct mstrfip_data *pvar;
	struct mstrfip_data_priv *pvar_priv;

	/* locate the periodic var which raises the irq */
	pvar = (irq_entry->flags & MSTRFIP_DATA_FLAGS_PROD) ?
		per_vars->prod_varlist[irq_entry->key] :
		per_vars->cons_varlist[irq_entry->key];
	pvar_priv = (struct mstrfip_data_priv *)pvar->priv;

	/* -1 to remove the IRQ message itself */
	irq->acq_count = mstrfip_trtl_buf_msg_counter(&per_vars->trtl) - 1;

	/* calls application's var handler */
	if (pvar_priv->mstrfip_data_handler != NULL)
	{
		pvar_priv->mstrfip_data_handler((struct mstrfip_dev *)mstrfip,
						pvar, irq);
	}
	else
	{ // no callback registered
		PROFIP_ERR("no user callback registered to process\n");
		mstrfip_handle_error(mstrfip, MSTRFIP_BA_PER_VAR_NO_CB);
	}
	/* trtlmsg have been consumed in the callback, reset wrnmsg count */
	per_vars->trtl.msg_count = 0;
	return 0;
}

static int mstrfip_handle_app_aper_msg_irq(struct mstrfip_desc *mstrfip,
					   struct mstrfip_irq_entry_desc *irq_entry,
					   struct mstrfip_irq *irq)
{
	pf_err(PF_ERR_MODULE_MQOSPI, MSTRFIP_INT_ERR_APER_MSG, 0, "Aper msg\n");
	return 0;
}

/**
 * Lookup table for IRQ handlers (one for each slot)
 */
static const mstrfip_irq_handler_t mstrfip_irq_handler[] ={
		[MSTRFIP_HMQ_O_APP_PER_VAR] =  mstrfip_handle_app_per_var_irq,
		[MSTRFIP_HMQ_O_APP_APER_VAR] = mstrfip_handle_app_aper_var_irq,
		[MSTRFIP_HMQ_O_APP_APER_MSG] = mstrfip_handle_app_aper_msg_irq,
		[MSTRFIP_HMQ_O_DIAG_PER_VAR]= mstrfip_handle_diag_per_var_irq,
		[MSTRFIP_HMQ_O_DIAG_APER_VAR] = mstrfip_handle_diag_aper_var_irq,
};


static void mstrfip_irq_run(void)
{
	struct mstrfip_desc *mstrfip = mstrfip_get();

	int timeout_flag = 0, mq_slot, res, timeout;
	struct mstrfip_irq irq = {0};
	struct mstrfip_irq_entry_desc *irq_entry = NULL;
	struct mstrfip_mqospi_buf *trtl;
	struct mstrfip_mqospi_buf *mfip_irq_trtlbuf[MSTRFIP_HMQ_ACQ_SLOT_COUNT];
	static int irq_tick = 0;
	unsigned char new_msg = 0;
	uint64_t last_data_timestamp;

	struct pollmqospi trtlp[MSTRFIP_HMQ_ACQ_SLOT_COUNT];

    OsWaitOnEnable();

    profip_wdg_start(PROFIP_WDG_MODULE_MASTERFIP, 2000 /*ms*/);
    last_data_timestamp = OsGetTime_us();

    for(;;)
    {
    	if(!mstrfip->ba.irq_run)
    	{
    		profip_wdg_trigger(PROFIP_WDG_MODULE_MASTERFIP);
    		mstrfip->ba.irq_stopped = 1U;
    		OsWait_ms(4);
    		last_data_timestamp = OsGetTime_us();
    		continue;
    	}

    	PROFIP_LOG(__OK__"MasterFip Irq thread started\n");

    	mstrfip->ba.irq_stopped = 0U;

		memset(&irq, 0, sizeof(irq));
		irq_entry = NULL;
		irq_tick = 0;
		new_msg = 0;

		for (mq_slot = 0; mq_slot < MSTRFIP_HMQ_ACQ_SLOT_COUNT; ++mq_slot) {
			trtlp[mq_slot].events = MQOSPI_POLLIN;
			trtlp[mq_slot].revents = 0;
			trtlp[mq_slot].idx_hmq = mq_slot;
			trtlp[mq_slot].idx_cpu = MSTRFIP_CPU0;
		}

		// init mstrfip_trtl_buf lookup table
		mfip_irq_trtlbuf[MSTRFIP_HMQ_O_APP_PER_VAR] = &mstrfip->ba.mcycle->per_vars.trtl;
		mfip_irq_trtlbuf[MSTRFIP_HMQ_O_APP_APER_VAR] = &mstrfip->ba.mcycle->aper_vars.trtl;
		mfip_irq_trtlbuf[MSTRFIP_HMQ_O_APP_APER_MSG] = NULL;
		mfip_irq_trtlbuf[MSTRFIP_HMQ_O_DIAG_PER_VAR] = &mstrfip->ba.mcycle->diag.trtl;
		mfip_irq_trtlbuf[MSTRFIP_HMQ_O_DIAG_APER_VAR] = &mstrfip->ba.mcycle->diag.trtl;

		/*
		 * purge mock-turtle queue in case of remaining messages from previous
		 * macro-cycle. This may happen when application switch dynamically
		 * macro-cycle.
		 */
		mstrfip_purge_trtl_hmq(mstrfip);

		/*
		 * poll timeout = 10 % more than macrocycle. Add + 1ms for very short
		 * macrocycle less than 10ms to be sure to geta number bigger than
		 * macrocycle length. (not neccessary to use round for so simple
		 * computation)
		 */
		timeout = (int)(1.1 * (mstrfip->ba.mcycle->cycle_ustime)) + 1;

		for(;;)
		{

			profip_wdg_trigger(PROFIP_WDG_MODULE_MASTERFIP);

			if(new_msg == 1U)
			{
				new_msg = 0U;
			}
			else
			{
				OsWait_ms(1);
			}

			irq_tick++;
			if(irq_tick % 10 == 0)
			{
				cas_set_irq_thread_tick((irq_tick) / 10);
			}

			if(!mstrfip->ba.irq_run)
			{
				break;
			}
			else
			{
				/* timeout is equivalent to 1.1 macrocycle duration.
				* In addition, to avoid a first timeout when the macrocycle starts,
				* because it waits for external event for instance, the first timeout
				* is mulitplied by 2
				*/
				mqospi_poll(trtlp, MSTRFIP_HMQ_ACQ_SLOT_COUNT, timeout);

				// no error: loop throught all slots till nevt
				for (mq_slot = 0; mq_slot < MSTRFIP_HMQ_ACQ_SLOT_COUNT; mq_slot++)
				{

					if (!(trtlp[mq_slot].revents & MQOSPI_POLLIN))
						continue; // no event reported by this slot
					/* event reported: increment evt and process the event */

					new_msg = 1U;
					last_data_timestamp = OsGetTime_us();

					trtl = mfip_irq_trtlbuf[mq_slot];
					if (trtl == NULL)
					{
						pf_err(PF_ERR_MODULE_LIBMASTERFIP, MSTRFIP_INT_ERR_NO_PTR_TO_BUFFER, mq_slot, "No pointer to mqospi buffer\n");
						continue;
					}

					/* get the turtle message */
					//PROFIP_DEBUG("read message for slot %d %d \n", mq_slot, trtlp[mq_slot].revents);
					res = mstrfip_read_trtlmsg(mstrfip, mq_slot, trtl);
					if (res)
						continue;

					/* extract from the message the irq entry if any */
					mstrfip_get_irq_entry(mstrfip, trtl, &irq_entry, &irq);
					if (!irq_entry) //message doesn't carry an irq
						continue; // no need to wake_up the client

					mstrfip_irq_handler[mq_slot](mstrfip, irq_entry, &irq);
				}

				if(last_data_timestamp + timeout + 2000 < OsGetTime_us())
				{
					if(!timeout_flag)
					{
						pf_err(PF_ERR_MODULE_LIBMASTERFIP, MSTRFIP_INT_POLL_TIMEOUT, (OsGetTime_us() - last_data_timestamp), "Irq timeout\n");
						mstrfip_handle_error(mstrfip, MSTRFIP_POLL_TIMEOUT);
						timeout_flag = 1;
					}
				}
				else
				{
					timeout_flag = 0;
				}
			}
		}
    }

	return;
}

int mstrfip_irq_start(struct mstrfip_desc *mstrfip)
{
  	PNIO_UINT32   TskId_masterfip_irq, Status;
	
	if(mstrfip->ba.irq_run)
	{
		PROFIP_ERR(__ERR__"Irq thread is already running\n");
		return 1;
	}

	if(mstrfip->ba.irq_started)
	{
		mstrfip->ba.irq_run = 1;
		return 0;
	}

	Status = OsCreateThread (mstrfip_irq_run, (PNIO_UINT8*)"MasterFIP_Irq", TASK_PRIO_LIBMFIP_IRQ_LOW, &TskId_masterfip_irq);
	if(PNIO_OK != Status)
	{
		return -1;
	}

	Status = OsCreateMsgQueue (TskId_masterfip_irq); // install the task message queue
	if(PNIO_OK != Status)
	{
		return -2;
	}

	Status = OsStartThread (TskId_masterfip_irq); // start, after the task message queue has been installed
	if(PNIO_OK != Status)
	{
		return -3;
	}

	mstrfip->ba.irq_started = 1;
	mstrfip->ba.irq_run = 1;

	return 0;
}

int mstrfip_irq_stop(struct mstrfip_desc *mstrfip)
{
	if (mstrfip->ba.irq_run == 0)
		return 0;

	/* synchronize properly the thread */
	mstrfip->ba.irq_run = 0;

	/* irq thread is stopped, discard any pending message */
	mstrfip->ba.mcycle->per_vars.trtl.msg_count = 0;

	return 0;
}
