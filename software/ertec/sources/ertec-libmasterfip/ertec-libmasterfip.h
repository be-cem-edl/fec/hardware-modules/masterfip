/*
 * Copyright (C) 2016-2017 CERN (www.cern.ch)
 * Author: Michel Arruat <michel.arruat@cern.ch>
 *
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#ifndef __LIB_MASTERFIP_H
#define __LIB_MASTERFIP_H

#ifdef __cplusplus
extern "C" {
#endif

/** @file libmasterfip.h */
#include <stdint.h>

/** @file masterfip-common.h */
#include "masterfip-common.h"

#include "ertec-libmasterfip-diag.h"

/**
 * An opaque struct representing a fip device
 */
struct mstrfip_dev;

/**
 * An opaque struct representing a fip ba macrocycle
 */
struct mstrfip_macrocycle;

/**
 * @enum mstrfip_error_list
 * Master Fip errors
 */
/*
 * No overlap with errno (range [1..133] and trtl range[83630 ... 83635]
 */

#define MFIP_CPU_HZ (62500000)

#define MSTRFIP_ERROR_CODE_OFFSET 10000 // no overlap with errno and trtl error code
enum mstrfip_error_list {
	MSTRFIP_INVALID_ANSWER_ACK = MSTRFIP_ERROR_CODE_OFFSET,
	MSTRFIP_INVALID_BITRATE,
	MSTRFIP_INVALID_BA_INSTR_COUNT,
	MSTRFIP_BA_IS_NULL,
	MSTRFIP_BA_MAX_VAR,
	MSTRFIP_BA_INVALID_VAR_COUNT,
	MSTRFIP_BA_PER_WIND_CFG_BAD,
	MSTRFIP_BA_INVALID_INSTR_PARAM,
	MSTRFIP_BA_NOT_STOP_STATE,
	MSTRFIP_BA_INCONSISTENCY,
	MSTRFIP_BA_WRONG_CYCLE_LENGTH,
	MSTRFIP_BA_INCONSISTENT_INSTR_SET,
	MSTRFIP_BA_WAIT_WIND_INSTR_MISSING,
	/* error code corresponding to dynamic error messages */
	MSTRFIP_BA_INVALID_MACROCYCLE,
	MSTRFIP_INVALID_HW_VERS,
	MSTRFIP_TRTL_MSG_COUNT_INCONSISTENCY,
	MSTRFIP_TRTL_MSG_COUNT_OVERFLOW,
	MSTRFIP_TRTL_MSG_OVERFLOW,
	MSTRFIP_TRTL_MSG_READ_ERR,
	MSTRFIP_TRTL_MSG_READ_NULL,
	MSTRFIP_POLL_TIMEOUT,
	MSTRFIP_POLL_ERROR,
	MSTRFIP_BA_APER_VAR_NO_CB,
	MSTRFIP_BA_APER_MSG_NO_CB,
	MSTRFIP_BA_PER_VAR_NO_CB,
	/**/
	/* Internal error codes*/
	MSTRFIP_INT_ERR_APER_MSG = 10030,
	MSTRFIP_INT_ERR_APER_MSG_REFRESH,
	MSTRFIP_INT_ERR_NO_PTR_TO_BUFFER,
    MSTRFIP_INT_ERR_DIAG_IDENT_VAR_ERR_STATUS,
	MSTRFIP_INT_POLL_TIMEOUT,
	__MSTRFIP_MAX_ERROR_CODE,
};

/**
 * @enum mstrfip_data_type
 * All possible FIP data type.
 */
enum mstrfip_data_type {
	MSTRFIP_PER_VAR, /**< periodic variable */
	MSTRFIP_APER_VAR, /**< aperiodic variable */
	MSTRFIP_IDENT_VAR, /**< identification aperiodic variable */
	MSTRFIP_APER_MSG, /**< aperiodic message */
	MSTRFIP_APER_MSG_ACK, /**< aperiodic acknowledged message */
};

/**
 * @enum mstrfip_data_status
 * Define all possible status a FIP data can have
 */
enum mstrfip_data_status {
	MSTRFIP_DATA_OK, /**< valid FIP data */
	MSTRFIP_DATA_FRAME_ERROR, /**< frame fault: CRC, TMO, BAD_NBYTES,..*/
	MSTRFIP_DATA_PAYLOAD_ERROR, /**< payload fault: !freshed, !significant..*/
	MSTRFIP_DATA_NOT_RECEIVED, /**< no new data received since last reading */
};

/**
 * @enum mstrfip_payload_errors
 * bit mask to decode payload status. Periodic FIP data have a status to
 * qualify the payload validity.
 * If not_freshed and/or not_significant bit are raised, this indicates that
 * the producer of the data is not working properly.
 * not_prompt bit to be explaine!!!!!
 */
enum mstrfip_payload_errors {
	MSTRFIP_FRAME_PAYLOAD_OK	 	= 0,
	MSTRFIP_FRAME_PAYLOAD_NOT_REFRESH 	= (1 << 1),
	MSTRFIP_FRAME_PAYLOAD_NOT_SIGNIFICANT 	= (1 << 2),
	MSTRFIP_FRAME_PAYLOAD_NOT_PROMPT 	= (1 << 3),
};

/**
 * presence list definition
 */
struct mstrfip_presencelist {
	uint8_t *addrlist; /**< adress list of present nodes */
	uint8_t addr_count; /**< number of present node */
};

/**
 * macrocycle window definition
 * One can see a macro cycle as a concatenation of various windows specialized
 * for a given task:
 * - periodic variable window: to schedule periodic variables
 * - aperiodic variable window: to schedule aperiodic variables
 * - aperiodic message window: to schedule aperiodic message
 * - wait window: no activity or just padding frames.
 */
struct mstrfip_ba_cycle_wind_cfg {
	uint8_t type; /**< type of the window */
	uint32_t us_length; /**< theoretical window duration in us */
};

/**
 * macrocycle definition
 * collection of windows composing the macrocycle (@see
 * mstrfip_ba_cycle_wind_cfg).
 */
struct mstrfip_cycle_cfg {
	struct mstrfip_ba_cycle_wind_cfg *windlist; /**< list of windows */
	uint8_t	wind_count; /**< number of windows*/
	uint32_t us_cycle_length; /**< theoretical macrocyle duration in us */
};

struct mstrfip_sw_cfg {
	int irq_thread_prio;
	int diag_thread_prio;
	/**
	 * The masterfip library waits on event programmed by the application,
	 * and calls application's function registered with the event. The
	 * waiting will be aborted after the timeout has elapsed. By default
	 * this timeout is fixed to 1.1 * macro cycle duration.
	 * Set to 0, the default value is kept.
	 */
	uint32_t event_ustimeout;
	/**
	 * User callback called in case of runtime error detected by the
	 * library
	 */
	void (* mstrfip_error_handler)(struct mstrfip_dev *dev,
					enum mstrfip_error_list error);
};

/**
 * masterfip irq definition
 * Gives some useful information about the interrupt
 */
struct mstrfip_irq {
	uint32_t hw_sec; /**< HW time (second part) from local oscillator */
	uint32_t hw_ns; /**< HW time (ns part) from local oscillator */
	uint32_t irq_count; /**< interrupt counter */
	uint32_t acq_count; /**< number of acquuisition since the previous irq */
};

/**
 * masterfip data definition
 * This object represents any type of FIP data: periodic var, aperiodic message
 * or aperiodic var. A field "type" gives the real nature of the FIP data.
 * The FIP frame payload size and data are given respectively by nbytes and
 * buffer fields.
 * The status can be(see enum mstrfip_data_status):
 * 	- MSTRFIP_DATA_OK: no error, valid data
 * 	- MSTRFIP_DATA_FRAME_ERROR: HW error.
 *
 */
#define ERTEC_MSTRFIP_DATA_BUFFER_SIZE 128

struct mstrfip_data {
	enum mstrfip_data_type type; /**< type of data: PER_VAR, APER_VAR,... */
	int id; /**< address of fip variable encoded in 2bytes |var|agt_addr| */
	uint8_t buffer[ERTEC_MSTRFIP_DATA_BUFFER_SIZE]; /**< var's data buffer */ //TODO sprawdz czy buffer powinien byc wskaznikiem czy tablica
	uint32_t bsz; /**< data size in bytes */
	enum mstrfip_data_status status; /**< FIP data status */
	/**
	 * frame error detected by the HW.
	 */
	enum mstrfip_frame_errors frame_error;
	/**
	 * payload errors: @see enum mstrfip_payload_errors for decoding
	 */
	uint32_t payload_error;
	void *priv; /**< private opaque structure */
};

/**
 * masterfip data configuration
 * Creating a FIP data requires to provide its configuration.
 */
struct mstrfip_data_cfg {
	int id; /**< address of the remote agent */
	enum mstrfip_data_flags flags; /**< produced or consumed var */
	int max_bsz; /**< max size in bytes of the payload */
	/*
	 * User callback called when this FIP data(periodic var, aperiodic var
	 * or aperiodic msg) is scheduled (produced or consumed by the master)
	 * on the bus.
	 */
	void (* mstrfip_data_handler)(struct mstrfip_dev *dev,
			struct mstrfip_data *data, struct mstrfip_irq *irq);
};

struct mstrfip_per_var_wind_cfg {
	/*
	 * Define a periodic window during which the given sorted list of
	 * periodic variables is scheduled
	 */
	struct mstrfip_data **varlist;
	/*
	 * number of periodic variables defined in var_list
	 */
	unsigned int var_count;
};

struct mstrfip_aper_var_wind_cfg {
	/**
	 * define the time (in us), relative to the macro cycle start, at which
	 * the aperiodic window ends.
	 */
	uint32_t end_ustime;
	/**
	 * Enabling diagnostic implies an extra traffic on the bus
	 * 1) Periodic traffic:
	 * 	a periodic window scheduling the two variables of the fip diag
	 * 	is inserted in the macro-cycle to ensure a good survey of the
	 * 	electrical quality of the bus.
	 * 2) Aperiodic traffic:
	 * 	In the aperiodic window, after having served application's
	 * 	requests(like schedule identification variables), if some time
	 * 	remains, the system can schedule:
	 * 	- Presence variables to keep uptodate the list of presence.
	 * 	- Identification of the FIP diag.
	 */
	unsigned int enable_diag;
	/**
	 * List of identification variables which can be scheduled during the
	 * aperiodic window.
	 */
	struct mstrfip_data **ident_varlist;
	int ident_var_count;
	/**
	 * callback registered by the application, to be notified when the
	 * requests to schedule identification var has been completed.(see
	 * mstrfip_ident_request() to know how to request identification
	 * traffic)
	 */
	void (* mstrfip_ident_var_handler)(struct mstrfip_dev *dev,
						struct mstrfip_irq *irq);
};
/**
 * @brief MFIP variable definition
 */
/**
 * @file libmstrfip-dev.c
 */
/**
 * @defgroup dev
 * Set of functions to manage the basic device and library configuration.
 * @{
 */
extern struct mstrfip_dev *mstrfip_open(void);

extern void mstrfip_reset(void);
extern void mstrfip_stop_threads(void);
/**@}*/

/**
 * @defgroup group1 BA configuration
 * Set of functions to manage master fip ba configuration.
 * @{
 */
extern int mstrfip_rtapp_check_version(struct mstrfip_dev *dev);

extern int mstrfip_version_get(struct mstrfip_dev *dev,
					struct mstrfip_version *vers);

extern int mstrfip_hw_speed_get(struct mstrfip_dev *dev,
					enum mstrfip_bitrate *bitrate);
extern int mstrfip_hw_response_time_get(struct mstrfip_dev *dev, int agent_count,
			int agent_addr_list[], uint32_t tr_nstime_list[]);
extern int mstrfip_hw_cfg_set(struct mstrfip_dev *dev,
					struct mstrfip_hw_cfg *cfg);
extern int mstrfip_sw_cfg_set(struct mstrfip_dev *dev,
					struct mstrfip_sw_cfg *cfg);
/**@}*/

/**
 * @defgroup group2 macrocycle configuration
 * Set of functions to manage masterfip ba macrocycle configuration.
 * mstrfip_macrocycle_create : instantiate a macro cycle object
 * mstrfip_xxx_create() : couple of functions allowing to create
 * fip-data objects like: periodic variables, aperiodic messages or
 * identification aperiodic variables
 * mstrfip-ba-xxx-append() : couple of functions allowing
 * to compose the macro cycle one wants to execute by concatenating
 * different type of window like periodic variable window,
 * aperiodic variable window, aperiodic message window or wait window.
 * A macrocycle should always be terminated by a wait window, which is
 * used to define the macrocycle length.
 * @{
 */
extern struct mstrfip_macrocycle *mstrfip_macrocycle_create(
						struct mstrfip_dev *dev);
extern int mstrfip_macrocycle_reset( struct mstrfip_dev *dev,
					struct mstrfip_macrocycle *mcycle);
extern struct mstrfip_data *mstrfip_var_create(struct mstrfip_macrocycle *,
			struct mstrfip_data_cfg *cfg);
extern int mstrfip_var_add_data_handler(struct mstrfip_macrocycle *, struct mstrfip_data*, \
		void (*phandler)(struct mstrfip_dev*, struct mstrfip_data*, struct mstrfip_irq*));
extern struct mstrfip_data *mstrfip_ident_var_create(struct mstrfip_macrocycle *,
				uint32_t agent_addr);
extern int mstrfip_per_var_wind_append(struct mstrfip_macrocycle *,
			struct mstrfip_per_var_wind_cfg *cfg);
extern int mstrfip_aper_var_wind_append(struct mstrfip_macrocycle *,
			struct mstrfip_aper_var_wind_cfg *cfg);
extern int mstrfip_wait_wind_append(struct mstrfip_macrocycle *,
				uint32_t silent_wait, uint32_t us_cycle_end);
/**@}*/

/**
 * @defgroup group3 macrocycle actions
 * Set of functions to load start stop reset ba
 * @{
 */
extern int mstrfip_ba_load(struct mstrfip_dev *dev,
				struct mstrfip_macrocycle *mcycle);
extern int mstrfip_ba_start(struct mstrfip_dev *dev);
extern int mstrfip_ba_stop(struct mstrfip_dev *dev);
extern int mstrfip_ba_reset(struct mstrfip_dev *dev);
/**@}*/

/**
 * @defgroup group4 runtime
 * Set of functions to manage mstrfip at runtime: acquire and control MFIP
 * activity
 * @{
 */
extern void mstrfip_var_update(struct mstrfip_dev *dev,
					struct mstrfip_data *var);
extern int mstrfip_varlist_update(struct mstrfip_dev *dev,
				struct mstrfip_data **varlist, int var_count);
extern void mstrfip_ident_update(struct mstrfip_dev *dev,
					struct mstrfip_data *ident_var);
extern int mstrfip_identlist_update(struct mstrfip_dev *dev,
			struct mstrfip_data **identlist, int ident_count);
extern int mstrfip_var_write(struct mstrfip_dev *dev, struct mstrfip_data *var);
extern int mstrfip_ident_request(struct mstrfip_dev *dev,
			struct mstrfip_data **identlist, int ident_count);
extern int mstrfip_report_get(struct mstrfip_dev *dev,
					struct mstrfip_report *report);
extern int mstrfip_presencelist_get(struct mstrfip_dev *dev,
				struct mstrfip_presencelist **presencelist);
extern struct mstrfip_diag_shm*  mstrfip_diag_get(void);
/**@}*/

/**
 * @defgroup util
 * Set of utilities functions.
 * @{
 */
extern const char *mstrfip_strerror(int err);
extern uint32_t mstrfip_get_turnaround_ustime(struct mstrfip_dev *dev);
extern void mstrfip_print(void);
extern void mstrfip_print_presence_list(void);
/**@}*/

#ifdef __cplusplus
};
#endif

#endif //__LIB_MASTERFIP_H
