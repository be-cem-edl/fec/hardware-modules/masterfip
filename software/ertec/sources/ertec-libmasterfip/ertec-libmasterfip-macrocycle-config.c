/*
 * Copyright (C) 2016-2017 CERN (www.cern.ch)
 * Author: Michel Arruat <michel.arruat@cern.ch>
 *
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#include <stdint.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>

#include "profip_log.h"
#include "mqospi.h"

#include "ertec-libmasterfip-priv.h"
#include "masterfip-common-priv.h"
#include "ertec-libmasterfip.h"

#define INV_U16(u16_var) ((((u16_var) & 0x00FF) << 8) | (((u16_var) & 0xFF00) >> 8))
/**
 * Reset macrocycle by deleting all objects dynamically allocated.
 * If the macrocycle is the one in used stop the macrocycle execution in
 * mockturtle and chnage is state to INITIAL saying that it waits for a new
 * configuration.
 * @param[in] dev device token
 * @param[in] mcycle pointer to the macrocycle object.
 * @return 0 on success, -1 on error and errno is set appropriately
 */
int mstrfip_macrocycle_reset(struct mstrfip_dev *dev,
			struct mstrfip_macrocycle *mcycle)
{
	int res = 0;
	struct mstrfip_desc* mstrfip = (struct mstrfip_desc *)dev;
	struct mstrfip_ba_macrocycle *mc = (struct mstrfip_ba_macrocycle *)mcycle;

	if (mstrfip->ba.mcycle == mc &&
		mstrfip->ba.state == MSTRFIP_FSM_RUNNING)
	{
		return -1;
	}

	mc->per_vars.prod_var_count = 0;
	mc->per_vars.cons_var_count = 0;
	mc->per_vars.sorted_var_count = 0;
	mc->per_vars.var.count = 0;
	mc->has_registered_cb = 0;
	mc->instr_count = 0;
	mc->comp_cycle_ustime = 0;
    mc->cycle_ustime = 0;
    mc->state = MSTRFIP_MACROCYCLE_INITIAL;

	/* last but least check if it was the macro cycle currently loaded */
	if (mstrfip->ba.mcycle == mc && mstrfip->ba.state == MSTRFIP_FSM_READY)
	{
		res = mstrfip_ba_reset(dev); // reset ba in mturtle
		mstrfip->ba.mcycle = NULL;
		mstrfip->ba.state = MSTRFIP_FSM_INITIAL;
	}

	return res;
}

/**
 * Create macrocycle object.
 * @param[in] dev device token
 * @return a pointer to the new object on success, NULL on error and errno
 * 	is set appropriately
 */
struct mstrfip_macrocycle *mstrfip_macrocycle_create(struct mstrfip_dev *dev)
{
	struct mstrfip_desc* mstrfip = (struct mstrfip_desc *)dev;
	struct mstrfip_macrocycle* mcycle = (struct mstrfip_macrocycle*)(&mstrfip->mcycle);
	int res;

	res = mstrfip_macrocycle_reset(dev, mcycle);
	if(res != 0)
	{
		return NULL;
	};

	return mcycle;
}

/**
 * mstrfip_data factory method.
 */
static struct mstrfip_data *mstrfip_data_create(struct mstrfip_ba_macrocycle *mc, struct mstrfip_data_cfg *var_cfg, int domain)
{
	struct mstrfip_ba_per_var_set *per_vars;
	struct mstrfip_data *pvar;
	struct mstrfip_data_priv *pvar_priv;

	if(mc == NULL)
	{
		PROFIP_ERR(__ERR__"mc is NULL\n");
		return NULL;
	}

	per_vars = &mc->per_vars;

	if(per_vars == NULL)
	{
		PROFIP_ERR(__ERR__"per_vars is NULL\n");
		return NULL;
	}

	if(per_vars->var.count >= MSTRFIP_DATA_BUF_SIZE)
	{
		PROFIP_ERR(__ERR__"out of space in the per vars buffer\n");
		return NULL;
	}

	pvar = &per_vars->var.list[per_vars->var.count];
	pvar_priv = &per_vars->var.list_priv[per_vars->var.count];
	pvar->priv = pvar_priv;

	pvar_priv->hdr.id = (uint16_t)var_cfg->id;
	pvar_priv->hdr.flags = (uint8_t)var_cfg->flags;

	if(var_cfg->max_bsz > ERTEC_MSTRFIP_DATA_BUFFER_SIZE)
	{
		PROFIP_ERR("var cfg max bsz too big\n");
		pvar_priv->hdr.max_bsz = ERTEC_MSTRFIP_DATA_BUFFER_SIZE;
		return NULL;
	}
	else
	{
		pvar_priv->hdr.max_bsz = var_cfg->max_bsz;
	}

	/* assign mq slot depending of the variable's domain */
	pvar_priv->hdr.mq_slot = (domain == MSTRFIP_APP_DOMAIN) ?
			MSTRFIP_HMQ_O_APP_PER_VAR : MSTRFIP_HMQ_O_DIAG_PER_VAR;

	pvar_priv->mstrfip_data_handler = var_cfg->mstrfip_data_handler;
	/* append IRQ_FLAG if an handler has been registered */
	if (pvar_priv->mstrfip_data_handler != NULL)
	{
		pvar_priv->hdr.flags |= MSTRFIP_DATA_FLAGS_IRQ;
	}
		
	/*
	 * Public part: nbytes gives the current payload size
	 * Periodic variable have a fix payload size, so by default nbytes is
	 * set with max_bsz
	 */
	pvar->bsz = pvar_priv->hdr.max_bsz;
	pvar->id = var_cfg->id;

	per_vars->var.count++;

	return pvar;
}

/**
 * Create a periodic variable using the given variable configuration and
 * assigned it to the given macrocycle.
 * @param[in] macrocycle pointer
 * @param[in] var_cfg variable configuration
 * @return a pointer to the new object on success, NULL on error
 * 		and errno is set appropriately.
 */
struct mstrfip_data *mstrfip_var_create(struct mstrfip_macrocycle *macrocycle,
				struct mstrfip_data_cfg *var_cfg) 
{
	struct mstrfip_ba_macrocycle * mc =
				(struct mstrfip_ba_macrocycle *)macrocycle;
	struct mstrfip_data *pvar;

	pvar = mstrfip_data_create(mc, var_cfg, MSTRFIP_APP_DOMAIN);

	if(pvar == NULL)
	{
		return NULL;
	}

	if (var_cfg->mstrfip_data_handler != NULL)
    {
		/* callback is registered: raise flag used later to start irq_thread */
		mc->has_registered_cb = 1;
	}

	return pvar;
}


int mstrfip_var_add_data_handler(struct mstrfip_macrocycle *macrocycle, struct mstrfip_data* pvar, \
		void (*phandler)(struct mstrfip_dev*, struct mstrfip_data*, struct mstrfip_irq*))
{
	struct mstrfip_ba_macrocycle * mc =
				(struct mstrfip_ba_macrocycle *)macrocycle;

    struct mstrfip_data_priv *pvar_priv;

    if(pvar != NULL)
    {
        pvar_priv = (struct mstrfip_data_priv *) pvar->priv;
    }

    if(pvar == NULL || pvar_priv == NULL || phandler == NULL)
    {
        return -1;
    }

	pvar_priv->mstrfip_data_handler = phandler;
	pvar_priv->hdr.flags |= MSTRFIP_DATA_FLAGS_IRQ;
    mc->has_registered_cb = 1;

    return 0;
}

/**
 * Create an aperiodic identification variable using the given
 * variable configuration and assigned it to the given macrocycle.
 * @param[in] macrocycle pointer
 * @param[in] agent_addr variable configuration
 * @return a pointer to the new object on success, NULL on error
 * 		and errno is set appropriately.
 */
struct mstrfip_data *mstrfip_ident_var_create(
		struct mstrfip_macrocycle *macrocycle, uint32_t agent_addr)
{
	struct mstrfip_data_cfg ident_cfg;
	struct mstrfip_data *p_ident_var;
	struct mstrfip_data_priv *pvar_priv;

	/* create identification variable */
	ident_cfg.max_bsz = MSTRFIP_IDENT_VAR_DATA_BSZ;
	ident_cfg.id = (MSTRFIP_AGENT_IDENT_VAR_ID) | (0xFF & agent_addr);
	ident_cfg.flags = MSTRFIP_DATA_FLAGS_CONS;
	ident_cfg.mstrfip_data_handler = NULL;

	p_ident_var = mstrfip_var_create(macrocycle, &ident_cfg);

	if(p_ident_var != NULL)
	{
		pvar_priv = (struct mstrfip_data_priv *)p_ident_var->priv;
		pvar_priv->hdr.key = agent_addr;
	}

	return p_ident_var;
}

/**
 * Append a periodic variable window in the macro cycle
 * using the given configuration
 * @param[in] macrocycle pointer to the given macrocyle
 * @param[in] cfg periodic window configuration
 * @return 0 on success, -1 on error and errno is set appropriately
 */
int mstrfip_per_var_wind_append(struct mstrfip_macrocycle *macrocycle,
		struct mstrfip_per_var_wind_cfg * cfg)
{
	struct mstrfip_ba_macrocycle *mcycle =
			(struct mstrfip_ba_macrocycle *)macrocycle;
	struct mstrfip_ba_per_var_set *per_vars = &mcycle->per_vars;
	struct mstrfip_data_priv *pvar_priv;
	struct mstrfip_ba_instr* instr;
	int i;

	/* sanity check before accepting the config */
	/* macrocycle state */
	if (mcycle->state != MSTRFIP_MACROCYCLE_INITIAL) {
		errno = MSTRFIP_BA_NOT_STOP_STATE;
		return -1; /* not allowed */
	}

	if(mcycle->instr_count >= MSTRFIP_BA_MACROCYCLE_INSTRLIST_SIZE)
	{
		return -2;
	}

	if(MSTRFIP_DATA_BUF_SIZE - per_vars->sorted_var_count < cfg->var_count)
	{
		return -3;
	}

	/* append the new list of periodic var into varlist */
	struct mstrfip_data ** copy_to = &(per_vars->sorted_varlist[per_vars->sorted_var_count]);
	memcpy(copy_to, cfg->varlist, cfg->var_count * sizeof(struct mstrfip_data *));

	/* keep up-to-date consumed and produced var count*/
	for (i = per_vars->sorted_var_count;
	     i < per_vars->sorted_var_count + cfg->var_count; ++i) {
		pvar_priv = (struct mstrfip_data_priv *)
					per_vars->sorted_varlist[i]->priv;
		if (pvar_priv->hdr.flags & MSTRFIP_DATA_FLAGS_CONS)
			++(per_vars->cons_var_count);
		else
			++(per_vars->prod_var_count);
	}
	/*
	 * append the new ba instruction in the instrlist: BA_SEND_LIST
	 * arg1, arg2 are start and stop index in sorted varlist
	 */
	instr = &mcycle->instrlist[mcycle->instr_count];
	instr->code = MSTRFIP_BA_PER_VAR_WIND;
	/* start index in sorted var list */
	instr->per_var_wind.start_var_idx = per_vars->sorted_var_count;
	/* update sorted_var_count */
	per_vars->sorted_var_count += cfg->var_count;
	/* stop index in sorted varlist */
	instr->per_var_wind.stop_var_idx = per_vars->sorted_var_count;

	++(mcycle->instr_count); /* increment instruction count */

	return 0;
}

/*
 * Requesting diagnostic implies some extra traffic on the bus:
 * 1) Periodic traffic:
 * 	a periodic window scheduling the two variables of the fip diag
 * 	is inserted in the macro-cycle to ensure a good survey of the
 * 	electrical quality of the bus.
 * 2) Aperiodic traffic:
 * 	- Presence variables are scheduled during the aperiodic window to
 *	  keep uptodate the list of presence.
 * 	- Identification of the FIP diag is also schedule during the
 * 	  aperiodic window at a rate defined by diag_period in sw config.
 */
static int mstrfip_diag_per_wind_append(struct mstrfip_ba_macrocycle *mcycle)
{
	struct mstrfip_data_cfg var_cfg;
	struct mstrfip_data *pvar;
	struct mstrfip_per_var_wind_cfg pwind_cfg;

	/* Create periodic diag variables */
	var_cfg.max_bsz = MSTRFIP_DIAG_VAR_DATA_BSZ;
	var_cfg.id = MSTRFIP_DIAG_CONS_VAR_ID; /* consumed FIP Diag var */
	var_cfg.flags = MSTRFIP_DATA_FLAGS_CONS;
	var_cfg.mstrfip_data_handler = NULL;
	pvar = mstrfip_data_create(mcycle, &var_cfg, MSTRFIP_DIAG_DOMAIN);
	if (pvar == NULL)
		return -1;
	mcycle->diag.varlist[0] = pvar;
	var_cfg.id = MSTRFIP_DIAG_PROD_VAR_ID; /* produced FIP Diag var */
	var_cfg.flags = MSTRFIP_DATA_FLAGS_PROD; /* IRQ flag*/
	var_cfg.mstrfip_data_handler = mstrfip_diag_var_handler;
	pvar = mstrfip_data_create(mcycle, &var_cfg, MSTRFIP_DIAG_DOMAIN);
	if (pvar == NULL)
		return -1;
	mcycle->diag.varlist[1] = pvar;

	/* callback is registered: raise flag used later to start irq_thread */
	mcycle->has_registered_cb = 1;

	/* insert diag periodic window in the ba instruction set and register
	 * the diag var handler
	 */
	pwind_cfg.varlist = mcycle->diag.varlist;
	pwind_cfg.var_count = 2;
	/*
	 * we should check that the aperiodic window last at least 1ms to be
	 * sure that the periodic diag window can be scheduled like it it was
	 * SEND_LIST diag_var and SEND_APER time-time of send list diag
	 */
	return mstrfip_per_var_wind_append(
			(struct mstrfip_macrocycle *)mcycle, &pwind_cfg);
}


/**
 * Append an aperiodic variable window in the macro cycle
 * using the given configuration
 * @param[in] macrocycle pointer to the given macrocyle
 * @param[in] cfg aperiodic variable window configuration
 * @return 0 on success, -1 on error and errno is set appropriately
 */
int mstrfip_aper_var_wind_append(struct mstrfip_macrocycle *macrocycle,
		struct mstrfip_aper_var_wind_cfg * cfg)
{
	struct mstrfip_ba_macrocycle *mcycle =
			(struct mstrfip_ba_macrocycle *)macrocycle;
	struct mstrfip_ba_instr* instr;
	struct mstrfip_data_cfg ident_cfg;
	struct mstrfip_data *p_ident_var;
	int res;

	/* sanity check before accepting the config */
	/* ba state */
	if (mcycle->state != MSTRFIP_MACROCYCLE_INITIAL) {
		errno = MSTRFIP_BA_NOT_STOP_STATE;
		return -1; /* not allowed */
	}

	/* let's try first to allocate space in the instr list*/
	/*
	 * Check if diagnostic is requested for the first time
	 */
	if (cfg->enable_diag && mcycle->diag.varlist[0] == NULL) {
		/*
		 * Scheduling diagnostic implies two ba instructions to be
		 * inserted(see comments in mstrfip_diag_per_wind_append() above).
		 */
		res = mstrfip_diag_per_wind_append(mcycle);
		if (res < 0)
			return res;
		/* create identification fip-diag variable */
		ident_cfg.max_bsz = MSTRFIP_IDENT_VAR_DATA_BSZ;
		ident_cfg.id = MSTRFIP_DIAG_IDENT_VAR_ID;
		ident_cfg.flags = MSTRFIP_DATA_FLAGS_CONS;
		ident_cfg.mstrfip_data_handler = NULL;
		p_ident_var = mstrfip_data_create(mcycle, &ident_cfg,
						  MSTRFIP_DIAG_DOMAIN);
		if (p_ident_var == NULL)
			return -1;
		mcycle->diag.ident_var = p_ident_var;
		((struct mstrfip_data_priv *)p_ident_var->priv)->hdr.key =
							(MSTRFIP_DIAG_ADDR);
		/* Allocate trtl msg buffer: diag requires a single message */
		mcycle->diag.trtl.msg_count = 0;
	}

	/* Check if enough space in instruction list */
	if(mcycle->instr_count >= MSTRFIP_BA_MACROCYCLE_INSTRLIST_SIZE)
	{
		return -2;
	}

	/* if app has set an identlist allocate trtl message buffer */
	if (cfg->ident_var_count) {
		/*
		 * TODO: optimize the max number of trtl message.
		 * Currently, max number of trtl messages equal to the number of
		 * agents. It's oversized because several ident variables are
		 * packed into a single trtl message, but knowing that a trtl
		 * message size is half Kb, * max number of agent (256) gives
		 * 128Kb of memory
		 */
		mcycle->aper_vars.mstrfip_ident_var_handler =
						cfg->mstrfip_ident_var_handler;
		/* callback is registered: raise flag used later to start irq_thread */
		if (cfg->mstrfip_ident_var_handler != NULL)
			mcycle->has_registered_cb = 1;
	}

	/*
	 * append the new ba instruction in the instrlist: BA_SEND_APER
	 * arg2 is the end time in us relative to the start of the ba cycle.
	 */
	instr = &mcycle->instrlist[mcycle->instr_count];
	instr->code = MSTRFIP_BA_APER_VAR_WIND;
	instr->aper_var_wind.ticks_end_time = us_to_ticks(cfg->end_ustime, MFIP_CPU_HZ);
	/* time to update instr count fields*/
	++(mcycle->instr_count); /* number of instruction */
	return 0;
}

/**
 * Append a wait window in the macro cycle using the given configuration.
 * Note: as the other windows, a wait window can be inserted at any place in the
 * macrocycle, but a macrocycle should always end with a wait window, in order
 * to know the macrocycle duration.
 * @param[in] macrocycle pointer to the given macrocyle
 * @param[in] silent_wait wait window configuration
 * @param[in] us_cycle_end wait window configuration
 * @return 0 on success, -1 on error and errno is set appropriately
 */
int mstrfip_wait_wind_append(struct mstrfip_macrocycle *macrocycle,
		uint32_t silent_wait, uint32_t us_cycle_end)
{
	struct mstrfip_ba_macrocycle *mcycle =
			(struct mstrfip_ba_macrocycle *)macrocycle;
	struct mstrfip_ba_instr* instr;

	if (mcycle->state != MSTRFIP_MACROCYCLE_INITIAL) {
		errno = MSTRFIP_BA_NOT_STOP_STATE;
		return -1; /* not allowed */
	}
	
	if(mcycle->instr_count >= MSTRFIP_BA_MACROCYCLE_INSTRLIST_SIZE)
	{
		return -1;
	}

	/* append the new ba instruction in the instrlist */
	instr = &mcycle->instrlist[mcycle->instr_count];
	instr->code = MSTRFIP_BA_WAIT_WIND;
	/* convert us time into cpu ticks given in ns */
	instr->wait_wind.ticks_end_time = us_to_ticks(us_cycle_end, MFIP_CPU_HZ);
	instr->wait_wind.is_silent = (silent_wait) ? 1 : 0;

	/* time to update instr count fields*/
	++(mcycle->instr_count); /* number of instruction */

    struct mstrfip_ba_per_var_set *per_vars = &mcycle->per_vars;
    struct mstrfip_data_priv *pvar_priv;

    /*debuglog*/

    PROFIP_DEBUG("\n"__DEBUG__"Variables list:\n");
    PROFIP_DEBUG(" ADDR|  ID |  DIR | IRQ | BSZ \n");
    PROFIP_DEBUG(" ----+-----+------+-----+-----\n");

	static const char* dir_dict[4] = {"    ", "PROD", "CONS", "    "};

    for(int i=0; i<per_vars->sorted_var_count; i++)
    {
        pvar_priv = (struct mstrfip_data_priv *)
					per_vars->sorted_varlist[i]->priv;

        PROFIP_DEBUG(" %3d | %3d | %s | %3d | %3d\n", \
			 (per_vars->sorted_varlist[i]->id) >> 8, \
			 (per_vars->sorted_varlist[i]->id) & 0xFF, \
        		dir_dict[pvar_priv->hdr.flags & 0x03], \
				(pvar_priv->hdr.flags & 0x80) >> 7, \
				pvar_priv->hdr.max_bsz);
    }

	static const char* instr_codes_dict[5] = {"", "PER ", "APER", "APER", "WAIT"};

    PROFIP_DEBUG("\n"__DEBUG__"Instructions list:\n");
    PROFIP_DEBUG(" ID | CODE |START| STOP\n");
    PROFIP_DEBUG(" ---+------+-----+-----\n");
    for(int i=0; i<mcycle->instr_count; i++)
    {
    	if(mcycle->instrlist[i].code == MSTRFIP_BA_PER_VAR_WIND)
    	{
    		PROFIP_DEBUG("%3d | %s | %3d | %3d\n", i, \
					instr_codes_dict[mcycle->instrlist[i].code], \
    				mcycle->instrlist[i].per_var_wind.start_var_idx, \
					mcycle->instrlist[i].per_var_wind.stop_var_idx);
    	}
    	else
    	{
    		PROFIP_DEBUG("%3d | %s |     |\n", i, instr_codes_dict[mcycle->instrlist[i].code]);
    	}
    }
    PROFIP_DEBUG("\n");

	return 0;
}
