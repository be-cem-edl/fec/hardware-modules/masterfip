// SPDX-FileCopyrightText: 2023 CERN (home.cern)
//
// SPDX-License-Identifier: LGPL-2.1-or-later

/**
 * @author Piotr Klasa <piotr.klasa@cern.ch>
 *
 * @file profip_log.h
 * @name ProFip logger
 * @brief Modulue for logging debug information and errors
 */


#ifndef PROFIP_LOG_H
#define PROFIP_LOG_H

#include <stdio.h>
#include "pniousrd.h"

#define PROFIP_LOG_LEVEL_NONE 4
#define PROFIP_LOG_LEVEL_ERROR 3
#define PROFIP_LOG_LEVEL_WARNING 2
#define PROFIP_LOG_LEVEL_INFO 1
#define PROFIP_LOG_LEVEL_DEBUG 0

#define __ERR__ "\e[31m[ERR]\e[0m "
#define __WARN__ "\e[33m[WARN]\e[0m "
#define __OK__ "\e[32m[OK]\e[0m "
#define __INFO__ "[INFO] "
#define __DEBUG__ "[DEBUG] "
#define __USER__ "\e[35m[USER]\e[0m "

#define PROFIP_PRINTF PNIO_ConsolePrintf //printf //

#ifndef PROFIP_LOG_LEVEL
#define PROFIP_LOG_LEVEL PROFIP_LOG_LEVEL_DEBUG
#endif

#if PROFIP_LOG_LEVEL <= PROFIP_LOG_LEVEL_ERROR
    #define PROFIP_ERR(...) PROFIP_PRINTF(__VA_ARGS__)
#else
    #define PROFIP_ERR(...)
#endif

#if PROFIP_LOG_LEVEL <= PROFIP_LOG_LEVEL_WARNING
    #define PROFIP_WARN(...) PROFIP_PRINTF(__VA_ARGS__)
#else
    #define PROFIP_WARN(...)
#endif

#if PROFIP_LOG_LEVEL <= PROFIP_LOG_LEVEL_INFO
    #define PROFIP_LOG(...) PROFIP_PRINTF(__VA_ARGS__)
#else
    #define PROFIP_LOG(...)
#endif

#if PROFIP_LOG_LEVEL <= PROFIP_LOG_LEVEL_DEBUG
    #define PROFIP_DEBUG(...) PROFIP_PRINTF(__VA_ARGS__)
#else
    #define PROFIP_DEBUG(...)
#endif

#endif


