// SPDX-FileCopyrightText: 2023 CERN (home.cern)
//
// SPDX-License-Identifier: LGPL-2.1-or-later

#ifndef DATA_UART_UTIL_H
#define DATA_UART_UTIL_H

/**
 * @desc Starts data uart thread
 */
int data_uart_util_init(void);

#endif
