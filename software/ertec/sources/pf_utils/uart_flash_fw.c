// SPDX-FileCopyrightText: 2023 CERN (home.cern)
//
// SPDX-License-Identifier: LGPL-2.1-or-later

#include <stdint.h>

#include "os.h"
#include "uart_flash_fw.h"

#include "pf_utils.h"
#include "profip_log.h"
#include "pniousrd.h"
#include "data_uart.h"
#include "data_uart_cmd.h"

#define DEFAULT_TIMEOUT_US 250000 /* 250 ms */
#define BUFFER_SIZE 	0x400000
#define MIN_IMG_SIZE	0x10000

static uint8_t fw_img[BUFFER_SIZE];
static int fw_img_size;

extern PNIO_UINT32 TcpFlashFirmware (void* pBuf, PNIO_UINT32 BufSize, void(*msg_ptr)(const char* fmt, ...));

static void UartFlashMsg(const char* fmt, ...)
{
	va_list args;
	va_start(args, fmt);

	char buff[100];

	vsnprintf (buff, 100, fmt, args);

	va_end (args);

	PNIO_printf(buff);
}

static int uart_program_flash(void)
{
	PNIO_UINT32 Status = 0;
	int rc = 1;

	PROFIP_LOG("\n\e[33mDO NOT SWITCH POWER OFF\e[0m\n\n");

	PROFIP_LOG("Erase & program flash\n");

    Status = TcpFlashFirmware (fw_img, fw_img_size, &UartFlashMsg);

	if (Status == PNIO_NOT_OK)
	{
		PROFIP_ERR(__ERR__"ERROR at firmware flashing\n");
	}
	else
	{
		PROFIP_LOG(__OK__"Flashing firmware finished\n");
		rc = 0;
	}

    return rc;
}

int uart_flash_fw(void)
{
	uint32_t check, size, crc;
	uint64_t volatile time_s, time_e;
	int rc;

	PROFIP_LOG("\nFw download via UART\n\n");

	data_uart_flush();

	data_uart_put_u32(CMD_GET_HEADER);

	time_s = OsGetTime_us();
	while(data_uart_data_to_read() < 12)
	{
		time_e = OsGetTime_us();
		if(time_e > time_s + DEFAULT_TIMEOUT_US)
		{
			PROFIP_ERR(__ERR__"Timeout\n");
			data_uart_put_u32(CMD_ERROR);
			return 1;
		}
		OsWait_ms(1);
	}

	data_uart_get_u32(&check);

	data_uart_get_u32(&size);

	data_uart_get_u32(&crc);

	PROFIP_LOG("File header:\n");
	PROFIP_LOG(" Control 0x%.8x\n", check);
	PROFIP_LOG(" Size    0x%.8x\n", size);
	PROFIP_LOG(" Crc     0x%.8x\n\n", crc);

	if(size >= BUFFER_SIZE || size < MIN_IMG_SIZE)
	{
		PROFIP_ERR(__ERR__"Wrong header\n");
		data_uart_put_u32(CMD_ERROR);
		return 1;
	}

	data_uart_put_u32(CMD_GET_PAYLOAD);

	int perc = 0;

	PROFIP_LOG("Payload:\n");

	for(uint32_t i=0; i<size; i++)
	{
		time_s = OsGetTime_us();
		while(data_uart_data_to_read() < 1)
		{
			time_e = OsGetTime_us();
			if(time_e > time_s + DEFAULT_TIMEOUT_US)
			{
				PROFIP_ERR(__ERR__"Timeout\n", i);
				data_uart_put_u32(CMD_ERROR);
				return 1;
			}
			OsWait_ms(1);
		}

		data_uart_getc(&(fw_img[i]));

		if(perc * size <= (i+1) * 100)
		{
			PROFIP_LOG("\r %d %%", perc);
			perc++;
		}
	}

	PROFIP_LOG("\n");

	fw_img_size = size;

	// Verify Crc
	uint32_t crc_calculated = posix_crc32(fw_img, size);

	if(crc != crc_calculated)
	{
		PROFIP_ERR(__ERR__"Wrong CRC %x != %x\n", crc, crc_calculated);
		data_uart_put_u32(CMD_ERROR);
		return 1;
	}

	PROFIP_LOG(__OK__"CRC OK %x\n", crc);

	//Program flash
	rc =  uart_program_flash();

	if(rc == 0)
	{
		PROFIP_LOG("OsReboot in 2 sec....\n");
	}

	data_uart_put_u32(CMD_SUCCESS);
	OsWait_ms(2000);

	if (rc == 0)
	{
        OsReboot();
	}

	return rc;
}
