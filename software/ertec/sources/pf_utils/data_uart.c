// SPDX-FileCopyrightText: 2023 CERN (home.cern)
//
// SPDX-License-Identifier: LGPL-2.1-or-later

#include <stdio.h>
#include <cyg/kernel/kapi.h>

#include "data_uart.h"
#include "ertec200p_reg.h"
#include "os.h"

#define INT_UART4 11
#define RXbuff_CAPACITY 32768

typedef struct RXbuff
{
	uint32_t read_ptr;
	uint32_t write_ptr;
	uint32_t no_records;	/*number of records in buffer*/
	uint8_t buff[RXbuff_CAPACITY];
}RXbuff;

static volatile RXbuff rx_buff __attribute__ ((section(".uncached_mem")));

int rx_buff_put(uint8_t c)
{
	int rc = 1;

	if(rx_buff.no_records < RXbuff_CAPACITY)
	{
		rx_buff.buff[rx_buff.write_ptr] = c;
		rx_buff.write_ptr = (rx_buff.write_ptr+1) % RXbuff_CAPACITY;
		rx_buff.no_records++;
		rc = 0;
	}

	return rc;
}

int rx_buff_get(uint8_t* c)
{
	int rc = 1;

	cyg_interrupt_disable(  );
	if(rx_buff.no_records > 0)
	{
		*c = rx_buff.buff[rx_buff.read_ptr];
		rx_buff.read_ptr = (rx_buff.read_ptr+1) % RXbuff_CAPACITY;
		rx_buff.no_records--;

		rc = 0;
	}
	cyg_interrupt_enable(  );

	return rc;
}

static inline void read_uart(void)
{
	cyg_interrupt_disable(  );
    while( ((REG32(U4_UART__UARTFR)) & U4_UART__UARTFR__RECEIVE_FIFO_EMPTY) == 0 )
    {
        uint32_t DR = REG32(U4_UART__UARTDR);
        rx_buff_put(DR & U4_UART__UARTDR__DATA);
    }
    cyg_interrupt_enable(  );
    return;
}

cyg_uint32 data_uart_isr(cyg_vector_t vector, cyg_addrword_t data)
{
    cyg_interrupt_acknowledge(INT_UART4);

    return CYG_ISR_CALL_DSR;
}

void data_uart_dsr(cyg_vector_t vector, cyg_ucount32 count, cyg_addrword_t data)
{
	read_uart();
}


/**
 *  @brief Init SPI receive interrupt
 *
 *  @return      			Void
 *
 *  This function..
 *
 */
static void data_uart_open( void )
{
	static cyg_interrupt intH;
	static cyg_handle_t hIntH;

	cyg_interrupt_create(
			INT_UART4, 		                /*vector*/
			0, 						        /*priority*/
			0,						        /*data ptr*/
			( cyg_ISR_t * )data_uart_isr,	/* *isr*/
			( cyg_DSR_t * )data_uart_dsr,	/* *dsr*/
			&hIntH,					        /*return handle*/
			&intH);
	/* *interrupt*/
	cyg_interrupt_attach( hIntH );
	cyg_interrupt_unmask(INT_UART4);
}	/*spi_open(  )*/


void data_uart_init(void)
{
	//reset rx buff
	rx_buff.read_ptr = 0;
	rx_buff.write_ptr = 0;
	rx_buff.no_records = 0;

    // Disable UART
	REG32(U4_UART__UARTCR) &= ~U4_UART__UARTCR__UART_ENABLE;

    // Clear rx erors
	REG32(U4_UART__UARTRSR_UARTECR) = U4_UART__UARTRSR_UARTECR_RESET__VALUE;

    // Low-power divisor
	REG32(U4_UART__USRTILPR) = U4_UART__USRTILPR_RESET__VALUE;

    // See Manual_ERTEC200P-2_V2_0.pdf
    // BRI = 1150200 (ideal baud rate)
    // Integer divisor
	REG32(U4_UART__UARTIBRD) = 67U; //33U - commented values for 230400
    // Fractional divisor
	REG32(U4_UART__UARTFBRD) = 52U; //58U - commented values for 230400

    // Enable FIFO's
	REG32(U4_UART__UARTLCR_H) |= U4_UART__UARTLCR_H__ENABLE_FIFOS;
    // Set word len to 8
	REG32(U4_UART__UARTLCR_H) |= U4_UART__UARTLCR_H__WORD_LENGHT;

    // FIFO RX/TX interrupt level = 1/8 full
	REG32(U4_UART__UARTIFLS) = 0;

    // Mask for RX interrupts
	REG32(U4_UART__UARTIMSC) |= U4_UART__UARTIMSC__RX_INT_MASK;

    // Clear interrupts
	REG32(U4_UART__UARTICR) |= 0x7FF;

    //enable RX and TX
	REG32(U4_UART__UARTCR) |= (U4_UART__UARTCR__RECEIVE_ENABLE |
                        U4_UART__UARTCR__TRANSMIT_ENABLE);

    //enable UART (RX + TX)
	REG32(U4_UART__UARTCR) |= U4_UART__UARTCR__UART_ENABLE;

    //start IRQ UART
	data_uart_open();
}

int data_uart_getc(uint8_t* c)
{
	read_uart();

    return rx_buff_get(c);
}

int data_uart_putc(uint8_t c)
{
		// blocking function
        while((REG32(U4_UART__UARTFR) & U4_UART__UARTFR__TRANSMIT_FIFO_FULL) != 0)
        {
        	OsWait_ms(1);
        }

        REG32(U4_UART__UARTDR) = (c & U4_UART__UARTDR__DATA);

        return 0;
}


// little endiann
int data_uart_get_u32(uint32_t *data)
{
	int rc = 0;
	uint8_t tmp;
	*data = 0;

	rc |= data_uart_getc(&tmp);
	*data |= (tmp << 0);

	rc |= data_uart_getc(&tmp);
	*data |= (tmp << 8);

	rc |= data_uart_getc(&tmp);
	*data |= (tmp << 16);

	rc |= data_uart_getc(&tmp);
	*data |= (tmp << 24);

	return rc;
}

// little endiann
int data_uart_put_u32(uint32_t data)
{
	int rc = 0;

	rc |= data_uart_putc((data & 0x000000FF) >> 0);
	rc |= data_uart_putc((data & 0x0000FF00) >> 8);
	rc |= data_uart_putc((data & 0x00FF0000) >> 16);
	rc |= data_uart_putc((data & 0xFF000000) >> 24);

	return rc;
}

int data_uart_flush(void)
{
	volatile uint32_t DR;

    while( ((REG32(U4_UART__UARTFR)) & U4_UART__UARTFR__RECEIVE_FIFO_EMPTY) == 0 )
    {
       DR = REG32(U4_UART__UARTDR);
       (uint32_t) DR;
    }

	//reset rx buff
	rx_buff.read_ptr = 0;
	rx_buff.write_ptr = 0;
	rx_buff.no_records = 0;

	return 0;
}

int data_uart_data_to_read(void)
{
	read_uart();

	return rx_buff.no_records;
}
