// SPDX-FileCopyrightText: 2023 CERN (home.cern)
//
// SPDX-License-Identifier: LGPL-2.1-or-later

/**
 * @author Piotr Klasa <piotr.klasa@cern.ch>
 *
 * @file pf_err.h
 * @name Profip Errors
 * @brief Modulue for handling all profip errors
 */

#ifndef PROFIP_ERR_H
#define PROFIP_ERR_H

#include <stdint.h>

enum pf_err_module_e
{
	PF_ERR_MODULE_NONE,
	PF_ERR_MODULE_USRIOD_MAIN,
	PF_ERR_MODULE_IODAPI_EVENT,
	PF_ERR_MODULE_PROFIP,
	PF_ERR_MODULE_MAP,
	PF_ERR_MODULE_GPIO,
	PF_ERR_MODULE_LED,
	PF_ERR_MODULE_MSTRFIP_CTRL,
	PF_ERR_MODULE_MQOSPI,
	PF_ERR_MODULE_SPIDMA,
	PF_ERR_MODULE_LIBMASTERFIP,
	PF_ERR_MODULE_CAS,
	PF_ERR_MODULE_WDG,
	PF_ERR_MODULE_NF_NODE,
	PF_ERR_MODULE_MF_HANDLER
};

int pf_err(enum pf_err_module_e module, int32_t int_err_nbr, uint32_t param, char* msg);

void pf_err_clear(void);

int pf_err_get_all(uint8_t** err_ptr, uint32_t* err_size);

void pf_err_print(void);

#endif
