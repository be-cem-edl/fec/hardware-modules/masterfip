// SPDX-FileCopyrightText: 2023 CERN (home.cern)
//
// SPDX-License-Identifier: LGPL-2.1-or-later

/**
 * @author Piotr Klasa <piotr.klasa@cern.ch>
 *
 * @file pf_utils.h
 * @name General functions
 */

#ifndef PF_UTILS_H
#define PF_UTILS_H

#include <stdint.h>

#ifndef FIP_MAX_NBR_OF_NODES
#define FIP_MAX_NBR_OF_NODES 30
#error FIP_MAX_NBR_OF_NODES shall be defined in Makefile
#endif

#define ARRAYSIZE(arr) (sizeof(arr) / sizeof(arr[0]))

#define CHANGE_ENDIANESS_16(val) ( (((val) & 0xFF) << 8) | (((val) & 0xFF00) >> 8) )
#define CHANGE_ENDIANESS_32(val) ( (((val) & 0xFF) << 24) | (((val) & 0xFF00) << 8) | (((val) & 0xFF0000) >> 8) | (((val) & 0xFF000000) >> 24) )

#define MAX(A,B) ( ((A) > (B)) ? (A) : (B) )
#define MIN(A,B) ( ((A) > (B)) ? (B) : (A) )

uint32_t posix_crc32(unsigned char *s, int len);

uint32_t pf_time_get(void);

void pf_time_set(uint32_t time);

#endif
