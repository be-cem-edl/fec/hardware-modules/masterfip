// SPDX-FileCopyrightText: 2023 CERN (home.cern)
//
// SPDX-License-Identifier: LGPL-2.1-or-later

#include "pf_err.h"
#include "profip_log.h"
#include "pf_utils.h"

#define PF_ERR_NRB_OF_ERR_SAVED 12

struct pf_err_buff_t
{
	enum pf_err_module_e module;
	uint8_t reserved;
	uint16_t cnt;
	int32_t int_err_nbr;
	uint32_t param;
};

static struct pf_err_buff_t pf_err_last_errors[PF_ERR_NRB_OF_ERR_SAVED];

static const char* pf_err_module_names[] =
{
	"UNDEFINED",
	"USRIOD_MAIN",
	"IODAPI_EVENT",
	"PROFIP",
	"MAP",
	"GPIO",
	"LED",
	"MSTRFIP_CTRL",
	"MQOSPI",
	"SPIDMA",
	"LIBMASTERFIP",
	"CAS",
	"WDG",
	"NF_NODE",
	"MF_HANDLER"
};

int pf_err_cmp(struct pf_err_buff_t* err1, struct pf_err_buff_t* err2)
{
	if(err1->module == err2->module && err1->int_err_nbr == err2->int_err_nbr)
	{
		return 0;
	}

	return 1;
}

int pf_err(enum pf_err_module_e module, int32_t int_err_nbr, uint32_t param, char* msg)
{
	PROFIP_ERR(__ERR__"[%s] [%d] [%d] %s", pf_err_module_names[module], int_err_nbr, param, msg);

	int i;

	struct pf_err_buff_t new = {module, 0U, 1U, int_err_nbr, param};
	struct pf_err_buff_t tmp;


	for(i=0; i<PF_ERR_NRB_OF_ERR_SAVED; i++)
	{
		tmp = pf_err_last_errors[i];
		pf_err_last_errors[i] = new;

		if(pf_err_cmp(&tmp, &pf_err_last_errors[0]) == 0)
		{
			if(tmp.cnt < 0xFFFF)
			{
				pf_err_last_errors[0].cnt = tmp.cnt+1;
			}
			else
			{
				pf_err_last_errors[0].cnt = 0xFFFF;
			}
			break;
		}

		new = tmp;
	}

	return int_err_nbr;
}

void pf_err_clear(void)
{
	memset(pf_err_last_errors, 0, sizeof(pf_err_last_errors));
}

int pf_err_get_all(uint8_t** err_ptr, uint32_t* err_size)
{
	*err_ptr = (uint8_t*) &pf_err_last_errors[0];
	*err_size = sizeof(pf_err_last_errors); 

	return 0;
}

void pf_err_print(void)
{
	int i;

	PROFIP_LOG("module      | int nbr | param | cnt\n");
	PROFIP_LOG("------------+---------+-------+-----\n");

	for(i = 0; i<PF_ERR_NRB_OF_ERR_SAVED; i++)
	{
		struct pf_err_buff_t *tmp = &pf_err_last_errors[i];

		char tmpname[] = "            ";
		int size_to_cpy = MIN( strlen(tmpname),  strlen(pf_err_module_names[tmp->module]) );
		memcpy(tmpname, pf_err_module_names[tmp->module], size_to_cpy);

		if(i==0 || tmp->cnt != 0)
		{
			PROFIP_LOG("%s| %7d | %5d | %3d\n", tmpname, tmp->int_err_nbr, tmp->param, tmp->cnt);
		}
	}
}
