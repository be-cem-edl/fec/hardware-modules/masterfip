// SPDX-FileCopyrightText: 2023 CERN (home.cern)
//
// SPDX-License-Identifier: LGPL-2.1-or-later

/**
 * @file mfh_helper.h
 * @author Piotr Klasa (piotr.klasa@cern.ch)
 * @brief This module contains functions that calculate parameters
 *        specific to the WorldFip network, such as the total macrocycle
 *        length or the turnaround time.
 */

#ifndef MFH_HELPER_H
#define MFH_HELPER_H

#include "ertec-libmasterfip.h"

/**
 * @returns Minimal periodic var window length based on given configuration.
*/
unsigned mfh_helper_calculate_pervar_wind_time_us(unsigned bus_speed, unsigned turn_around_time_us,\
		unsigned prod_varlist_cnt, unsigned cons_varlist_cnt,\
		struct mstrfip_data** prod_varlist, struct mstrfip_data** cons_varlist);

/**
 * @returns Recommended turnaround time based on given bus speed.
*/
unsigned mfh_helper_calculate_turnaround_time_us(unsigned int bus_speed);

/**
 * @returns Recommended aperiodic var windows based on given bus speed.
*/
unsigned mfh_helper_calculate_aper_var_wind_time_us(unsigned int bus_speed);

/**
 * @returns Minimal window length for diagnostic periodic var
 *          based on given bus speed and turnaround time.
*/
unsigned mfh_helper_calculate_diagpervar_wind_time_us(unsigned bus_speed, unsigned turn_around_time_us);

#endif
