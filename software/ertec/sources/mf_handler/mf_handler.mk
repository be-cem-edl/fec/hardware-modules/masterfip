# SPDX-FileCopyrightText: 2023 CERN (home.cern)
#
# SPDX-License-Identifier: LGPL-2.1-or-later

SRC_C += $(SRC_ROOT_DIR_APPL)/mf_handler/mfh_helper.c
SRC_C += $(SRC_ROOT_DIR_APPL)/mf_handler/mf_handler.c
SRC_C += $(SRC_ROOT_DIR_APPL)/mf_handler/nf_node.c

INCD += -I$(SRC_ROOT_DIR_APPL)/mf_handler/
