// SPDX-FileCopyrightText: 2023 CERN (home.cern)
//
// SPDX-License-Identifier: LGPL-2.1-or-later

#include <stddef.h>

#include "nf_node.h"
#include "os.h"
#include "pf_diag.h"
#include "pf_err.h"
#include "pf_alarm.h"
#include "profip_log.h"
#include "pf_utils.h"

#define MAX_SIZE_FOR_WF_VARIABLE 128 	/* bytes */
#define IDENT_VAR_SIZE 8 				/* bytes */
#define NF_NODE_CONS_STATUS_Q_SIZE 8

struct nf_node_status_t
{
	uint8_t cons_status;
	uint8_t prod_status;
	uint8_t cons_var_cnt;
	uint8_t prod_var_cnt;
	uint8_t prev_cons_status;
	uint8_t cons_err_cnt;
};

struct nf_node_t
{
	uint8_t is_initialized;
	uint8_t valid_prod_data;

	uint8_t agt_id;
	uint8_t reset_flag;

	uint16_t prod_slot;
	uint16_t cons_slot;

	uint8_t cons_status_queue[NF_NODE_CONS_STATUS_Q_SIZE];
	uint8_t cons_status_q_idx;

    struct mstrfip_data* ident_var;
    struct mstrfip_data* prod_var;
    struct mstrfip_data* cons_var;
    struct mstrfip_data* reset_var;

	struct nf_node_status_t status;
};

// #define NFNODE_TIME_TEST
#ifdef NFNODE_TIME_TEST
static uint8_t old_v[4];
static uint64_t old_t[4];
#endif

static struct nf_node_t nodes[FIP_MAX_NBR_OF_NODES];

/* ---- STATIC FUNCTIONS DECLATATIONS --- */

static int nf_node_update_cons_var(struct nf_node_t *node, struct mstrfip_dev *dev);

static struct nf_node_t* nf_node_get_by_slot(uint16_t slot);

static struct nf_node_t* nf_node_get_by_agt_id(uint32_t agt_id);

static struct nf_node_t* nf_node_get_empty_node(void);


static uint8_t nf_node_get_worst_status(struct nf_node_t *node) {
	uint8_t status = 0;

	for (int i=0; i<NF_NODE_CONS_STATUS_Q_SIZE; i++) {
		if (node->cons_status_queue[i] > status)
			status = node->cons_status_queue[i];
	}

	return status;
}

static struct nf_node_t* nf_node_get_by_agt_id(uint32_t agt_id)
{
	int i;

	for(i = 0; i < FIP_MAX_NBR_OF_NODES; i++)
	{
		struct nf_node_t* node = &nodes[i];
		if(node->is_initialized && node->agt_id == agt_id)
		{
			return node;
		}
	}

	return NULL;
}

static struct nf_node_t* nf_node_get_by_slot(uint16_t slot)
{
	int i;

	for(i = 0; i < FIP_MAX_NBR_OF_NODES; i++)
	{
		struct nf_node_t* node = &nodes[i];
		if(node->is_initialized && ( node->prod_slot == slot || node->cons_slot == slot) )
		{
			return node;
		}
	}

	return NULL;
}

static struct nf_node_t* nf_node_get_empty_node(void)
{
	int i;

	for(i = 0; i < FIP_MAX_NBR_OF_NODES; i++)
	{
		struct nf_node_t* node = &nodes[i];
		if(!node->is_initialized)
		{
			return node;
		}
	}

	return NULL;
}

void nf_node_init(void)
{
	memset(nodes, 0, sizeof(struct nf_node_t) * FIP_MAX_NBR_OF_NODES);
}

void nf_node_clear_err_coutners_all(void)
{
	int i;

	for(i=0; i<FIP_MAX_NBR_OF_NODES; i++)
	{
		nodes[i].status.cons_err_cnt = 0;
	}
}

static int nf_node_update_cons_var(struct nf_node_t *node, struct mstrfip_dev *dev)
{
	int ret = 0;

	mstrfip_var_update(dev, node->cons_var);

	node->status.cons_var_cnt++;
	node->status.prev_cons_status = node->status.cons_status;
	node->status.cons_status = node->cons_var->status;

	// if, for example, every second frame is incorrect,
	// then send the worst error from the last 8 frames.
	uint8_t prev_worst_status = nf_node_get_worst_status(node);

	node->cons_status_q_idx = (node->cons_status_q_idx + 1) % NF_NODE_CONS_STATUS_Q_SIZE;
	node->cons_status_queue[node->cons_status_q_idx] = node->status.cons_status;

	if (node->status.cons_status > prev_worst_status)
	{
		char tmpBuff[64];
		snprintf(tmpBuff, 64, "MasterFIP cons data is not OK (error: %d, var id: 0x%.4x)\n", node->status.cons_status, node->cons_var->id);
		ret = pf_err(PF_ERR_MODULE_NF_NODE, IE_NF_NODE_ERR_CONS_VAR_OTHER, node->status.cons_status, tmpBuff);

		if(node->status.cons_err_cnt < 0xFF)
		{
			node->status.cons_err_cnt++;
		}
	}

	if(ret == 0 && node->status.cons_status != 0)
	{
		ret = IE_NF_NODE_ERR_CONS_VAR_OTHER;
	}

	if(ret == 0 && (node->cons_var->bsz > MAX_SIZE_FOR_WF_VARIABLE))
	{
		ret = pf_err(PF_ERR_MODULE_NF_NODE, IE_NF_NODE_ERR_CONS_WRONG_SIZE,
				node->cons_var->bsz, "Wrong size for cons variable\n");
	}

	return ret;
}

int nf_node_update_all_cons_var(struct mstrfip_dev *dev)
{
	int i;

	OsEnterX(MUTEX_PRODDATA);

	for (i=0; i<FIP_MAX_NBR_OF_NODES; i++)
	{
		struct nf_node_t* node = &nodes[i];

		if (node->cons_var)
			nf_node_update_cons_var(node, dev);

#ifdef NFNODE_TIME_TEST
		if(node->cons_slot == 4)
		{
			if(old_v[2] != node->cons_var->buffer[0])
			{
				old_v[2] = node->cons_var->buffer[0];
				old_t[2] = OsGetTime_us();
			}
		}
#endif
	}

	OsExitX(MUTEX_PRODDATA);

	return 0;
}

int nf_node_update_all_ident_var(struct mstrfip_dev *dev)
{
	int i;

	OsEnterX(MUTEX_PRODDATA);

	for (i=0; i<FIP_MAX_NBR_OF_NODES; i++)
	{
		struct nf_node_t* node = &nodes[i];

		if (node->ident_var)
			mstrfip_ident_update(dev, node->ident_var);
	}

	OsExitX(MUTEX_PRODDATA);

	return 0;
};

int nf_node_send_all_prod_var(struct mstrfip_dev *dev)
{
	int i;

	OsEnterX(MUTEX_PRODDATA);

	for (i=0; i<FIP_MAX_NBR_OF_NODES; i++)
	{
		struct nf_node_t* node = &nodes[i];

#ifdef NFNODE_TIME_TEST
			if (node->prod_slot == 4)
			{
				if (old_v[1] != node->prod_var->buffer[0])
				{
					old_v[1] = node->prod_var->buffer[0];
					old_t[1] = OsGetTime_us();
				}
			}
#endif

		if (node->prod_var)
		{
			mstrfip_var_write(dev, node->prod_var);
			node->status.prod_status = NF_NODE_DATA_OK;
			node->status.prod_var_cnt++;
		}

		if (node->reset_var && node->reset_flag < 2)
		{
			mstrfip_var_write(dev, node->reset_var);

			memset(node->reset_var->buffer, 0, node->reset_var->bsz);
			node->reset_flag++;
		}
	}

	OsExitX(MUTEX_PRODDATA);

	return 0;
}

int nf_node_remove_all(void)
{
	memset(nodes, 0, sizeof(struct nf_node_t) * FIP_MAX_NBR_OF_NODES);

	return 0;
}

uint32_t nf_node_nbr_of_prod_var(void)
{
	int i, cnt = 0;

	for (i=0; i<FIP_MAX_NBR_OF_NODES; i++)
	{
		struct nf_node_t* node = &nodes[i];

		if (node->prod_var)
			cnt++;

		if (node->reset_var)
			cnt++;
	}
	return cnt;
}

uint32_t nf_node_nbr_of_cons_var(void)
{
	int i, cnt = 0;

	for (i=0; i<FIP_MAX_NBR_OF_NODES; i++)
	{
		struct nf_node_t* node = &nodes[i];

		if (node->cons_var)
			cnt++;
	}
	return cnt;
}

uint32_t nf_node_nbr_of_ident_var(void)
{
	int i, cnt = 0;

	for (i=0; i<FIP_MAX_NBR_OF_NODES; i++)
	{
		struct nf_node_t* node = &nodes[i];

		if (node->ident_var)
			cnt++;
	}
	return cnt;
}

uint32_t nf_node_get_prod_varlist(struct mstrfip_data** prod_varlist, uint32_t prod_varlist_size)
{
	int i, pos = 0;

	for (i=0; i<FIP_MAX_NBR_OF_NODES; i++)
	{
		struct nf_node_t* node = &nodes[i];

		if (pos < prod_varlist_size && node->prod_var)
			prod_varlist[pos++] = node->prod_var;

		if (pos < prod_varlist_size && node->reset_var)
			prod_varlist[pos++] = node->reset_var;
	}

	return pos;
}

uint32_t nf_node_get_cons_varlist(struct mstrfip_data** cons_varlist, uint32_t cons_varlist_size)
{
	int i, pos = 0;

	for (i=0; i<FIP_MAX_NBR_OF_NODES; i++)
	{
		struct nf_node_t* node = &nodes[i];

		if (pos < cons_varlist_size && node->cons_var)
			cons_varlist[pos++] = node->cons_var;
	}

	return pos;
}

uint32_t nf_node_get_ident_varlist(struct mstrfip_data** ident_varlist, uint32_t ident_varlist_size)
{
	int i, pos = 0;

	for (i=0; i<FIP_MAX_NBR_OF_NODES; i++)
	{
		struct nf_node_t* node = &nodes[i];

		if (pos < ident_varlist_size && node->ident_var)
			ident_varlist[pos++] = node->ident_var;
	}

	return pos;
}

enum nf_node_status nf_node_read_cons_var(uint16_t slot, uint8_t* data, uint32_t size, int cons_expired)
{
	struct nf_node_t *node = nf_node_get_by_slot(slot);

	if (!node)
	{
		struct nf_node_status_t status = {0};

		status.cons_status = NF_NODE_NOT_EXIST;
		status.prod_status = NF_NODE_NOT_EXIST;

		// clear var & copy status
		memset(data, 0, size);
		memcpy(data + size - sizeof(struct nf_node_status_t), &status, sizeof(struct nf_node_status_t));
		return NF_NODE_NOT_EXIST;
	}

	OsEnterX(MUTEX_PRODDATA);

	// copy node data
	if(node->cons_var != NULL && node->cons_slot == slot)
	{
		if(cons_expired)
			node->status.cons_status = NF_NODE_DATA_EXPIRED;

		if(node->cons_var->bsz + sizeof(struct nf_node_status_t) <= size) {

			if (node->status.cons_status == NF_NODE_DATA_FRAME_ERROR) {
				/* Send 0x0 in case of TIMEOUT */
				memset(node->cons_var->buffer, 0, node->cons_var->bsz);
				memset(data, 0, size);
			} else {
				memcpy(data, node->cons_var->buffer, node->cons_var->bsz);
			}
		}
		else
			node->status.cons_status = NF_NODE_WRONG_SIZE;

	}

	// copy status
	if(size >= sizeof(struct nf_node_status_t))
		memcpy(data + size - sizeof(struct nf_node_status_t), &node->status, sizeof(struct nf_node_status_t));
	else
		node->status.cons_status = NF_NODE_WRONG_SIZE;

#ifdef NFNODE_TIME_TEST
	if(slot == 4)
	{
		if(data[0] != old_v[3])
		{
			old_v[3] = data[0];
			old_t[3] = OsGetTime_us();

			PROFIP_LOG("T1 : %lld\n", (old_t[1] - old_t[0])/1000);
			PROFIP_LOG("T2 : %lld\n", (old_t[2] - old_t[0])/1000);
			PROFIP_LOG("T3 : %lld\n\n", (old_t[3] - old_t[0])/1000);
		}
	}
#endif

	OsExitX(MUTEX_PRODDATA);

	// if, for example, every second frame is incorrect,
	// then send the worst error from the last 8 frames.
	node->cons_status_queue[node->cons_status_q_idx] = node->status.cons_status;

	return nf_node_get_worst_status(node);
}

enum nf_node_status nf_node_write_prod_var(uint16_t slot, uint8_t* data, uint32_t size, int prod_expired)
{
	struct nf_node_t *node = nf_node_get_by_slot(slot);

	if (!node || !node->prod_var || node->prod_slot != slot)
		return NF_NODE_NOT_EXIST;

	OsEnterX(MUTEX_PRODDATA);

	if (prod_expired)
		node->status.prod_status = NF_NODE_DATA_EXPIRED;

	if (size >= node->prod_var->bsz)
	{
		memcpy(node->prod_var->buffer, data, node->prod_var->bsz);

#ifdef NFNODE_TIME_TEST
		if(slot == 4)
		{
			if(data[0] != old_v[0])
			{
				old_v[0] = data[0];
				old_t[0] = OsGetTime_us();
			}
		}
#endif
		node->valid_prod_data = 1U;
	}
	else
	{
		node->status.prod_status = NF_NODE_WRONG_SIZE;
	}

	OsExitX(MUTEX_PRODDATA);

	return node->status.prod_status;
}

enum nf_node_status nf_node_write_reset_var(uint16_t slot)
{
	struct nf_node_t *node = nf_node_get_by_slot(slot);

	if (!node || !node->reset_var)
		return NF_NODE_NOT_EXIST;

	PROFIP_LOG(__USER__"Send reset var to node 0x%.2x\n", node->agt_id);

	OsEnterX(MUTEX_PRODDATA);

	node->reset_flag = 0U;
	node->reset_var->buffer[1] = node->agt_id;

	OsExitX(MUTEX_PRODDATA);
	return NF_NODE_DATA_OK;
}

int nf_node_add_prod_var(uint16_t slot, uint8_t agt_id, uint32_t bsz, struct mstrfip_macrocycle * mcycle)
{
	int ret = IE_NF_NODE_OK;
    struct mstrfip_data_cfg var_cfg = {
        .flags = MSTRFIP_DATA_FLAGS_PROD,
		.id = PROD_VAR_ADDR(agt_id),
        .max_bsz = bsz
    };

	OsEnterX(MUTEX_PRODDATA);

	struct mstrfip_data* pvar;

	struct nf_node_t *node = nf_node_get_by_agt_id(agt_id);

	if (!node)
		node = nf_node_get_empty_node();

	if (!node) {
		ret = pf_err(PF_ERR_MODULE_NF_NODE, IE_NF_NODE_ERR_CANT_CREATE_VAR, agt_id,
				"Can't add prod var: can't get empty node\n");
		goto exit_add_prod_var;
	}

	if (node->prod_var) {
		ret = pf_err(PF_ERR_MODULE_NF_NODE, IE_NF_NODE_ERR_DUPLICATED_AGT, agt_id,
				"Can't add prod var: duplicated agt id\n");
		goto exit_add_prod_var;
	}

	pvar = mstrfip_var_create(mcycle, &var_cfg);

	if (!pvar) {
		ret = pf_err(PF_ERR_MODULE_NF_NODE, IE_NF_NODE_ERR_CANT_CREATE_VAR, agt_id,
				"Can't add prod var: can't create var\n");
		goto exit_add_prod_var;
	}

	node->is_initialized = 1U;
	node->prod_var = pvar;
	node->agt_id = agt_id;
	node->prod_slot = slot;

exit_add_prod_var:
	OsExitX(MUTEX_PRODDATA);
	return ret;
}

int nf_node_add_cons_var(uint16_t slot, uint8_t agt_id, uint32_t bsz, struct mstrfip_macrocycle * mcycle)
{
	int ret = IE_NF_NODE_OK;

    struct mstrfip_data_cfg var_cfg = {
        .flags = MSTRFIP_DATA_FLAGS_CONS,
		.id = CONS_VAR_ADDR(agt_id),
        .max_bsz = bsz
    };

    OsEnterX(MUTEX_PRODDATA);

	struct mstrfip_data* pvar;

	struct nf_node_t *node = nf_node_get_by_agt_id(agt_id);

	if (!node)
		node = nf_node_get_empty_node();

	if (!node) {
		ret = pf_err(PF_ERR_MODULE_NF_NODE, IE_NF_NODE_ERR_CANT_CREATE_VAR, agt_id,
				"Can't add cons var: can't get empty node\n");
		goto exit_add_cons_var;
	}

	if (node->cons_var) {
		ret = pf_err(PF_ERR_MODULE_NF_NODE, IE_NF_NODE_ERR_DUPLICATED_AGT, agt_id,
				"Can't add cons var: duplicated agt id\n");
		goto exit_add_cons_var;
	}

	pvar = mstrfip_var_create(mcycle, &var_cfg);

	if (!pvar) {
		ret = pf_err(PF_ERR_MODULE_NF_NODE, IE_NF_NODE_ERR_CANT_CREATE_VAR, agt_id,
				"Can't add cons var: can't create var\n");
		goto exit_add_cons_var;
	}

	node->is_initialized = 1U;
	node->cons_var = pvar;
	node->agt_id = agt_id;
	node->cons_slot = slot;

exit_add_cons_var:
	OsExitX(MUTEX_PRODDATA);
	return ret;
}

int nf_node_add_ident_var(uint8_t agt_id, struct mstrfip_macrocycle * mcycle)
{
	int ret = IE_NF_NODE_OK;
	struct mstrfip_data* pvar;

	OsEnterX(MUTEX_PRODDATA);

	struct nf_node_t *node = nf_node_get_by_agt_id(agt_id);

	if (!node)
		node = nf_node_get_empty_node();

	if (!node) {
		ret = pf_err(PF_ERR_MODULE_NF_NODE, IE_NF_NODE_ERR_CANT_CREATE_VAR, agt_id,
				"Can't add indent var: can't get empty node\n");
		goto exit_add_ident_var;
	}

	if (node->ident_var) {
		ret = IE_NF_NODE_OK; // ident variable already exists, but its OK
		goto exit_add_ident_var;
	}

	pvar = mstrfip_ident_var_create(mcycle, agt_id);

	if (!pvar) {
		ret = pf_err(PF_ERR_MODULE_NF_NODE, IE_NF_NODE_ERR_CANT_CREATE_VAR, agt_id,
				"Can't add ident var: can't create var\n");
		goto exit_add_ident_var;
	}

	node->is_initialized = 1U;
	node->ident_var = pvar;
	node->agt_id = agt_id;

exit_add_ident_var:
	OsExitX(MUTEX_PRODDATA);
	return ret;
}

int nf_node_add_reset_var(uint8_t agt_id, struct mstrfip_macrocycle * mcycle)
{
	int ret = IE_NF_NODE_OK;
	struct mstrfip_data* pvar;

    struct mstrfip_data_cfg var_cfg = {
        .flags = MSTRFIP_DATA_FLAGS_PROD,
		.id = RESET_VAR_ADDR(agt_id),
        .max_bsz = 4U
    };

	OsEnterX(MUTEX_PRODDATA);

	struct nf_node_t *node = nf_node_get_by_agt_id(agt_id);

	if (!node) {
		ret = pf_err(PF_ERR_MODULE_NF_NODE, IE_NF_NODE_ERR_CANT_CREATE_VAR, agt_id,
				"Can't add reset var: node doesn't exist\n");
		goto exit_add_reset_var;
	}

	if (node->reset_var) {
		ret = IE_NF_NODE_OK; // reset variable already exists, but its OK
		goto exit_add_reset_var;
	}

	pvar = mstrfip_var_create(mcycle, &var_cfg);

	if (!pvar) {
		ret = pf_err(PF_ERR_MODULE_NF_NODE, IE_NF_NODE_ERR_CANT_CREATE_VAR, agt_id,
				"Can't add prod var: can't create var\n");
		goto exit_add_reset_var;
	}

	node->reset_var = pvar;

exit_add_reset_var:
	OsExitX(MUTEX_PRODDATA);
	return ret;
}

void nf_node_clear_err(void)
{
	int i;

	for(i = 0; i < FIP_MAX_NBR_OF_NODES; i++)
	{
		struct nf_node_t* node = &nodes[i];
		node->status.cons_err_cnt = 0;
	}
}

enum nf_node_status nf_node_get_status_by_slot(uint16_t slot)
{
	int ret = NF_NODE_DATA_OK;

	struct nf_node_t *node = nf_node_get_by_slot(slot);

	if (node == NULL)
		return NF_NODE_NOT_EXIST;

	if(node->prod_slot == slot && node->prod_var != NULL)
		ret |= node->status.prod_status;

	if(node->cons_slot == slot && node->cons_var != NULL)
		ret |= nf_node_get_worst_status(node);

	return ret;
}

char* nf_node_get_status_name(enum nf_node_status status)
{
	switch(status)
	{
	case NF_NODE_DATA_OK:
		return "ok";
	case NF_NODE_DATA_FRAME_ERROR:
		return "\e[31mdata frame error\e[0m";
	case NF_NODE_DATA_PAYLOAD_ERROR:
		return "\e[31mdata payload error\e[0m";
	case NF_NODE_DATA_NOT_RECEIVED:
		return "\e[31mdata not received\e[0m";
	case NF_NODE_NOT_EXIST:
		return "\e[31mnot exist\e[0m";
	case NF_NODE_DATA_EXPIRED:
		return "\e[31mdata expired\e[0m";
	case NF_NODE_WRONG_SIZE:
	default:
		return "\e[31mother error\e[0m";
	}
}

enum nf_node_status nf_node_get_ident_var(uint32_t slot, uint8_t* buff, uint32_t size)
{
	struct nf_node_t* node = nf_node_get_by_slot(slot);

	if (!node)
		return NF_NODE_NOT_EXIST;

	if (size != IDENT_VAR_SIZE)
		return NF_NODE_WRONG_SIZE;

	if (!node->ident_var)
		return NF_NODE_NOT_EXIST;

	memcpy(buff, node->ident_var->buffer, IDENT_VAR_SIZE);
	return NF_NODE_DATA_OK;
}

int nf_node_wait_for_valid(uint32_t timeout_ms)
{
	unsigned int i, all_valid;
	uint64_t volatile time_start = OsGetTime_us();

	for(;;) {
		all_valid = 1U;

		for (i=0; i<FIP_MAX_NBR_OF_NODES; i++) {
			struct nf_node_t *node = &nodes[i];

			if(!node->is_initialized || !node->prod_var)
				continue;

			all_valid &= node->valid_prod_data;
		}

		if (all_valid)
			return 0;

		if (OsGetTime_us() > time_start + timeout_ms * 1000)
			return -1;

		OsWait_ms(1);
	}

	return 0;
}


void nf_node_print(void)
{
	int i,j;
	for(i=0; i<FIP_MAX_NBR_OF_NODES; i++)
	{
		struct nf_node_t *node = &nodes[i];

		if(!node->is_initialized)
			continue;

		PROFIP_LOG("NanoFIP node 0x%.2x\n", node->agt_id);

		if (node->ident_var)
		{
			PROFIP_LOG("\t"__OK__"IDENT %d bytes\n\t", node->ident_var->bsz);
			for(j=0; j<node->ident_var->bsz; j++)
				PROFIP_LOG("%.2x ", node->ident_var->buffer[j]);
			PROFIP_LOG("\n\n");
		}

		if (node->prod_var)
		{
			PROFIP_LOG("\t");

			if(node->status.prod_status == 0)
				PROFIP_LOG(__OK__);
			else
				PROFIP_LOG(__ERR__);

			PROFIP_LOG("PROD %d bytes (slot %d)", node->prod_var->bsz, node->prod_slot);
			PROFIP_LOG(" (cnt %d)", node->status.prod_var_cnt);

			for(j=0; j<node->prod_var->bsz; j++)
			{
				if(j%32 == 0)
					PROFIP_LOG("\n\t");
				else if(j%8 == 0)
					PROFIP_LOG("  ");
				PROFIP_LOG("%.2x ", node->prod_var->buffer[j]);
			}
			PROFIP_LOG("\n\n");
		}

		if (node->cons_var)
		{
			PROFIP_LOG("\t");

			if(node->status.cons_status == 0)
				PROFIP_LOG(__OK__);
			else
				PROFIP_LOG(__ERR__);

			PROFIP_LOG("CONS %d bytes (slot %d)", node->cons_var->bsz, node->cons_slot);
			PROFIP_LOG(" (cnt %d, err %d)", node->status.cons_var_cnt, node->status.cons_err_cnt);

			for(j=0; j<node->cons_var->bsz; j++)
			{
				if(j%32 == 0)
					PROFIP_LOG("\n\t");
				else if(j%8 == 0)
					PROFIP_LOG("  ");

				PROFIP_LOG("%.2x ", node->cons_var->buffer[j]);
			}
			PROFIP_LOG("\n\n");
		}
	}
}

void nf_node_print_status(void)
{
	int i;

	for(i=0; i<FIP_MAX_NBR_OF_NODES; i++)
	{
		struct nf_node_t *node = &nodes[i];

		if (node->is_initialized)
		{
			PROFIP_LOG("nanoFIP node 0x%.2x:  %s\n", node->agt_id,
					nf_node_get_status_name(nf_node_get_worst_status(node) |
							node->status.prod_status));
		}
	}
}

