// SPDX-FileCopyrightText: 2023 CERN (home.cern)
//
// SPDX-License-Identifier: LGPL-2.1-or-later

/**
 * @file mf_handler.h
 * @author Piotr Klasa (piotr.klasa@cerh.ch)
 * @brief Interface for managing the WorldFip network through the
 *        "libmasterFip" library.
 * 
 * Based on the configuration passed to this module, MF_Handler create
 * a macrocycle and programs it. It also provides an interface to
 * starts/stops macrocycle and to write and read variables from nodes.
 * Values (read/written) are updated after each macrocycle)
 */

#ifndef MF_HANDLER_H
#define MF_HANDLER_H

#include <stdint.h>

#include "pf_utils.h"
#include "nf_node_status.h"

/* Critical errors related to MasterFIP initialization */
enum mf_h_rc_t
{
	MF_H_RC_OK,							///< no problems
	MF_H_RC_DUPLICATED_AGTS,			///< wrong configuration, two nodes has the same agent ID
	MF_H_RC_CANT_CREATE_PROFIP_THREAD,  ///< OS problem, cannot create thread
	MF_H_RC_CANT_CREATE_PROFIP_QUEUE,   ///< OS problem, cannot create queue
	MF_H_RC_CANT_START_PROFIP_THREAD,   ///< OS problem, cannnot start thread
	MF_H_RC_NO_RMQ_COMMUNICATON,	    ///< MasterFip didnt replied (correctly) to a synchronous request throught RMQ 
	MF_H_RC_WRONG_VERSION,				///< MasterFip has an incompatible version
	MF_H_RC_CANT_GET_BUS_SPEED,			///< MasterFip didn't send the bus speed 
	MF_H_RC_MF_CONFIG_NOT_SET,          ///< Cannot write configuration to MasterFip
	MF_H_RC_CANT_CREATE_MACROCYCLE,     ///< Cannot create macrocycle
	MF_H_RC_NO_VARIABLE_DEFINED,        ///< Wrong configuration: no variable defined
	MF_H_RC_CANT_ADD_PERIODIC_VAR_WINDOW, ///< Can't add periodic var window
	MF_H_RC_CANT_ADD_APER_VAR_WIND,     ///< Can't add aperiodic var window
	MF_H_RC_CANT_ADD_WAIT_WINDOW,       ///< Can't add wait var window
	MF_H_RC_CANT_LOAD_MACROCYCLE,       ///< Can't load macrocycle
	MF_H_RC_WRONG_WAIT_WIND,            ///< Wait windows is too short
	MF_H_RC_CANT_START_MACROCYCLE,      ///< Can't start macrocycle
	MF_H_RC_CANT_ADD_WF_VARIABLE,       ///< Cant' add variable to macrocycle
	MF_H_RC_NO_MACROCYCLE,              ///< Macrocycle not exists
	MF_H_RC_CANT_CREATE_VAR,            ///< Can't create variable
	MF_H_RC_MF_NO_DATA,                 ///< MasterFIP didnt send data on time
	MF_H_RC_PROD_DATA_INVALID,          ///< Prod data for nodes not received from Profinet in time
	MF_H_RC_OTHER_ERROR                 ///< All other errors
};

/* MasterFip node configuration */
struct mf_h_cfg_agts_t
{
	uint8_t is_initialized;
	uint8_t agt_idx;
	uint8_t prod_bsz;
	uint8_t cons_bsz;
	uint8_t enable_ident;
	uint8_t enable_reset;
	uint16_t slot;
};

/* masterFip input configuration */
struct mf_h_cfg_t
{
	struct mf_h_cfg_agts_t agts[FIP_MAX_NBR_OF_NODES];

	uint8_t diag_enabled;

	/* inout */
	uint32_t turnaround_time_us;
	uint32_t wait_wind_time_us;
	uint32_t aper_var_wind_time_us;
};

/* actual parameters of MasterFip */
struct mf_h_stat_t
{
	uint32_t turnaround_time_us;
	uint32_t wait_wind_time_us;
	uint32_t aper_var_wind_time_us;
	uint8_t bus_speed;
	uint8_t prod_var_nbr;
	uint8_t cons_var_nbr;
};

/**
 * @brief Initialize MasterFip handler
*/
void mf_handler_init(void);

/**
 * @brief Based on passed configuration it resets the masterFip
 *        and programs the MasterFIP with new configuration.
*/
enum mf_h_rc_t mf_handler_program_macrocycle(struct mf_h_cfg_t* cfg);

/**
 * @brief Starts the macrocycle
*/
int mf_handler_start_macrocycle(void);

/**
 * @brief Compare actual MasterFip configuration with the one passsed in parameter.
 * @return 0, if configurations are the same
*/
int mf_handler_cmp_cfg(struct mf_h_cfg_t* cfg);

/**
 * @brief Stops the macrocycle
*/
int mf_handler_stop_macrocycle(void);

/**
 * @brief Resets the masterFip and clears the entire module configuration
*/
void mf_handler_reset_masterfip(void);

/**
 * @brief Write variable to a node. 
 *        This value will be sent in the next unstarted macrocycle
*/
enum nf_node_status mf_handler_write_var(uint8_t slot, uint8_t* data, uint32_t size);

/**
 * @brief Reads variable from a node. 
 *        This is the most current value for the variable.
 * @return 0 if the current value of the variable is correct, 
 *         otherwise nf_node_status
*/
enum nf_node_status mf_handler_read_var(uint8_t slot, uint8_t* data, uint32_t size);

/**
 * @brief Puts "agt id" into a "reset" variable. If a node receives
 *        such a variable, it resets itself. In the next macrocycle,
 *        the reset variable is automatically set to zero so that
 *        the module does not reset again.
*/
void mf_handler_write_reset_var(uint8_t slot);

/**
 * @brief returns the pointer to a structure with actual MasterFip
 *        parameters such as turnaround_time_us, wait_wind_time_us, 
 * 		  bus_speed, prod_var_nbr, cons_var_nbr.
*/
struct mf_h_stat_t* mf_handler_get_stat();

/**
 * @brief fills the given buffer with an identification variable.
 * @returns 0 on success.
 */
int mf_handler_get_ident_var(uint32_t slot, uint8_t* buff, uint32_t size);

/**
 * @brief Crears all MF_Handler error counters.
*/
void mf_handler_clear_err(void);

/**
 * @brief Prints information about MasterFip and all 
 *        nodes to console.
*/
void mf_handler_print_status(void);

/**
 * @brief Prints information about MasterFip configuration
 *        to console.
*/
void mf_handler_print_config(void);

/**
 * @brief Prints masterFIP version to the console.
*/
void mf_handler_print_version(void);

/**
 * @brief Prints all information about nodes to the console.
*/
void mf_handler_print_nodes(void);

/**
 * @brief Prints information about all configured slots
 *        to the console.
*/
void mf_handler_print_slots(void);

/**
 * @brief Prints presence list to the console.
*/
void mf_handler_print_presence_list(void);

/**
 * @brief Prints macrocycle configuration to 
 *        the console.
*/
void mf_handler_print_macrocycle(void);

#endif
