// SPDX-FileCopyrightText: 2023 CERN (home.cern)
//
// SPDX-License-Identifier: LGPL-2.1-or-later

/**
 * @file pf_alarm.h
 * @author Piotr Klasa (piotr.klasa@cerh.ch)
 * @brief Module responsible for setting and clearing alarms
 * @version 0.1
 */

#ifndef PF_ALARM_H
#define PF_ALARM_H

#include <stdint.h>

enum pf_alarm_type
{
	/* general ProFip errors - programming */
	PF_ALARM_SW_PROBLEM = 0, 		///< error occurred in software (not related to external factors)
	PF_ALARM_WRONG_FPGA_BITSTREAM, 	///< FPGA (on SVEC) is not programmed with correct bitstream
	PF_ALARM_WRONG_MF_VERS, 		///< masterFip (on Mock Turtle) has incompatible version
	PF_ALARM_WR_CFG_NO_SLOTS, 		///< wrong configuration - no nanoFip nodes added to macrocycle defined
	PF_ALARM_WR_CFG_WAIT_WIND, 		///< wrong configuration - soo short macrocycle duration
	PF_ALARM_DUPLICATED_AGTS, 		///< wrong configuration - each node should have a unique ID

	/* general ProFip errors - runtime */
	PF_ALARM_NO_PROD_DATA, 			///< no new data received since last reading
	PF_ALARM_NO_CONS_DATA, 			///< no data send received since last writting
	PF_ALARM_RUNTIME_SW_PROBLEM, 	///< error occurred in software (not related to external factors)

	/* node errors */
	PF_ALARM_DATA_FRAME_ERROR,  	///< frame fault: CRC, TMO, BAD_NBYTES,..
	PF_ALARM_PAYLOAD_ERROR, 		///< payload fault: !freshed, !significant..
	PF_ALARM_DATA_NOT_RECEIVED,		///< no data received since last reading

	PF_ALARM_CNT 					///< number of all alarm types
};

/**
 * @name pf_alarm_set
 * @param alarm
 * @param slot
 * @brief set alarm for slot
*/
void pf_alarm_set(enum pf_alarm_type alarm, uint8_t slot);

/**
 * @name pf_alarm_reset
 * @param alarm
 * @param slot
 * @brief reset alarm for slot
*/
void pf_alarm_reset(enum pf_alarm_type alarm, uint8_t slot);

/**
 * @name pf_alarm_init
 * @brief initialize module
*/
uint32_t pf_alarm_init();

/**
 * @name pf_alarm_ar_disconnected
 * @brief Information for the alarm module that the connection to ProFinet has been lost. All alarms will need to be resent after reconnecting.
*/
void pf_alarm_ar_disconnected();

/**
 * @name pf_alarm_asynq_req_completed
 * @brief Information for the module that previous async request (alarm is a async request) has beed finished.
*/
void pf_alarm_asynq_req_completed();

/**
 * @name pf_alarm_trigger
 * @brief Trigger send all pending async messages.
*/
void pf_alarm_trigger();

/**
 * @name pf_alarm_print
 * @brief Prints all active alarms
*/
void pf_alarm_print(void);

#endif
