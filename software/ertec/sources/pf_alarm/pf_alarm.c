// SPDX-FileCopyrightText: 2023 CERN (home.cern)
//
// SPDX-License-Identifier: LGPL-2.1-or-later

#include "pf_alarm.h"

#include "os.h"
#include "os_taskprio.h"
#include "pniousrd.h"
#include "usriod_utils.h"
#include "pf_err.h"
#include "profip_log.h"
#include "profip_led.h"
#include "pf_utils.h"

#define NBR_OF_SLOTS (FIP_MAX_NBR_OF_NODES + 2)

#define ALARM_INACTIVE 0
#define ALARM_REQ_TO_ACTIVE 1
#define ALARM_REQ_TO_INACTIVE 2
#define ALARM_ACTIVE 3

static const uint16_t pf_alarm_nbr[] =
{
	/* PF_ALARM_SW_PROBLEM */ 				0x100,
	/* PF_ALARM_WRONG_FPGA_BITSTREAM */ 	0x101,
	/* PF_ALARM_WRONG_MF_VERS */ 			0x102,
	/* PF_ALARM_WR_CFG_NO_SLOTS */ 			0x103,
	/* PF_ALARM_WR_CFG_WAIT_WIND */ 		0x104,
	/* PF_ALARM_DUPLICATED_AGTS */			0x105,
	/* PF_ALARM_NO_PROD_DATA */ 			0x200,
	/* PF_ALARM_NO_CONS_DATA */ 			0x201,
	/* PF_ALARM_RUNTIME_SW_PROBLEM */ 		0x202,
	/* PF_ALARM_DATA_FRAME_ERROR */ 		0x300,
	/* PF_ALARM_PAYLOAD_ERROR */ 			0x301,
	/* PF_ALARM_DATA_NOT_RECEIVED */ 		0x302
};

static const char* pf_alarm_desc[] =
{
	"SW_PROBLEM        ",
	"WR_FPGA_BITSTREAM ",
	"WRONG_MF_VERS     ",
	"WR_CFG_NO_SLOTS   ",
	"WR_CFG_WAIT_WIND  ",
	"DUPLICATED_AGTS   ",
	"NO_PROD_DATA      ",
	"NO_CONS_DATA      ",
	"RUNTIME_SW_PROBLEM",
	"DATA_FRAME_ERROR  ",
	"PAYLOAD_ERROR     ",
	"DATA_NOT_RECEIVED "
};

static const char* pf_alarm_status_name[] =
{
	"INACTIVE    ",
	"ACTIVATION  ",
	"DEACTIVATION",
	"ACTIVE      "
};

static uint8_t pf_alarms[NBR_OF_SLOTS][PF_ALARM_CNT];

static volatile uint8_t asynq_req_in_progress = 0;

/* Pf err task id*/
static uint32_t TskId_profip_alarm;

// Semaphore for triggering alarm task
static uint32_t pf_alarm_sem;

// for protection against changing alarm state between request and action
static uint32_t pf_alarm_tab_sem;

/* ---- static functions definitions ---- */

static void pf_alarm_main();

static void pf_alarm_update_leds(void);

/* ---- static functions implementations ---- */

static void pf_alarm_update_leds(void)
{
	int slot,alarm_type;
	int err = 0;

	for(slot=0; slot<NBR_OF_SLOTS; slot++)
	{
		for(alarm_type=0; alarm_type<PF_ALARM_CNT; alarm_type++)
		{
			if(pf_alarms[slot][alarm_type] == ALARM_REQ_TO_ACTIVE || pf_alarms[slot][alarm_type] == ALARM_ACTIVE)
			{
				err = 1;
				break;
			}
		}

		if(err == 1)
		{
			break;
		}
	}

	if(err == 1)
	{
		profip_led_blink(PROFIP_LED_RED, PROFIP_LED_BLINK_SLOW);
	}
	else
	{
		profip_led_off(PROFIP_LED_RED);
	}
}

static uint32_t alarm_set(enum pf_alarm_type alarm, uint8_t slot)
{
	uint32_t Status = PNIO_NOT_OK;
	PNIO_DEV_ADDR Addr;
	uint16_t DiagTag = pf_alarm_nbr[alarm];

    Addr.Geo.Slot = slot;
    Addr.Geo.Subslot = 1U;

    if( 0 != NumOfAr )
    {
		Status = PNIO_diag_channel_add
				(   PNIO_SINGLE_DEVICE_HNDL,  // Device handle
					PNIO_DEFAULT_API,         // API number
					&Addr,					  // location (slot/subslot)
					0x8000,				      // channel number 0..0x7fff , ChannelNumber shall be set to 0x8000 (special value indicating the whole submodule),
					pf_alarm_nbr[alarm],      // error number (see IEC 61158)
					PNIO_SUB_PROP_IN_OUT,	  // channel direction (input, output, input-output)
					DIAG_CHANPROP_TYPE_BYTE,  // channel type (data size)
					PNIO_FALSE,               // no maintenance required
					PNIO_FALSE,               // no maintenance demanded
					DiagTag);                 // user defined diag tag
    }

	if(Status == PNIO_OK)
	{
		PROFIP_ERR(__ERR__"ALARM 0x%x for slot %d active\n", pf_alarm_nbr[alarm], slot);
	}

	return Status;
}

static uint32_t alarm_reset(enum pf_alarm_type alarm, uint8_t slot)
{
	uint32_t Status = PNIO_NOT_OK;
	PNIO_DEV_ADDR Addr;
	uint16_t DiagTag = pf_alarm_nbr[alarm];

    Addr.Geo.Slot = slot;
    Addr.Geo.Subslot = 1U;

    if( 0 != NumOfAr )
    {
		Status = PNIO_diag_channel_remove
				(
					PNIO_SINGLE_DEVICE_HNDL,
					PNIO_DEFAULT_API,
					&Addr,                          // location (slot/subslot)
					0x8000,				            // channel number 0..0x7fff
					pf_alarm_nbr[alarm],            // error number (see IEC 61158), ChannelNumber shall be set to 0x8000 (special value indicating the whole submodule),
					PNIO_SUB_PROP_IN_OUT,	        // channel direction (input, output, input-output)
					DIAG_CHANPROP_TYPE_BYTE,        // channel type (data size)
					DiagTag,                        // user defined diag tag
					DIAG_CHANPROP_SPEC_ERR_DISAPP_MORE); // DIAG_CHANPROP_SPEC_ERR_DISAPP, DIAG_CHANPROP_SPEC_ERR_DISAPP_MORE
    }

    if(Status == PNIO_OK)
    {
    	PROFIP_ERR(__OK__"ALARM 0x%x for slot %d inactive\n", pf_alarm_nbr[alarm], slot);
    }

    return Status;
}

void pf_alarm_set(enum pf_alarm_type alarm, uint8_t slot)
{
	uint8_t trigger_thread = 0;

	OsTakeSemB(pf_alarm_tab_sem);

	if(pf_alarms[slot][alarm] == ALARM_INACTIVE || pf_alarms[slot][alarm] == ALARM_REQ_TO_INACTIVE)
	{
		pf_alarms[slot][alarm] = ALARM_REQ_TO_ACTIVE;

		if( 0 != NumOfAr )
		{
			trigger_thread = 1;
		}
	}

	OsGiveSemB(pf_alarm_tab_sem);

	if(trigger_thread)
	{
		OsGiveSemB(pf_alarm_sem);
	}
}

void pf_alarm_reset(enum pf_alarm_type alarm, uint8_t slot)
{
	uint8_t trigger_thread = 0;

	OsTakeSemB(pf_alarm_tab_sem);

	if(pf_alarms[slot][alarm] == ALARM_ACTIVE || pf_alarms[slot][alarm] == ALARM_REQ_TO_ACTIVE)
	{
		pf_alarms[slot][alarm] = ALARM_REQ_TO_INACTIVE;

		if( 0 != NumOfAr )
		{
			trigger_thread = 1;
		}
	}

	OsGiveSemB(pf_alarm_tab_sem);

	if(trigger_thread)
	{
		OsGiveSemB(pf_alarm_sem);
	}
}

/* processing all asynq requests in loop */
static void pf_alarm_main()
{
	int slot,alarm_type;

	PROFIP_LOG(__OK__"ProFip alarm thread started\n");

    /* Initialize semaphores */
    OsAllocSemB(&pf_alarm_sem);

	OsAllocSemB(&pf_alarm_tab_sem);
	OsGiveSemB(pf_alarm_tab_sem);

	for(;;)
	{
		OsTakeSemB(pf_alarm_sem);

		OsTakeSemB(pf_alarm_tab_sem);

		for(slot=0; slot<NBR_OF_SLOTS; slot++)
		{
			if(asynq_req_in_progress)
				break;

			for(alarm_type=0; alarm_type<PF_ALARM_CNT; alarm_type++)
			{
				if(pf_alarms[slot][alarm_type] == ALARM_REQ_TO_ACTIVE)
				{
					if(alarm_set(alarm_type, slot) == PNIO_OK)
					{
						pf_alarms[slot][alarm_type] = ALARM_ACTIVE;
						asynq_req_in_progress = 1U;
						break;
					}
					else
					{
						PROFIP_ERR(__ERR__"Cant set alarm %d %d\n", slot, alarm_type);
					}
				}

				if(pf_alarms[slot][alarm_type] == ALARM_REQ_TO_INACTIVE)
				{
					if(alarm_reset(alarm_type, slot) == PNIO_OK)
					{
						pf_alarms[slot][alarm_type] = ALARM_INACTIVE;
						asynq_req_in_progress = 1U;
						break;
					}
					else
					{
						PROFIP_ERR(__ERR__"Cant reset alarm %d %d\n", slot, alarm_type);
					}
				}
			}
		}

		OsGiveSemB(pf_alarm_tab_sem);

		pf_alarm_update_leds();
	}
}

void pf_alarm_asynq_req_completed()
{
	asynq_req_in_progress = 0U;

	if( 0 != NumOfAr )
	{
		OsGiveSemB(pf_alarm_sem);
	}
}

void pf_alarm_trigger()
{
	if( 0 != NumOfAr )
	{
		OsGiveSemB(pf_alarm_sem);
	}
}

void pf_alarm_ar_disconnected()
{
	if(0 == NumOfAr)
	{
		int slot,alarm_type;

		OsTakeSemB(pf_alarm_tab_sem);

		for(slot=0; slot<NBR_OF_SLOTS; slot++)
		{
			/* alarm status could be lost during disconnection */

			for(alarm_type=0; alarm_type<PF_ALARM_CNT; alarm_type++)
			{
				if(pf_alarms[slot][alarm_type] == ALARM_ACTIVE)
				{
					pf_alarms[slot][alarm_type] = ALARM_REQ_TO_ACTIVE;
				}
			}
		}

		OsGiveSemB(pf_alarm_tab_sem);

		asynq_req_in_progress = 0U;

		PROFIP_DEBUG("pf_alarm_ar_disconnected\n");
	}
}

uint32_t pf_alarm_init(void)
{
		uint32_t Status;

        /* Start profip thread */
	    Status = OsCreateThread (pf_alarm_main, (PNIO_UINT8*)"ProFip_alarm", TASK_PRIO_PROFIP_ALARM_LOW, &TskId_profip_alarm);
        if(PNIO_OK != Status)
        {
        	PROFIP_ERR("Cannot create pf alarm thread\n");
            return Status;
        }

	    Status = OsCreateMsgQueue (TskId_profip_alarm);   // install the task message queue
        if(PNIO_OK != Status)
        {
        	PROFIP_ERR("Cannot create pf alarm msg queue!\n");
            return Status;
        }

	    Status = OsStartThread (TskId_profip_alarm);		// start, after the task message queue has been installed
        if(PNIO_OK != Status)
        {
        	PROFIP_ERR("Cannot start pf alarm thread!\n");
            return Status;
        }

        return PNIO_OK;
}

void pf_alarm_print(void)
{
	int slot,alarm_type;
	int exists = 0;

	PROFIP_LOG("slot | type | description       | state\n");
	PROFIP_LOG("-----+------+-------------------+-------\n");

	for(slot=0; slot<NBR_OF_SLOTS; slot++)
	{
		for(alarm_type=0; alarm_type<PF_ALARM_CNT; alarm_type++)
		{
			uint8_t alarm_status = pf_alarms[slot][alarm_type];

			if(alarm_status != ALARM_INACTIVE)
			{
				PROFIP_LOG(" %3d | %4x | %s| %s\n", slot,
							pf_alarm_nbr[alarm_type],
							pf_alarm_desc[alarm_type], 
							pf_alarm_status_name[alarm_status]);
				exists = 1;
			}
		}
	}

	if(exists == 0)
	{
		PROFIP_LOG("     |      |                   | no active alarms\n");
	}
}
