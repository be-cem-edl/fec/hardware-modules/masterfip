// SPDX-FileCopyrightText: 2024 CERN (home.cern)
//
// SPDX-License-Identifier: LGPL-2.1-or-later

#ifndef DATA_UART_CMD_H
#define DATA_UART_CMD_H

#define CMD_SEND_DIAG_SHM 		0x06B6B6B0
#define CMD_SEND_DIAG_SHM_EXT 	0x07B7B7B0
#define CMD_FW_UPDATE     		0x09B9B9B0
#define CMD_SET_TIME      		0x0CBCBCB0

#define CMD_GET_HEADER			0x0AAAAAA0
#define CMD_GET_PAYLOAD			0x0BBBBBB0
#define CMD_SUCCESS				0x0CCCCCC0
#define CMD_ERROR				0x0DDDDDD0

#define CMD_DIAG_SYNC			0x1AAAAAA1

#endif
