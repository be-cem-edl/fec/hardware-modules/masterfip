// SPDX-FileCopyrightText: 2023 CERN (home.cern)
//
// SPDX-License-Identifier: LGPL-2.1-or-later

#include "profip_wdg.h"
#include "bspadapt.h"
#include "os.h"
#include "os_taskprio.h"
#include "pniousrd.h"
#include "pf_err.h"
#include "profip_log.h"

#define ENABLE_PF_WDG

#define WDG_TASK_PERIOD_MS (10)
#define WDG_MODULES_NBR (3)

#define WDG_INT_ERR_MODULE_CNT_EXPIRED -1

	/* critical errors related to WDG */
#define WDG_INT_ERR_CANT_CREATE_WDG_THREAD -2
#define WDG_INT_ERR_CANT_CREATE_WDG_QUEUE -3
#define WDG_INT_ERR_CANT_START_WDG_THREAD -4
#define WDG_INT_ERR_CRITICAL_ERROR_OTHER_ERROR -5


/* Watchdog task id*/
PNIO_UINT32   TskId_profip_wdg_main;

/* Watchdog semaphore */
static uint32_t wdg_sem;

struct profip_wdg_cfg_t
{
	uint32_t timeout_ms;
	uint32_t cnt;
	uint32_t is_started;
	uint32_t expired;
};

static struct profip_wdg_cfg_t profip_wdg_cfg[WDG_MODULES_NBR];

static void profip_wdg_main(void)
{
	int i;
	int nbr_of_modules_ok = 0;

	OsWaitOnEnable();

	PROFIP_LOG(__OK__"Watchdog thread started\n");

#ifdef ENABLE_PF_WDG
	Bsp_hw_watchdog_start();
#endif

	for(;;)
	{
		nbr_of_modules_ok = 0;

		OsTakeSemB(wdg_sem);

		for(i=0; i<WDG_MODULES_NBR; i++)
		{
			if(profip_wdg_cfg[i].is_started)
			{
				if(profip_wdg_cfg[i].cnt > 0)
				{
					nbr_of_modules_ok++;
					profip_wdg_cfg[i].cnt--;

					if(profip_wdg_cfg[i].expired == 1U)
					{
						PROFIP_LOG(__OK__"WDG: Module %d works again\n", i);
						profip_wdg_cfg[i].expired = 0U;
					}
				}
				else
				{
					if(profip_wdg_cfg[i].expired == 0U)
					{
						profip_wdg_cfg[i].expired = 1U;
						pf_err(PF_ERR_MODULE_WDG, WDG_INT_ERR_MODULE_CNT_EXPIRED, i, "Module counter expired\n");
					}
				}
			}
			else
			{
				nbr_of_modules_ok++;
			}
		}

		 OsGiveSemB(wdg_sem);

		if(nbr_of_modules_ok == WDG_MODULES_NBR)
		{
#ifdef ENABLE_PF_WDG
			Bsp_hw_watchdog_trigger();
#endif
		}

		OsWait_ms(WDG_TASK_PERIOD_MS);
	}
}


uint32_t profip_wdg_init(void)
{
	PNIO_UINT32 Status;
	int i;

    /* Initialize semaphores */
    OsAllocSemB(&wdg_sem);

	for(i=0; i<WDG_MODULES_NBR; i++)
	{
		profip_wdg_cfg[i].cnt = 0;
		profip_wdg_cfg[i].is_started = 0;
	}

    OsGiveSemB(wdg_sem);

#ifdef ENABLE_PF_WDG
	Bsp_hw_watchdog_init(WDG_TASK_PERIOD_MS * 100, BSP_WD_1MS);
#endif

	/* Start WDG thread */ //TODO - change priority to the highest!
	Status = OsCreateThread (profip_wdg_main, (PNIO_UINT8*)"Wdg_Main", TASK_PRIO_WDG, &TskId_profip_wdg_main);
	if(PNIO_OK != Status)
	{
		PROFIP_ERR("Cannot create wdg thread\n");
		return WDG_INT_ERR_CANT_CREATE_WDG_THREAD;
	}

	Status = OsCreateMsgQueue (TskId_profip_wdg_main);   // install the task message queue
	if(PNIO_OK != Status)
	{
		PROFIP_ERR("Cannot create wdg msg queue!\n");
		return WDG_INT_ERR_CANT_CREATE_WDG_QUEUE;
		return Status;
	}

	Status = OsStartThread (TskId_profip_wdg_main);		// start, after the task message queue has been installed
	if(PNIO_OK != Status)
	{
		PROFIP_ERR("Cannot start wdg thread!\n");
		return WDG_INT_ERR_CANT_START_WDG_THREAD;
		return Status;
	}

	return PNIO_OK;
}

void profip_wdg_start(enum profip_wdg_module_t module, uint32_t timeout_ms)
{
	OsTakeSemB(wdg_sem);

	if(module < WDG_MODULES_NBR && profip_wdg_cfg[module].is_started == 0)
	{
		profip_wdg_cfg[module].timeout_ms = timeout_ms;
		profip_wdg_cfg[module].is_started = 1U;
		profip_wdg_cfg[module].cnt = 1 + (profip_wdg_cfg[module].timeout_ms / WDG_TASK_PERIOD_MS);
	}

	OsGiveSemB(wdg_sem);
}

void profip_wdg_trigger(enum profip_wdg_module_t module)
{
	OsTakeSemB(wdg_sem);

	if(module < WDG_MODULES_NBR)
	{
		profip_wdg_cfg[module].cnt = 1 + (profip_wdg_cfg[module].timeout_ms / WDG_TASK_PERIOD_MS);
	}

	OsGiveSemB(wdg_sem);
}
