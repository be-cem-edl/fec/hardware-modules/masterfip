// SPDX-FileCopyrightText: 2023 CERN (home.cern)
//
// SPDX-License-Identifier: LGPL-2.1-or-later

/**
 * @copyright 2023 CERN (www.cern.ch)
 * @author Piotr Klasa <piotr.klasa@cern.ch>
 * @file profip_led.h
 * @brief Module for controlling LED on PROFIP device
 */

#ifndef PROFIP_LED_H
#define PROFIP_LED_H

#include <stdint.h>

#define PROFIP_LED_BLINK_FAST 1U /* 8 Hz */
#define PROFIP_LED_BLINK_SLOW 8U /* 1 Hz */

typedef enum
{
	PROFIP_LED_YELLOW = 0,
	PROFIP_LED_RED = 1,
	PROFIP_LED_GREEN = 2
}profip_led_T;

/**
 * @name profip_led_init
 * @brief Initialize leds
 */
void profip_led_init(void);

/**
 * @name profip_led_init
 * @brief Blink led
 */
void profip_led_blink(profip_led_T led, uint8_t mode);

/**
 * @name profip_led_on
 * @brief Turn on the selected LED
 * @param led
 */
void profip_led_on(profip_led_T led);

/**
 * @name profip_led_off
 * @brief Turn off the selected LED
 * @param led
 */
void profip_led_off(profip_led_T led);

/**
 * @name profip_led_toggle
 * @brief Toggle the selected LED
 * @param led
 */
void profip_led_toggle(profip_led_T led);


uint8_t profip_led_get(profip_led_T led);

#endif
