// SPDX-FileCopyrightText: 2023 CERN (home.cern)
//
// SPDX-License-Identifier: LGPL-2.1-or-later

/**
 * @author Piotr Klasa <piotr.klasa@cern.ch>
 * @file profip_gpio.h
 * @brief Module for controlling GPIOs on PROFIP device
 */

#ifndef PROFIP_GPIO_H
#define PROFIP_GPIO_H

#include <stdint.h>

enum profip_gpio_dir_T
{
	PROFIP_GPIO_DIR_OUT = 0,
	PROFIP_GPIO_DIR_IN = 1,
};

enum profip_gpio_alt_T
{
	PROFIP_GPIO_GPIO = 0,
	PROFIP_GPIO_ALT1 = 1,
	PROFIP_GPIO_ALT2 = 2,
	PROFIP_GPIO_ALT3 = 3
};

typedef struct
{
	uint8_t port;
	uint8_t pin;
	enum profip_gpio_dir_T dir;
	enum profip_gpio_alt_T alt;
	uint8_t init_val;
}profip_gpio_cfg_T;

typedef enum {
	PROFIP_GPIO_LED_YELLOW = 0U,
	PROFIP_GPIO_LED_RED,
	PROFIP_GPIO_LED_GREEN,
	PROFIP_GPIO_STATUS_C_0_RMQ_0,
	PROFIP_GPIO_STATUS_C_0_RMQ_1,
	PROFIP_GPIO_STATUS_C_0_RMQ_2,
	PROFIP_GPIO_STATUS_C_0_RMQ_3,
	PROFIP_GPIO_STATUS_C_0_RMQ_4,
	PROFIP_GPIO_STATUS_C_0_RMQ_5,
	PROFIP_GPIO_STATUS_C_1_RMQ_0,
	PROFIP_GPIO_IP_RESET,
	PROFIP_GPIO_MT_CPU_RESET,
	PROFIP_GPIO_MT_CPU_RESET_LOOPBACK,
	PROFIP_GPIO_DATA_UART_TX,
	PROFIP_GPIO_DATA_UART_RX
}profip_gpio_T;

/**
 * @name profip_gpio_init
 * @brief Call this function before using the other ones
 */
void profip_gpio_init(void);

/**
 * @name profip_gpio_write
 * @param gpio
 * @param value
 * @brief Sets the given value to the selected port
 */
void profip_gpio_write(profip_gpio_T gpio, uint8_t value);


/**
 * @name profip_gpio_read
 * @param gpio
 * @returns value
 * @brief Returns the value of the selected port. 1 = High, 0 = Low
 */
uint8_t profip_gpio_read(profip_gpio_T gpio);

#endif
