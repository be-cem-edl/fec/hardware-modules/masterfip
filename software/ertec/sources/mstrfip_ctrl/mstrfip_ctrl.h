// SPDX-FileCopyrightText: 2023 CERN (home.cern)
//
// SPDX-License-Identifier: LGPL-2.1-or-later

/**
 * @file mstrfip_ctrl.h
 * @name MasterFip control
 * @brief Functions for controlling MasterFip
 */

#ifndef MSTRFIP_CTRL_H
#define MSTRFIP_CTRL_H

/**
 * @defgroup mstrfip_ctrl MasterFip control
 * Set of functions to control
 * @{
 */

/**
 * @brief Disables all MockTurtle CPUs
*/
int mstrfip_ctrl_cpu_disable(void);

/**
 * @brief Enables all MockTurtle CPUs
*/
int mstrfip_ctrl_cpu_enable(void);

/**@}*/
#endif
