// SPDX-FileCopyrightText: 2023 CERN (home.cern)
//
// SPDX-License-Identifier: LGPL-2.1-or-later

#include "mstrfip_ctrl.h"

#include "profip_log.h"
#include "profip_gpio.h"

#include "os.h"

int mstrfip_ctrl_cpu_disable(void)
{
    PROFIP_DEBUG(__DEBUG__"All CPUs disabled\n");

    profip_gpio_write(PROFIP_GPIO_IP_RESET, 0U);
    profip_gpio_write(PROFIP_GPIO_MT_CPU_RESET, 0U);

    OsWait_ms(5);

    return 0;
}

int mstrfip_ctrl_cpu_enable(void)
{
	PROFIP_DEBUG(__DEBUG__"All CPUs enabled\n");

    profip_gpio_write(PROFIP_GPIO_IP_RESET, 1U);
    profip_gpio_write(PROFIP_GPIO_MT_CPU_RESET, 1U);

    OsWait_ms(25);

    return 0;
}
