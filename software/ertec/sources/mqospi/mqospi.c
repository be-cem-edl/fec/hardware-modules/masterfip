// SPDX-FileCopyrightText: 2023 CERN (home.cern)
//
// SPDX-License-Identifier: LGPL-2.1-or-later

#include <stdio.h>
#include "mqospi.h"
#include "profip_gpio.h"
#include "pnioerrx.h"
#include "pnio_types.h"

#include "bspadapt.h"
#include <cyg/hal/hal_io.h>
#include <stdint.h>

#include "profip_log.h"
#include "fr_timer.h"
#include "spi_dma.h"
#include "pf_err.h"
#include "pf_utils.h"

//#define MQOSPI_TRACE /* uncomment for printing the whole SPI communication */

#define ET_DEFAULT_TIMEOUT_US 30000

/* Statuses of RMQs */
#define MQOSPI_RMQ_EMPTY 1U
#define MQOSPI_RMQ_NOT_EMPTY 0U

/* Internal mqospi error codes */
#define MQOSPI_NO_VALID_FRAME -100
#define MQOSPI_SYNC_TIMEOUT -101
#define MQOSPI_PURGE_TIMEOUT -102
#define MQOSPI_WRONG_SYNC_ID_SYNC -103
#define MQOSPI_WRONG_CTRL -104
#define MQOSPI_WRONG_SYNC_ID_ASYNC -105

#define NBR_OF_ALL_RMQS 7
#define RMQ_INDEX(cpu_id, rmq_id) (6 * (cpu_id) + (rmq_id))

/* private functions */
static int mqospi_take_semaphore(unsigned int timeout_us);
static int mqospi_give_semaphore(void);

static int mqospi_spi_write_message(unsigned char idx_cpu, unsigned char idx_hmq, struct mqospi_msg *msg);
static int mqospi_spi_read_message(unsigned char idx_cpu, unsigned char idx_hmq, struct mqospi_msg *msg);

static uint16_t mqospi_next_sync(void);

/* SyncID verification*/
static uint16_t old_syncId[NBR_OF_ALL_RMQS] = {0};

/* mqospi poll map
 * index = core_id * 6 + rmq_id */
const uint8_t mqospi_poll_map[NBR_OF_ALL_RMQS] = {
		PROFIP_GPIO_STATUS_C_0_RMQ_0,
		PROFIP_GPIO_STATUS_C_0_RMQ_1,
		PROFIP_GPIO_STATUS_C_0_RMQ_2,
		PROFIP_GPIO_STATUS_C_0_RMQ_3,
		PROFIP_GPIO_STATUS_C_0_RMQ_4,
		PROFIP_GPIO_STATUS_C_0_RMQ_5,
		PROFIP_GPIO_STATUS_C_1_RMQ_0
};

static int mqospi_take_semaphore(unsigned int timeout_us)
{
	OsEnterX(MUTEX_MQOSPI);

	return 0;
}

static int mqospi_give_semaphore(void)
{
	OsExitX(MUTEX_MQOSPI);

	return 0;
}

static uint32_t calc_ctrlnum(struct mqospi_msg *msg)
{
    uint32_t ctrlnbr = 0;

	if (msg->hdr.len > 0) {
		ctrlnbr = msg->data[0] ^ 0xAAAAAAAA;
		ctrlnbr += msg->data[msg->hdr.len - 1] ^ 0x55555555;
	}

    return ctrlnbr;
}

/** @brief write one message to a given RMQ
 *  @param idx_cpu
 *  @param idx_hmq RMQ ID
 *  @param msg message
 *  @return on success: msg size (header size + payload size),
 *  		on fault: error code
 **/
static int mqospi_spi_write_message(unsigned char idx_cpu, unsigned char idx_hmq, struct mqospi_msg *msg)
{
	int ret;

	unsigned char wr_buff[ sizeof(struct mqospi_msg) + 2*4 ];

	uint32_t total_size, payload_size;

    total_size = (msg->hdr.len + sizeof(msg->hdr)/4);
    payload_size = msg->hdr.len;

	// basic_info
    wr_buff[3] = 0; //direction: write
    wr_buff[2] = idx_cpu*6 + idx_hmq; //((idx_cpu << 3) &  0x08) | ((idx_hmq << 0) & 0x0007);  //cpu_id & hmq_id
    wr_buff[1] = payload_size; //size
    wr_buff[0] = 0; //reserved

    memcpy(wr_buff+4, (unsigned char*) msg, total_size*4);

    //add CRC at the end of the frame - not implemented in mockturtle yet
    /*uint32_t crc = crc32_1byte((wr_buff + 4), total_size*4);
    memcpy(wr_buff + 4 + total_size*4, &crc, sizeof(crc));*/

	#ifdef MQOSPI_TRACE
	PROFIP_PRINTF("TRACE (W) [");
	int i;
	for(i=0; i<(total_size + 1); i++)
	{
		PROFIP_PRINTF("%8x ", ((uint32_t*)wr_buff)[i]);
	}
	PROFIP_PRINTF("]\n");
	#endif

	spi_dma_start_frame();

	ret = spi_dma_send((uint32_t*)wr_buff, (total_size + 1), ET_DEFAULT_TIMEOUT_US);

    spi_dma_end_frame();

	return (ret == 0) ? (total_size): ret;
}

/* trace MQOSPI eroors */
#define TRACE_MQOSPI_E

#ifdef TRACE_MQOSPI_E
#define TRACE_MQOSPI_BUFF_SIZE 24
struct trace_mqospi{
	struct mqospi_msg msg;
	uint32_t header;
	unsigned char idx_cpu;
	unsigned char idx_hmq;
	int ret;
};

static struct trace_mqospi trace_mqospi_buff[TRACE_MQOSPI_BUFF_SIZE];
static uint32_t trace_mqospi_buff_idx = 0;

void trace_mqospi_error(int ret, unsigned char idx_cpu, unsigned char idx_hmq, uint32_t header, struct mqospi_msg* msg)
{
	if(ret != -1)
	{
		if(trace_mqospi_buff_idx < TRACE_MQOSPI_BUFF_SIZE)
		{
			trace_mqospi_buff[trace_mqospi_buff_idx].idx_cpu = idx_cpu;
			trace_mqospi_buff[trace_mqospi_buff_idx].idx_hmq = idx_hmq;
			trace_mqospi_buff[trace_mqospi_buff_idx].header = header;
			trace_mqospi_buff[trace_mqospi_buff_idx].ret = ret;
			memcpy(&(trace_mqospi_buff[trace_mqospi_buff_idx].msg), msg, sizeof(struct mqospi_msg));
			trace_mqospi_buff_idx++;
		}
	}
}

#endif

/** @brief read one message from a given RMQ
 *  @param idx_cpu
 *  @param idx_hmq
 *  @param msg
 *  @return on success: msg size (payload size)
 *  		on fault: error code
 **/
static int mqospi_spi_read_message(unsigned char idx_cpu, unsigned char idx_hmq, struct mqospi_msg *msg)
{
	static int msg_cnt[7] = {0};
	msg_cnt[idx_hmq + 6*idx_cpu]++;

	unsigned char wr_rd_buff[2 * 4];
	int ret = 0;
	uint32_t size, sync_id;
    uint32_t header = 0;

	// basic_info
    wr_rd_buff[3] = 1; //direction: read
    wr_rd_buff[2] = idx_cpu*6 + idx_hmq; //((idx_cpu << 3) &  0x08) | ((idx_hmq << 0) & 0x0007);  //cpu_id & hmq_id
    wr_rd_buff[1] = 0; // reserved
    wr_rd_buff[0] = 0; // reserved

    spi_dma_start_frame();

	#ifdef MQOSPI_TRACE
    int i;
	PROFIP_PRINTF("TRACE (R) [");
	PROFIP_PRINTF("%8x ", ((uint32_t*)wr_rd_buff)[0]);
	#endif

    ret = spi_dma_exchange((uint32_t*)wr_rd_buff, (uint32_t*)wr_rd_buff, 2, ET_DEFAULT_TIMEOUT_US);

	#ifdef MQOSPI_TRACE
    PROFIP_PRINTF("%8x ", ((uint32_t*)wr_rd_buff)[1]);
	#endif

    if(ret == 0)
    {
    	header = (((uint32_t*)wr_rd_buff)[1]); //read header with message size
    	size = (header & 0xFFFF) - 1; //size is (payload size) - crc(4 bytes)
    	sync_id = (header & 0xFFFF0000) >> 16;

        if(size > 1 && size < 128)
        {
        	//reveive payload size + crc(4 bytes)
        	ret = spi_dma_receive(((uint32_t*)msg->data), (size + 1), ET_DEFAULT_TIMEOUT_US);

			#ifdef MQOSPI_TRACE
			for(i=0; i<size; i++)
			{
				PROFIP_PRINTF("%8x ", ((uint32_t*)msg->data)[i] );
			}
			#endif

        	msg->hdr.flags = 0;
        	msg->hdr.len = size;
        	msg->hdr.msg_id = 0;
        	msg->hdr.rt_app_id = 0;
        	msg->hdr.seq = 0;
        	msg->hdr.sync_id = sync_id;
        }
        else
        {
        	ret = MQOSPI_NO_VALID_FRAME;
        	char buff_tmp[90];
        	snprintf(buff_tmp, 90, "No valid frame in RMQ (%d, %d, %lx, %lu, %lu)\n", idx_cpu, idx_hmq, header, size, sync_id);
        		pf_err(PF_ERR_MODULE_MQOSPI, MQOSPI_NO_VALID_FRAME, header, buff_tmp);
        }
    }

    if(ret == 0)
    {
    	//check control nbr
    	uint32_t ctrlnbr_calculated = calc_ctrlnum(msg);
    	uint32_t ctrlnbr_received = ((uint32_t*)msg->data)[size];
    	if(ctrlnbr_calculated != ctrlnbr_received)
    	{
    		char buff_tmp[90];
        	ret = MQOSPI_WRONG_CTRL;
        	snprintf(buff_tmp, 90, "Wrong CtrlNbr (%lu != %lu) (RMQ %d C %d) %d\n",
        			ctrlnbr_calculated, ctrlnbr_received, idx_hmq, idx_cpu, msg_cnt[idx_hmq + 6* idx_cpu]);
        	pf_err(PF_ERR_MODULE_MQOSPI, MQOSPI_WRONG_CTRL, header, buff_tmp);
    	}
    }

#ifdef TRACE_MQOSPI_E
    if(ret != 0)
    {
    	trace_mqospi_error(ret, idx_cpu, idx_hmq, header, msg);
    }
#endif

	spi_dma_end_frame();

	#ifdef MQOSPI_TRACE
	PROFIP_PRINTF("]\n");
	#endif

	return (ret == 0) ? size : ret;
}


/* public functions */

int mqospi_init(void)
{
	spi_dma_init();

	mqospi_reset();

	return 0;
}

static uint16_t mqospi_next_sync(void)
{
	static uint16_t synq = 0;
	synq++;
	return synq;
}

int mqospi_msg_sync(unsigned int idx_cpu, unsigned int idx_hmq, struct mqospi_msg *msg, int timeout)
{
	int ret;
	uint64_t volatile time_s, time_e;
	uint16_t w_sync = mqospi_next_sync();
	msg->hdr.sync_id = w_sync;

	mqospi_take_semaphore(1000);
	time_s = OsGetTime_us();

	ret = mqospi_spi_write_message(idx_cpu, idx_hmq, msg);

	if(ret >= 0)
	{
		/* wait for response */
		uint8_t rmq_status_gpio =  mqospi_poll_map[idx_cpu * 6 + idx_hmq];

		while(profip_gpio_read(rmq_status_gpio) == MQOSPI_RMQ_EMPTY)
	    {
	        time_e = OsGetTime_us();
			if(time_e > time_s + timeout /*us*/)
			{
				ret = MQOSPI_SYNC_TIMEOUT;
				pf_err(PF_ERR_MODULE_MQOSPI, MQOSPI_SYNC_TIMEOUT, (idx_cpu * 6 + idx_hmq), "Sync timeout\n");
				break;
			}
	    }

		/* read the response */
		if(ret >= 0)
		{
			ret = mqospi_spi_read_message(idx_cpu, idx_hmq, msg);
		}

	}

	mqospi_give_semaphore();

	if(ret >= 0 && w_sync != msg->hdr.sync_id)
	{
		ret = MQOSPI_WRONG_SYNC_ID_SYNC;
		pf_err(PF_ERR_MODULE_MQOSPI, MQOSPI_WRONG_SYNC_ID_SYNC, (idx_cpu * 6 + idx_hmq), "Wrong sync\n");
	}

	return (ret >= 0) ? 0 : ret;
}

int mqospi_msg_async_send(unsigned int idx_cpu, unsigned int idx_hmq, struct mqospi_msg *msg, unsigned int n)
{
	int ret;
	mqospi_take_semaphore(1000);

	msg->hdr.sync_id = mqospi_next_sync();

	ret = mqospi_spi_write_message(idx_cpu, idx_hmq, msg);

	mqospi_give_semaphore();

    return (ret >= 0) ? 0U : ret;
}

int mqospi_msg_async_recv(unsigned int idx_cpu, unsigned int idx_hmq, struct mqospi_msg *msg, unsigned int n)
{
	int ret;

	mqospi_take_semaphore(1000);

    ret = mqospi_spi_read_message(idx_cpu, idx_hmq, msg);

	mqospi_give_semaphore();

	if(ret >= 0)
	{
		unsigned rmq_index = RMQ_INDEX(idx_cpu, idx_hmq);

		if(old_syncId[rmq_index] != 0 && (((uint16_t)(old_syncId[rmq_index] + 1)) != msg->hdr.sync_id))
		{
			char msg_buff[90];
			snprintf(msg_buff, 90, "Wrong sync ID for async communication %d != %d\n, RMQ %d", msg->hdr.sync_id, old_syncId[rmq_index], rmq_index);
			pf_err(PF_ERR_MODULE_MQOSPI, MQOSPI_WRONG_SYNC_ID_ASYNC, (msg->hdr.sync_id - old_syncId[rmq_index]), msg_buff);

		    trace_mqospi_error(MQOSPI_WRONG_SYNC_ID_ASYNC, idx_cpu, idx_hmq, old_syncId[rmq_index], msg);
		}

		old_syncId[rmq_index] = msg->hdr.sync_id;
	}

	return (ret >= 0) ? 1U : ret;
}

int mqospi_poll(struct pollmqospi* poll, unsigned int n, int timeout)
{
	uint8_t rmq_status_gpio;
	uint8_t rmq_status;
	int ret = 0;

	for(int i = 0; i<n; i++)
	{
		if(poll[i].events == MQOSPI_POLLIN)
		{
			rmq_status_gpio = mqospi_poll_map[poll[i].idx_cpu * 6 + poll[i].idx_hmq];
			rmq_status = profip_gpio_read(rmq_status_gpio);

			poll[i].revents = (rmq_status == MQOSPI_RMQ_NOT_EMPTY) ? (MQOSPI_POLLIN) : (0);
		}
		ret++;
	}

    return ret;
}

int mqospi_purge(unsigned int idx_cpu, unsigned int idx_hmq)
{
	uint8_t rmq_status_gpio;
	uint64_t volatile time_s, time_e;
	unsigned rmq_index = RMQ_INDEX(idx_cpu, idx_hmq);

	struct mqospi_msg msg;
	int ret = 0;

	time_s = OsGetTime_us();

	rmq_status_gpio = mqospi_poll_map[RMQ_INDEX(idx_cpu, idx_hmq)];

	while(profip_gpio_read(rmq_status_gpio) == MQOSPI_RMQ_NOT_EMPTY)
	{
		mqospi_spi_read_message(idx_cpu, idx_hmq, &msg);

		ret++;

		time_e = OsGetTime_us();

		if(time_e > time_s + 1000000 /*us*/)
		{
			pf_err(PF_ERR_MODULE_MQOSPI, MQOSPI_PURGE_TIMEOUT, (idx_cpu * 6 + idx_hmq), "Purge timeout\n");
			return MQOSPI_PURGE_TIMEOUT;
		}
	}

	/* clear syncID for async communication*/
	old_syncId[rmq_index] = 0;

	if(ret > 0)
	{
		PROFIP_DEBUG("removed %d messages from c %d rmq %d\n", ret, idx_cpu, idx_hmq);
	}

	return ret;
}


void mqospi_reset(void)
{
	int i=0;

	mqospi_purge(0, 0);
	mqospi_purge(0, 1);
	mqospi_purge(0, 2);
	mqospi_purge(0, 3);
	mqospi_purge(0, 4);
	mqospi_purge(0, 5);
	mqospi_purge(1, 0);

	for(i = 0; i < NBR_OF_ALL_RMQS; i++)
	{
		old_syncId[i] = 0;
	}
}
