// SPDX-FileCopyrightText: 2023 CERN (home.cern)
//
// SPDX-License-Identifier: LGPL-2.1-or-later

#include "fr_timer.h"
#include "ertec200p_reg.h"
#include "hal_io.h"

void fr_timer_start(struct fr_timer_t *timer)
{
	HAL_READ_UINT32(U_TIMER__TIM_4_COUNT_REG, timer->start_time_us);
}

uint32_t fr_timer_stop(struct fr_timer_t *timer)
{
	uint32_t tmp;
	HAL_READ_UINT32(U_TIMER__TIM_4_COUNT_REG, tmp);
	return timer->start_time_us - tmp;
}

void fr_timer_init(void)
{
	// Peripheral clock is 125MHz, so set the prescaler to 124 -> 1Tick = 1us
    HAL_WRITE_UINT32(U_TIMER__TIM_4_PRESCALER_REG, 124);

    HAL_WRITE_UINT32(U_TIMER__TIM_4_LOAD_REG, 125 * 1000 * 1000 );
    HAL_WRITE_UINT32(U_TIMER__TIM_4_MODE_REG, (1 << 0)); //INIT BUT

    uint32_t tmp;
    HAL_READ_UINT32(U_TIMER__GATE_TRIG_CONTROL_REG, tmp);
    HAL_WRITE_UINT32(U_TIMER__GATE_TRIG_CONTROL_REG, tmp | (1 << 10) | (1<<4)); //CLK EN
}
