// SPDX-FileCopyrightText: 2023 CERN (home.cern)
//
// SPDX-License-Identifier: LGPL-2.1-or-later

#include <stddef.h>

#include "spi_dma.h"

#include "pf_utils.h"
#include "profip_log.h"
#include "ecos_ertec_bsp_spi.h"
#include "pnioerrx.h"
#include "bspadapt.h"
#include "gdma_cfg.h"
#include "gdma_com.h"
#include "gdma_int.h"
#include "pf_err.h"

/* dma buffer size should be equal to RMQ SIZE + 4 bytes for basic info + 4 bytes for reserve*/
#define DMA_BUFF_SIZE (130 * 4)

#if DMA_BUFF_SIZE != 8 + 4 * RMQ_WIDTH
	#error Wrong dma buff size
#endif

/* spi_dma internal error codes */
#define SPI_DMA_ERR_TIMEOUT -1
#define SPI_DMA_ERR_SEM_TAKEN -2
#define SPI_DMA_ERR_WRONG_SIZE -3

// DMA buffers for RX & TX data
static volatile uint8_t dma_src8[DMA_BUFF_SIZE] __attribute__ ((section(".uncached_mem")));
static volatile uint8_t dma_dst8[DMA_BUFF_SIZE] __attribute__ ((section(".uncached_mem")));

static volatile uint32_t spi_dma_sem;

static int dmajob_tx;
static int dmajob_rx;

static volatile char dma_rx_done = 0U;

void dma_done_tx(LSA_INT16 a, LSA_UINT32 b)
{

}

void dma_done_rx(LSA_INT16 a, LSA_UINT32 b)
{
	dma_rx_done = 1U;
}

int spi_dma_receive(uint32_t* data, size_t size, uint32_t timeout_us)
{
	return spi_dma_exchange(NULL, data, size, timeout_us);
}

int spi_dma_send(uint32_t* data, size_t size, uint32_t timeout_us)
{
	return spi_dma_exchange(data, NULL, size, timeout_us);
}

int spi_dma_exchange(uint32_t* data_in, uint32_t* data_out, size_t size, uint32_t timeout_us)
{
	uint64_t volatile time_s, time_e;

	if(size*4 > DMA_BUFF_SIZE)
	{
		pf_err(PF_ERR_MODULE_SPIDMA, SPI_DMA_ERR_WRONG_SIZE, size, "wrong size\n");
		return SPI_DMA_ERR_WRONG_SIZE;
	}

	if (OsTakeSemB(spi_dma_sem) != PNIO_OK)
	{
		pf_err(PF_ERR_MODULE_SPIDMA, SPI_DMA_ERR_SEM_TAKEN, 0U, "semaphore taken\n");
		return SPI_DMA_ERR_SEM_TAKEN;
	}

	// memcpy out data to dma buffer
	if(data_in != NULL)
	{
		for(int i=0; i<size; i++)
		{
			((uint32_t*)dma_src8)[i] = CHANGE_ENDIANESS_32(data_in[i]);
		}
	}

	spi_rxb_flush();
	time_s = OsGetTime_us();

	dma_rx_done = 0U;

    //send basic info & first dword
    gdma_change_transfer_count(0, size * 4);
    gdma_change_transfer_count(1, size * 4);
    gdma_trigger_job(dmajob_rx);
    gdma_trigger_job(dmajob_tx);

    /*TX job shall always be sucessfull so take semaphore and wait */


    while(dma_rx_done == 0)
    {
        time_e = OsGetTime_us();

        if(time_e > time_s + timeout_us)
		{
			pf_err(PF_ERR_MODULE_SPIDMA, SPI_DMA_ERR_TIMEOUT, 0U, "spi dma timeout\n");

			OsGiveSemB(spi_dma_sem);
			return SPI_DMA_ERR_TIMEOUT;
		}
    }

    // memcpy dma data to in buffer
    if(data_out != NULL)
    {
		for(int i=0; i<size; i++)
		{
			data_out[i] = CHANGE_ENDIANESS_32( ((uint32_t*)dma_dst8)[i] );
		}
    }

    OsGiveSemB(spi_dma_sem);
    return 0;
}

int spi_dma_init(void)
{
	// init SPI
	spi_init();

	//init semaphores
	OsAllocSemB((PNIO_UINT32*) &spi_dma_sem);
	OsGiveSemB(spi_dma_sem);

	// prepare DMA transfers descriptions for TX & RX
	gdma_transfer_descr_t transfers_tx[1] = {
		{
				.magic_number = GDMA_TRANSFER_DESCR_MAGIC,
				.src_addr = (LSA_UINT32*) dma_src8,
				.dst_addr = (LSA_UINT32*) (0x40010008),//SPI1DR //dma_test_dst,
				.src_amode = GDMA_SRC_AMODE_INCREMENT,
				.dst_amode = GDMA_SRC_AMODE_HOLD,
				.transfer_count = 8,
				.elem_size = GDMA_ESIZE_8BIT,
				.burst_mode = GDMA_BURST_MODE_INCR4
		}
	};

	gdma_transfer_descr_t transfers_rx[1] = {
		{
				.magic_number = GDMA_TRANSFER_DESCR_MAGIC,
				.src_addr = (LSA_UINT32*) (0x40010008),//SPI1DR
				.dst_addr = (LSA_UINT32*) dma_dst8,
				.src_amode = GDMA_SRC_AMODE_HOLD,
				.dst_amode = GDMA_SRC_AMODE_INCREMENT,
				.transfer_count = 8,
				.elem_size = GDMA_ESIZE_8BIT,
				.burst_mode = GDMA_BURST_MODE_SINGLE
		}
	};

	gdma_job_descriptor_t job_desc_tx = {
			 .num_transfers = 1,					/**< size of the array / structure field "transfers" */
			 .transfers = transfers_tx,				/**< pointer to array of GDMA_TRANSFER_DESCR-structs describing all transfers of a job */
			 .trigger = GDMA_TRIGGER_SW,			/**< trigger source of DMA-job : hardware or software possible. See enum */
			 .priority = 30,                  		/**< priority of job : 0 - 31 */
			 .hw_flow_src = GDMA_SPI1_SSPTRINTR,	/**< hw_dma_req signal to use */
			 .job_done_callback = dma_done_tx,		/**< job done callback (IRQ) for this job. If set to LSA_NULL -> interrupt disabled
														 The parameters are: 1. job handle, 2. additional user defined callback argument (see field cbf_arg below) */
			 .cbf_arg = (LSA_UINT32) 0U
			 };

	gdma_job_descriptor_t job_desc_rx = {
			 .num_transfers = 1,
			 .transfers = transfers_rx,
			 .trigger = GDMA_TRIGGER_SW,
			 .priority = 31,
			 .hw_flow_src = GDMA_SPI1_SSPRXDMA,
			 .job_done_callback = dma_done_rx,
			 .cbf_arg = (LSA_UINT32) 0U
			 };

	gdma_init();

	dmajob_tx = gdma_add_job(0, &job_desc_tx);
	dmajob_rx = gdma_add_job(0, &job_desc_rx);

	gdma_enable_job(dmajob_tx);
	gdma_enable_job(dmajob_rx);

	return 0;
}

int spi_dma_start_frame(void)
{
    SPI_CS_LOW;

	return 0;
}

int spi_dma_end_frame(void)
{
    SPI_CS_HIGH;

	return 0;
}

