// SPDX-FileCopyrightText: 2023 CERN (home.cern)
//
// SPDX-License-Identifier: LGPL-2.1-or-later

/**
 * @author Piotr Klasa <piotr.klasa@cern.ch>
 *
 * @file spi_dma.h
 * @brief Module for sending and receiving data throung SPI using DMA.
 */

#ifndef SPI_DMA_H
#define SPI_DMA_H

#include <stdint.h>

/**
 * @name spi_dma_send
 * @param data
 * @param size
 * @param timeout_us
 * @return 0 on success or error code
 * @brief Sends data over SPI, the function is blocking,
 *        if data cannot be sent for timeout_us, an error
 *        is returned.
 */
int spi_dma_send(uint32_t* data, size_t size, uint32_t timeout_us);

/**
 * @name spi_dma_receive
 * @param data - pointer to receive data
 * @param size
 * @param timeout_us
 * @return 0 on success or error code
 * @brief Receive data from SPI, the function is blocking,
 *        if data cannot be received for timeout_us, an error
 *        is returned.
 */
int spi_dma_receive(uint32_t* data, size_t size, uint32_t timeout_us);

/**
 * @name spi_dma_exchange
 * @param data_out
 * @param data_in - pointer to receive data
 * @param size
 * @param timeout_us
 * @brief Exchange data to/from SPI, the function is blocking,
 *        if data cannot be exchanged for timeout_us,
 *        an error is returned.
 */
int spi_dma_exchange(uint32_t* data_out, uint32_t* data_in, size_t size, uint32_t timeout_us);

/**
 * @name spi_dma_init
 * @brief initialize the spi and dma
 */
int spi_dma_init(void);

/**
 * @name spi_dma_end_frame
 * @brief Sets the CS pin. This is an external function
 *        because the user can perform multiple spi_dma_exchanges
 *        in one translation.
 */
int spi_dma_end_frame(void);

/**
 * @name spi_dma_start_frame
 * @brief Sets the CS pin. This is an external function
 *        because the user can perform multiple spi_dma_exchanges
 *        in one translation.
 */
int spi_dma_start_frame(void);

#endif
