@echo off
REM #########################################################################
REM  EK-ERTEC 200P V4.7 auto download and install dependencies
REM #########################################################################
echo Downloading IODDevKit dependecies

REM You can change the output folder for the installed tools, but this will not change the eclipse MINGW_PATH
IF [%1] == [] (SET devkitRootFolder=%~dp0..
) ELSE (
SET devkitRootFolder=%1
echo ### WARNING: You have to change the MINGW Path in related Eclipse projects to given Path manually! ###
)
SET outputFolder=%devkitRootFolder%\installed_tools

REM Where you want the tools to be installed
SET installscript=%~dp0get_dependencies.ps1

SET ecosFolder=%devkitRootFolder%\ecos

if not defined proxy_ip_addr (
SET proxy_ip_addr="''"
) 

if not defined proxy_port (
SET proxy_port="''"
)

REM By default all Tools will be downloaded and added to PATH
REM You can stop downloading them by following parameters
SET download_Eclipse=$true 
SET download_OpenJDK=$true 
SET download_ARM_GCC=$true 
SET download_MinGW=$true

REM # Add OpenJDK to Global Machine Path
SET add_OpenJDK_PATH=$true

REM # Copy ECOS to MING GW msys folder this is automatic turned off when you turn off MinGW Download
SET copy_ECOS=$true

REM # Remove Tempfolder after completion
SET cleanUp_Temp=$true

REM # Remove Zip files after unzipping
SET removeArchives=$true
REM #########################################################################

REM # Start download and install
powershell %installscript%^
 -outputFolder %outputFolder%^
 -ecos %ecosFolder%^
 -proxy_ip_addr %proxy_ip_addr%^
 -proxy_port %proxy_port%^
 -download_Eclipse %download_Eclipse%^
 -download_OpenJDK %download_OpenJDK%^
 -download_ARM_GCC %download_ARM_GCC%^
 -download_MinGW %download_MinGW%^
 -copy_ECOS %copy_ECOS%^
 -cleanUp_Temp %cleanUp_Temp%^
 -removeArchives %removeArchives%^
 -add_OpenJDK_PATH %add_OpenJDK_PATH%
REM #########################################################################