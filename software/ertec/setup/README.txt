####################################################################################################
 EK-ERTEC 200P V4.7 - automatically download and install dependencies
####################################################################################################

!!!! PLEASE DO NOT CHANGE "get_dependencies.ps1" FILE CONTENT !!!!

Prerequisites:
1) Stable internet connection
2) Open Powershell console with administrator rights, and check Powershell version with below command;
	- "Get-Host | Select-Object Version"
3) Powershell version should be greater than V5.0.
4) Allow to PowerShell script execution using following command in Powershell with administrator rights; "Set-ExecutionPolicy Unrestricted" and click "A" (Yes to All).


StepList:
1) Open Command Prompt with administrator rights
2) Go to "install.bat" file path.
                For example: cd ...\setup\
3) If your company use proxy for Internet access, please set your Proxy IP (proxy_ip_addr) and Port (proxy_port) before execute "install.bat" command.
                For example: "set proxy_ip_addr=192.168.0.1"
                             "set proxy_port=9400"
                             "install.bat"
4) If you don't use proxy, only execute "install.bat" command.

Note: - If you want to download and add OpenJDK to PATH by yourself, just deactivate it in the script "install.bat", by setting the corresponding flag to "$false".
                For example: SET download_Eclipse=$false
