// SPDX-FileCopyrightText: 2023 CERN (home.cern)
//
// SPDX-License-Identifier: LGPL-2.1-or-later

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <errno.h>
#include <unistd.h>

#include <libmasterfip.h>

static int mfipdiag_shm_attach()
{
	char path[32], dev_id_str[4];
	int i;
	int shm_id;
	void *ptr_shm;

	for (i = 0; i < 2; ++i) { /* Max 2 masterFIP boards */
		int ret;

		ret = snprintf(path, sizeof(path), "/dev/masterfip.%d", i);
		if (ret < 0 || ret >= sizeof(path)) {
			errno = EINVAL;
			return -1;
		}
		ret = readlink(path, dev_id_str, sizeof(dev_id_str));
		if (ret < 0)
			continue;
		/* the link is valid creates corresponding shm */
		shm_id = shmget(MSTRFIP_DIAG_SHM_KEY + i,
				sizeof(struct mstrfip_diag_shm), 0666 | IPC_CREAT);
		if(shm_id == -1 )
			continue;

		ptr_shm = shmat(shm_id, (void *)0, 0);
		if (ptr_shm == (void *)-1 ) {
			fprintf(stderr, "attach to MasterFIP shm failed: %s\n",
				strerror(errno));
			return -1;
		}
	}

	return 0;
}

/*
 * This program is used just to create the masterfip diag shm
 * In the FEC boot sequence, this program is part of the install_masterfip.sh
 * in order to create the masterFIP diag shm before diamon clic agent is
 * started. This way diamon agent does just an attach to teh SHM instead of
 * creating it, because the size of the SHM is not the same between alsthom and
 * masterFIP
 */
int main(int argc, char *argv[])
{
	int err;

	err = mfipdiag_shm_attach();
	if (err)
		fprintf(stderr, "Creating MasterFIP shm failed: %s\n",
				strerror(errno));
	return 0;
}
