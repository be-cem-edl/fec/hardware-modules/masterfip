# SPDX-FileCopyrightText: 2023 CERN (home.cern)
#
# SPDX-License-Identifier: LGPL-2.1-or-later

# install rules for "./extest"
DESTDIR ?=
prefix ?= /usr/local
exec_prefix ?= $(prefix)
libdir ?= $(exec_prefix)/lib
includedir ?= $(exec_prefix)/include/extest

.DEFAULT_GOAL := all

install_prog_global: 

install_libs_global: libextest.a
	mkdir -m 0775 -p $(DESTDIR)$(libdir) $(DESTDIR)$(includedir)
	install -D -t $(DESTDIR)$(libdir) -m 0644 libextest.a
	install -D -t $(DESTDIR)$(includedir) -m 0644 extest.h

.PHONY: install_libs_global install_prog_global