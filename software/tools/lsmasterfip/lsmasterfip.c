/*
 * SPDX-FileCopyrightText: 2024 CERN (home.cern)
 * SPDX-License-Identifier: LGPL-2.1-or-later
 */

#include <stdlib.h>
#include <stdio.h>
#include <errno.h>
#include <inttypes.h>
#include <string.h>
#include <getopt.h>

#include <libmasterfip.h>

#define PROGRAM_NAME "lsmockturtle"
#define ARGS_MAX 3  // including argv[0]
#define ARGS_STR "hv"
#define ARGS_MAX_LENGTH 0

/**
 * Print the help message
 * @param[in] name the program name
 */
static void help(const char *program_name)
{
	fputs("\n", stderr);
	fprintf(stderr, "%s [options]\n\n", program_name);
	fputs("Show the MasterFIP devices available on the system.\n\n",
	      stderr);
	fputs("Options:\n", stderr);
	fputs("-v   show more information\n", stderr);
	fputs("-h   show this help\n", stderr);
	fputc('\n', stderr);
}


/**
 * Convert a MockTurtle device name into a MockTurtle id
 * @param[in] MockTurtle device name
 * @param[out] MockTurtle device it
 * @return 0 on success, otherwise -1 and errno is appropriately set
 */
static int trtl_name_to_id(const char *name, uint32_t *id) {
	int ret;

	ret = sscanf(name, "trtl-%4x", id);
	if (ret != 1) {
		errno = EINVAL;
		return -1;
	}
	return 0;
}

#define VERSION_MAJ(_v) ((_v >> 16) & 0xFF)
#define VERSION_MIN(_v) ((_v >> 8) & 0xFF)
static void mstrfip_print_version(const struct mstrfip_version *vers)
{
	fprintf(stdout, "  FPGA Identifier: 0x%08"PRIx32"\n", vers->fpga_id);
	fprintf(stdout, "  FPGA Version: %u.%u.%u\n",
		VERSION_MAJ(vers->fpga_version),
	 	VERSION_MIN(vers->fpga_version),
		vers->fpga_version & 0xFF);
	fprintf(stdout, "  RealTime Application Version: %u.%u.%u\n",
		VERSION_MAJ(vers->rt_version),
	 	VERSION_MIN(vers->rt_version),
		vers->rt_version & 0xFF);
	fprintf(stdout, "  Library Version: %u.%u.%u\n",
		VERSION_MAJ(vers->lib_version),
		VERSION_MIN(vers->lib_version),
		vers->lib_version & 0xFF);
}

int main(int argc, char *argv[])
{
	int verbose = 0, err, opt, i;
	char **list;

	atexit(mstrfip_exit);

	if (argc > ARGS_MAX) {
		fputs("Too many arguments. See help (-h)\n", stderr);
		exit(EXIT_FAILURE);
	}

	while ((opt = getopt(argc, argv, ARGS_STR)) != -1) {
		/* if (strlen(optarg) > MAX_ARG_LENGTH) { */
		/* 	fprintf(stderr, "Argument too long\n"); */
		/* 	exit(EXIT_FAILURE); */
		/* } */

		switch (opt) {
		default:
			help(PROGRAM_NAME);
			exit(EXIT_SUCCESS);
			break;
		case 'v':
			verbose++;
			break;
		}
	}

	err = mstrfip_init();
	if (err) {
		fprintf(stderr, "Cannot init MasterFIP library: %s\n",
			mstrfip_strerror(errno));
		exit(1);
	}

	list = mstrfip_list();
	if (!list)
		goto out_no_list;

	for (i = 0; list[i]; ++i) {
		struct mstrfip_dev *mstrfip;
		unsigned int lun;
		uint32_t devid;

		err = trtl_name_to_id(list[i], &devid);
		if (err) {
			fprintf(stderr,
				"Invalid MockTurtle device name '%s'",
				list[i]);
			goto err_invalid_id;
		}

		mstrfip = mstrfip_open_by_id(devid);
		if (!mstrfip) {
			fprintf(stderr,
				"Can't open MasterFIP 0x%04"PRIx32": %s\n",
				devid, mstrfip_strerror(errno));
			continue;
		}
		err = mstrfip_id_to_lun(devid, &lun);
		if (err) {
			mstrfip_close(mstrfip);
			fprintf(stderr,
				"Can't find LUN for MasterFIP 0x%04"PRIx32": %s\n",
				devid, mstrfip_strerror(errno));
			continue;
		}

		fprintf(stdout, "ID: 0x%04"PRIx32", LUN: %d\n" , devid, lun);
		if (verbose > 0) {
			struct mstrfip_version vers;

			err = mstrfip_version_get(mstrfip, &vers);
			if (err) {
				fprintf(stderr,
					"Can't get version for MasterFIP  0x%04"PRIx32": %s\n",
					devid, mstrfip_strerror(errno));
			}
			mstrfip_print_version(&vers);
		}
		mstrfip_close(mstrfip);
	}

err_invalid_id:
	mstrfip_list_free(list);
out_no_list:
	exit(err);
}
