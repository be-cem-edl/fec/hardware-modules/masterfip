// SPDX-FileCopyrightText: 2024 CERN (home.cern)
//
// SPDX-License-Identifier: LGPL-2.1-or-later

/*!
 * @file   fip_fdm.h
 * @brief  FIP Device Manager library declarations
 * @author Michael Davis
 */

#ifndef __LIBFIPFDM_FIP_MFIP_H
#define __LIBFIPFDM_FIP_MFIP_H

#include <stdint.h>
#include <stdbool.h>
#include <errno.h>
#include <time.h>
#include <sys/time.h>

#include <masterfip/libmasterfip.h>

// Types

/*!
 * FIP variable callback type.
 *
 * Specifies the type of callback functions which are linked to variables in the BA macrocycle.
 */

typedef void FipMfipVarCallback(struct mstrfip_dev *dev, struct mstrfip_data *data,
		 	    struct mstrfip_irq *irq);



/*!
 * FIP error reporting callback type
 *
 * @param[in] error    Error code to be logged
 */

typedef int32_t LogErrorCallback(const char *error_string);



/*!
 * FIP read system time callback type
 *
 * @param[out] time    Current time read from the appropriate timing system
 */

typedef int32_t ReadTimeCallback(struct timeval *time);



/*!
 * FIP BA macrocycle program startup/shutdown callback type
 */

typedef void BAUpDownCallback(void);


struct jtag_fip_per_var_desc {
    uint32_t             id;
    int 	             dir;
    int 	             bsz;
    FipMfipVarCallback  *cb;        //!< Pointer to callback function for this variable
    void               **data_ptr;
};

struct jtag_fip_per_var_wind_desc {
    int                         nvar;
    struct jtag_fip_per_var_desc *varlist;
};

struct jtag_fip_aper_msg_wind_desc {
    int end_ustime;   //!< us time in the macrocycle of
    int max_prod_msg;
    int max_cons_msg;
};

struct jtag_fip_wait_wind_desc {
    int end_ustime;
    int silent;
};

/*!
 * All possible macrocycle windows used by FGC macrocycle
 */
enum jtag_fip_wind_type {
    JTAG_FIP_PER_VAR_WIND,
    JTAG_FIP_APER_MSG_WIND,
    JTAG_FIP_WAIT_WIND,
};

/*
 * FGC FIP Macrocycle is made of a concatenation of windows which can be:
 * periodic variable window: in charge of scheduling periodic varaiables.
 * aperiodic message window: schedules aperiodic messages
 * wait window: to define some precise timing in the macrocycle.
 */
struct jtag_fip_mcycle_desc {
    enum jtag_fip_wind_type                  type;           //!<
    union {
        struct jtag_fip_per_var_wind_desc    per_var_wind;   //!< periodic variable window
	struct jtag_fip_aper_msg_wind_desc   aper_msg_wind;  //!< aperiodic message window
	struct jtag_fip_wait_wind_desc       wait_wind;      //!< wait window
    };
};

/*!
 * Valid frame codes for WorldFIP frames.
 *
 * This specifies a subset of the frame types supported by WorldFIP, see:
 * "FIP Device Manager Software Version 4 User Reference Manual", p.3-60.
 *
 * For communication with nanoFIP devices, we do not support aperiodic messages
 * (SEND_MSG), aperiodic variables (SEND_APER), or external synchronisation waiting
 * with stuffing (SYN_WAIT). It is OK to synch wait without stuffing because we
 * know that our device will be Bus Arbitrator--nanoFIP devices cannot be BA.
 */

enum jtag_fip_per_var_dir
{
	JTAG_FIP_PER_VAR_CONS,
	JTAG_FIP_PER_VAR_PROD,
};



/*!
 * Struct for FIP device error statistics
 */

struct FIP_device_stats
{
    uint32_t var_recv_count;                 //!< Count of received variables. Reset and updated by the library.
    uint32_t var_miss_count;                 //!< Count of missed variables. Reset by the library but updated by the application.
    uint32_t var_late_count;                 //!< Count of received variables that missed their deadline. Reset by the library but updated by the application.

    uint32_t nonsignificance_fault_count;    //!< Non-significance faults
    uint32_t promptness_fail_count;          //!< Promptness failures
    uint32_t significance_fault_count;       //!< Significance status
    uint32_t user_error_count;               //!< FIP user errors
};



/*!
 * Struct for FIP fieldbus error statistics
 */

struct FIP_fieldbus_stats
{
    int32_t  error_flag;                           //!< Flag to indicate that an error was detected during the gateway's WorldFIP processing
    uint32_t start_count;                          //!< Count of WorldFIP interface starts/restarts
    uint32_t start_time;                           //!< Time of last WorldFIP interface start

    uint32_t interface_error_count;                //!< Count of WorldFIP interface errors
    uint32_t last_interface_error;                 //!< Value of last WorldFIP interface error received

    struct FIP_device_stats errors;                //!< Total FIP errors for all devices
};



/*!
 * Struct for FIP FDM configuration
 */

struct FIP_config
{
    struct mstrfip_sw_cfg      sw_config;          //!< FDM Software configuration
    struct mstrfip_hw_cfg      hw_config;          //!< FDM Hardware configuration
    uint8_t                    thread_priority;    //!< FDM thread priority

    // Callback functions

    LogErrorCallback          *log_error_func;     //!< Callback to log error messages
    ReadTimeCallback          *read_time_func;     //!< Callback to read current time
    BAUpDownCallback          *ba_up_func;         //!< Callback when the BA macrocycle program starts up
    BAUpDownCallback          *ba_down_func;       //!< Callback when the BA macrocycle program shuts down

    // Statistics

    struct FIP_fieldbus_stats *fieldbus_stats;     //!< Pointer to data structure for fieldbus statistics
};



// Constants

static const uint8_t  FIP_DIAG_DEV_ADDR               = 0x7F;       //!< Address of the WorldFIP diagnostic device on the bus

static const uint8_t  FIP_DEFAULT_THREAD_PRIORITY     = 65;         //!< Pritority of the FIP FDM thread
static const uint8_t  FIP_WATCHDOG_THREAD_PRIORITY    = 37;         //!< Priority of the Watchdog thread.

static const uint32_t FIP_THREAD_STACK_SIZE           = 1048576;    //!< Pthread stack size.



// Inline function declarations

/*!
 * Sleep for the specified no. of milliseconds.
 *
 * Note that nanosleep is thread-safe.
 *
 * @param[in] ms    No. of milliseconds to sleep
 */

static inline void msSleep(uint16_t ms)
{
    struct timespec time;

    time.tv_sec  = ms / 1000;
    time.tv_nsec = (ms % 1000) * 1000000;

    // sleep for the specified no. of ms and resume if interrupted

    while(nanosleep(&time, &time) && errno == EINTR) ;
}


/*!
 * Get device ID from MPS variable context.
 *
 * See "FIP DEVICE MANAGER Software Version 4 User Reference Manual", p.3-34.
 *
 * @param[in] fdm_mps_var_ref    MPS variable context
 *
 * @returns FIP address of the device sending the status
 */

static inline uint8_t fipMfipGetDeviceId(struct mstrfip_data *data)
{
    return data->id & 0xFF;
}

// External functions

#ifdef __cplusplus
extern "C" {
#endif

/*!
 * Reset device stats to zero.
 *
 * @param[out] stats    Device stats struct to update.
 */

void fipMfipResetDeviceStats(struct FIP_device_stats *stats);



/*!
 * Returns the current time.
 *
 * Calls the time callback specified in read_time_func() above, if defined.
 * Otherwise use gettimeofday().
 *
 * @param[out] timeNow    struct to overwrite with current time in seconds and microseconds
 *
 * @returns The current time in seconds.
 */

int32_t fipMfipGetTime(struct timeval *timeNow);



/*!
 * Log an error message.
 *
 * Calls the logging callback specified in log_error_func() above, if defined.
 * Otherwise write the log message to stderr.
 *
 * @param[in] format    printf-style format string and arguments
 *
 * @returns The result of the call to fprintf or the specified callback. By convention, the result should be
 *          the number of characters written in the case of success, or a negative number in case of failure.
 */

int32_t fipMfipLogError(const char *format, ...);



/*!
 * Get the current FDM configuration.
 *
 * If FDM has not yet been configured, fipGetConfig() returns a set of default values.
 *
 * @param[out] config    Pointer to struct to hold returned config data
 *
 * @retval  0   Configuration returned successfully
 * @retval -1   Failure to initialise default configuration. The error code is set in errno.
 */

int32_t fipMfipGetConfig(struct FIP_config *config);



/*!
 * Set the FDM configuration.
 *
 * Note that this function does not check that the configuration is valid.
 *
 * @param[in] config    Pointer to struct containing config values to set
 *
 * @retval  0   Configuration set successfully
 * @retval -1   Set configuration failed. The error code is set in errno.
 */

int32_t fipMfipSetConfig(struct FIP_config *config);



/*!
 * Start the FIP interface and FIP Device Manager.
 *
 * @retval  0   FIP interface started successfully
 * @retval -1   Starting the FIP interface failed. The error is code is set in errno.
 */

int32_t fipMfipStartInterface(int dev_id, enum mstrfip_bitrate bus_speed);



/*!
 * Stop the FIP interface.
 */

void fipMfipStopInterface(void);



/*!
 * Restart the FIP interface.
 */

int32_t fipMfipRestartInterface(int dev_id, enum mstrfip_bitrate bus_speed);



/*!
 * Initialise the FDM Bus Arbitrator macrocycle program.
 *
 * @param[in] program    Array of WorldFIP frames to be loaded into the program
 * @param[in] nframes    No. of frames (size of the program array)
 */

int32_t fipMfipInitProtocol(const struct jtag_fip_mcycle_desc *mcycle_desc,
		     uint32_t nentries);



/*!
 * Start the FDM Bus Arbitrator macrocycle program.
 *
 * @retval  0   FDM BA started successfully
 * @retval -1   Starting the FDM BA failed. The error code is set in errno.
 */

int32_t fipMfipStartProtocol(void);



/*!
 * Shut down the FDM Bus Arbitrator macrocycle program.
 */

void fipMfipStopProtocol(void);



/*!
 * Restart the FDM Bus Arbitrator macrocycle program.
 */

int32_t fipMfipRestartProtocol(void);



/*!
 * Write a single Producer variable value into its FDM context
 *
 * @param[in] id_dat    FIP frame ID of the variable to be written
 */

void fipMfipWriteVar(uint32_t id_dat);



/*!
 * Write all the Producer variable values into their FDM contexts
 */

void fipMfipWriteVars(void);



/*!
 * Read a Consumer variable value from its FDM context
 *
 * @param[in]     var_id         ID of the variable to read
 * @param[in,out] error_stats    WorldFIP error stats to update
 *
 * @retval true     Variable was read successfully
 * @retval false    An error occurred while reading the variable
 */

bool fipMfipReadVar(struct mstrfip_data *data, struct FIP_device_stats *error_stats);



/*!
 * Get time of last WorldFIP interface start.
 *
 * @returns UNIX time that the WorldFIP interface was started
 */

uint32_t fipMfipGetStartTime(void);



/*!
 * Report whether the WorldFIP interface is running
 *
 * @retval true if interface is running
 * @retval false if interface is not running
 */

bool fipMfipIsInterfaceUp(void);



/*!
 * Increment cycle count.
 *
 * Call this function once per FIP FDM cycle, probably from within a FIP producer callback.
 *
 * @returns Updated cycle count (after incrementing)
 */

uint32_t fipMfipCycleTick(void);



/*!
 * Get the number of FIP FDM cycles.
 */

uint32_t fipMfipGetCycleCount(void);



/*!
 * Register a device with FDM and Diamon.
 *
 * This lets FDM and Diamon know if a device should be present. The Diamon error bit for
 * each device is the XOR of the registered and present bits.
 *
 * @param[in] address    Address of the device on the WorldFIP bus
 */

void fipMfipConfigureDevice(uint8_t address);

/*!
 * Unregister a device with FDM and Diamon.
 *
 * This lets FDM and Diamon know if a device should be present. The Diamon error bit for
 * each device is the XOR of the registered and present bits.
 *
 * @param[in] address    Address of the device on the WorldFIP bus
 */

 void fipMfipUnconfigureDevice(uint8_t address);
 

/*!
 * Determine if the device has been configured and registered with FDM and Diamon.
 *
 * @param[in] address    Address of the device on the WorldFIP bus
 *
 * @retval true    if the device was previously registered with fipConfigureDevice()
 * @retval false   if the device has not been registered
 */

bool fipMfipIsDeviceConfigured(uint8_t address);


/*!
 * Copy the status of which devices are configured into shared memory
 *
 * This is used to copy device statuses to Diamon.
 *
 * @param[in] ptr    Pointer to shared memory area (target for copy operation)
 */

void fipMfipGetAllConfigured(uint8_t *ptr);



/*!
 * Set the status of a device as present.
 *
 * @param[in] address    Address of the device in the range 0..255. Address 0 is reserved for the front-end.
 */

void fipMfipSetDevicePresent(uint8_t address);



/*!
 * Set the status of a device as not present.
 *
 * @param[in] address    Address of the device in the range 0..255. Address 0 is reserved for the front-end.
 */

void fipMfipSetDeviceNotPresent(uint8_t address);



/*!
 * Report whether a device is present.
 *
 * @param[in] address    Address of the device in the range 0..255. Address 0 is reserved for the front-end.
 */

bool fipMfipIsDevicePresent(uint8_t address);



/*!
 * Copy the status of which devices are present into shared memory.
 *
 * This is used to copy device statuses to Diamon.
 *
 * @param[in] ptr    Pointer to shared memory area (target for copy operation)
 */

void fipMfipGetAllPresent(uint8_t *ptr);

#ifdef __cplusplus
}
#endif

#endif

// EOF