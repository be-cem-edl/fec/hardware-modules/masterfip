<!--
SPDX-FileCopyrightText: 2023 CERN (home.cern)

SPDX-License-Identifier: CC-BY-SA-4.0+
-->

# nfipjtag

nfipjtag is a tool for programming system boards with nanofip over WorldFIP. \
nfipjtag code is taken from fgc: \
https://gitlab.cern.ch/pelson/fgc/-/tree/master/sw/lib/fip_fgcd/src \
https://gitlab.cern.ch/pelson/fgc/-/tree/master/sw/utilities/unix/nfipjtag/src \
https://ohwr.org/project/nanofip/wikis/WP10 \
This is a modified version that can be used in any project that uses masterfip. \
The program reads and decodes the SVF file (with gateware description) and sends TMS and TDI signals over WorldFIP to nanofip.

### prerequirements
The same as for masterfip.

### building
```make```

### running
All mandatory and optional parameters are described in help (./nfipjtag -h). \
eg. \
```sudo ./nfipjtag -b2 -d 1 diot_hydra2_top_svf/diot_hydra2_top_VERIFY.svf```

### jtag trst
To program the gateware correctly the trst jtag signal must be high, i.e. make sure that there is no resistor that pulls down the trst signal.
In the FGC, the TRTS signal is driven by a modified version of nanofip (option -r). The current official version of nanoFIP does not drive the TRST line, so for reprogramming to work, the TRST signal cannot be pulled down. In future versions the nanoFIP will control TRST, so there will be no need to desolder the pull-down resistor.

### fast mode (-f)
The fast mode allows to program the gateware up to 12 times faster. \
Fast mode checks the TDO signal once every 63 TDO signals, so it may not detect all programming error. \
Therfore this mode is for development purposes only. \
e.g. \
``` sudo ./nfipjtag -1640 -2200 -t116 -b2 -f -d 1 diot_hydra2_top_svf/diot_hydra2_top_VERIFY.svf ``` \

To speed up programming, the default macrocycle values might be changed to e.g. ```-1640 -2200 -t116```.
