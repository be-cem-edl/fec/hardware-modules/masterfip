// SPDX-FileCopyrightText: 2023 CERN (home.cern)
//
// SPDX-License-Identifier: LGPL-2.1-or-later

#include "FIPmon3.h"

#include <mqueue.h>
#include <stdio.h>
#include <fcntl.h>
#include <unistd.h>
#include <time.h>
#include <stdlib.h>
#include <sys/ioctl.h>
#include <sys/mman.h>
#include <sys/io.h>
#include <sys/signal.h>
#include <sys/errno.h>
#include <time.h>
#include <sys/time.h>

unsigned int dev_id;
int trigmode;
int inttrig;
unsigned long BAperiod;
int display;
enum mstrfip_bitrate fip_speed; /* FIP bus speed get from the HW */
time_t first_date;
struct mstrfip_data *fip_diag_var[VAR_PER_AGENT];

int runStatus = STATUS_STOP;

void fip_error_handler(struct mstrfip_dev *dev, enum mstrfip_error_list error)
{
    //fprintf(stderr, "ftb_mstrfip_error_cb error reported:(%d) %s\n", error,
    mstrfip_strerror(error);
}

static void signal_init() {
    sigset_t newmask;
    /*
     * block signals: set properly the signal mask in the main thread
     * because other threads will inherit the thread sigmask
     */
    sigemptyset(&newmask);
    sigaddset(&newmask, SIGINT);
    sigaddset(&newmask, SIGTERM);
    sigaddset(&newmask, SIGABRT);
    sigaddset(&newmask, SIGFPE);
    sigaddset(&newmask, SIGILL);
    sigaddset(&newmask, SIGUSR1);
    pthread_sigmask(SIG_BLOCK, &newmask, NULL);
}

void sigproc(){
    signal(SIGINT, sigproc); /*  */
    runStatus = STATUS_STOP;
}

void termproc(){
    signal(SIGTERM, termproc);
    runStatus = STATUS_STOP;
}


/********** MAIN program  ****************/
int main(int argc, char *argv[]){


    first_date=time(0);

	/* used for the option parameters */
	char * options_list = "c:htdp:";
	int    option;
	char * help = "\n>FIPmon3 [-h] -c<card number> -p<cycle> [-t] [-o] [-m] [-q]\n\n-h       : this help\n-c<card> : to specify FIP card slot number\n-p<cycle>: to specify BA period in ms, default:100ms(mandatory even with SYN_WAIT mode)\n-t       : to activate triggered mode";
    /* for the "press ENTER" action */
    unsigned char val_char;
//    int mem_etat_stdin;
//    mem_etat_stdin=
    fcntl(STDIN_FILENO,F_GETFL);
    fcntl(STDIN_FILENO,F_SETFL,O_NONBLOCK);

	trigmode=0; //no timing, no cycle
	inttrig=1;
	BAperiod=MACROCYCLE;
	display=1;
	dev_id = 0;

	while ((option = getopt (argc, argv, options_list)) != -1) {
		switch (option) {

		case 'c' :
			sscanf(optarg, "%d", &dev_id);
			break;
			if (optarg && optarg[0] != '-') {
				dev_id = atoi(optarg);
                	}
                	break;

		case 't' :
			trigmode = 1;
		    	inttrig=0;
		    	break;

		case 'p' :
			BAperiod = atoi(optarg)*1000;
			if(BAperiod<1){
				fprintf (stderr,help ,argv [0]);
				return (-1);
			}
			break;

		case 'd' :
			display = 0;
			break;

		case 'h' :
			fprintf (stderr,help ,argv [0]);
			return (-1);

		case '?' :
			fprintf (stderr, help,argv [0]);
			return (-1);

		default	:
			break;
		}
	}

	signal_init(); /* block all signals */

    printf("start_fip\n");
    start_fip();

    printf("start_user_AELE\n");
    start_user_var();

    printf("start_BA\n");
    start_BA();

    runStatus=STATUS_RUN;

    while(runStatus==STATUS_RUN){
        /* if we press ENTER, we stop the program */
        if(read(STDIN_FILENO,&val_char,1) == 1){
            runStatus = STATUS_STOP;
            stop_BA();
        }

        signal(SIGINT, sigproc);
        signal(SIGTERM, termproc);

        display_monitoring();

        usleep(100000); //refresh of the display in usec
    }

  	return 0;
}



