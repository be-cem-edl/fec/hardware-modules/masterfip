// SPDX-FileCopyrightText: 2023 CERN (home.cern)
//
// SPDX-License-Identifier: LGPL-2.1-or-later

/*============================================================================*/
/* Name : BA_fdm_init.c :                                                     */
/*============================================================================*/
/* Description : all fonctions used to start or stop a FIP network            */
/*                                                                            */
/*============================================================================*/
/* Auteur   : julien.palluel@cern.ch                                          */
/*============================================================================*/
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <errno.h>
#include <getopt.h>
#include <signal.h>
#include <unistd.h>
#include "FIPmon3.h"

//struct mstrfip_data *fip_diag_var[2];
struct mstrfip_dev *mfip;
struct mstrfip_macrocycle *mc;
struct mstrfip_diag_shm *fdgContext;

unsigned short adr_var_fip[]={
	0x067F,0x057F
};


/* called on normal process termination */
void fip_exit() {
    mstrfip_exit();
    /* TO be completed if necessary */
    if (mfip != NULL)
        mstrfip_close(mfip);
}


void start_fip(){

    struct mstrfip_hw_cfg hw_cfg;
    struct mstrfip_sw_cfg sw_cfg;

    int res = mstrfip_init();
    if (res) {
        fprintf(stderr, "Cannot init demo library: %s\n",
            mstrfip_strerror(errno));
        exit(0);
    }


    mfip = mstrfip_open_by_lun(dev_id);
    if (!mfip) {
        fprintf(stderr, "Cannot open mfip %x : %s\n",dev_id, mstrfip_strerror(errno));
        exit(0);
    }

    /* reset RT app */
    if (mstrfip_rtapp_reset(mfip) < 0) {
        fprintf(stderr, "Cannot reset rt app: %s\n",
            mstrfip_strerror(errno));
        exit(1);
    }


    res = mstrfip_hw_speed_get(mfip, &fip_speed);
    if (res) {
        fprintf(stderr, "Cannot get speed: %s\n",
            mstrfip_strerror(errno));
        exit(0);
    }

    hw_cfg.enable_ext_trig = trigmode;
    hw_cfg.enable_int_trig = inttrig;

    hw_cfg.enable_ext_trig_term = 0; // look like the term is needed for the timing pulse
    /* set HW config */
    if (mstrfip_hw_cfg_set(mfip, &hw_cfg) < 0) {
        fprintf(stderr, "Cannot configure HW: %s\n",
            mstrfip_strerror(errno));
        exit(0);
    }

    /* set SW config */
    sw_cfg.irq_thread_prio = 50;
    sw_cfg.diag_thread_prio = 20;
    sw_cfg.mstrfip_error_handler = fip_error_handler;
    if (mstrfip_sw_cfg_set(mfip, &sw_cfg) < 0) {
        fprintf(stderr, "Cannot configure SW: %s\n",
        mstrfip_strerror(errno));
        exit(0);
    }


}


/*============================================================================*/
/* Name: start_user_AELE()                                                    */
/*                                                                            */
/* Description: used to manage  MPS variables                                 */
/*============================================================================*/
void start_user_var(){

    struct mstrfip_aper_var_wind_cfg apwind_cfg;


    /* create macro cycle */
    mc = mstrfip_macrocycle_create(mfip);
    if (mc == NULL) {
        fprintf(stderr, "Create macrocyle failed: %s\n",
            mstrfip_strerror(errno));
        exit(0);
    }

    /* append an aperiodic window */
    memset(&apwind_cfg, 0, sizeof(struct mstrfip_aper_var_wind_cfg));
    apwind_cfg.end_ustime = BAperiod * 0.9; /* end at 90% of cycle */
    apwind_cfg.enable_diag = 1;
    if (mstrfip_aper_var_wind_append(mc, &apwind_cfg) < 0)
        exit(0);

    /* append BA wait window with bourrage */
    if (mstrfip_wait_wind_append(mc, 0, BAperiod) < 0)
        exit(0);



}

/*============================================================================*/
/* Name: start_BA()                                                           */
/*                                                                            */
/* Description: contructs and starts the Bus Arbiter                          */
/*============================================================================*/
void start_BA(){

    if (mstrfip_ba_load(mfip, mc) < 0) {
        fprintf(stderr, "Loading macrocycle failed: %s\n",
            mstrfip_strerror(errno));
        exit(0);
    }

    if (mstrfip_ba_start(mfip) < 0) {
        printf("Build macro cycle failed. Exit\n");
        exit(0);
    }
    //start diagnostic
    fdgContext = mstrfip_diag_get(mfip);
}


/*============================================================================*/
/* Name: stop_BA()                                                            */
/*                                                                            */
/* Description: stops the Bus Arbiter                                         */
/*============================================================================*/
void stop_BA(){
    mstrfip_ba_stop(mfip);
    mstrfip_close(mfip);
}



