// SPDX-FileCopyrightText: 2023 CERN (home.cern)
//
// SPDX-License-Identifier: LGPL-2.1-or-later

/*============================================================================*/
/* Name : display.c :                                                         */
/*============================================================================*/
/* Description : used to display all the monitoring                           */
/* informations on the screen                                                 */
/*============================================================================*/
/* Auteur   : julien.palluel@cern.ch                                          */
/*============================================================================*/

#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <unistd.h>
#include <time.h>
#include "FIPmon3.h"

int Count_list,j,i=1;

unsigned char old_var_write;

char *str_building[] = { "IP8", "IP1", "IP2", "IP3", "IP4", "IP5", "IP6", "IP7", "EX4", "BA7", "PS" };
char *str_system[] = { "NULL", "CRYO", "RECTIFER(PO)", "QPS", "BLM/BPM", "SURVEY", "DOSIMETRY", "EXPERIMENT", "RF" };
char *str_segment[] = { "0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17" };
char *str_segment_letter[] = { "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "U", "Z", "O", "P" };
char *str_bus_status[] = { "OK", "OK / WARNING", "Data Error", "No Response", "Bus Fault", "Medium Fault" };
char *str_sign_prompt_fresh[] = { "/", "S/", "/P/", "S/P/", "/F", "S/F", "/P/F", "S/P/F" };
char *str_speed[] = { "31.25KHz", "1MHz", "2.5MHz", "Undefined" };

/* display of sync mode*/
char *ba_mode[]={"BA_WAIT","SYN_WAIT_SILENT"};

/*status string*/
char *status[]={"FAULT","OK   "};

extern struct mstrfip_diag_shm *fdgContext;
//extern struct mstrfip_dev *mfip;

/*============================================================================*/
/* Name: clrscr()                                                             */
/*                                                                            */
/* Description: clear the screen                                              */
/*                                                                            */
/*============================================================================*/
void clrscr(){
    printf("%c[;H%c[2J",27,27);
}

/*============================================================================*/
/* Name: display_monitoring()                                                 */
/*                                                                            */
/* Description : used to display all the monitoring                           */
/* informations on the screen                                                 */
/*============================================================================*/
void display_monitoring(){



    //int segment=0;
    clrscr();

    //test if the identification variable is loaded and if yes, print it as the fip speed!
    if(fdgContext->system_id!=0){
        printf(" --::::%s -%s- APPLICATION (EXIT : press ENTER)::::--\n",str_system[fdgContext->system_id],str_speed[fip_speed]);
        printf("   MACROCYCLE : FIXED=%ldms  CALCULATED=%dms  MODE : %s \n",BAperiod/1000,fdgContext->comp_cycle_uslength/1000,ba_mode[trigmode]);
        printf ("\nIDENTIFICATION FIELDBUS\n  Area:%s System:%s Segment Number:%s \n",str_building[fdgContext->building_id],str_system[fdgContext->system_id],str_segment_letter[fdgContext->segment_id]);
    }
    else{
        printf(" --::::%s -%s- APPLICATION (EXIT : press ENTER)::::--\n",str_system[fdgContext->system_id],str_speed[fip_speed]);
        printf("   MACROCYCLE : FIXED=%ldms  CALCULATED=%dms  MODE : %s \n",BAperiod/1000,fdgContext->comp_cycle_uslength/1000,ba_mode[trigmode]);
        printf ("\nIDENTIFICATION FIELDBUS\n  Loading...\n");
    }

    // FIPDIAG COMMUNICATION
    printf("\nFIPDIAG COM   BUS Status:%s  Var Stat:%s\n",str_bus_status[fdgContext->com_status],str_sign_prompt_fresh[fdgContext->var_status]);
    printf("  Last Restart              : %s",ctime(&first_date));              //date when the program has been started
    long localDate = fdgContext->last_cycle_date;
    printf("  Cycle counter             : %d\n",fdgContext->cycle_count);
    printf("  Cycle Date                : %s",ctime(&localDate));
    printf("  Com OK Counter            : %d\n",fdgContext->com_ok_count);
    printf("  Sent Data Value           : %d\n",old_var_write);                             //display the produced variable
    printf("  Received Data Value       : %d\n",fdgContext->acq[0]);                             //display the consumed variable
    printf("  R/W Data error            : %d \n",fdgContext->bad_data_count);             //number of time there was a difference between read and wrote variables
    printf("  Recovery Com OK Counter   : %d\n",fdgContext->com_recover_count);
    localDate = fdgContext->com_first_ok_date;
    //number of com recovered
    printf("  Last Recovery Com OK Date : %s",ctime(&localDate));
    //date of the last recovery com
    printf("  Com Fault Counter         : %d \n",fdgContext->com_fault_count);              //number of com lost
    localDate = fdgContext->com_first_fault_date;
    printf("  Last Com Fault Date       : %s",ctime(&localDate));       //date of the last com lost

    // need to delay of 1 cycle the display of this var
    old_var_write=fdgContext->ctrl[0];

    // TRANSACTIONS MONITORING (if there more traffic on channel2 than channel1, we print an error
    printf("\nTRANSACTIONS MONITORING \n");

    printf("  Nb of Tx OK per unit of time : %d\n",fdgContext->tx_ok_count);
    printf("  Nb of Rx OK per unit of time : %d\n",fdgContext->rx_ok_count);
    printf("  Nb of Rx Frames Error        : %d\n",fdgContext->rx_bad_count);
    printf("  Nb of Tx Frames error        : %d\n\n",fdgContext->tx_bad_count);


   //presence list
    if((fdgContext->present_list[0]&1)){
        printf ("ACTIVE NODES LIST\n");
    }
    else{
        printf ("ACTIVE NODES LIST -> Loading Table...\n");
    }
    printf ("     0  1  2  3  4  5  6  7  8  9  10 11 12 13 14 15\n");
    for (Count_list=0; Count_list < 16; Count_list++){
            printf ("%3d ",Count_list*16);

        //do it twice because we display 16 bits in a rows
        for(j=0;j<8;j++){
            if(i&(fdgContext->present_list[2*Count_list])){
                printf (" X ");
                i=i<<1;
            }
            else{
                printf (" . ");
                i=i<<1;
            }

        }
        i=1;

        for(j=0;j<8;j++){
            if(i&(fdgContext->present_list[2*Count_list+1])){
                printf (" X ");
                i=i<<1;
            }
            else{
                printf (" . ");
                i=i<<1;
            }
        }

        i=1;
        printf ("\n");
    }



}




