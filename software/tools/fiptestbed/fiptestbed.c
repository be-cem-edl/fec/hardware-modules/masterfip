// SPDX-FileCopyrightText: 2023 CERN (home.cern)
//
// SPDX-License-Identifier: LGPL-2.1-or-later

#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <errno.h>
#include <getopt.h>
#include <signal.h>
#include <unistd.h>

#include <libmasterfip.h>

#include "fiptestbed.h"

struct ftb_dev *ftb;

static void ftb_help()
{
	fprintf(stderr,
		"fiptestbed -D [<PCI dev id hex format> | <lun decimal format >] -t -e -i -l <cycle length us>  \n");
	fprintf(stderr, "-D device id in hex format or lun in decimal format\n");
	fprintf(stderr, "-d enable diagnostic. Disabled by default\n");
	fprintf(stderr, "-e enable external trig. Disable by default\n");
	fprintf(stderr, "-i enable internal trig. Disable by default\n");
	fprintf(stderr, "-r master response time(tr) in us. \n");
	fprintf(stderr, "-s swap macro-cycle rate( -s 20 swap every 20 macro cycle). Disable by default\n");
	fprintf(stderr, "-t set 50ohms termination on external trigger. None by default.\n");
	fprintf(stderr, "-c ba cycle length in us\n");
	fprintf(stderr, "-a nanofip board dev addr and payload size addr:nbytes.\n");
	fprintf(stderr, "-u update display rate (-u 100: 1 over 100 macro cycles).\n");
	fprintf(stderr, "-l logging rate (-l 100: 1 over 100 macro cycles).Undefined means no logging\n");
	fprintf(stderr, "-n number of agents(adresses range [1..n].\n");

	fprintf(stderr, "\n");
	exit(1);
}

/* called on normal process termination */
void ftb_exit() {
	mstrfip_exit();
	/* TO be completed if necessary */
	if (ftb->dev != NULL)
		mstrfip_close(ftb->dev);
}

static void ftb_signal_init() {
	sigset_t newmask;
	/*
	 * block signals: set properly the signal mask in the main thread
	 * because other threads will inherit the thread sigmask
	 */
	sigemptyset(&newmask);
	sigaddset(&newmask, SIGINT);
	sigaddset(&newmask, SIGTERM);
	sigaddset(&newmask, SIGABRT);
	sigaddset(&newmask, SIGFPE);
	sigaddset(&newmask, SIGILL);
	sigaddset(&newmask, SIGUSR1);
	pthread_sigmask(SIG_BLOCK, &newmask, NULL);
}


void ftb_run()
{
	sigset_t newmask;
	siginfo_t siginfo;
	int signo, res;
	struct timespec ts;

	sigemptyset(&newmask); /* unblock any signal */
	sigaddset(&newmask, SIGINT);
	sigaddset(&newmask, SIGTERM);
	sigaddset(&newmask, SIGABRT);
	sigaddset(&newmask, SIGFPE);
	sigaddset(&newmask, SIGILL);
	sigaddset(&newmask, SIGUSR1);
	fprintf(stdout, "Main thread is going to sigwaitinfo.\n");

	if (ftb->opt.swap_rate != 0) {
		ts.tv_sec = (ftb->opt.cycle_uslength * ftb->opt.swap_rate) / 1000000;
		ts.tv_nsec = ((ftb->opt.cycle_uslength * ftb->opt.swap_rate) % 1000000) * 1000;
	}
	for (;;) {
		if (ftb->opt.swap_rate != 0)
			signo = sigtimedwait(&newmask, &siginfo, &ts);
		else
			signo = sigwaitinfo(&newmask, &siginfo);
		if (signo > 0 ) {
			/*
			 * signal part of our set: call shutdown
			 */
			ftb_exit();
			exit(1);
		}
		/*
		 * probably wait was interrupted by a signal not in our set,
		 * just ignore it
		 */
		// swap macrocycle
		res = ftb_ba_stop(ftb);
		if (res) {
			fprintf(stderr, "ba_stop: %d %s\n", res,
					mstrfip_strerror(res));
			exit(1);
		}
		res = ftb_ba_start(ftb);
		if (res) {
			fprintf(stderr, "ba_start: %d %s\n", res,
					mstrfip_strerror(res));
			exit(1);
		}
	}
}

static void ftb_read_speed(struct ftb_dev* ftb)
{
	char *speed;
	int res;

	res = mstrfip_hw_speed_get(ftb->dev, &ftb->fbus_speed);
	if (res) {
		fprintf(stderr, "Can't get FIP speed: %s. Exit\n",
			mstrfip_strerror(errno));
		exit(1);
	}

	switch (ftb->fbus_speed) {
	case MSTRFIP_BITRATE_31:
		speed = "31.25 kb/s";
		break;
	case MSTRFIP_BITRATE_1000:
		speed = "1 Mb/s";
		break;
	case MSTRFIP_BITRATE_2500:
		speed = "2.5 Mb/s";
		break;
	default:
	case MSTRFIP_BITRATE_UNDEFINED:
		speed = "Undefined speed";
		break;
	}
	snprintf(ftb->bus_speed_str, 31, "%s", speed);
}

int main(int argc, char *argv[])
{
	int res, i;
	char c;
	char nanofipdev_str[32], *ptr;
	int addr, var_length;

	ftb = malloc(sizeof(struct ftb_dev));
	if (ftb == NULL) {
		perror("Can't instantiate FTB device\n");
		exit(1);
	}

	memset(ftb, 0, sizeof(struct ftb_dev));
	for (i = 0; i < FTB_AGENT_COUNT; ++i)
		ftb->var_length[i] = FTB_VAR_PER_AGENT;
	/* set dev to -1 to check later if PCI devid or lun as been provided */
	ftb->opt.dev_lun = -1;
	ftb->opt.dev_id = -1;
	ftb->opt.update_display_rate = 100; //default
	ftb->error_detected = 0;
	ftb->cycle_count_before_exit = 0;
	ftb->opt.agent_count = FTB_AGENT_COUNT;
	while ((c = getopt (argc, argv, "h?D:deits:c:l:a:u:n:r:")) != -1) {
		switch (c) {
		case 'h':
		case '?':
			ftb_help();
			break;
		case 'D':
			// dev can be provided by PCI device id in hex format
			// or by lun in decimal format
			if (strstr(optarg, "0x")) {
				sscanf(optarg, "%x", &ftb->opt.dev_id);
				snprintf(ftb->dev_id_str, 31, "dev-id:%s",
						optarg);
			}
			else {
				sscanf(optarg, "%d", &ftb->opt.dev_lun);
				snprintf(ftb->dev_id_str, 31, "dev-lun:%s",
						optarg);
			}
			break;
		case 'e':
			ftb->opt.ext_trig = 1;
			break;
		case 'i':
			ftb->opt.int_trig = 1;
			break;
		case 't':
			ftb->opt.ext_trig_term = 1;
			break;
		case 'd':
			ftb->opt.enable_aper_wind = 1;
			break;
		case 'c':
			sscanf(optarg, "%d", &ftb->opt.cycle_uslength);
			break;
		case 'r':
			sscanf(optarg, "%d", &ftb->opt.tr_ustime);
			break;
		case 'l':
			sscanf(optarg, "%d", &ftb->opt.log_rate);
			break;
		case 's':
			sscanf(optarg, "%d", &ftb->opt.swap_rate);
			break;
		case 'u':
			sscanf(optarg, "%d", &ftb->opt.update_display_rate);
			break;
		case 'n':
			sscanf(optarg, "%d", &ftb->opt.agent_count);
			break;
		case 'a':
			sscanf(optarg, "%s", nanofipdev_str);
			ptr = strchr(nanofipdev_str, ':');
			*ptr = '\0';
			addr = atoi(nanofipdev_str);
			++ptr;
			var_length = atoi(ptr);
			ftb->var_length[addr-1] = var_length;
			break;
		}
	}
	// checks mandatory arguments
	if ((ftb->opt.dev_id == -1 && ftb->opt.dev_lun == -1) ||
			ftb->opt.cycle_uslength == 0) {
		ftb_help();
		exit(1);
	}

	ftb_signal_init(); /* block all signals */

	res = mstrfip_init();
	if (res) {
		fprintf(stderr, "Cannot init fip library: %s\n",
				mstrfip_strerror(errno));
		exit(1);
	}
	/* device can be specified by lun or by pci dev id */
	ftb->dev = (ftb->opt.dev_lun == -1) ?
		   mstrfip_open_by_id(ftb->opt.dev_id) :
		   mstrfip_open_by_lun(ftb->opt.dev_lun);
	if(ftb->dev == NULL) {
		fprintf(stderr, "Cannot open mstrfip dev: %s\n",
				mstrfip_strerror(errno));
		exit(1);
	}

	/* reset RT app */
	if (mstrfip_rtapp_reset(ftb->dev) < 0) {
		fprintf(stderr, "Cannot reset rt app: %s\n",
				mstrfip_strerror(errno));
		exit(1);
	}

	/* Get FIP bus speed */
	ftb_read_speed(ftb);

	if (ftb_ba_init(ftb) < 0) {
		fprintf(stderr, "Cannot init ba: %s\n",
				mstrfip_strerror(errno));
		exit(1);
	}

	/* time to start ba */
	ftb_ba_start(ftb);

	/* run for ever till a signal is raised */
	ftb_run();

	return 0;
}
