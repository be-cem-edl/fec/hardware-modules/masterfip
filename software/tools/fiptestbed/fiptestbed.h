// SPDX-FileCopyrightText: 2023 CERN (home.cern)
//
// SPDX-License-Identifier: LGPL-2.1-or-later

#ifndef _FIPTESTBED_H_
#define _FIPTESTBED_H_

#define FTB_AGENT_COUNT 25
#define FTB_VAR_PER_AGENT 2
#define FTB_VAR_LENGTH 2 /* data size in bytes */
struct ftb_opt {
	int dev_id;
	int dev_lun;
	int ext_trig;
	int int_trig;
	int ext_trig_term;
	int cycle_uslength;
	int enable_aper_wind;
	int update_display_rate;
	int log_rate;
	int swap_rate;
	int agent_count;
	uint32_t tr_ustime;
};

/* counter for various errors */
struct ftb_var_error {
	int write_failed; /* writing new value failed */
	int missed; /* missed data (not updated) */
	int bad_data; /* mismatch between write/read data */
	int not_freshed; /* other than 0x5 promptness&significance */
	int not_significant; /* other than 0x5 promptness&significance */
	int tmo; /* time out no reply from agent */
	int hw_err; /* hw bit error in the frame */
	int hw_bsz; /* hw wrong nb of bytes in the frame */
	int hw_ctrl; /* hw bad control byte in the frame */
	int hw_pdu; /* hw bad pdu byte in the frame */
};

struct ftb_ba {
	struct mstrfip_macrocycle *mc;
	struct mstrfip_data *cons_varlist[FTB_AGENT_COUNT];/* consumed varlist */
	struct mstrfip_data *prod_varlist[FTB_AGENT_COUNT];/* produced varlist */
	struct mstrfip_data *ident_varlist[FTB_AGENT_COUNT];/* identification varlist */
};

struct ftb_dev {
	struct mstrfip_dev *dev;
	struct ftb_opt opt;

	enum mstrfip_bitrate fbus_speed; /* FIP bus speed get from the HW */

	struct ftb_ba ba[2];
	int ba_idx;

	struct ftb_var_error var_err[FTB_AGENT_COUNT];

	uint8_t pctrl_byte[FTB_AGENT_COUNT][2]; /* value to send to agent in the previous cycle*/
	uint8_t nctrl_byte[FTB_AGENT_COUNT][2]; /* value to send to agent acquired in the next cycle*/
	int var_length[FTB_AGENT_COUNT];
	uint32_t tr_nstime[FTB_AGENT_COUNT];
	int cycle_counter;
	int cb_error_counter;
	int ident_refresh_counter;
	uint64_t hwcycle_nstime; /* between two conscutives irq var */
	uint32_t swcycle_ustime; /* between two conscutives irq var */
	uint64_t hwirq_nsjitter; /* current jitter */
	uint64_t swirq_usjitter; /* current jitter */
	uint64_t max_hwirq_nsjitter; /* keeps the max jitter */
	uint64_t max_swirq_usjitter; /* keeps the max jitter */
	uint32_t cb_ustime; /* var handler duration */
	uint32_t max_cb_ustime; /* current jitter */
	char start_date[128];
	uint64_t hw_nstime;
	uint64_t sw_ustime;
	char bus_speed_str[32];
	char dev_id_str[32];
	int error_detected;
	int cycle_count_before_exit;
	int irq_count;
	int irq_lost;
};

extern const uint16_t ftb_var_id[2];
extern struct ftb_dev *ftb;

extern int ftb_ba_init(struct ftb_dev *ftb);
extern void ftb_mstrfip_var_handler(struct mstrfip_dev *dev,
			struct mstrfip_data *var, struct mstrfip_irq *irq);
extern void ftb_mstrfip_prod_var_handler(struct mstrfip_dev *dev,
			struct mstrfip_data *var, struct mstrfip_irq *irq);
extern void ftb_mstrfip_ident_var_handler(struct mstrfip_dev *dev,
						struct mstrfip_irq *irq);
extern void ftb_mstrfip_error_handler(struct mstrfip_dev *dev,
					enum mstrfip_error_list error);
extern int ftb_ba_start(struct ftb_dev *ftb);
extern int ftb_ba_stop(struct ftb_dev *ftb);

#endif //_FIPTESTBED_H_
