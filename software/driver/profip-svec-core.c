// SPDX-FileCopyrightText: 2023 CERN (home.cern)
//
// SPDX-License-Identifier: GPL-2.0-or-later

#include <linux/module.h>
#include <linux/platform_device.h>
#include <linux/mfd/core.h>
#include <linux/mod_devicetable.h>

#include "wb_uart/platform_data/wb_uart_pdata.h"

enum profip_dev_offsets {
	PROFIP_MFD_TRTL_MEM_START 		= 0x0003C000,
	PROFIP_MFD_TRTL_MEM_END			= 0x0005C000,
	PROFIP_MFD_WBUART_TERM_MEM_START 	= 0x0000C000,
	PROFIP_MFD_WBUART_TERM_MEM_END 		= 0x0000CF00,
	PROFIP_MFD_WBUART_DATA_MEM_START 	= 0x0000E000,
	PROFIP_MFD_WBUART_DATA_MEM_END 		= 0x0000EF00	
};

/* MFD devices */
enum profip_mfd_devs_enum {
	PROFIP_MFD_TRTL = 0, 
	PROFIP_MFD_WBUART_TERM,
	PROFIP_MFD_WBUART_DATA
};

static struct wb_uart_platform_data wbuart_term_pd = {
	.flags = WB_UART_BIG_ENDIAN,
	.wb_uart_name = "pf",
};

static struct wb_uart_platform_data wbuart_data_pd = {
	.flags = WB_UART_BIG_ENDIAN,
	.wb_uart_name = "pfdata",
};

static struct resource profip_mfd_res[] = {
	{
		.name = "mock-turtle-mem",
		.flags = IORESOURCE_MEM,
		.start = PROFIP_MFD_TRTL_MEM_START,
		.end = PROFIP_MFD_TRTL_MEM_END,
	}, {
		.name = "mock-turtle-irq_in",
		.flags = IORESOURCE_IRQ | IORESOURCE_IRQ_HIGHLEVEL,
		.start = 0,
	}, {
		.name = "mock-turtle-irq_out",
		.flags = IORESOURCE_IRQ | IORESOURCE_IRQ_HIGHLEVEL,
		.start = 1,
	}, {
		.name = "mock-turtle-irq_con",
		.flags = IORESOURCE_IRQ | IORESOURCE_IRQ_HIGHLEVEL,
		.start = 3,
	}, {
		.name = "mock-turtle-irq_not",
		.flags = IORESOURCE_IRQ | IORESOURCE_IRQ_HIGHLEVEL,
		.start = 2,
	},
};

static struct resource profip_mfd_wbuart_term_res[] = {
	{
		.name  = "wb-uart-mem",
		.flags = IORESOURCE_MEM,
		.start = PROFIP_MFD_WBUART_TERM_MEM_START,
		.end   = PROFIP_MFD_WBUART_TERM_MEM_END,
	},
	{
		.name = "wb-uart-irq-rx",
		.flags = IORESOURCE_IRQ | IORESOURCE_IRQ_HIGHLEVEL,
		.start = 4,
	}
};

static struct resource profip_mfd_wbuart_data_res[] = {
	{
		.name  = "wb-uart-mem",
		.flags = IORESOURCE_MEM,
		.start = PROFIP_MFD_WBUART_DATA_MEM_START,
		.end   = PROFIP_MFD_WBUART_DATA_MEM_END,
	},
	{
		.name = "wb-uart-irq-rx",
		.flags = IORESOURCE_IRQ | IORESOURCE_IRQ_HIGHLEVEL,
		.start = 5,
	}
};

static const struct mfd_cell profip_svec_mfd_devs[] = {
	[PROFIP_MFD_TRTL] = {
		.name = "mock-turtle",
		.platform_data = NULL,
		.pdata_size = 0,
		.num_resources = ARRAY_SIZE(profip_mfd_res),
		.resources = profip_mfd_res,
	},
	[PROFIP_MFD_WBUART_TERM] = {
		.name = "wb-uart",
		.platform_data = &wbuart_term_pd,
		.pdata_size = sizeof(struct wb_uart_platform_data),
		.num_resources = ARRAY_SIZE(profip_mfd_wbuart_term_res),
		.resources = profip_mfd_wbuart_term_res,
	},
	[PROFIP_MFD_WBUART_DATA] = {
		.name = "wb-uart",
		.platform_data = &wbuart_data_pd,
		.pdata_size = sizeof(struct wb_uart_platform_data),
		.num_resources = ARRAY_SIZE(profip_mfd_wbuart_data_res),
		.resources = profip_mfd_wbuart_data_res,
	},
};

static int profip_svec_probe(struct platform_device *pdev)
{
	struct resource *rmem;
	int irq;

	rmem = platform_get_resource(pdev, IORESOURCE_MEM, 0);
	if (!rmem) {
		dev_err(&pdev->dev, "Missing memory resource\n");
		return -EINVAL;
	}

	irq = platform_get_irq(pdev, 0);
	if (irq < 0) {
		dev_err(&pdev->dev, "Missing IRQ number\n");
		return -EINVAL;
	}

	/*
	 * We know that this design uses the HTVIC IRQ controller.
	 * This IRQ controller has a linear mapping, so it is enough
	 * to give the first one as input
	 */

	return mfd_add_devices(&pdev->dev, PLATFORM_DEVID_AUTO,
			       profip_svec_mfd_devs,
			       ARRAY_SIZE(profip_svec_mfd_devs),
			       rmem, irq, NULL);
}

static int profip_svec_remove(struct platform_device *pdev)
{
	mfd_remove_devices(&pdev->dev);

	return 0;
}

/**
 * List of supported platform
 */
enum profip_svec_version {
	PROFIP_SVEC_VER = 0,
};

static const struct platform_device_id profip_svec_id_table[] = {
	{
		.name = "profip-svec",
		.driver_data = PROFIP_SVEC_VER,
	}, {
		.name = "id:000010DC53564D42",
		.driver_data = PROFIP_SVEC_VER,
	}, {
		.name = "id:000010dc53564d42",
		.driver_data = PROFIP_SVEC_VER,
	},
	{},
};

static struct platform_driver profip_svec_driver = {
	.driver = {
		.name = "profip-svec",
		.owner = THIS_MODULE,
	},
	.id_table = profip_svec_id_table,
	.probe = profip_svec_probe,
	.remove = profip_svec_remove,
};
module_platform_driver(profip_svec_driver);

MODULE_AUTHOR("Piotr Klasa <piotr.klasa@cern.ch>");
MODULE_LICENSE("GPL");
MODULE_VERSION(DRV_VERSION);
MODULE_DESCRIPTION("Driver for the ProFIP SVEC");
MODULE_DEVICE_TABLE(platform, profip_svec_id_table);

MODULE_SOFTDEP("pre: svec_fmc_carrier mockturtle wb-uart");//
