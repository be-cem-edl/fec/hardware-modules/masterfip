#include <stdlib.h>
#include <unistd.h>
#include <stdio.h>
#include <string.h>
#include <time.h>

#include "mfiptest.h"
#include "mfip-mcycle-config.h"

#define _PROD MSTRFIP_DATA_FLAGS_PROD
#define _CONS MSTRFIP_DATA_FLAGS_CONS

/* FipData fields               addr,sz,   dir,          cb */
struct mft_data_desc prod_var = {0x0, 0, _PROD, NULL};
struct mft_data_desc cons_var = {0x0, 0, _CONS, NULL};

struct mft_wind_pvar_desc wind_prod_per_var = {
	.pvar_count = 1,
	.pvars_desc = {
		&prod_var,
	},
};

struct mft_wind_pvar_desc wind_cons_per_var = {
	.pvar_count = 1,
	.pvars_desc = {
		&cons_var,
	},
};

struct mft_wind_wait_desc wind_wait_1 = {.end_ustime = 0, .silent = 0};
struct mft_wind_wait_desc wind_wait_2 = {.end_ustime = 0, .silent = 0};

struct mft_mcycle_desc mcycle_desc = {
	.wind_count = 4,
	.winds_desc = {
		{
			.type = MFT_WIND_PER_VAR,
			.wind_pvar_desc = &wind_prod_per_var,
		},
		{
			.type = MFT_WIND_WAIT,
			.wind_wait_desc = &wind_wait_1,
		},
		{
			.type = MFT_WIND_PER_VAR,
			.wind_pvar_desc = &wind_cons_per_var,
		},
		{
			.type = MFT_WIND_WAIT,
			.wind_wait_desc = &wind_wait_2,
		},
	},
};

struct mft_options opts = {
	.ext_trig = 0,
	.tr_ustime = 0,
	.irq_thread_prio = 60,
};

void init_mcycle_config(struct mcycle_cfg_case *cfg, struct mft_config_data *mcycle)
{
	mcycle->opts = &opts;
	mcycle->opts->tr_ustime = cfg->tr_time;

	mcycle->mcycle_desc = &mcycle_desc;
	mcycle->mcycle_desc->winds_desc[0].wind_pvar_desc->pvars_desc[0]->addr= cfg->var_addr[0];
	mcycle->mcycle_desc->winds_desc[0].wind_pvar_desc->pvars_desc[0]->bsz= cfg->var_sz[0];
	mcycle->mcycle_desc->winds_desc[0].wind_pvar_desc->pvars_desc[0]->cb= cfg->cb[0];
	mcycle->mcycle_desc->winds_desc[1].wind_wait_desc->end_ustime= cfg->wait_wind_time[0];
	mcycle->mcycle_desc->winds_desc[2].wind_pvar_desc->pvars_desc[0]->addr= cfg->var_addr[1];
	mcycle->mcycle_desc->winds_desc[2].wind_pvar_desc->pvars_desc[0]->bsz= cfg->var_sz[1];
	mcycle->mcycle_desc->winds_desc[2].wind_pvar_desc->pvars_desc[0]->cb= cfg->cb[1];
	mcycle->mcycle_desc->winds_desc[3].wind_wait_desc->end_ustime= cfg->wait_wind_time[1];

	mcycle->mfip_error_handler = cfg->error_cb;
}
