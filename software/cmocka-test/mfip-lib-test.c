#include <stdarg.h>
#include <stddef.h>
#include <setjmp.h>
#include <errno.h>
#include <cmocka.h>

#include <stdlib.h>

#include "mfip-mcycle-config.h"
#include "mfiptest.h"

#include <libmockturtle-internal.h>
#include <masterfip/libmasterfip-priv.h>
#include <masterfip/libmasterfip.h>

enum mtrtl_mocked {
	MFIP_SPEED = 0x1,
	MFIP_BA_CYCLE,
};

static int group_setup(void **state)
{
	struct mstrfip_desc *fip_dev = calloc(1, sizeof(struct mstrfip_desc));
	if (fip_dev == NULL)
		return -1;
	fip_dev->current_error = 0;
	*state = fip_dev;

 	mstrfip_init();

	return 0;
}

static int group_teardown(void **state)
{
	mstrfip_exit();
	free(*state);
	return 0;
}

static int mocked_feature = 0;
int __wrap_trtl_msg_sync(struct trtl_dev *trtl, unsigned int idx_cpu,
			 unsigned int idx_hmq, struct trtl_msg *msg_s,
		  	 struct trtl_msg *msg_r, int timeout)
{
	struct mstrfip_hw_speed_trtlmsg *hw_speed;

	switch (mocked_feature) {
	case MFIP_SPEED:
		hw_speed = (struct mstrfip_hw_speed_trtlmsg *)msg_r->data;
		hw_speed->bitrate = (int)mock();
		return (int)mock(); /* dequeue value to return */
		break;
	case MFIP_BA_CYCLE:
		msg_r->data[0] = MSTRFIP_REP_ACK;
		return 0;
		break;
	default:
		return -1;
	}
	return -1;
}

static struct trtl_config_rom cfgrom;
const struct trtl_config_rom *__wrap_trtl_config_get(struct trtl_dev *trtl)
{
	/* set mockturtle CPU clock */
	cfgrom.clock_freq = 62500000;
	return &cfgrom;
}

int __wrap_trtl_msg_async_send(struct trtl_dev *trtl, unsigned int idx_cpu,
			       unsigned int idx_hmq, struct trtl_msg *msg,
		  	       unsigned int n)
{
	return 0;
}

static void test_lib_fip_speed(void **state) {
	struct mstrfip_desc *dev = (struct mstrfip_desc *)*state;
	enum mstrfip_bitrate bitrate;
	int res, i;
	struct speed_case {
		int ret;   /* expected returned code */
		int speed; /* expected returned speed */
	} test_case [] = {
		{-1, -1},  /* test negative value returned by the HW */
		{0, 1000}, /* test  wrong positive speed returned by the HW */
		{0, MSTRFIP_BITRATE_31}, /* Returned valid speed */
		{0, MSTRFIP_BITRATE_1000}, /* Returned valid speed */
		{0, MSTRFIP_BITRATE_2500}, /* Returned valid speed */
	};
	int test_case_sz = sizeof(test_case)/sizeof(struct speed_case);
	struct speed_case *ptr_case = test_case;

	mocked_feature = MFIP_SPEED;
	for (i = 0; i < test_case_sz; ++ptr_case, ++i) {
		will_return(__wrap_trtl_msg_sync, ptr_case->speed); /* enqueue speed from mock function */
		will_return(__wrap_trtl_msg_sync, ptr_case->ret); /* enqueue value to return by the mock function */
		res = mstrfip_hw_speed_get((struct mstrfip_dev *)dev, &bitrate);
		assert_int_equal(res, ptr_case->ret);
		if (ptr_case->ret == -1)
			assert_int_equal(bitrate, MSTRFIP_BITRATE_UNDEFINED);
		else
			assert_int_equal(bitrate, ptr_case->speed);
	}
}

#define SPEED_NB  3
#define TEST_CASE_NB  5
/*
 * Test the masterfip library checking if the provided macro cycle
 * configuration is feasible.
 */
static void test_mcycle(void **state) {
	struct mstrfip_desc *dev = (struct mstrfip_desc *)*state;
	int res, speed_idx, test_case_idx;
	struct mft_dev *mft;

	struct test_case {
		int speed;
		int expected_failure[TEST_CASE_NB]; /* =1 tells if test should failed */
		struct mcycle_cfg_case test_case[TEST_CASE_NB];
	} test_cases[] = {
 	  {
		.speed = MSTRFIP_BITRATE_31,
		.expected_failure = {0, 0, 0, 1, 1},
		.test_case =
		{
		  	/* 31.25 Kb/s speed: various payload size and turn around time
		   	 * The wait window duration is computed to the minum for each
		   	 * test case
		   	*/
			{{0x0, 0x0}, {64, 65}, {NULL, NULL}, {20788, 41842}, 424, NULL},
			{{0x0, 0x0}, {124, 124}, {NULL, NULL}, {35557, 71133}, 424, NULL},
			{{0x0, 0x0}, {64, 65}, {NULL, NULL}, {21004, 42274}, 532, NULL},
			{{0x0, 0x0}, {64, 65}, {NULL, NULL}, {21003, 42274}, 532, NULL},
			{{0x0, 0x0}, {64, 65}, {NULL, NULL}, {21004, 42273}, 532, NULL},
		},
	  },
	  {
		.speed = MSTRFIP_BITRATE_1000,
		.expected_failure = {0, 0, 0, 1, 1},
		.test_case =
		{
			/*
		   	 * 1 Mb/s speed: various payload size and turn around time
		   	 * The wait window duration is computed to the minum for each
		   	* test case
		   	*/
			{{0x0, 0x0}, {64, 65}, {NULL, NULL}, {675, 1377}, 13, NULL},
			{{0x0, 0x0}, {124, 124}, {NULL, NULL}, {1155, 2329}, 13, NULL},
			{{0x0, 0x0}, {64, 65}, {NULL, NULL}, {713, 1453}, 32, NULL},
			{{0x0, 0x0}, {64, 65}, {NULL, NULL}, {712, 1453}, 32, NULL},
			{{0x0, 0x0}, {64, 65}, {NULL, NULL}, {713, 1452}, 32, NULL},
		},
	  },
	  {
		.speed = MSTRFIP_BITRATE_2500,
		.expected_failure = {0, 0, 0, 1, 1},
		.test_case =
		{
			/*
		   	 * 2.5 Mb/s speed: various payload size and turn around time
		   	 * The wait window duration is computed to the minum for each
		   	 * test case
		   	 */
			{{0x0, 0x0}, {64, 65}, {NULL, NULL}, {281, 590}, 10, NULL},
			{{0x0, 0x0}, {124, 124}, {NULL, NULL}, {473, 965}, 10, NULL},
			{{0x0, 0x0}, {64, 65}, {NULL, NULL}, {321, 664}, 30, NULL},
			{{0x0, 0x0}, {64, 65}, {NULL, NULL}, {320, 664}, 32, NULL},
			{{0x0, 0x0}, {64, 65}, {NULL, NULL}, {321, 663}, 32, NULL},
		},
	  },
	};
	struct mft_config_data mcycle_cfg_data;

	mft = calloc(1, sizeof(struct mft_dev));
	if (mft == NULL) {
		perror("Can't instantiate mft device\n");
		exit(1);
	}
	mft->dev = (struct mstrfip_dev *)dev;

	mocked_feature = MFIP_BA_CYCLE;
	for (speed_idx = 0; speed_idx < SPEED_NB; speed_idx++) {
		dev->ba.bitrate = test_cases[speed_idx].speed;
		fprintf(stdout, "\t\tBus Speed %s\n", mft_speed2str(dev->ba.bitrate));
		for (test_case_idx = 0; test_case_idx < TEST_CASE_NB; ++test_case_idx) {
			init_mcycle_config(&test_cases[speed_idx].test_case[test_case_idx], &mcycle_cfg_data);
			fprintf(stdout, "\t\t\tTest case %d\n", test_case_idx);
			mft->cfg_data = &mcycle_cfg_data;
			res = mft_init_hw_sw_cfg(mft);
			assert_int_equal(res, 0);
			res = mft_create_mcycle(mft);
			if (res)
				fprintf(stderr, "Create macrocycle failed: %s\n",
					mstrfip_strerror(errno));
			assert_int_equal(res, 0);
			res = mstrfip_macrocycle_isvalid(mft->dev, mft->mcycle.mc);
			if (res != 0) {
				if ((errno == MSTRFIP_BA_INVALID_MACROCYCLE) &&
			    	    (test_cases[speed_idx].expected_failure[test_case_idx] == 1))
					/*
					 * it is expected to fail with this error
					 * reset error.
					 */
					res = 0;
				else
					fprintf(stderr, "%s\n", mstrfip_strerror(errno));
			}
			mft_delete_mcycle(mft);
			assert_int_equal(res, 0);
		}
	}
	free(((struct mstrfip_desc *)mft->dev)->mcycle_list);
	free(mft);
}

int main(void) {
	const struct CMUnitTest tests[] = {
		cmocka_unit_test(test_lib_fip_speed),
		cmocka_unit_test(test_mcycle),
	};

	return cmocka_run_group_tests(tests, group_setup, group_teardown);
}
