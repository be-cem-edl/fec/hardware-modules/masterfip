REPO_PARENT ?= $(shell pwd)
-include $(REPO_PARENT)/../common.mk

#overwrite rules from Make.auto
%: %.o
	$(LINK.o) $^  $(LOADLIBES) $(LDLIBS) -o $@

%.o: %.c
	$(COMPILE.c) $< $(OUTPUT_OPTION)

%: %.c
	$(LINK.c) $^  $(LOADLIBES) $(LDLIBS) -o $@

LINUX=$(KERNELSRC)
export LINUX
export CC
export LD
export CROSS_COMPILE
