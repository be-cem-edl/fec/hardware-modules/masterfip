/*
 * Copyright (C) 2016-2017 CERN (www.cern.ch)
 * Author: Michel Arruat <michel.arruat@cern.ch>
 *
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#include <mockturtle-rt.h>

#include "masterfip-smem.h"
#include "hw/masterfip_hw.h"
#include "masterfip-rt-ba.h"

/*
 * TODO: turnaround time and silence time should be computed according to the
 * FIP speed. Currently the value corresponds to the 1MBit/s speed and with
 * 100MHz as CPU core clock.
 */
inline void mstrfip_load_stime_counter()
{
	dp_writel(ba->hw.s_ticks, MASTERFIP_CSR_BASE + MASTERFIP_REG_SILEN); // reset inactive
}

inline void mstrfip_load_trtime_counter()
{
	dp_writel(ba->hw.tr_ticks, MASTERFIP_CSR_BASE + MASTERFIP_REG_TURNAR); // reset inactive
}

/* start turn aound counter */
void mstrfip_start_trcounter()
{
	uint32_t val;

	val = dp_readl( MASTERFIP_CSR_BASE + MASTERFIP_REG_TURNAR);
	val |= MASTERFIP_TURNAR_START; /* raise start bit */
	dp_writel(val, MASTERFIP_CSR_BASE + MASTERFIP_REG_TURNAR);
}

/* start silence time counter */
inline void mstrfip_start_scounter(int flg)
{
	uint32_t val;

	val = dp_readl( MASTERFIP_CSR_BASE + MASTERFIP_REG_SILEN);
	val |= MASTERFIP_SILEN_START; /* raise start bit */
	dp_writel(val, MASTERFIP_CSR_BASE + MASTERFIP_REG_SILEN);
}

/*
 * Called at every ba-start in order to sync with the internal macrocycle
 * oscillator or with the external pulse if it is requested.
 * In case an external pulse is expected but doesn't arrive,
 * the system starts in sync with the macrocycle counter.
 */
int mstrfip_wait_macrocyc_counter()
{
	uint32_t val;
	/* macrocycle counter value the system is waiting for */
	uint32_t ticks = delays_in_cpu_ticks[IDX_1US_DELAY];
	/*
	 * risk of being stuck in infinite loop in case HW is mulfunctioning.
	 * So we update cycle_state in smem to be able to know that CPU0 is
	 * stuck through get-report on CPU1
	 */
	smem_data->report.cycle_state = MSTRFIP_FSM_WAIT_EXT_TRG;
	ba->led_reg |= MASTERFIP_LED_EXT_SYNC_ERR;
	ba->led_reg &= ~MASTERFIP_LED_EXT_SYNC_ACT;
	dp_writel(ba->led_reg, MASTERFIP_CSR_BASE + MASTERFIP_REG_LED);
	for (;;) {
		/* check if a new ba state is requested */
		if (smem_data->ba_state_cmd != MSTRFIP_BA_STATE_CMD_START)
			/* requested state is RESET or STOP: abort execution */
			return MSTRFIP_MACROCYC_ABORT;
		if (ba->hw.ext_trig) {
			val = dp_readl( MASTERFIP_CSR_BASE +
					MASTERFIP_REG_EXT_SYNC_P_CNT);
			if (val != ba->report.ext_sync_pulse_count) {
				// New external pulse time to leave
				break;
			}
		}
		if (ba->hw.freerun) {
			val = dp_readl( MASTERFIP_CSR_BASE +
					MASTERFIP_REG_MACROCYC_TIME_CNT);
			if (val < ticks) { /* count down turn around time counter */
				/*
				 * TODO: register in the report that we are not in sync
			 	 * with ext trig....
			 	 */
				++ba->report.int_sync_pulse_count;
				break;
			}
		}
	}
	ba->led_reg |= MASTERFIP_LED_EXT_SYNC_ACT;
	ba->led_reg &= ~MASTERFIP_LED_EXT_SYNC_ERR;
	dp_writel(ba->led_reg, MASTERFIP_CSR_BASE + MASTERFIP_REG_LED);
	return 0;
}

inline void mstrfip_start_cycle_counter()
{
	uint32_t val;

	/*
	 * Initial sequence to start the macro cycle counter.
	 * Then it is re-armed automatically by TX ends bit.
	 */
	/* Raise start bit and load macrocycle length */
	val = MASTERFIP_MACROCYC_START | ba->cycle.nticks;
	dp_writel(val, MASTERFIP_CSR_BASE + MASTERFIP_REG_MACROCYC);
}

/*
 * when the serializer has sent the frame, the master waits for the
 * turnaround time before writing the next frame into the serializer
 * In case an external pulse is connected, sync-flag is used to re-sync
 * the internal oscillator with the external pulse.
 * This is done at the begining of every macro cycle. Nevertheless we check
 * also the turn around counter in case the external pulse is missing.
 */
void mstrfip_wait_trtime()
{
	uint32_t val;

	for (;;) {
		/* count down turn around time counter */
		val = dp_readl( MASTERFIP_CSR_BASE + MASTERFIP_REG_TURNAR_TIME_CNT);
		if (val < delays_in_cpu_ticks[IDX_300NS_DELAY])
			return;
	}
}

void mstrfip_wait_stime()
{
	uint32_t tr;
	for (;;) {
		tr = dp_readl( MASTERFIP_CSR_BASE + MASTERFIP_REG_SILEN_TIME_CNT);
		if (!tr) /* count down silence time counter */
			return;
	}
}


/**
 * It waits till the macrocycle counter reachs the given ticks value
 * and arm tr counter before exiting.
 * @param[in] macrocyc_counter_ticks value to wait for
 */
void mstrfip_wait_and_rearm_tr(int macrocyc_counter_ticks)
{
	int now_ticks;

	/*
	 * risk of being stuck in infinite loop in case macrocyc counter
	 * is mulfunctioning. So we update cycle_state in smem to be able to
	 * know that CPU0 is stuck through get-report on CPU1
	 */
	smem_data->report.cycle_state = MSTRFIP_FSM_WAIT_MACROCYC_TICKS_COUNTER;
	while(1) {
		now_ticks = dp_readl(MASTERFIP_CSR_BASE +
				     MASTERFIP_REG_MACROCYC_TIME_CNT);
		if ( now_ticks < macrocyc_counter_ticks ) {
			/*
			 * start manually the turn around counter
			 */
			mstrfip_start_trcounter();
			return;
		}
	}
}
