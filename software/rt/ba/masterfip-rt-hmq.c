/*
 * Copyright (C) 2016-2017 CERN (www.cern.ch)
 * Author: Michel Arruat <michel.arruat@cern.ch>
 *
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#include <mockturtle-rt.h>

#include "masterfip-smem.h"
#include "hw/masterfip_hw.h"
#include "masterfip-rt-ba.h"
#include "masterfip-mq.h"

/* Creates a hmq_buf serializing object for the control output slot */
void mstrfip_hmq_claim_out_buf(int hmq_slot, struct trtl_fw_msg* msg)
{
	// Removed checking if the queue is not full, because RT SW cannot wait.
	// Such an event is very unlikely, but if it happens, the data in
	// the queue will be overwritten.

	mq_claim(MFIP_MQ, hmq_slot);
	mq_map_out_message(MFIP_MQ, hmq_slot, msg);
	
	msg->header->rt_app_id = CONFIG_RT_APPLICATION_ID;	
}

static void mstrfip_hmq_flush_buffer(int hmq_slot)
{
	struct hmq_slot_ctx* sctx = &ba->slot_ctx[hmq_slot];
	struct mstrfip_acq_trtlmsg_desc *acq_trtlmsg;

	#ifdef MFIP_FOR_PROFIP
	static uint16_t sync_cnt[MSTRFIP_HMQ_ACQ_SLOT_COUNT + 1] = {0};

    sctx->obuf.header->sync_id = (sync_cnt[hmq_slot]++);
	#endif

	masterfip_mq_send(hmq_slot, &(sctx->obuf));
	/*
	 * prepare the slot context for next acquisition
	 * Claim is required to map the buffer to the available entry in the
	 * FIFO hmq slot.
	 */
	mstrfip_hmq_claim_out_buf(hmq_slot, &sctx->obuf);
	acq_trtlmsg = (struct mstrfip_acq_trtlmsg_desc *)sctx->obuf.payload;
	acq_trtlmsg->hmq_slot = hmq_slot; /* trtl msg's payload size in words */
	acq_trtlmsg->nentries = 0; /* trtl msg's payload size in words */
	acq_trtlmsg->irq_woffset = 0; /* trtl msg's payload size in words */
	sctx->obuf.header->len = MSTRFIP_ACQ_TRTLMSG_DESC_WSZ;
}

void mstrfip_hmq_append_data_entry(int hmq_slot, uint32_t nwords,
		uint32_t bsz, uint32_t key, uint32_t **payload_ptr, int flg)
{
	struct mstrfip_acq_trtlmsg_desc *acq_trtlmsg;
	struct hmq_slot_ctx* sctx = &ba->slot_ctx[hmq_slot];
	struct mstrfip_data_entry_desc *data  = NULL;

	/*Check space to insert data entry of this size: (header + nwords) */
	if (sctx->obuf.header->len + nwords +
		MSTRFIP_DATA_ENTRY_DESC_WSZ > MSTRFIP_DATA_ENTRY_WSZ) {
		/* no space: flush the buffer to make it ready for new entry */
		++(sctx->slot_msg_count);
		mstrfip_hmq_flush_buffer(hmq_slot);
	}
	/* set the data entry header */
	data = (struct mstrfip_data_entry_desc *)
				&(((uint32_t*)sctx->obuf.payload)[sctx->obuf.header->len]);
	/* Set the data entry header assuming the payload will be valid */
	data->type = MSTRFIP_DATA_ENTRY_TYPE;
	data->key = key;
	data->payload_bsz = bsz;
	data->entry_wsz = nwords + MSTRFIP_DATA_ENTRY_DESC_WSZ;
	data->error = 0;
	sctx->obuf.header->len += MSTRFIP_DATA_ENTRY_DESC_WSZ; /* data entry header sz */
    sctx->obuf.header->flags = 0;
    sctx->obuf.header->seq = 0;
    sctx->obuf.header->sync_id = 0;
    sctx->obuf.header->rt_app_id = CONFIG_RT_APPLICATION_ID;
    sctx->obuf.header->msg_id = 0;

	*payload_ptr = (uint32_t *)&(((uint32_t*)sctx->obuf.payload)[sctx->obuf.header->len]); /* payload ptr */
	sctx->obuf.header->len += nwords; /* incrment by payload size */
	/* increment number of data entries in th emturtle message header */
	acq_trtlmsg = (struct mstrfip_acq_trtlmsg_desc *)sctx->obuf.payload;
	++(acq_trtlmsg->nentries);
#ifdef RTDEBUG_RX_FRAME
	if (flg)
		pr_debug(__FILE__":%d %d %d %d %d %d %d", __LINE__, 
			data->type, data->key,
			data->payload_bsz, data->entry_wsz, 
			data->error, sctx->obuf.header->len);
#endif
}

/*
 * For non atomic transaction like RP_MSG_ACK/RP_ACK/RP_FIN, it's only at the
 * end of the transaction that you know if it is valid or not. So this function
 * cancel the last data entry by moving it into error state and freeing the
 * space taken by the payload which is not valid because the full transaction
 * didn't complete successfully.
 */
void mstrfip_hmq_invalidate_last_data_entry(int hmq_slot,
				uint32_t nwords, uint32_t err, int flg)
{
	struct hmq_slot_ctx* sctx = &ba->slot_ctx[hmq_slot];
	struct mstrfip_data_entry_desc *data  = NULL;
	uint32_t pos;

	pos = sctx->obuf.header->len - nwords - MSTRFIP_DATA_ENTRY_DESC_WSZ;
	/* set the data entry header */
	data = (struct mstrfip_data_entry_desc *)
				&(((uint32_t*)sctx->obuf.payload)[pos]);
	/* invalid data entry:overwrite data entry size and set error */
	data->payload_bsz = 0;
	data->entry_wsz = MSTRFIP_DATA_ENTRY_DESC_WSZ;
	data->error = err;
	sctx->obuf.header->len -= nwords;
#ifdef RTDEBUG_RX_FRAME
	if (flg && err != MSTRFIP_FRAME_TMO)
		pr_debug(__FILE__":%d %d %d %d", __LINE__, sctx->obuf.header->len, data->key, data->error);
#endif
}

/*
 * For dynamic payload like msgs, the payload size is not known in advance.
 * Therefore the data entry is created using the max payload size and after the
 * processing of the incoming message the real payload size is adjusted.
 */
inline void mstrfip_hmq_set_last_data_entry_payload_sz(int hmq_slot,
					struct rx_frame_ctx *frame, int flg)
{
	struct hmq_slot_ctx* sctx = &ba->slot_ctx[hmq_slot];
	struct mstrfip_data_entry_desc *data  = NULL;
	uint32_t pos, nwords;

	pos = sctx->obuf.header->len - frame->payload_wsz -
						MSTRFIP_DATA_ENTRY_DESC_WSZ;
	/* set the data entry header */
	data = (struct mstrfip_data_entry_desc *)&(((uint32_t*)sctx->obuf.payload)[pos]);
	/* Set datalen, wsz and bsz according to the acquired payload size */
	data->payload_bsz = frame->acq_payload_bsz;
	nwords = (data->payload_bsz / 4) + (!!(data->payload_bsz % 4));
	data->entry_wsz = MSTRFIP_DATA_ENTRY_DESC_WSZ + nwords;
	sctx->obuf.header->len = pos + data->entry_wsz;
#ifdef RTDEBUG_RX_FRAME
	if (flg)
		pr_debug(__FILE__":%d %d %d %d %d %d %d %d %d", __LINE__,
			sctx->obuf.header->len , data->key, data->entry_wsz,
			data->payload_bsz,
			sctx->((uint32_t*)obuf.payload)[pos + MSTRFIP_DATA_ENTRY_DESC_WSZ],
			sctx->((uint32_t*)obuf.payload)[pos + MSTRFIP_DATA_ENTRY_DESC_WSZ + 1],
			sctx->((uint32_t*)obuf.payload)[pos + MSTRFIP_DATA_ENTRY_DESC_WSZ + 2],
			sctx->((uint32_t*)obuf.payload)[pos + data->entry_wsz -1]);
#endif
}

static inline void mstrfip_hmq_append_irq_entry(int hmq_slot, uint32_t var_key,
				uint32_t var_flags)
{
	struct hmq_slot_ctx* sctx = &ba->slot_ctx[hmq_slot];
	struct mstrfip_acq_trtlmsg_desc *acq_trtlmsg =
		(struct mstrfip_acq_trtlmsg_desc *)sctx->obuf.payload;
	struct mstrfip_irq_entry_desc *irq;
	uint32_t secs, ticks;

	secs = lr_readl(MT_CPU_LR_REG_TAI_SEC);
	ticks = lr_readl(MT_CPU_LR_REG_TAI_CYCLES);

	/* map the struct irq_entry to the right place in the output msg */
	irq = (struct mstrfip_irq_entry_desc *)
			&(((uint32_t*)sctx->obuf.payload)[sctx->obuf.header->len]);

	/* first incrment number of entries and set irq offset */
	++(acq_trtlmsg->nentries);
	acq_trtlmsg->irq_woffset = sctx->obuf.header->len;

	/* then fill the irq entry */
	irq->type = MSTRFIP_IRQ_ENTRY_TYPE;
	irq->wsz = MSTRFIP_IRQ_ENTRY_DESC_WSZ;
	irq->key = var_key;
	irq->flags = var_flags;
	irq->hw_sec = secs;
	irq->hw_ticks = ticks;
	/*
	 * Nb of var msg pending sent since the
	 * previous irq. Used by the app to empty
	 * the driver msg queue on IRQ reception.
	 */
	irq->trtlmsg_count = sctx->slot_msg_count;
	/* irq_count is incrmented only for application data */
	if ( (hmq_slot == MSTRFIP_HMQ_O_APP_PER_VAR) ||
		(hmq_slot == MSTRFIP_HMQ_O_APP_APER_VAR) ||
		(hmq_slot == MSTRFIP_HMQ_O_APP_APER_MSG) )
		irq->irq_count = ++ba->mstrfip_irq_count;
	sctx->obuf.header->len += MSTRFIP_IRQ_ENTRY_DESC_WSZ;
#ifdef RTDEBUG_IRQ
	if (trace_count < 60) {
		pp_printf("post irq msg. secs:%d ticks:%d "
				"msg_count:%d irq_count: %d\n",
				secs, ticks, ((uint32_t*)obuf.payload)[5], ((uint32_t*)obuf.payload)[4]);
		++trace_count;
	}
#endif
}

/*
 * Append an irq entry and flush hmq buffer
 */
void mstrfip_hmq_post_irq(int hmq_slot, uint32_t var_key,
				uint32_t var_flags, int flg)
{
	struct hmq_slot_ctx* sctx = &ba->slot_ctx[hmq_slot];

	++(sctx->slot_msg_count);
	/* append irq in the hmq buffer.(place is guaranteed) */
	mstrfip_hmq_append_irq_entry(hmq_slot, var_key, var_flags);
	/* flush hmq buffer*/
	mstrfip_hmq_flush_buffer(hmq_slot);
	sctx->slot_msg_count = 0; /* reset it */
}
