/*
 * Copyright (C) 2016-2017 CERN (www.cern.ch)
 * Author: Michel Arruat <michel.arruat@cern.ch>
 *
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#include <mockturtle-rt.h>

#include "masterfip-smem.h"
#include "hw/masterfip_hw.h"
#include "masterfip-rt-ba.h"

/* checks if the serializer has finished to send the frame. */
int mstrfip_wait_end_of_tx(uint32_t bsz)
{
	uint32_t tx_status;
	int curr_bsz;
	int res;

	/*
	 * To avoid the risk of looping for ever in case the STOP bit indicating
	 * the end of serialization is not raised, we check that the current
	 * bytes number serilaized by the HW is incrementing and is not
	 * exceeding the expected number of bytes.
	 */
	curr_bsz = 0;
	/*
	 * risk of being stuck in infinite loop in case HW is mulfunctioning.
	 * So we update cycle_state in smem to be able to know that CPU0 is
	 * stuck through get-report on CPU1
	 */
	smem_data->report.cycle_state = MSTRFIP_FSM_WAIT_TX_END;
	res =0;
	for(;;) {

		tx_status = dp_readl( MASTERFIP_CSR_BASE + MASTERFIP_REG_TX_STAT);
		if (tx_status & MASTERFIP_TX_STAT_STOP)
			break;
		curr_bsz = (tx_status & MASTERFIP_TX_STAT_CURR_BYTE_INDX_MASK )
			>> MASTERFIP_TX_STAT_CURR_BYTE_INDX_SHIFT;

		/* the register count one byte more :
		 * probably one of the bytes added by the hardware (head or
		 * tail)
		 * Anyway if the current bytes is bigger this is abnormal and
		 * we leave the loop and return an error
		 */
		if (curr_bsz > bsz + 1) {
			res = 1;
			break;
		}
		/*
		 * Ultimate check to leave before end of macrocycle in case of
		 * curr_bsz is within limit and STOP has not come.
		 */
		if (dp_readl(MASTERFIP_CSR_BASE + MASTERFIP_REG_MACROCYC_TIME_CNT)
		    < ba->hw.tr_ticks) {
			res = 1;
			break;
		}
	}
	(res) ? ++ba->report.tx_err : ++ba->report.tx_ok;
	return res;
}

static inline int mstrfip_wait_preamb(struct rx_frame_ctx *frame) {
	uint32_t silence, rx_status, fd;
	uint32_t res, rx_on;
	/* margin tacking in account TMO processing */
	uint32_t silence_margin_ticks = delays_in_cpu_ticks[IDX_5US_DELAY];

	// reset RX registers before receiving traffic.
	dp_writel(0x01, MASTERFIP_CSR_BASE + MASTERFIP_REG_RX_CTRL);  // rst rx

	/*
	 * risk of being stuck in infinite loop in case HW is mulfunctioning.
	 * So we update cycle_state in smem to be able to know that CPU0 is
	 * stuck through get-report on CPU1
	 */
	smem_data->report.cycle_state = MSTRFIP_FSM_WAIT_RX_PREAMB;
	rx_on = 0;
	res = MSTRFIP_FRAME_OK;
	/*
	 * wait_preamb is executed rigth after end of transmitting an ID_DAT, and
	 * we expect an incoming RP_DAT from a node. Even if the serializer
	 * said that TX is finished, the field drive adds extra traffic, and
	 * we have observed that sometimes we check FD_CD bit on the field
	 * drive a bit too early and instead of detecting the RP_DAT traffic we
	 * still detect the remaining extra traffic of the previous ID_DAT.
	 * This extra traffic is equivalent to 5 bits. Therefore we wait the
	 * time required to serialize a byte according to the bus speed.
	 */
	delay(ba->hw.byte_ticks);
	do {
		/* check RX register */
		rx_status = dp_readl( MASTERFIP_CSR_BASE + MASTERFIP_REG_RX_STAT);
		if (rx_status & MASTERFIP_RX_STAT_PREAM_OK) { /* preamb detected */
			res = MSTRFIP_FRAME_OK;
			break; /* frame is being received, leave the loop */
		}
		if (!rx_on) {
			fd = dp_readl( MASTERFIP_CSR_BASE + MASTERFIP_REG_FD);
			/*
			 * check if fielddrive carrier detect is high meaning
			 * traffic on the line
		 	 */
			if (fd & MASTERFIP_FD_CD) {
				rx_on = 1;
			}
		}
		/* check silence counter to know if we can wait more */
		silence = dp_readl( MASTERFIP_CSR_BASE + MASTERFIP_REG_SILEN_TIME_CNT);
		if (silence < silence_margin_ticks) { /* time-out expired: counter reached 0 */
			if (rx_on) {
				/*
				 * RX traffic (begin of a frame) has been
				 * detected but didn't end properly.
				 */
				++ba->report.rx_err;
				res = MSTRFIP_FRAME_ERR;
			}
			else {
				/*
				 * No RX traffic detected. Agent didn't reply
				 */
				if (frame->report_tmo) /* count tmo error */
					++ba->report.rx_tmo;
				res = MSTRFIP_FRAME_TMO;
			}
			break;
		}
	} while (1);
	return res;
}

/*
 * Check control byte of the response frame and does all actions required
 * according to the control byte of the frame.
 * The control byte of the frame is returned using the output
 * parameter(ctrl_byte). To be sure to not be stuck for ever in case of
 * corrupted frame, we check that the remaining time in the macro cycle is more
 * than a turn around time. FRAME error is returned if we reach the limit.
 */
static inline int mstrfip_get_ctrl_byte(struct rx_frame_ctx *frame, int flg)
{
	uint32_t rx_status;
	uint32_t res;

	/*
	 * risk of being stuck in infinite loop in case HW is mulfunctioning.
	 * So we update cycle_state in smem to be able to know that CPU0 is
	 * stuck through get-report on CPU1
	 */
	smem_data->report.cycle_state = MSTRFIP_FSM_WAIT_RX_CTRL_BYTE;
	res = MSTRFIP_FRAME_OK;
	do {
		rx_status = dp_readl( MASTERFIP_CSR_BASE + MASTERFIP_REG_RX_STAT);
		if (rx_status & MASTERFIP_RX_STAT_CTRL_BYTE_OK) /*ctr_byte received */
			break;
		if (dp_readl(MASTERFIP_CSR_BASE + MASTERFIP_REG_MACROCYC_TIME_CNT)
		    < ba->hw.tr_ticks) {
			/*
			 * probably corrupted frame: remains turn around time
			 * before next macro cycle. Leave to be ready for next one.
			 */
			++ba->report.rx_err;
			res = MSTRFIP_FRAME_ERR;
			break;
		}
	} while (1);
	if (res == MSTRFIP_FRAME_OK)
		/* stores control byte for further check */
		frame->acq_ctrl_byte = dp_readl(
			MASTERFIP_CSR_BASE + MASTERFIP_REG_RX_PAYLD_CTRL);
	return res;
}

/* Insert new entry in the consumed aperiodic message FIFO */
//static void __attribute__ ((noinline)) mstrfip_cons_msg_fifo_add(uint32_t var_id)
static int mstrfip_cons_msg_fifo_add(uint32_t var_id)
{
	uint32_t r_idx, i;

	// Adding a pending request in asynchronous message FIFO requires a
	// macrocycle configured with aperiodic messages FIFO
	if (ba->msg.entries == NULL)
		return -1;
	/*
	 * check if we still have this var_id registered: if it is registered
	 * this means that the master didn't have the time to schedule a
	 * message for it, so no point to register it again, it will be
	 * schedule as soon as possible.
	 */
	for (i =  0, r_idx = ba->msg.r_idx; i < ba->msg.entries_count; ++i) {
		if (ba->msg.entries[r_idx] == var_id)
			return 0;
		r_idx = (r_idx + 1) % ba->msg.entries_sz;
	}
	/* new request */
	/* check if cons aper mesg FIFO is full */
	if (ba->msg.entries_count == ba->msg.entries_sz) {
		/*
		 * TODO: increment a new field in the report variable to tell
		 * that FIFO is saturated
		 */
		/* increment read_index loosing the oldest request */
		ba->msg.r_idx = (ba->msg.r_idx + 1) %
			ba->msg.entries_sz;
		--ba->msg.entries_count;
	}
	ba->msg.entries[ba->msg.w_idx] = var_id;
	ba->msg.w_idx = (ba->msg.w_idx + 1) % ba->msg.entries_sz;
	++ba->msg.entries_count;
	return 0;
}

/*
 * additional check/action are done depending of frame type defined by the
 * pdu_type
 */
static inline int mstrfip_chk_frame(uint32_t *payload_ptr,
				    struct rx_frame_ctx *frame, int frame_err)
{
	int res;
	uint32_t nwords, shift_bsz, refresh_val;

	switch (frame->pdu_type) {
	case MSTRFIP_PDU_MPS_VAR: /* periodic var frame  expected*/
		if (frame->acq_ctrl_byte != MSTRFIP_RP_DAT &&
		    frame->acq_ctrl_byte != MSTRFIP_RP_DAT_MSG)
			return MSTRFIP_FRAME_BAD_CTRL;
		if ((frame_err == MSTRFIP_FRAME_OK) &&
		    (frame->acq_ctrl_byte == MSTRFIP_RP_DAT_MSG) ) { /* agent request for mesage */
			/* store agent's request in the msg fifo */
			res = mstrfip_cons_msg_fifo_add(frame->var_id); /* stores agent's id */
			if (res < 0)
				return MSTRFIP_FRAME_BAD_CTRL;
		}
		/*
		 * Ultimately check MPS status (refresh bit): in case of not
		 * fresh data, rx_mps_status err is incremented, but frame is
		 * considered OK because MPS status error is delivered to teh
		 * client
		 */
		if (frame_err == MSTRFIP_FRAME_OK) {
			nwords = (frame->acq_payload_bsz / 4) + (!!(frame->acq_payload_bsz % 4));
			shift_bsz = (frame->acq_payload_bsz - 1) % 4; /* total byts - MPS byte */
			refresh_val = payload_ptr[nwords - 1] &
				(MSTRFIP_MPS_REFRESH_BIT << (shift_bsz * 8));
			if (!refresh_val)
				++ba->report.rx_mps_status_err;
		}
		break;
	case MSTRFIP_PDU_NONE: /* aperiodic message frame expected */
		if (frame->acq_ctrl_byte != MSTRFIP_RP_MSG_NOACK &&
		    frame->acq_ctrl_byte != MSTRFIP_RP_FIN &&
		    frame->acq_ctrl_byte != MSTRFIP_RP_MSG_ACK_EVEN &&
		    frame->acq_ctrl_byte != MSTRFIP_RP_MSG_ACK_ODD)
			return MSTRFIP_FRAME_BAD_CTRL;
		break;
	case MSTRFIP_PDU_SMMPS_PRES_VAR: /* aperiodic presence var frame expected */
	case MSTRFIP_PDU_SMMPS_ID_VAR: /* aperiodic identification var frame expected */
		if (frame->acq_ctrl_byte != MSTRFIP_RP_DAT)
			return MSTRFIP_FRAME_BAD_CTRL;
		break;
	}
	return MSTRFIP_FRAME_OK; // FRAME_OK from "ctrl_byte" perspective
}

static inline int mstrfip_read_data_regs(uint32_t *buf, int from_regidx,
			int to_regidx, struct rx_frame_ctx *frame, int flg)
{
	int regidx, res;
	uint32_t dummy_val;

	regidx = from_regidx;
	res = 0;
	if (buf != NULL) {
		/* checks PDU_TYPE and bsz if it's a per or aper var frame */
		if (from_regidx == 0 && frame->pdu_type != MSTRFIP_PDU_NONE) {
			/* PER/APER frames: pdu_type and bsz in the first word */
			buf[0] = dp_readl( MASTERFIP_CSR_BASE +
					   MASTERFIP_REG_RX_PAYLD_REG1);
			if ((buf[0] & 0xFF) != frame->pdu_type) /* chk PDU_TYPE */
				res = MSTRFIP_FRAME_BAD_PDU;
			if (((buf[0] >> 8) & 0xFF) != frame->data_bsz )
				res |= MSTRFIP_FRAME_BAD_BSZ;
			/*
			 * TODO: remove from the payload PDU_TYPE + NBYTES bytes.
			 * Unfortunatly buf is pointing to MSQ slot register and
			 * byte adressing doesn't work.
			 */
			++regidx;
		}
		for (; regidx < to_regidx; ++regidx) {
			/*
			 * reads remaining words within the limit of expected
			 * payload size to not overflow buffer
			 */
			if (regidx <= frame->payload_wsz)
				buf[regidx] = dp_readl( MASTERFIP_CSR_BASE +
							MASTERFIP_REG_RX_PAYLD_REG1 + (regidx * 4));
		}
		if (res) // either MSTRFIP_FRAME_BAD_PDU | MSTRFIP_FRAME_BAD_BSZ
			++ba->report.rx_err;
		return res;
	}
	else {
		for (; regidx < to_regidx; ++regidx) {
			dummy_val = dp_readl( MASTERFIP_CSR_BASE + MASTERFIP_REG_RX_PAYLD_REG1
					      + (regidx * 4));
		}
	}
	return MSTRFIP_FRAME_OK;
}

static inline int mstrfip_read_frame_rx_reg(uint32_t *buf,
					    struct rx_frame_ctx *frame, int flg)
{
	int res, from_regidx, to_regidx;
	uint32_t rx_status, curr_nwords;

	res = MSTRFIP_FRAME_OK;
	to_regidx = 0;
	curr_nwords = 0;
	rx_status = 0;

	/*
	 * risk of being stuck in infinite loop in case HW is mulfunctioning.
	 * So we update cycle_state in smem to be able to know that CPU0 is
	 * stuck through get-report on CPU1
	 */
	smem_data->report.cycle_state = MSTRFIP_FSM_WAIT_RX_END;

	do {
		rx_status = dp_readl( MASTERFIP_CSR_BASE + MASTERFIP_REG_RX_STAT);
		if (rx_status & MASTERFIP_RX_STAT_FRAME_OK) {
			++ba->report.rx_ok;
			res |= MSTRFIP_FRAME_OK;
			break;
		}
		if (rx_status & MASTERFIP_RX_STAT_FRAME_CRC_ERR) { /* Frame error */
			++ba->report.rx_err;
			res |= MSTRFIP_FRAME_ERR;
			break;
		}
		if (curr_nwords > frame->payload_wsz) {
			++ba->report.rx_err;
			res |= MSTRFIP_FRAME_BAD_BSZ;
			break;
		}

		if (dp_readl(MASTERFIP_CSR_BASE + MASTERFIP_REG_MACROCYC_TIME_CNT)
		    < ba->hw.tr_ticks) {
			/*
			 * probably corrupted frame: remains turn around time
			 * before next macro cycle. Leave to be ready for next one.
			 */
			return MSTRFIP_FRAME_ERR;
		}

		/* check RX current bytes register:nb of bytes deserialized */
		curr_nwords = dp_readl( MASTERFIP_CSR_BASE +
					MASTERFIP_REG_RX_STAT_CURR_WORD_INDX);
		if (curr_nwords == to_regidx) {/* no new word to read */
			continue;
		}
		from_regidx = to_regidx;
		to_regidx = curr_nwords;
		res |= mstrfip_read_data_regs(buf, from_regidx, to_regidx,
					      frame, flg);
	} while (1);

	/* if no error, check if we have read all the reg data words */
	if (!res) {
		curr_nwords = dp_readl( MASTERFIP_CSR_BASE +
					MASTERFIP_REG_RX_STAT_CURR_WORD_INDX);
		if (curr_nwords > to_regidx) {

			from_regidx = to_regidx;
			to_regidx = curr_nwords;
			res |= mstrfip_read_data_regs(buf, from_regidx,
						      to_regidx, frame, flg);
		}
		frame->acq_payload_bsz =
			(rx_status & MASTERFIP_RX_STAT_BYTES_NUM_MASK) >>
			MASTERFIP_RX_STAT_BYTES_NUM_SHIFT;
	}
	return res;
}

void mstrfip_start_tx(uint16_t bsz)
{
	uint32_t tx_ctrl_val = bsz << 8 | 0x2;
	uint32_t val;

	/*
	 * before sending anything we check if the line is free.
	 * If carrier detect is high that is to say that the line is not free
	 * that is to say some traffic still on going (could be a crasy agent
	 * sending too much bytes), then this error is registered to be able to
	 * diagnose such problem, but the master continue its job even if the
	 * frame will conflict with the on going traffic
	 */
	val = dp_readl( MASTERFIP_CSR_BASE + MASTERFIP_REG_FD);
	if (val & MASTERFIP_FD_CD) { /* fielddrive carrier detect is activated */
		++ba->report.fd_cd;
	}
	/* Write TX start to start frame transmission */
	dp_writel(0x01, MASTERFIP_CSR_BASE + MASTERFIP_REG_TX_CTRL);  // rst tx
	dp_writel(tx_ctrl_val, MASTERFIP_CSR_BASE + MASTERFIP_REG_TX_CTRL);
	// reset RX to be ready to receive a reply
	dp_writel(0x01, MASTERFIP_CSR_BASE + MASTERFIP_REG_RX_CTRL);  // rst rx
}


int mstrfip_handle_rx_frame(int hmq_slot, struct rx_frame_ctx *frame,
			    int flg)
{
	uint32_t *payload_ptr;
	uint32_t mcycle_time;
	int res;

	/*
	 * hmq slot undefined means that the received frame is processed in
	 * order to consume all the bytes but the no one cares about the
	 * payload. This is typically the case of a presence frame or RP_FIN
	 * frame for example
	 */
	if (hmq_slot == MSTRFIP_HMQ_O_UNDEFINED) {
		/* no payload to push in the HMQ */
		payload_ptr = NULL;
	}
	else { /* we care about the payload */
		/* prepare to slot queue to receive a new data entry */
		mstrfip_hmq_append_data_entry(hmq_slot, frame->payload_wsz,
					      frame->payload_bsz, frame->key, &payload_ptr,
					      flg);
	}
	mcycle_time = 0; //to stop compiler complain
	frame->acq_ctrl_byte = MSTRFIP_RP_UNKNOWN; /* reset previuos control byte */
	res = mstrfip_wait_preamb(frame);
	if (smem_data->record.cfg.flg) {
		mcycle_time = ba->cycle.nticks - dp_readl(MASTERFIP_CSR_BASE +
							  MASTERFIP_REG_MACROCYC_TIME_CNT);
		if (res == MSTRFIP_FRAME_OK) {
			/* substract time for preamb (2bytes)*/
			mcycle_time -= ba->hw.byte_ticks * 2;
		}
	}
	if (res == MSTRFIP_FRAME_OK) {
		res = mstrfip_get_ctrl_byte(frame, flg); /* get ctrl_byte */
		if (res == MSTRFIP_FRAME_OK) {
			res = mstrfip_read_frame_rx_reg(payload_ptr, frame, flg);
			res |= mstrfip_chk_frame(payload_ptr, frame, res);
		}
	}
	if (hmq_slot != MSTRFIP_HMQ_O_UNDEFINED) {
		if((res != MSTRFIP_FRAME_OK)) {
			/*
			 * An error has been detected: it's not a valid frame or
			 * not the expected one in terms of PDU or CTRL type:
			 * data is invalid, overwrite data entry size and set
			 * error.
			 */
			mstrfip_hmq_invalidate_last_data_entry(hmq_slot,
							       frame->payload_wsz, res, flg);
		}
		else {
			/* Check if it's an aperiodi message frame */
			if (frame->pdu_type == MSTRFIP_PDU_NONE) {
				/* set proper payload size */
				mstrfip_hmq_set_last_data_entry_payload_sz(
					hmq_slot, frame, flg);
			}
		}
	}
	if (smem_data->record.cfg.flg) {
		mstrfip_record(mcycle_time, 0, ba->report.cycle_state,
			       frame->acq_ctrl_byte, frame->var_id,
			       res, MSTRFIP_EVTREC_DIR_CONS);
	}
	return res;
}

#define MASTERFIP_REG_TX_COUNT 66
void mstrfip_write_frame_tx_reg(uint32_t *buf, int ctrl_byte, int nwords)
{
	int i;

//	dp_writel(0x01, MASTERFIP_CSR_BASE + MASTERFIP_REG_TX_CTRL);  // rst tx
	dp_writel(ctrl_byte, MASTERFIP_CSR_BASE + MASTERFIP_REG_TX_PAYLD_CTRL);   // TX ctrl byte

#ifdef DBG_EMUL_LONG_PAYLOAD
	nwords = 32; /* long payload emulation: forces 32 words */
#endif
	/*
	 * nwords cannot exceed max number of tx registers defined in the HW
	 * This should never happen, but it's not forbidden just to emulate
	 * abnormal payload size
	 */
	nwords = (nwords > MASTERFIP_REG_TX_COUNT) ? MASTERFIP_REG_TX_COUNT : nwords;
	for (i = 0; i < nwords; ++i)
		dp_writel(buf[i], MASTERFIP_CSR_BASE +
			  MASTERFIP_REG_TX_PAYLD_REG1 + i*4);
}

/* Send a question frame; ID_DAT, ID_MSG, ID_RQ2 */
void mstrfip_write_question_tx_reg(uint32_t question, uint32_t varid)
{
	uint32_t swapped_varid;

//	dp_writel(0x01, MASTERFIP_CSR_BASE + MASTERFIP_REG_TX_CTRL);  // rst tx
	dp_writel(question, MASTERFIP_CSR_BASE + MASTERFIP_REG_TX_PAYLD_CTRL);   // TX ctrl byte
	// TX 1st data register for writing the payload of the frame */
	// varid bytes swap: var(byte1) must be serialized first followed by agt addr(byte0)
	swapped_varid = ((varid & 0xFF) << 8) | ((varid & 0xFF00) >> 8);
	dp_writel(swapped_varid, MASTERFIP_CSR_BASE + MASTERFIP_REG_TX_PAYLD_REG1); // varid 2 bytes
}

/* Send a RP_FIN frame to finalize aperiodic message transaction */
inline void mstrfip_write_msg_rpfin_tx_reg()
{
	/* RP_FIN frame doesn't have a payload just a control byte */
//	dp_writel(0x01, MASTERFIP_CSR_BASE + MASTERFIP_REG_TX_CTRL);  // rst tx
	dp_writel(MSTRFIP_RP_FIN, MASTERFIP_CSR_BASE + MASTERFIP_REG_TX_PAYLD_CTRL); //TX ctrl byte
}

/* Send a RP_ACK frame to ack reception of an aperiodic message transaction */
inline void mstrfip_write_msg_rpack_tx_reg(uint32_t ctrl_byte)
{
	/* RP_ACK frame doesn't have a payload just a control byte */
//	dp_writel(0x01, MASTERFIP_CSR_BASE + MASTERFIP_REG_TX_CTRL);  // rst tx
	if (ctrl_byte == MSTRFIP_RP_MSG_ACK_EVEN)
		dp_writel(MSTRFIP_RP_ACK_EVEN,
			  MASTERFIP_CSR_BASE + MASTERFIP_REG_TX_PAYLD_CTRL); //TX ctrl byte
	else
		dp_writel(MSTRFIP_RP_ACK_ODD,
			  MASTERFIP_CSR_BASE + MASTERFIP_REG_TX_PAYLD_CTRL); //TX ctrl byte
}

void mstrfip_record(uint32_t mcycle_time,
		    uint32_t new_mcycle, uint32_t wind_type,
		    uint32_t ctrl_type, uint32_t var_id,
		    uint32_t status, uint32_t dir)
{
	uint32_t idx, evt_desc, st;

	if ((smem_data->record.cfg.nentries == MSTRFIP_MAX_RECORD_COUNT) ||
	    (smem_data->record.cfg.nentries == smem_data->record.cfg.req_nentries)) {
		/* reset record flag and record filters */
		smem_data->record.cfg.flg = 0;
		smem_data->record.cfg.ctrltype_trigger = 0x0;
		smem_data->record.cfg.varid_trigger = 0x0;
		return;
	}
	/*
	 * check if the event matches recording filters in order to start
	 * recording
	 */
	/*
	 * in record, status can be only OK, TMO or ERR: so convert status into
	 * a status compatible with recording trigger.
	 */
	st = MSTRFIP_EVTREC_STATUS_ERR;
	if (status == MSTRFIP_FRAME_TMO)
		st = MSTRFIP_EVTREC_STATUS_TMO;
	else if (status == MSTRFIP_FRAME_OK)
		st = MSTRFIP_EVTREC_STATUS_OK;
	if (smem_data->record.cfg.nentries == 0) {
		if ((smem_data->record.cfg.ctrltype_trigger) &&
		    (ctrl_type != smem_data->record.cfg.ctrltype_trigger))
			return;
		if ((smem_data->record.cfg.varid_trigger) &&
		    (var_id != smem_data->record.cfg.varid_trigger))
			return;
		if ((smem_data->record.cfg.status_trigger) &&
		    (st != smem_data->record.cfg.status_trigger))
			return;
	}

	idx = smem_data->record.cfg.nentries;
	smem_data->record.buf[idx].time = mcycle_time;
	evt_desc = var_id << MSTRFIP_EVTREC_VARID_SHIFT;
	evt_desc |= (ctrl_type << MSTRFIP_EVTREC_CTRLBYTE_SHIFT);
	evt_desc |= (new_mcycle << MSTRFIP_EVTREC_MCYCLE_SHIFT);
	evt_desc |= (wind_type << MSTRFIP_EVTREC_WIND_SHIFT);
	evt_desc |= (st << MSTRFIP_EVTREC_STATUS_SHIFT);
	evt_desc |= dir;
	smem_data->record.buf[idx].evt_desc = evt_desc;
	++smem_data->record.cfg.nentries;
}
