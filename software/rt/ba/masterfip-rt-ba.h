/*
 * Copyright (C) 2016-2017 CERN (www.cern.ch)
 * Author: Michel Arruat <michel.arruat@cern.ch>
 *
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#ifndef __MASTERFIP_BA_H
#define __MASTERFIP_BA_H

#include "masterfip-common.h"
#include "masterfip-common-priv.h"
#include "masterfip-mq.h"

#define MSTRFIP_REPORT_RX_TMO_ERR 1

// max size in word available to insert data entries in a single trtl message
#define MSTRFIP_DATA_ENTRY_WSZ								\
	TRTLMSG_MAX_PAYLOAD_WSZ - MSTRFIP_IRQ_ENTRY_DESC_WSZ

#define MSTRFIP_BSS_PARTITION_BAINSTR 1
#define MSTRFIP_BSS_PARTITION_PERVAR 2
#define MSTRFIP_BSS_PARTITION_APERMSG 3

#define MSTRFIP_PRESENCE_VAR 0x14
#define MSTRFIP_IDENTIFICATION_VAR 0x10
#define MSTRFIP_PADDING_ID_DAT 0x9080

#define MSTRFIP_MACROCYC_ABORT 1

/* PDU (Protocol Data Unit) types */
enum mstrfip_frame_pdu_type {
	MSTRFIP_PDU_NONE = 0x0, /* aper message doesn't have pdu type */
	MSTRFIP_PDU_MPS_VAR = 0x40, /* MPS variables pdu type */
	MSTRFIP_PDU_SMMPS_PRES_VAR = 0x50, /* SMMPS presence variables pdu type */
	MSTRFIP_PDU_SMMPS_ID_VAR = 0x52, /* SMMPS Identification variable pdu type */
};

/*
 *
 */
struct hmq_slot_ctx {
	struct trtl_fw_msg obuf; /* mapped to the appropriate hmq slot */
	uint32_t slot_msg_count; /* number of trtl msg sent since the last irq */
};

struct cons_msg_fifo {
	uint32_t entries[10]; /* entries:set of pointers to each payload in fifo */
	uint32_t entries_sz; /* size of the entries table */
	uint32_t entries_count; /* current number of entries */
	uint32_t r_idx; /* entries read index: pointer to get the payload */
	uint32_t w_idx; /* entries write index: pointer to write the payload */
};

struct rx_frame_ctx {
	uint32_t pdu_type;	/* PDU-type byte */
	uint32_t irq_flag;	/* */
	/* tmo on presence var should not be considered as an error */
	uint32_t report_tmo;	/* tells if tmo is counted into rx_err */
	/*
	 * Periodic variable address:
	 * Useful only for RP_DAT_MSG: an agent replying to an ID_DAT with a
	 * RP_DAT_MSG, request from the master to schedule as soon as possible
	 * an ID_MSG transaction. Therefore the variable id used for the ID_DAT
	 * is kept to be able to build the right ID_MSG later on.
	 */
	uint32_t var_id;
	/* key used to locate data structures by the upper layers */
	uint32_t key;
	uint32_t data_bsz; /* user data size in bytes */
	uint32_t payload_bsz; /* frame payload size in bytes */
	uint32_t payload_wsz; /* frame payload size in words*/
	uint32_t acq_ctrl_byte; /* control byte read from HW */
	/*
	 * payload size read from HW: payload messages size is dynamic.
	 * The payload size is given by the HW.
	 */
	uint32_t acq_payload_bsz;
};

/*
 * Identification variable context: during an aperiodic window, we check in the
 * SMEM if there is some pending request for scheduling ident var. In case of
 * demand, the the request is copied locally in this structure, and the system
 * starts to serve the demand. Once it's done an irq is pushed in order to
 * notify the client. As it is an activity schedule during an aperiodic window,
 * we might need several macro cycle to serve the full demand, and then we need
 * to keep some indexes like row, col, count,...
 */
struct ba_ident_var_ctx {
	uint32_t state[MSTRFIP_IDENT_REQUEST_WSIZE];
	uint32_t row; /* row idx in state array */
	uint32_t col; /* col idx in state array */
	uint32_t request; /* raised till the request is served */
	uint32_t app_count; /* count app ident vars */
	uint32_t diag_count; /* count diag ident vars */
};

struct ba_var_cfg {
	uint32_t nhdrs;
	struct mstrfip_data_hdr *hdrs;/* points to the var hdr list */
};

/*
 * Data object(FIFO) to register agents requesting to send aperiodic messages.
 * During the periodic window an agent can express its need of sending an
 * aperiodic message by replying to an ID_DAT a RP_MSG (instead of a RP_DAT).
 * The var-id is registered, in order to schedule later an ID_MSG during
 * a next aperiodic msg window if there is enough time.
 * So basically this data object acts as a FIFO used to store/consume agent's
 * request for aperiodic message. The size of the FIFO is defined by an
 * attribute of the aperiodic message window.
 */
struct ba_consmsg_cfg {
	uint32_t r_idx; /* entries read index: pointer to consume request */
	uint32_t w_idx; /* entries write index: pointer to register request */
	uint32_t entries_sz; /* max number of entries in FIFO  */
	uint32_t entries_count; /* current number of entries in FIFO */
	uint32_t *entries; /* entries: pointer to the FIFO */
};

struct ba_cycle_cfg {
	uint32_t nticks; /* cycle duration in cpu ticks */
	uint32_t ninstr;
	struct mstrfip_ba_instr *instr; /* points to the ba instr list */
};

/*
 * couple of constants computed once.
 */
struct ba_hw_cfg {
	uint32_t ext_trig; /* use external trigger if set */
	/*
	 * free running allows a system configured with external trigger to
	 * continue running even if the trigger disappear.
	 */
	uint32_t freerun; /* allow free running if set */
	uint32_t tr_ticks; /* turn around time in cpu ticks */
	uint32_t s_ticks; /* silence time in cpu ticks */
	uint32_t byte_ticks; /* TX time for a byte in cpu ticks */
	uint32_t idfr_ticks; /* TX time for an ID frame in cpu ticks */
	uint32_t rpfin_ticks; /* TX time for a RP_FIN frame in cpu ticks */
	uint32_t mstr_pres_rpdat[2]; /* master presence var response */
};

/*
 * Defines ba layout data resident in the BSS space
 */
struct bss_ba_cfg {
	uint32_t wsz; /* curent size in word */
	struct ba_var_cfg var; /* all about  periodic var */
	struct ba_consmsg_cfg msg; /* all about  consumed aperiodic message */
	struct ba_cycle_cfg cycle; /* all about macro cycle instruction set */
	struct ba_hw_cfg hw; /* all about hardware parameters */
	struct ba_ident_var_ctx ident_ctx; /* runtime ident var context */
	/* runtime HMQ slot context */
	struct hmq_slot_ctx slot_ctx[MSTRFIP_HMQ_ACQ_SLOT_COUNT];
	struct mstrfip_report report;
	uint32_t mstrfip_irq_count;
	uint32_t led_reg; /* led register cache value */
	/*
 	 * next_presence_iddat: presence traffic is done only if there is
	 * available time in aperiodic window. this field is the next presence
	 * var to schedule on the bus.
 	 */
	uint16_t next_pres_iddat;
};

/*
 * This array contains delays expressed in cpu clock and computed once at the
 * startup.
 */
extern uint32_t delays_in_cpu_ticks[];
#define IDX_300NS_DELAY 0 /* 300ns delay at index 0 */
#define IDX_500NS_DELAY 1 /* 500ns delay at index 1 */
#define IDX_1US_DELAY 2   /* 1us delay at index 2 */
#define IDX_5US_DELAY 3   /* 5us delay at index 3 */

/*
 * BA data is located into the bss section. It is initialized and
 * partitioned at run time according to the ba programming.
 * This number is the remaining space taking in account the code segment size.
 * If the code segment increases, the linker will warn that this configuration
 * doesn't fit anymore in the RAM space. Then it's a matter of compromise:
 * reduce the bss remaining space (if it is compatible with the most demanding
 * FIP application), or increase ram size for CPU1 (if enough space in
 * gateware).
 * At run time the system checks that all the configuration can fit into the bss
 * remaining space.
 */
#define MSTRFIP_BSS_REMAINING_WSZ 20000

extern struct bss_ba_cfg *ba;
extern struct smem_data_cfg volatile *smem_data;

/* Defined in masterfip-rt-frames.c */
extern int mstrfip_wait_end_of_tx(uint32_t bsz);
extern int mstrfip_handle_rx_frame(int hmq_slot, struct rx_frame_ctx *frame,
								   int flg);
extern void mstrfip_write_frame_tx_reg(uint32_t *buf, int ctrl_byte, int nwords);
extern void mstrfip_write_question_tx_reg(uint32_t question, uint32_t varid);
extern void mstrfip_write_msg_rpack_tx_reg(uint32_t ctrl_byte);
extern void mstrfip_write_msg_rpfin_tx_reg();
extern void mstrfip_start_tx(uint16_t bsz);
extern void mstrfip_record(uint32_t mcycle_time, uint32_t new_mcycle,
						   uint32_t wind_type, uint32_t ctrl_type,
						   uint32_t var_id, uint32_t status, uint32_t dir);

/* Defined in masterfip-rt-hmq.c */
extern void mstrfip_hmq_claim_out_buf(int hmq_slot, struct trtl_fw_msg* msg);
extern void mstrfip_hmq_claim_in_buf(int hmq_slot, struct trtl_fw_msg* msg);
extern void mstrfip_hmq_set_last_data_entry_payload_sz(int hmq_slot,
													   struct rx_frame_ctx *frame, int flg);
extern void mstrfip_hmq_invalidate_last_data_entry(int hmq_slot,
												   uint32_t nwords, uint32_t err, int flg);
extern void mstrfip_hmq_append_data_entry(int hmq_slot, uint32_t nwords,
										  uint32_t bsz, uint32_t key, uint32_t **payload_ptr, int flg);
extern void mstrfip_hmq_post_irq(int hmq_slot, uint32_t var_key,
								 uint32_t var_flags, int flg);

/* Defined in masterfip-rt-timers.c */
extern void mstrfip_start_cycle_counter();
extern int mstrfip_wait_macrocyc_counter();
extern void mstrfip_start_scounter(int flg);
extern void mstrfip_start_trcounter();
extern void mstrfip_load_trtime_counter();
extern void mstrfip_load_stime_counter();
extern void mstrfip_wait_trtime();
extern void mstrfip_wait_stime();
extern void mstrfip_wait_and_rearm_tr(int macrocyc_counter_ticks);

/* Defined in masterfip-rt-apervar.c */
extern void mstrfip_do_ba_aper_var_wind(int new_cycle, uint32_t time_ticks);
extern void mstrfip_response_time(uint32_t from_agt_addr, uint32_t to_agt_addr,
								  volatile uint32_t *buf);

/* Defined in masterfip-rt-pervar.c */
extern void mstrfip_do_ba_per_var_wind(int new_cycle,
									   int start_idx, int stop_idx);

/* Defined in masterfip-rt-apermsg.c */
extern void mstrfip_do_ba_aper_msg_wind(int new_cycle,
										struct mstrfip_ba_aper_msg_wind_args *args);

/* Defined in masterfip-rt-wait.c */
extern void mstrfip_do_ba_wait_wind(int new_cycle, int last_instr,
									uint32_t silent_flg, uint32_t time_ticks);

#endif /* __MASTERFIP_BA_H */
