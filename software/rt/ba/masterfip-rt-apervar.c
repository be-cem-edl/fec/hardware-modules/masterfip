/*
 * Copyright (C) 2016-2017 CERN (www.cern.ch)
 * Author: Michel Arruat <michel.arruat@cern.ch>
 *
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#include <mockturtle-rt.h>

#include "masterfip-smem.h"
#include "hw/masterfip_hw.h"
#include "masterfip-rt-ba.h"

//static inline void delta_time(uint32_t start_sec, uint32_t start_cycles,
//			      uint32_t end_sec, uint32_t end_cycles,
//			      uint32_t *sec, uint32_t *ns)
//{
//	uint32_t cpu_freq_hz = trtl_config_rom_get()->clock_freq;
//
//	if ((end_cycles > start_cycles) && (start_sec == end_sec)) {
//		*ns = ticks_to_ns(end_cycles - start_cycles, cpu_freq_hz);
//		*sec = 0;
//	}
//	else {
//		*ns = ticks_to_ns(cpu_freq_hz - start_cycles + end_cycles,
//				  cpu_freq_hz);
//		*sec = end_sec - start_sec -1;
//	}
//}

/*
 * Returns 0 if end time is reached, else 1 meaning remains some time to do
 * more tasks
 */
static int send_ident_var(int new_cycle, uint32_t now_ticks, uint32_t end_ticks,
			  int *remains_time)
{
	int addr, iddat;
	uint32_t slot, mcycle_time;
	struct rx_frame_ctx frame;

	*remains_time = 0; // to be safe
	for (; ba->ident_ctx.row < MSTRFIP_IDENT_REQUEST_WSIZE;
	     ++ba->ident_ctx.row) {
		if (! ba->ident_ctx.state[ba->ident_ctx.row])
			continue; //skip:no ident request for corresponding agts
		for (; ba->ident_ctx.col < 32; ++ba->ident_ctx.col) {
			if (!(ba->ident_ctx.state[ba->ident_ctx.row] &
			      (1 << ba->ident_ctx.col))) /* bit not set */
				continue;

			addr = (ba->ident_ctx.row * 32) +
				ba->ident_ctx.col;
			iddat = (MSTRFIP_IDENTIFICATION_VAR << 8) | addr; // agent ident var
			/* fip diag ident var is routed to fip diag slot */
			if (addr == MSTRFIP_DIAG_ADDR) {
				slot = MSTRFIP_HMQ_O_DIAG_APER_VAR;
				++ba->ident_ctx.diag_count;
			}
			else {
				slot = MSTRFIP_HMQ_O_APP_APER_VAR;
				++ba->ident_ctx.app_count;
			}
			mstrfip_write_question_tx_reg(MSTRFIP_ID_DAT, iddat);

			if (new_cycle) {
				if (mstrfip_wait_macrocyc_counter() ==
				    MSTRFIP_MACROCYC_ABORT)
					return MSTRFIP_MACROCYC_ABORT;
				new_cycle = 0; /* for next iteration */
			}
			else {
				mstrfip_wait_trtime();
			}

			mstrfip_start_tx(2);
			if (smem_data->record.cfg.flg) {
				mcycle_time = ba->cycle.nticks -
					dp_readl(MASTERFIP_CSR_BASE +
						 MASTERFIP_REG_MACROCYC_TIME_CNT);
				mstrfip_record(mcycle_time, new_cycle,
				       	       ba->report.cycle_state,
					       MSTRFIP_ID_DAT, iddat, 0,
					       MSTRFIP_EVTREC_DIR_PROD);
			}
			mstrfip_wait_end_of_tx(2);
			/* ident var served, reset corresponding bit */
			ba->ident_ctx.state[ba->ident_ctx.row] &=
				(~(1 << ba->ident_ctx.col));
			frame.data_bsz = MSTRFIP_IDENT_VAR_DATA_BSZ;
			frame.payload_bsz = frame.data_bsz +
				MSTRFIP_VAR_PAYLOAD_HDR_BSZ;
			frame.payload_wsz =
				MSTRFIP_IDENT_VAR_PAYLOAD_NWORDS;
			frame.var_id = iddat;
			frame.key = addr; //agent addr for ident var
			frame.pdu_type = MSTRFIP_PDU_SMMPS_ID_VAR;
			frame.report_tmo = 1;
			mstrfip_handle_rx_frame(slot, &frame, 0);
			now_ticks = dp_readl( MASTERFIP_CSR_BASE +
					      MASTERFIP_REG_MACROCYC_TIME_CNT);
			if (now_ticks <= end_ticks) {
				goto end; /* time to leave loops */
			}
		}
		ba->ident_ctx.col = 0; // reset col idx for the next line
	}
	ba->ident_ctx.request = 0; /* all the ident request have been served */
	*remains_time = 1; /* remains some time to accomplish more task */

end:
	if (!ba->ident_ctx.request) { // fully served, time to push data
		if (ba->ident_ctx.app_count)
			mstrfip_hmq_post_irq(MSTRFIP_HMQ_O_APP_APER_VAR, 0,
					     MSTRFIP_DATA_FLAGS_CONS, 0);
		if (ba->ident_ctx.diag_count)
			mstrfip_hmq_post_irq(MSTRFIP_HMQ_O_DIAG_APER_VAR, 0,
					     MSTRFIP_DATA_FLAGS_CONS, 0);
	}
	return 0;
}

static int get_presence_frame(uint32_t pres_iddat)
{
	struct rx_frame_ctx frame;
	int res;

	frame.data_bsz = MSTRFIP_PRESENCE_VAR_DATA_BSZ;
	frame.payload_bsz = frame.data_bsz +
		MSTRFIP_VAR_PAYLOAD_HDR_BSZ;
	frame.payload_wsz = MSTRFIP_PRESENCE_VAR_PAYLOAD_NWORDS;
	frame.var_id = pres_iddat;
	frame.key = pres_iddat;
	frame.pdu_type = MSTRFIP_PDU_SMMPS_ID_VAR;
	/* tmo is not counted as en rx_err for presence var */
	frame.report_tmo = 0;
	/* get frame but no data to push */
	res = mstrfip_handle_rx_frame(MSTRFIP_HMQ_O_UNDEFINED,
				      &frame, 0);
	return res;
}

static int send_presence_var(int new_cycle, uint32_t now_ticks,
			     uint32_t end_ticks)
{
	int res, j, agent_addr, bsz;
	uint32_t mcycle_time;
	uint32_t volatile *presence_request =
		smem_data->aper_var.presence_request;

	mstrfip_write_question_tx_reg(MSTRFIP_ID_DAT, ba->next_pres_iddat);

	if (new_cycle) {
		if (mstrfip_wait_macrocyc_counter() == MSTRFIP_MACROCYC_ABORT)
			return MSTRFIP_MACROCYC_ABORT;
	}
	else
		mstrfip_wait_trtime();

	while (now_ticks > end_ticks ) {
		/*
		 * bsz = 2 for an ID_DAT. Frame structure
		 * |identifier (2bytes)|
		 */
		mstrfip_start_tx(2);
		if (smem_data->record.cfg.flg) {
			mcycle_time = ba->cycle.nticks -
				dp_readl(MASTERFIP_CSR_BASE +
					 MASTERFIP_REG_MACROCYC_TIME_CNT);
			mstrfip_record(mcycle_time, new_cycle,
				       ba->report.cycle_state,
				       MSTRFIP_ID_DAT,
				       ba->next_pres_iddat, 0,
				       MSTRFIP_EVTREC_DIR_PROD);
		}
		mstrfip_wait_end_of_tx(2);
		new_cycle = 0; /* not anymore a new cycle */
		agent_addr = ba->next_pres_iddat & 0xFF;
		if (agent_addr == 0) { // presence var for the master
			/*
			 * master produces its RP_DAT: hard coded according to
			 * what has been observed with the current FIP
			 */
			mstrfip_write_frame_tx_reg(ba->hw.mstr_pres_rpdat,
						   MSTRFIP_RP_DAT, 2);
			mstrfip_wait_trtime();
			/*
			 * Frame presence var structure
			 * |PDU|length|byte0|byte1|..|byten|
			 */
			bsz = MSTRFIP_PRESENCE_VAR_DATA_BSZ +
				MSTRFIP_VAR_PAYLOAD_HDR_BSZ;
			mstrfip_start_tx(bsz);
			if (smem_data->record.cfg.flg) {
				mcycle_time = ba->cycle.nticks -
					dp_readl(MASTERFIP_CSR_BASE +
						 MASTERFIP_REG_MACROCYC_TIME_CNT);
				mstrfip_record(mcycle_time, 0, //new_cycle cannot start with RP_DAT
					       ba->report.cycle_state,
					       MSTRFIP_RP_DAT, 0x0, /* master var id */
					       0,MSTRFIP_EVTREC_DIR_PROD);
			}
			mstrfip_wait_end_of_tx(bsz);
			ba->next_pres_iddat += 1;
			mstrfip_write_question_tx_reg(MSTRFIP_ID_DAT,
						      ba->next_pres_iddat);
			presence_request[0] |= 1; // master is present
		}
		else {
			res = get_presence_frame(ba->next_pres_iddat);
			j = agent_addr / 32;
			if (res == MSTRFIP_FRAME_OK)
			{
				presence_request[j] |= (1 << (agent_addr%32));
			}
			else
				presence_request[j] &= (~(1 << (agent_addr%32)));
			/* prepare TX reg with the next question */
			if (agent_addr == 0xFF)
				// 0x1: first agent, skip master presence
				ba->next_pres_iddat = (MSTRFIP_PRESENCE_VAR << 8) | 0x1;
			else
				ba->next_pres_iddat += 1;
			mstrfip_write_question_tx_reg(MSTRFIP_ID_DAT,
						      ba->next_pres_iddat);
		}
		now_ticks = dp_readl( MASTERFIP_CSR_BASE +
				      MASTERFIP_REG_MACROCYC_TIME_CNT);
		mstrfip_wait_trtime();
	}
	return 0;
}

/*
 * In order to determine accurately the end of the aperiodic window, it always
 * finishes by a bourrage frame, because a bourrage frame is fully controlled
 * by the master while expecting an RP_DAT depends of the real response time
 * of the agent (turn around time + propagation time).
 * The bourrage frame is a deterministic transaction.
 */
inline void mstrfip_do_ba_aper_var_wind(int new_cycle, uint32_t time_ticks)
{
	struct smem_aper_var volatile *aper_var = &smem_data->aper_var;
	/*
	 * macro cycle counter is counting down: end_ticks gives the
	 * counter value when the aper var window ends.
	 */
	uint32_t end_ticks = ba->cycle.nticks - time_ticks;
	/*
	 * time required  to send an ultimate aper var transaction
	 * followed by a bourrage frame
	 */
	uint32_t end_margin_ticks = 2 * (ba->hw.idfr_ticks + ba->hw.s_ticks);
	uint32_t now_ticks;
	int remains_time, i;

	ba->report.cycle_state = MSTRFIP_FSM_RUN_APER_VAR;
	/* if there is not ongoing
	 * check from the SMEM if one has requested identification traffic */
	if (!ba->ident_ctx.request && aper_var->ident_req) {
		rt_smem_lock(&smem_data->smem_mtx);
		for (i = 0; i < MSTRFIP_IDENT_REQUEST_WSIZE; ++i) {
			ba->ident_ctx.state[i] = aper_var->ident_request[i];
		}
		aper_var->ident_req = 0; /* ack the request */
		rt_smem_unlock(&smem_data->smem_mtx);
		/* new demand for identification: reset properly row & col */
		ba->ident_ctx.request = 1;
		ba->ident_ctx.row = 0;
		ba->ident_ctx.col= 1; /* skip ident var of the master 0x0010 */
	}

	if (new_cycle)
		/* we are still in the last turn around time of the previous cycle */
		now_ticks = ba->cycle.nticks;
	else /* get where we are in the current cycle */
		now_ticks = dp_readl( MASTERFIP_CSR_BASE +
				      MASTERFIP_REG_MACROCYC_TIME_CNT);
	remains_time = 1;
	if (ba->ident_ctx.request) {
		if (send_ident_var(new_cycle, now_ticks,
				   end_margin_ticks + end_ticks,
				   &remains_time) == MSTRFIP_MACROCYC_ABORT)
			return;
		new_cycle = 0; /* not anymore the beginning of a new cycle*/
		if (remains_time)
			now_ticks = dp_readl(MASTERFIP_CSR_BASE +
					     MASTERFIP_REG_MACROCYC_TIME_CNT);
	}
	if (remains_time)
		if (send_presence_var(new_cycle, now_ticks,
				      end_margin_ticks + end_ticks) ==
		    MSTRFIP_MACROCYC_ABORT)
			return;
	/*
	 * send a single bourrage frame (fully controlled by the master no
	 * answer form any agent expected.
	 */
	mstrfip_write_question_tx_reg(MSTRFIP_ID_DAT, MSTRFIP_PADDING_ID_DAT);
	mstrfip_wait_trtime();
	mstrfip_start_tx(2);
	mstrfip_wait_end_of_tx(2);

	/*
	 * wait end of apervar-window keeping a margin of tr time + 50ticks
	 * in order to prepare next transaction and rearm tr counter
	 */
	mstrfip_wait_and_rearm_tr(end_ticks + ba->hw.tr_ticks +
				  delays_in_cpu_ticks[IDX_500NS_DELAY]);
}

inline void mstrfip_response_time(uint32_t from_agt_addr, uint32_t to_agt_addr,
				  volatile uint32_t *buf)
{
	uint32_t next_pres_iddat = (MSTRFIP_PRESENCE_VAR << 8);
	uint32_t pres_rpdat_tickstime = 13 * ba->hw.byte_ticks;
	uint32_t mcycle_tickstime_end, mcycle_tickstime_start;
	int i, res, j;

	next_pres_iddat += from_agt_addr;
	mstrfip_write_question_tx_reg(MSTRFIP_ID_DAT, next_pres_iddat);
	for (i = from_agt_addr, j = 0; i <= to_agt_addr; ++i, ++j) {
		mstrfip_start_tx(2);
		mstrfip_wait_end_of_tx(2);
		mcycle_tickstime_start = ba->cycle.nticks - dp_readl(MASTERFIP_CSR_BASE +
								     MASTERFIP_REG_MACROCYC_TIME_CNT);
		res = get_presence_frame(next_pres_iddat);
		mcycle_tickstime_end = ba->cycle.nticks - dp_readl(MASTERFIP_CSR_BASE +
								   MASTERFIP_REG_MACROCYC_TIME_CNT);
		if (res == MSTRFIP_FRAME_OK)
			buf[j] = ticks_to_ns(mcycle_tickstime_end -
					     mcycle_tickstime_start -
					     pres_rpdat_tickstime,
					     trtl_config_rom_get()->clock_freq);
		else
			buf[j] = 0; /* No response time means TMO */
		/* prepare tx register with next question */
		next_pres_iddat += 1;
		mstrfip_write_question_tx_reg(MSTRFIP_ID_DAT,
					      next_pres_iddat);
		mstrfip_wait_trtime();
	}
}
