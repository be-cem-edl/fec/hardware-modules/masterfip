/*
 * Copyright (C) 2016-2017 CERN (www.cern.ch)
 * Author: Michel Arruat <michel.arruat@cern.ch>
 *
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#include <string.h>

#include <mockturtle-rt.h>

#include "masterfip-smem.h"
#include "hw/masterfip_hw.h"
#include "masterfip-rt-ba.h"
#include "masterfip-mq.h"

struct smem_data_cfg volatile *smem_data; /* pointer to the smem data */
/*
 * buffer in bss region: the dynamic ba configuration and runtime data
 * is mapped into this buffer at run time.
 */
static uint32_t bss_buf[MSTRFIP_BSS_REMAINING_WSZ];
struct bss_ba_cfg *ba; /* pointer to the bss_buf */

uint32_t delays_in_cpu_ticks[4];
#ifdef RTDEBUG_BA
static int trace_count;
#endif

static void do_init()
{
	uint32_t cpu_hz = trtl_config_rom_get()->clock_freq;

	delays_in_cpu_ticks[IDX_300NS_DELAY] = ns_to_ticks(300, cpu_hz);
	delays_in_cpu_ticks[IDX_500NS_DELAY] = ns_to_ticks(500, cpu_hz);
	delays_in_cpu_ticks[IDX_1US_DELAY] = us_to_ticks(1, cpu_hz);
	delays_in_cpu_ticks[IDX_5US_DELAY] = us_to_ticks(5, cpu_hz);
}

static void hw_mq_purge()
{
	mq_purge(MFIP_MQ, MSTRFIP_HMQ_O_APP_PER_VAR);
	mq_purge(MFIP_MQ, MSTRFIP_HMQ_O_APP_APER_VAR);
	mq_purge(MFIP_MQ, MSTRFIP_HMQ_O_APP_APER_MSG);
	mq_purge(MFIP_MQ, MSTRFIP_HMQ_O_DIAG_PER_VAR);
	mq_purge(MFIP_MQ, MSTRFIP_HMQ_O_DIAG_APER_VAR);
	mq_purge(MFIP_MQ, MSTRFIP_HMQ_IO_CPU0_CMD);
}

/*
 * best control voltage range for the quartz oscillator to minimize drift:
 * should be 1.6V according to the oscillator data sheet
 */
static void do_hw_reset()
{
	// purge hmq to cancel any remaining messages from previous running
	hw_mq_purge();

	/*
	 * We found that when the system re-starts an ID_DAT is sent, apparently
	 * the one remainning in the TX buffer from the previous run.
	 * The reset was supposed to clean evrything but apparently not.
	 * To eliminate this parasite frame, we explicitly reset TX buffer
	 */
	dp_writel(0x01, MASTERFIP_CSR_BASE + MASTERFIP_REG_TX_CTRL);  // rst tx
	/* Clear leds */
	dp_writel(0x0, MASTERFIP_CSR_BASE + MASTERFIP_REG_LED);
}

static void do_reset()
{
	int i;
	struct mstrfip_acq_trtlmsg_desc *acq_trtlmsg;

	do_hw_reset();

	pp_printf("do_reset\n");
	/*
	 * initialize the local pointer to the shared memory.
	 * The smem is not reseted here because in case of internal reset due
	 * to software exception like (bad address, or dividing by zero,...) we
	 * want to keep untouched the content of smem registers for analysis
	 * using diad tool. Only the ba state is updated showing that the mock
	 * turtle has been reseted.
	 */
	smem_data = (struct smem_data_cfg volatile *)smem_buf;
	smem_data->report.ba_state = MSTRFIP_FSM_INITIAL;

	/* initialize the ba pointer to the buffer allocated for this purpose */
	ba = (struct bss_ba_cfg *)bss_buf;
	memset(ba, 0, sizeof(struct bss_ba_cfg)); /* reset memory */
	/*
	 * Set the offset wsz: initial size used while formatting the bss space
	 * depending on macro cycle definition.
	 */
	ba->wsz = sizeof(struct bss_ba_cfg) / 4;

	/* TODO:
	 * clear internal data model : ba cycle, periodic vars ....
	 * Load silence time and turn around counter
	 * Load silence time and turn around counter
	 */
	ba->report.ba_state = MSTRFIP_FSM_INITIAL;
	ba->next_pres_iddat = (MSTRFIP_PRESENCE_VAR << 8);

	for (i = 0; i < 5; ++i) {
		mstrfip_hmq_claim_out_buf(i, &(ba->slot_ctx[i].obuf));
		acq_trtlmsg = (struct mstrfip_acq_trtlmsg_desc *)
			ba->slot_ctx[i].obuf.payload;
		acq_trtlmsg->hmq_slot = i; /* set hmq slot once */
		acq_trtlmsg->nentries = 0;
		acq_trtlmsg->irq_woffset = 0;
		ba->slot_ctx[i].obuf.header->len = MSTRFIP_ACQ_TRTLMSG_DESC_WSZ;
		ba->slot_ctx[i].slot_msg_count = 0;
	}

	ba->ident_ctx.col = 1; // skip 0x0010 which is ident var of the master
	ba->hw.mstr_pres_rpdat[0] = MSTRFIP_PDU_SMMPS_PRES_VAR | 0x03800500;
	ba->hw.mstr_pres_rpdat[1] = 0x30001D;
}

/* Sends an acknowledgement reply */
static inline void ctl_ack( uint32_t seq, uint16_t sync_id )
{
	struct trtl_fw_msg obuf;
	mstrfip_hmq_claim_out_buf(MSTRFIP_HMQ_IO_CPU0_CMD, &obuf);

	uint32_t* payload = ((uint32_t*)obuf.payload);

	payload[0] = MSTRFIP_REP_ACK;
	payload[1] = seq;

	obuf.header->len = 2;
	obuf.header->flags = TRTL_HMQ_HEADER_FLAG_ACK;
	obuf.header->sync_id = sync_id;
	obuf.header->seq = seq;
	obuf.header->msg_id = MSTRFIP_REP_ACK;

	masterfip_mq_send(MSTRFIP_HMQ_IO_CPU0_CMD, &obuf);
}

/* Sends an NACK (error) reply */
static inline void ctl_nack( uint32_t seq, uint16_t sync_id, int err )
{
	struct trtl_fw_msg obuf;
	mstrfip_hmq_claim_out_buf(MSTRFIP_HMQ_IO_CPU0_CMD, &obuf);

	uint32_t* payload = ((uint32_t*)obuf.payload);

	payload[0] = MSTRFIP_REP_NACK;
	payload[1] = seq;

	obuf.header->len = 2;
	obuf.header->flags = TRTL_HMQ_HEADER_FLAG_ACK;
	obuf.header->sync_id = sync_id;
	obuf.header->seq = seq;
	obuf.header->msg_id = MSTRFIP_REP_NACK;

	masterfip_mq_send(MSTRFIP_HMQ_IO_CPU0_CMD, &obuf);
}

static inline void set_hw_config(uint32_t seq, uint16_t sync_id, struct trtl_fw_msg *ibuf)
{
	/* map struct hw_cfg to the input msg */
	struct mstrfip_hw_cfg_trtlmsg *hw_cfg =
		(struct mstrfip_hw_cfg_trtlmsg *)ibuf->payload;
	uint32_t ext_sync = hw_cfg->enb_ext_trig_term;

	// reset CORE & fieldrive : This reset is done here because it resets
	// all the mfip registers and we want to keep the current mfip state in
	// case of CPU reset due to some software bug (exception)
	dp_writel(0x03, MASTERFIP_CSR_BASE + MASTERFIP_REG_RST);

	if (hw_cfg->enb_ext_trig == 0)
		ext_sync |=  MASTERFIP_EXT_SYNC_CTRL_OE_N; /* disable external sync pulse */
	else {
		ba->hw.ext_trig = 1; /* Use external synchro pulse */
//		ext_sync |= 0x10000;
	}
	ba->hw.freerun = hw_cfg->enb_int_trig;
	dp_writel(ext_sync, MASTERFIP_CSR_BASE + MASTERFIP_REG_EXT_SYNC_CTRL);
	dp_writel(ba->led_reg, MASTERFIP_CSR_BASE + MASTERFIP_REG_LED);
	/*
	 * Get silence and turn around time in CPU ticks unit.
	 * compute an ID_DAT length in ticks unit and finally
	 * load/start once tr and silence counters.
	 */

	ba->hw.tr_ticks = hw_cfg->tr_ticks;
	ba->hw.s_ticks = hw_cfg->ts_ticks;
	ba->hw.byte_ticks = hw_cfg->bit_ticks * 8;
	ba->hw.idfr_ticks = ba->hw.byte_ticks * 8;
	ba->hw.rpfin_ticks = ba->hw.byte_ticks * 6;
	/* load silence time counter */
	mstrfip_load_stime_counter();
	/* tr counter : loaded once and started manually */
	mstrfip_load_trtime_counter();
	mstrfip_start_trcounter();
	/* Create the response */
	ctl_ack(seq, sync_id);
}

static inline void get_response_time(uint32_t seq, uint16_t sync_id, struct trtl_fw_msg *ibuf)
{
	/* incoming message providing the range of agent address */
	struct mstrfip_response_time_trtlmsg *req =
		(struct mstrfip_response_time_trtlmsg *)ibuf->payload;
	/* output message returning response time for each requested agent */
	struct mstrfip_response_time_trtlmsg *ans;
	struct trtl_fw_msg obuf;
	volatile uint32_t *ptr, val, org_val;

	mstrfip_hmq_claim_out_buf(MSTRFIP_HMQ_IO_CPU0_CMD, &obuf);
	/* map the appropriate data struct to the output msg */
	ans = (struct mstrfip_response_time_trtlmsg *)obuf.payload;
	/* copy the range of agent address & mcycle length into answer */
	ans->from_agt_addr = req->from_agt_addr;
	ans->to_agt_addr = req->to_agt_addr;
	ans->cycle_nticks = req->cycle_nticks;
	ans->trtl_hdr.id = MSTRFIP_CMD_GET_RESPTIME;
	ans->trtl_hdr.seq = seq;

	obuf.header->len = MSTRFIP_RESPONSE_TIME_TRTLMSG_WSZ;
	obuf.header->flags = TRTL_HMQ_HEADER_FLAG_ACK;
	obuf.header->sync_id = sync_id;
	obuf.header->seq = seq;
	obuf.header->msg_id = MSTRFIP_CMD_GET_RESPTIME;


	ptr = &(((uint32_t*)obuf.payload)[obuf.header->len]); /* where to put the answer's payload */

	/* Frame reception requires to have macrocycle loaded */
	if (ba->hw.ext_trig) { /* Use external synchro pulse */
		org_val = dp_readl(MASTERFIP_CSR_BASE +
				   MASTERFIP_REG_EXT_SYNC_CTRL);
		val = org_val | MASTERFIP_EXT_SYNC_CTRL_OE_N; /*disable ext pulse */
		dp_writel(val, MASTERFIP_CSR_BASE + MASTERFIP_REG_EXT_SYNC_CTRL);
	}
	ba->cycle.nticks = req->cycle_nticks;
	mstrfip_start_cycle_counter();

	/* schedule the batch of presence var to get agent's response time */
	mstrfip_response_time(req->from_agt_addr, req->to_agt_addr, ptr);
 	obuf.header->len += (req->to_agt_addr - req->from_agt_addr); // set datalen
	if (ba->hw.ext_trig) {
		dp_writel(org_val,
			  MASTERFIP_CSR_BASE + MASTERFIP_REG_EXT_SYNC_CTRL);
	}

	masterfip_mq_send(MSTRFIP_HMQ_IO_CPU0_CMD, &obuf);
}

static inline void get_hw_bitrate(uint32_t seq, uint16_t sync_id, struct trtl_fw_msg *ibuf)
{
	/*TODO : HW access skipped for the time being */
	struct mstrfip_hw_speed_trtlmsg *hw_speed;
	struct trtl_fw_msg obuf;
	uint32_t hw_bitrate;

	hw_bitrate = dp_readl(MASTERFIP_CSR_BASE + MASTERFIP_REG_SPEED) &
		MASTERFIP_REG_SPEED_MASK;

	mstrfip_hmq_claim_out_buf(MSTRFIP_HMQ_IO_CPU0_CMD, &obuf);
	/* map the appropriate data struct to the output msg */
	hw_speed = (struct mstrfip_hw_speed_trtlmsg *)obuf.payload;

	/* Create the response */
	hw_speed->trtl_hdr.id = MSTRFIP_REP_BITRATE;
	hw_speed->trtl_hdr.seq = seq;
	hw_speed->bitrate = hw_bitrate;

	obuf.header->len = MSTRFIP_HW_SPEED_TRTLMSG_WSZ;
	obuf.header->flags = TRTL_HMQ_HEADER_FLAG_ACK;
	obuf.header->sync_id = sync_id;
	obuf.header->seq = seq;
	obuf.header->msg_id = MSTRFIP_REP_BITRATE;

	pp_printf("get_hw_bitrate (%ld)\n", hw_bitrate);

	masterfip_mq_send(MSTRFIP_HMQ_IO_CPU0_CMD, &obuf);
}

/*
 * periodic variables payload is formated this way:
 * |pdu 1byte|length 1byte|byt0|byte1|..|byten|mps_status 1byte|
 * Init the 2 bytes header and the last byte (mps status)
 * For produced var mps status byte is used to indicate to the agent if the
 * data is freshed and significant. Therefore when the app write a new payload,
 * mps_status byte is updated from the CPU1 and when the master sends the
 * payload to the agent mps status byte is updated from CPU0.
 */
static void mstrfip_per_var_payload_init()
{
	struct smem_per_var volatile *per_var = &smem_data->per_var;
	uint32_t smem_pos;
	uint32_t *payload_buf;
	int i;
	int word_idx, bsz;

	payload_buf = (uint32_t *)(per_var->payload_buf_addr);
	for (i = 0; i < ba->var.nhdrs; ++i) {
		if (ba->var.hdrs[i].flags & MSTRFIP_DATA_FLAGS_PROD) {
			smem_pos = per_var->payload_wsz * ba->var.hdrs[i].key;
			payload_buf[smem_pos] = MSTRFIP_PDU_MPS_VAR |
				((ba->var.hdrs[i].max_bsz + 1) << 8);
			bsz = ba->var.hdrs[i].max_bsz + MSTRFIP_VAR_PAYLOAD_HDR_BSZ;
			word_idx = bsz / 4;
			payload_buf[smem_pos + word_idx] = 0x0;
		}
	}
}

/*
 * max message payload sz:
 * nwords(1w)|bsz(1w)|irq_flag(1w)|payload(262byte:6byte addr+256byte data|
 * This gives a max size of 69 words. Max msg payload is provided in aper msg
 * window config
 */
#define MSTRFIP_MSG_HEADER_WSZ 3
#define MSTRFIP_MSG_ADDR_BSZ 6
static int mstrfip_smem_partitioning()
{
	struct mstrfip_ba_aper_msg_wind_args *args;
	uint32_t i, nvar, sz, nwords, bsz;
	uint32_t *smem_ptr, smem_wsz, *ptr;

	/*
	 * First reset smem ba data partition except the last field record.
	 * We are sure that record field is the last member of smem_data_cfg
	 * struct. Not reseting record field allows to first start recording
	 * with diag tool, which raise record flag in the config and then
	 * launch an application. This way we can record the traffic from
	 * begenning.
	 */
	nwords = ( sizeof(struct smem_data_cfg) -
		   sizeof(struct smem_record_traffic) ) / 4;
	for (i = 0; i < nwords; ++i)
		((uint32_t *)smem_data)[i] = 0x0;

	smem_ptr = (uint32_t *) smem_data + (sizeof(struct smem_data_cfg) / 4);

	/*
	 *
	 */
	smem_data->report.ext_sync_pulse_count = 0;
	smem_data->report.int_sync_pulse_count = 0;

	/*
	 * Set-up aperiodic msg dynamic area: payload circular buffer
	 */
	smem_data->aper_msg.payload_buf_addr = (uint32_t)smem_ptr;
	args = NULL;
	sz = 0;
	nwords = 0;
	/*
	 * get from the aper-msg-wind config the size of the FIFO which
	 * determines the max number of payloads to store (pending messages)
	 */
	for (i = 0; i < ba->cycle.ninstr; ++i) {
		if (ba->cycle.instr[i].code == MSTRFIP_BA_APER_MSG_WIND) {
			args = &ba->cycle.instr[i].aper_msg_wind;
			break;
		}
	}
	if (args) { //aper msg window is defined
		sz = args->prod_msg_fifo_sz;
		bsz = args->prod_msg_max_bsz + MSTRFIP_MSG_ADDR_BSZ;
		nwords = ((bsz / 4) + (!!(bsz % 4))) + MSTRFIP_MSG_HEADER_WSZ;
	}
	smem_data->aper_msg.payload_buf_sz = sz; /* max payloads in the buffer */
	smem_data->aper_msg.payload_wsz = nwords;
	smem_ptr += smem_data->aper_msg.payload_buf_sz;
	/* initialize payload circular buffer */
	ptr = (uint32_t *)(smem_data->aper_msg.payload_buf_addr);
	for (i = 0; i < sz; ++i) {
		ptr[i] = (uint32_t)(smem_ptr +
				    (i * smem_data->aper_msg.payload_wsz));
	}
	smem_data->aper_msg.payload_count = 0; /* current number of payload */
	smem_data->aper_msg.r_idx = 0;
	smem_data->aper_msg.w_idx = 0;
	smem_ptr += smem_data->aper_msg.payload_buf_sz *
		smem_data->aper_msg.payload_wsz;

	/*
	 * Set-up periodic var dynamic area: payload entries
	 * retrieve number and max payload size of produced var
	 */
	for (i = 0, nvar = 0, sz = 0; i < ba->var.nhdrs; ++i) {
		if (ba->var.hdrs[i].flags & MSTRFIP_DATA_FLAGS_PROD) {
			++nvar;
			sz = (sz < ba->var.hdrs[i].max_bsz) ?
				ba->var.hdrs[i].max_bsz : sz;
		}
	}
	/* locate payload and payload state areas in the SMEM */
	smem_data->per_var.payload_buf_addr = (uint32_t)smem_ptr; /* where per-var payloads start */
	/*
	 * compute payload_wsz which  the max payload size in words
	 * for per var payload_bsz = sz + HDR(PDU(2b) + Lentgh(1b)) + MPS(1b)
	 */
	sz += MSTRFIP_VAR_PAYLOAD_HDR_BSZ + MSTRFIP_VAR_MPS_STATUS_BSZ;
	smem_data->per_var.payload_wsz = (sz / 4) + (!!(sz % 4));
	smem_data->per_var.prod_nvar = nvar;
	smem_ptr += smem_data->per_var.payload_wsz * nvar; /* per-var payloads ends */
	/* Init header and tail of the periodic vars payload */
	mstrfip_per_var_payload_init();

	smem_data->per_var.payload_state_addr = (uint32_t)smem_ptr; /* per_var payload state */
	smem_ptr += (nvar / 32) + (!!(nvar % 32));  /* where the smem ends */

	smem_wsz = smem_ptr - (uint32_t *)smem_data;
	if (smem_wsz > MSTRFIP_SMEM_BUF_WSZ)
		return -1;
	smem_data->smem_wsz  = smem_wsz;

	return 0;
}

/*
 * BSS partitioning: ba is pointing to a buffer located in the BSS space and
 * using all the remaining space. The goal is to map on this buffer the
 * bss_ba_cfg and set properly the different partitions:
 * instructions: set of instructions defining macro cycle.
 * periodic variables: sequence od periodic variables to be scheduled
 * aperiodic messages: FIFO to store agent's request for aperiodic msg
 */
static uint32_t* mstrfip_bss_partitioning(int partition, uint32_t nwords)
{
	uint32_t * ptr;

	/* Check first if it remains enough available space */
	if (ba->wsz + nwords > MSTRFIP_BSS_REMAINING_WSZ)
		return NULL;
	ptr = (uint32_t *)ba + ba->wsz; /* wsz tells where to append it */

	switch (partition) {
	case MSTRFIP_BSS_PARTITION_BAINSTR:
		/* set instr pointer */
		if (ba->cycle.instr == NULL)
			ba->cycle.instr = (struct mstrfip_ba_instr *)ptr;
		break;

	case MSTRFIP_BSS_PARTITION_PERVAR:
		/*
		 * set hdrs pointer: list of periodic var(headers),
		 * in a proper sequence to play during the macro cycle
		 *
		 */
		if (ba->var.hdrs == NULL)
			ba->var.hdrs = (struct mstrfip_data_hdr *)ptr;
		break;

	case MSTRFIP_BSS_PARTITION_APERMSG:
		/* set entries pointer: FIFO for aper msg request */
		if (ba->msg.entries == NULL)
			ba->msg.entries = ptr;
		break;
	}

	ba->wsz += nwords;

	return ptr;
}

/*
 * Depending of ba complexity it may happen that more than one trtl message is
 * necessary to send the ba configuration
 */
static inline void set_ba_cycle(uint32_t seq, uint16_t sync_id, struct trtl_fw_msg *ibuf)
{
	uint32_t *ptr;
	int res, i;
	uint32_t nwords;
	struct mstrfip_ba_instr *instr;
	struct mstrfip_ba_instr_hdr_trtlmsg* instr_trtlmsg;

	instr_trtlmsg = (struct mstrfip_ba_instr_hdr_trtlmsg *)ibuf->payload;
	ba->cycle.nticks = instr_trtlmsg->cycle_ticks_length;
	if (ba->hw.ext_trig && ba->hw.freerun)
		/*
		 * macro cycle counter can reseted by the arrival of an
		 * external pulse or by the internal counter. If application
		 * wants to use ext-trig + int-trig to supply to the absence of
		 * int-trig, the macro cycle counter is increased by a few of
		 * CPU ticks, in order to be sure that the ext pulse will reset
		 * the macrocycle-counter before the internal counter.
		 */
		ba->cycle.nticks += delays_in_cpu_ticks[IDX_500NS_DELAY]; /* add 500ns */

	/*
	 * set ba cycle partition
	 */
	nwords = ((sizeof(struct mstrfip_ba_instr) *
		   instr_trtlmsg->instr_count) / 4);
	ptr = mstrfip_bss_partitioning(MSTRFIP_BSS_PARTITION_BAINSTR, nwords);
	if (ptr == NULL) {
		do_reset();
		ctl_nack(seq, sync_id,  -1);
		return;
	}

	/* copy instruction set (memcpy doesn't do the job: to investigate) */
	for (i = 0; i < nwords; ++i)
		ptr[i] = ((uint32_t*)ibuf->payload)[MSTRFIP_BA_INSTR_HDR_TRTLMSG_WSZ + i];
	/* update the number of instructions and increment the size accordingly */
	ba->cycle.ninstr += instr_trtlmsg->instr_count;

	/*
	 * set aperiodic message partition if any: check if an aperiodic message
	 * window is defined and set accordling the partition.
	 * An entry in the FIFO is 1 word size. In case of sevral aperiodic
	 * msg window the biggets size is used to size the FIFO.
	 */
	for (i = 0, nwords = 0; i < ba->cycle.ninstr; ++i) {
		instr = &ba->cycle.instr[i];
		if (instr->code == MSTRFIP_BA_APER_MSG_WIND) {
			nwords = (nwords < instr->aper_msg_wind.cons_msg_fifo_sz) ?
				instr->aper_msg_wind.cons_msg_fifo_sz : nwords;
		}
	}
	if (nwords != 0) { /* aperiodic message window found */
		ptr = mstrfip_bss_partitioning(MSTRFIP_BSS_PARTITION_APERMSG,
					       nwords);
		ba->msg.entries_sz = nwords;
	}

	/*
	 * TODO : check that all other config has been done like set var, set
	 * hw config,..etc...
	 * Configure the smem and the local ba_config data
	 * In case where the requested config doesn't fit into the memory
	 * space, an error should be returned and the system should return in
	 * INITIAL state
	 */

	/*
	 * All the configuration has been sent by the application
	 * time to init two global data: one resident in smem and
	 * one resident in bss section
	 */
	res = mstrfip_smem_partitioning();
	smem_data->bss_wsz  = ba->wsz;

	/* TODO: define set of errors send back with ctl_nack */
	(res != 0) ? ctl_nack(seq, sync_id,  -1) : ctl_ack(seq, sync_id);
	/*
	 * BA cycle has been received, time to move into the next state
	 * BA_READY
	 */
	ba->report.ba_state = MSTRFIP_FSM_READY;
}

static inline void set_var_list(uint32_t seq, uint16_t sync_id, struct trtl_fw_msg *ibuf)
{
	struct mstrfip_data_hdr_trtlmsg *var_hdrs =
		(struct mstrfip_data_hdr_trtlmsg *)ibuf->payload;
	uint32_t *ptr;
	uint32_t nwords;
	int i;

	/* set ptr to point where to store the var headers list */
	nwords = ((sizeof(struct mstrfip_data_hdr) * var_hdrs->ndata) / 4);
	ptr = mstrfip_bss_partitioning(MSTRFIP_BSS_PARTITION_PERVAR, nwords);
	/* memcpy is less performant */
	for (i = 0; i < nwords; ++i)
	{
		ptr[i] = ((uint32_t*)ibuf->payload)[MSTRFIP_DATA_HDR_TRTLMSG_WSZ + i];
	}
	/* update the number of var headers and increment the size accordingly */
	ba->var.nhdrs += var_hdrs->ndata;

	ctl_ack(seq, sync_id);
}

static void do_ba_running()
{
	pp_printf("do_ba_running\n");
	int instr_idx, new_cycle, last_instr;
	uint32_t ba_state_cmd;
	struct mstrfip_ba_instr *instr;
#ifdef RTDEBUG_BA
	uint32_t psecs=0, pticks=0, secs, ticks, delta;
#endif
	ba->report.ba_state = MSTRFIP_FSM_RUNNING;

	for (;;) {
		/*
		 * runs BA : before executing an instruction we check first if
		 * the applictaion sends a STOP command
		 */
		for (instr_idx = 0; instr_idx < ba->cycle.ninstr; ++instr_idx) {
			ba_state_cmd = smem_data->ba_state_cmd;

			switch (ba_state_cmd) {
			case MSTRFIP_BA_STATE_CMD_STOP:
				ba->report.ba_state = MSTRFIP_FSM_READY;
				return;
				break;

			case MSTRFIP_BA_STATE_CMD_RESET:
				do_reset();
				return;
				break;
			}

			/* first instruction index indicates a new ba cycle */
			new_cycle = !instr_idx;
			instr = &ba->cycle.instr[instr_idx];
			switch (instr->code) {
			case MSTRFIP_BA_PER_VAR_WIND:
				mstrfip_do_ba_per_var_wind(new_cycle,
					instr->per_var_wind.start_var_idx,
					instr->per_var_wind.stop_var_idx);
				break;
			case MSTRFIP_BA_APER_VAR_WIND:
				mstrfip_do_ba_aper_var_wind(new_cycle,
					instr->aper_var_wind.ticks_end_time);
				break;
			case MSTRFIP_BA_APER_MSG_WIND:
				mstrfip_do_ba_aper_msg_wind(new_cycle,
					&instr->aper_msg_wind);
				break;
			case MSTRFIP_BA_WAIT_WIND:
				last_instr =
					(instr_idx == (ba->cycle.ninstr -1)) ?
								1 : 0;
				mstrfip_do_ba_wait_wind(new_cycle, last_instr,
					instr->wait_wind.is_silent,
					instr->wait_wind.ticks_end_time);
				break;
			case MSTRFIP_BA_NEXT_MACRO:
				break;
			}
		}
#ifdef RTDEBUG_BA
		if (trace_count < 50) {
			secs = lr_readl(MT_CPU_LR_REG_TAI_SEC);
			ticks = lr_readl(MT_CPU_LR_REG_TAI_CYCLES);
			delta = (secs - psecs)*1000000 +
				/* convert ticks in us */
				(int)((ticks - pticks) / MSTRFIP_CPU_SPEED_MHZ);
			psecs = secs;
			pticks = ticks;
			pr_debug(__FILE__":%d %u", __LINE__, (unsigned int)delta);

			++trace_count;
		}
#endif
	}
}


static void do_ba_ready()
{
	int i;
	struct mstrfip_acq_trtlmsg_desc *acq_trtlmsg;

	pp_printf("do_ba_ready\n");
	uint32_t ba_state_cmd;

	/*Prepare MQ slots*/
	for (i = 0; i < 5; ++i) {
		mstrfip_hmq_claim_out_buf(i, &(ba->slot_ctx[i].obuf));
		acq_trtlmsg = (struct mstrfip_acq_trtlmsg_desc *)
			ba->slot_ctx[i].obuf.payload;
		acq_trtlmsg->hmq_slot = i; /* set hmq slot once */
		acq_trtlmsg->nentries = 0;
		acq_trtlmsg->irq_woffset = 0;
		ba->slot_ctx[i].obuf.header->len = MSTRFIP_ACQ_TRTLMSG_DESC_WSZ;
		ba->slot_ctx[i].slot_msg_count = 0;
	}


	/* cycle counter */
	mstrfip_start_cycle_counter();

//	/* enable safe window */
//	val = dp_readl( MASTERFIP_CSR_BASE +
//			MASTERFIP_REG_EXT_SYNC_CTRL);
//	val |= 0x1000000;
//	dp_writel(val, MASTERFIP_CSR_BASE +
//			MASTERFIP_REG_EXT_SYNC_CTRL);

//	ba->report.ba_state = MSTRFIP_FSM_READY;
	for(;;) {
		ba_state_cmd = smem_data->ba_state_cmd;

		switch (ba_state_cmd) {
		case MSTRFIP_BA_STATE_CMD_START:
			do_ba_running();
			/*
			 * returned from RUNNING state further to:
			 * STOP or RESET cmd. If it was RESET jump into config
			 * state, otherwhise wait for a new START or RESET cmd.
			 */
			if (ba->report.ba_state == MSTRFIP_FSM_INITIAL) {
				return;
			}
			break;
		case MSTRFIP_BA_STATE_CMD_RESET:
			do_reset();
			/* back to do_config state */
			return;
		}
	}
	return;
}

#define _CMD(id, func)				\
	case id:				\
	{					\
		func(seq, sync_id, &ibuf);	\
		break;				\
	}
/* Receives command messages and call matching command handlers */
static void do_config()
{
	uint32_t cmd, seq, sync_id;
	uint32_t ba_state_cmd;
	struct trtl_fw_msg ibuf;

	ba->report.ba_state = MSTRFIP_FSM_INITIAL;
	for(;;) {
		ba_state_cmd = smem_data->ba_state_cmd;
		if ((ba->report.ba_state != MSTRFIP_FSM_INITIAL) &&
		    (ba_state_cmd == MSTRFIP_BA_STATE_CMD_RESET))
		{
			do_reset();
		}

		/* HMQ control slot empty? */
		if ((mq_poll_in(MFIP_MQ, 1 << MSTRFIP_HMQ_IO_CPU0_CMD)) == 0)
		{
			continue;
		}

		masterfip_map_in_message(MSTRFIP_HMQ_IO_CPU0_CMD, &ibuf);
		cmd = ((struct mstrfip_trtlmsg_hdr *)ibuf.payload)->id;
		seq = ibuf.header->seq;
		sync_id = ibuf.header->sync_id;

		switch(cmd) {
		_CMD(MSTRFIP_CMD_SET_HW_CFG, set_hw_config)
		_CMD(MSTRFIP_CMD_GET_BITRATE, get_hw_bitrate)
		_CMD(MSTRFIP_CMD_SET_VAR_LIST, set_var_list)
		_CMD(MSTRFIP_CMD_SET_BA_CYCLE, set_ba_cycle)
		_CMD(MSTRFIP_CMD_GET_RESPTIME, get_response_time)
		default:
			ctl_nack(seq, sync_id, -1);
			break;
		}

		/* Drop the message once handled */
		mq_discard(MFIP_MQ, MSTRFIP_HMQ_IO_CPU0_CMD);
		if (ba->report.ba_state == MSTRFIP_FSM_READY) {
			do_ba_ready();
		}
	}
}

int main()
{
	pp_printf("\e[32mMasterFIP core0 BA "__TIME__"\e[0m \n");

#ifdef RTDEBUG_BA
	int id, vers;
	id = dp_readl( MASTERFIP_CSR_BASE + MASTERFIP_REG_ID);
	vers = dp_readl( MASTERFIP_CSR_BASE + MASTERFIP_REG_VER);
	pr_debug(__FILE__":%d %d %d", __LINE__, id, vers);
#endif

	do_reset();
	do_init();
	do_config();

	return 0;
}
