/*
 * Copyright (C) 2016-2017 CERN (www.cern.ch)
 * Author: Michel Arruat <michel.arruat@cern.ch>
 *
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#include <string.h>

#include <mockturtle-rt.h>

#include "masterfip-smem.h"
#include "masterfip-common-priv.h"
#include "hw/masterfip_hw.h"
#include "masterfip-mq.h"

static struct smem_data_cfg volatile *smem_data;
static int trace_count;
static uint32_t prev_cycle_counter; /* keeps the previous cycle_counter value */

uint32_t rt_version = MFIP_RT_VERSION;
uint32_t rt_git_version = MFIP_GIT_VERSION;

static struct trtl_fw_msg ctl_claim_out_buf(uint32_t slot)
{
	struct trtl_fw_msg obuf;

	// Removed checking if the queue is not full, because RT SW cannot wait.
	// Such an event is very unlikely, but if it happens, the data in
	// the queue will be overwritten.

	mq_claim(MFIP_MQ, slot);
	mq_map_out_message(MFIP_MQ, slot, &obuf);
	
	obuf.header->rt_app_id = CONFIG_RT_APPLICATION_ID;
	obuf.header->flags = 0;
	obuf.header->msg_id = 0;

	return obuf;
}

/* Sends an NACK (error) reply */
static inline void ctl_nack( uint32_t seq, uint32_t sync_id, int err )
{
	struct trtl_fw_msg obuf = ctl_claim_out_buf(MSTRFIP_HMQ_IO_CPU1_CMD);

	uint32_t* payload = ((uint32_t*)obuf.payload);

	payload[0] = MSTRFIP_REP_NACK;
	payload[1] = seq;
	payload[2] = err;

	obuf.header->len = 3;
	obuf.header->flags = TRTL_HMQ_HEADER_FLAG_ACK;
	obuf.header->seq = seq;
	obuf.header->sync_id = sync_id;
	obuf.header->msg_id = MSTRFIP_REP_NACK;

	masterfip_mq_send(MSTRFIP_HMQ_IO_CPU1_CMD, &obuf);
}

static void mstrfip_sw_init()
{
	/* initialize the local pointer to the shared memory */
	smem_data = (struct smem_data_cfg volatile *)smem_buf;

	trace_count = 0;
	prev_cycle_counter = 0;
}

static void set_ba_state(uint32_t seq, uint16_t sync_id, struct trtl_fw_msg *ibuf)
{
	struct mstrfip_ba_state_trtlmsg *ba_state_msg =
		(struct mstrfip_ba_state_trtlmsg *)ibuf->payload;


	rt_smem_lock(&smem_data->smem_mtx); /* lock SMEM */
	smem_data->ba_state_cmd = ba_state_msg->state;

	rt_smem_unlock(&smem_data->smem_mtx); /* unlock SMEM */
}

/*
 * This function assumes that the given var index is correct in order to
 * properly locate the rigth variable in the SMEM. The fip library is
 * responsible to check the parameters
 */
#define MSTRFIP_PDU_MPS_VAR 0x40
static void set_var_payload(uint32_t seq, uint16_t sync_id, struct trtl_fw_msg *ibuf)
{
	struct smem_per_var volatile *per_var = &smem_data->per_var;
	struct mstrfip_var_payload_trtlmsg *payload_msg =
		(struct mstrfip_var_payload_trtlmsg *)ibuf->payload;
	volatile uint32_t *payload =
		((uint32_t*)ibuf->payload) + MSTRFIP_VAR_PAYLOAD_HDR_TRTLMSG_WSZ;
	uint32_t smem_pos = per_var->payload_wsz * payload_msg->key;
	uint32_t bsz = payload_msg->bsz;
	uint32_t nwords = bsz / 4 + (!!(bsz % 4));
	uint32_t buf[32], val, shift_bsz, word_idx;
	uint32_t *payload_buf;
	int i;

	/*
	 * Var frame bsz = payload bsz + 1 (MPS status)
	 */
	buf[0] = MSTRFIP_PDU_MPS_VAR | ((bsz + 1) << 8);
	for (i = 0; i < nwords; i++) {
		val = *(payload + i);
		buf[i] |= (val & 0xFFFF) << 16;
		buf[i+1] = (val >> 16) & 0xFFFF;
	}
	/*
	 * find out where to append mps_status byte:
	 * shift_bsz: which byte of the last word to append mps_status
	 * word_idx : which word index
	 */
	shift_bsz = (bsz + MSTRFIP_VAR_PAYLOAD_HDR_BSZ) % 4; /* payload + 2 hdr bytes */
	word_idx = (bsz + MSTRFIP_VAR_PAYLOAD_HDR_BSZ) / 4;
	/* set significance and refresment bits */
	buf[word_idx] |= (0xFFFFFFFF &
			  (((MSTRFIP_MPS_REFRESH_BIT | MSTRFIP_MPS_SIGNIFICANCE_BIT)) <<
			   (shift_bsz * 8)));
	/*
	 * Check refresh bit of the MSP status: if it's still equal to 1, this
	 * means that the payload has not been sent to the agent. In this case
	 * a warning is reported to the client
	 */
	nwords = word_idx + 1;
	payload_buf = (uint32_t *)(per_var->payload_buf_addr);
	rt_smem_lock(&smem_data->smem_mtx); /* lock SMEM */
	for (i = 0; i < nwords; i++) {
		payload_buf[smem_pos + i] = buf[i];
	};
	rt_smem_unlock(&smem_data->smem_mtx); /* unlock SMEM */
}

static void set_msg_payload(uint32_t seq, uint16_t sync_id, struct trtl_fw_msg *ibuf)
{
	struct smem_aper_msg volatile *aper_msg = &smem_data->aper_msg;
	struct mstrfip_msg_payload_trtlmsg *payload_msg =
		(struct mstrfip_msg_payload_trtlmsg *)ibuf->payload;
	volatile uint32_t *payload =
		ibuf->payload + MSTRFIP_MSG_PAYLOAD_HDR_TRTLMSG_WSZ;
	uint32_t bsz, nwords, nwords_payload;
	uint32_t *wptr, val, *payload_buf;
	uint32_t buf[67] = {0}; /* max size of a message frame. Init to 0 */
	int i;

	/* check if aper mesg FIFO is full */
	if (aper_msg->payload_count == aper_msg->payload_buf_sz) {
		return;
	}
	/* build the message frame:|nwords|bsz|frame|
	 * As preamble we need two extra words containing the number of words
	 * and the number of bytes(exact number of bytes used by serializer)
	 * frame payload: 3bytes(dest addr)+3bytes(src addr)+bsz(payload)
	 */
	/* frame payload bsz is payload + 6 bytes for addresses*/
	bsz = payload_msg->bsz + MSTRFIP_MSG_PAYLOAD_HDR_BSZ;
	nwords = bsz/4 + !!(bsz%4);
	buf[0] = nwords;
	buf[1] = bsz; /* used by the serializer */
	buf[2] = payload_msg->flags; /* msg flags: direction + irq */
	buf[3] = payload_msg->dest_addr << 8; /* destination addr 3 bytes */
	nwords_payload = (payload_msg->bsz/4) + (!!(payload_msg->bsz % 4));
	for (i = 0; i < nwords_payload; i++) {
		val = *(payload + i);
		buf[4+i] |= (val & 0xFFFF) << 16;
		buf[4+i+1] = val >> 16;
	}
	payload_buf = (uint32_t *)(aper_msg->payload_buf_addr);
	wptr = (uint32_t *)(payload_buf[aper_msg->w_idx]);
	/*
	 * Set the new msg paylod in the message FIFO.
	 * This task is accomplished in a critical section and it's reduced to
	 * the strict minimum to notlock the SMEM more than necessary.
	 * For info: in the worst case (writing the max payload 268 bytes) the
	 * SMEM is locked during 5us. This can happen when the CPU0 needs also
	 * to access the SMEM and therefore CPU0 is delayed by a max of 5us
	 * TODO: better locking policy could be to use different mutex for
	 * periodic var and aperiodic messages. This way two CPUs accessing at
	 * the same time SMEM (different areas) interleaves their access ....
	 * to be checked
	 */
	rt_smem_lock(&smem_data->smem_mtx); /* lock SMEM */
	/*
	 * memcpy to SMEM doesn't work properly and anyway it's not giving
	 * better performance than writing single word in a for loop
	 */
	for (i = 0; i < nwords + 3; ++i)
		wptr[i] = buf[i];
	aper_msg->w_idx = (aper_msg->w_idx + 1) % aper_msg->payload_buf_sz;
	++aper_msg->payload_count;
	rt_smem_unlock(&smem_data->smem_mtx); /* unlock SMEM */
}

static void get_present_list(uint32_t seq, uint16_t sync_id, struct trtl_fw_msg *ibuf)
{
	uint32_t volatile *presence_request =
		smem_data->aper_var.presence_request;
	int i;

	struct trtl_fw_msg msg = ctl_claim_out_buf(MSTRFIP_HMQ_IO_CPU1_CMD);

	uint32_t* payload = ((uint32_t*)msg.payload);

	payload[0] = MSTRFIP_CMD_GET_PRESENT_LIST;
	payload[1] = seq;
	for (i = 0; i < MSTRFIP_PRESENCE_REQUEST_WSIZE; ++i)
	{
		payload[2+i] = presence_request[i];
	}

	msg.header->len = 2 + MSTRFIP_PRESENCE_REQUEST_WSIZE;
	msg.header->flags = TRTL_HMQ_HEADER_FLAG_ACK;
	msg.header->msg_id = MSTRFIP_CMD_GET_PRESENT_LIST;
	msg.header->seq = seq;
	msg.header->sync_id = sync_id;

	masterfip_mq_send(MSTRFIP_HMQ_IO_CPU1_CMD, &msg);
}

/*
 * This function assumes that the given var index is correct in order to
 * properly locate the rigth variable in the SMEM. The fip library is
 * responsible to check the parameters
 */
static void set_ident_list(uint32_t seq, uint16_t sync_id, struct trtl_fw_msg *ibuf)
{
	struct smem_aper_var volatile *aper_var = &smem_data->aper_var;
	int i;

	rt_smem_lock(&smem_data->smem_mtx); /* lock SMEM */
	for (i = 0; i < MSTRFIP_IDENT_REQUEST_WSIZE; ++i)
		/* bitwise OR to not overwrite any previous pending request */
		aper_var->ident_request[i] |= ((uint32_t*)ibuf->payload)[2 + i];
	aper_var->ident_req = 1; /* identification var traffic should be scheduled*/
	rt_smem_unlock(&smem_data->smem_mtx); /* unlock SMEM */
}


static void get_report(uint32_t seq, uint16_t sync_id, struct trtl_fw_msg *ibuf)
{
	uint32_t volatile *ptr_src = (uint32_t *)&smem_data->report;
	uint32_t nwords, *ptr_dest;
	int i;

	struct trtl_fw_msg msg = ctl_claim_out_buf(MSTRFIP_HMQ_IO_CPU1_CMD);

	uint32_t* payload = ((uint32_t*)msg.payload);

	payload[0] = MSTRFIP_CMD_GET_REPORT;
	payload[1] = seq;
	ptr_dest = (uint32_t *)&(payload[2]);

	nwords = sizeof(struct mstrfip_report) / 4;
	for (i = 0; i < nwords; ++i)
	{
		ptr_dest[i] = ptr_src[i];
	}

	msg.header->len = nwords + 2;
	msg.header->flags = TRTL_HMQ_HEADER_FLAG_ACK;
	msg.header->msg_id = MSTRFIP_CMD_GET_REPORT;
	msg.header->seq = seq;
	msg.header->sync_id = sync_id;

	masterfip_mq_send(MSTRFIP_HMQ_IO_CPU1_CMD, &msg);
}

static void get_rt_version(uint32_t seq, uint16_t sync_id, struct trtl_fw_msg *ibuf)
{
	struct mstrfip_rt_version_trtlmsg *rt_vers;
	struct trtl_fw_msg obuf = ctl_claim_out_buf(MSTRFIP_HMQ_IO_CPU1_CMD);

	/* map the appropriate data struct to the output msg */
	rt_vers = (struct mstrfip_rt_version_trtlmsg *)obuf.payload;

	/* Create the response */
	rt_vers->trtl_hdr.id = MSTRFIP_REP_RT_VERSION;
	rt_vers->trtl_hdr.seq = seq;
	rt_vers->rt_id = MSTRFIP_RT_ID; /* masterFIP rt software id */
	rt_vers->rt_version = rt_version; /* evaluated at compile time */
	rt_vers->rt_git_version = rt_git_version; /* evaluated at compile time */
	rt_vers->fpga_id = dp_readl( MASTERFIP_CSR_BASE + MASTERFIP_REG_ID);
	rt_vers->fpga_version = dp_readl( MASTERFIP_CSR_BASE + MASTERFIP_REG_VER);
	obuf.header->len = MSTRFIP_RT_VERSION_TRTLMSG_WSZ;
	obuf.header->flags = TRTL_HMQ_HEADER_FLAG_ACK;
	obuf.header->sync_id = sync_id;
	obuf.header->seq = seq;
	obuf.header->msg_id = MSTRFIP_REP_RT_VERSION;

	pp_printf("get rt vers\n");

	masterfip_mq_send(MSTRFIP_HMQ_IO_CPU1_CMD, &obuf);
}

#define _CMD(id, func)				\
	case id:				\
	{					\
		func(seq, sync_id, &ibuf);	\
		break;				\
	}

int main()
{
	pp_printf("\e[32mMasterFIP [core 1 CMD]\e[0m \n");
	uint32_t cmd, seq;
	uint16_t sync_id;
	struct trtl_fw_msg ibuf;
	uint32_t* payload;

	mstrfip_sw_init();

	for(;;) {

		/* HMQ control slot empty? */
		if(((mq_poll_in(MFIP_MQ, 1 << MSTRFIP_HMQ_IO_CPU1_CMD)) != 0))
		{
			masterfip_map_in_message(MSTRFIP_HMQ_IO_CPU1_CMD, &ibuf);

			payload = ((uint32_t*)ibuf.payload);
            
			cmd = payload[0];
			seq = ibuf.header->seq;
			sync_id = ibuf.header->sync_id;

			switch(cmd) {
			_CMD(MSTRFIP_CMD_SET_BA_STATE, set_ba_state)
			_CMD(MSTRFIP_CMD_SET_VAR_PAYLOAD, set_var_payload)
			_CMD(MSTRFIP_CMD_SET_MSG_PAYLOAD, set_msg_payload)
			_CMD(MSTRFIP_CMD_GET_PRESENT_LIST, get_present_list)
			_CMD(MSTRFIP_CMD_REQ_IDENT_VAR, set_ident_list)
			_CMD(MSTRFIP_CMD_GET_REPORT, get_report)
			_CMD(MSTRFIP_CMD_GET_RT_VERSION, get_rt_version)
			default:
				ctl_nack(seq, sync_id, -1);
				break;
			}

			/* Drop the message once handled */
			mq_discard(MFIP_MQ, MSTRFIP_HMQ_IO_CPU1_CMD);      
		}
	}
	return 0;
}
