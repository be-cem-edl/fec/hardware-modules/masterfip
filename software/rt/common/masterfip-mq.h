#ifndef MASTERFIP_MQ_H
#define MASTERFIP_MQ_H

#include <mockturtle-rt.h>
#include "masterfip-common-priv.h"

#ifdef profip
#define MFIP_FOR_PROFIP
#define MFIP_MQ TRTL_RMQ
#define TRTLMSG_MAX_PAYLOAD_WSZ MSTRFIP_TRTLMSG_MAX_PAYLOAD_WSZ_RMQ
#else
#define MFIP_MQ TRTL_HMQ
#define TRTLMSG_MAX_PAYLOAD_WSZ MSTRFIP_TRTLMSG_MAX_PAYLOAD_WSZ_HMQ
#endif

#ifdef MFIP_FOR_PROFIP
void mstrfip_add_ctrlnbr(struct trtl_fw_msg *msg);
#endif

static inline void masterfip_map_in_message(unsigned idx_mq,
                                            struct trtl_fw_msg *msg)
{
    mq_map_in_message(MFIP_MQ, idx_mq, msg);

    // The RMQ header is extended compared to HMQ header by two uint32_t values
    // at the beginning of the frame (e.g. frame size). 
    // To process the header in the same way as the HMQ header, 
    // RMQ header is shifted by 8 bytes.
#ifdef MFIP_FOR_PROFIP
    msg->header = (struct trtl_hmq_header *)(((uint32_t*)msg->header) + 2);
#endif
}

static inline void masterfip_mq_send(int slot, struct trtl_fw_msg *msg)
{
#ifdef MFIP_FOR_PROFIP
    mstrfip_add_ctrlnbr(msg);
#endif
    mq_send(MFIP_MQ, slot);
}

#endif
