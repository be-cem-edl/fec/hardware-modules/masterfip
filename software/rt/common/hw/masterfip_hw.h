#ifndef __MASTERFIP_HW_h
#define __MASTERFIP_HW_h

#include "fmc_masterfip_csr.h"
/*#include "mf_regs.h"*/

#define MASTERFIP_CSR_BASE 0x0 /* base address of mstrfip wishbone core */

/* missing declaration in fmc_masterfip_csr.h */
#define MASTERFIP_REG_SPEED_MASK 0x3
#define MASTERFIP_EXT_SYNC_OE_SHIFT 0x2 

#define MASTERFIP_DAC_1_6V 0x1A8F6
#define MASTERFIP_ADC_OFF 0x304

#endif /* __MASTERFIP_HW.h */
