/*
 * Copyright (C) 2016-2017 CERN (www.cern.ch)
 * Author: Michel Arruat <michel.arruat@cern.ch>
 *
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <math.h>

#include <mockturtle/libmockturtle.h>

#include "libmasterfip-priv.h"
#include "libmasterfip.h"

/* return 1 if the action is allowed otherwhise returns 0 and set errno */
/* TODO : review completely this routine */
int mstrfip_is_action_allowed(struct mstrfip_desc *mstrfip, int action)
{
	if (mstrfip->ba.state != MSTRFIP_FSM_INITIAL) {
		errno = MSTRFIP_BA_NOT_STOP_STATE;
		return 0; /* not allowed */
	}
	if (action == __MSTRFIP_CREATE_BA)
		return 1; /* this action doesn't require more checks */

//	if (!mstrfip->ba.max_instr) {/* Max instruction count is null:first, create BA */
//		errno = MSTRFIP_BA_IS_NULL;
//		return 0; /* not allowed */
//	}
	/* TODO: commentted for the time being */
//	if (mstrfip->ba.var_list_count == mstrfip->ba.max_var_list) {
//		errno = MSTRFIP_BA_MAX_VAR_LIST;
//		return 0; /* not allowed */
//	}
	if (action == __MSTRFIP_CONFIG_BA)
		return 1; /* this action doesn't require more checks */

	return 1;
}

/*
 * Check aperiodic messages window config:
 */
int mstrfip_ba_check_aper_msg_wind_cfg(struct mstrfip_ba *ba,
					struct mstrfip_aper_msg_wind_cfg *cfg)
{
	/*
	 * Application can register a callback per message or a global callback
	 * called when the aper-msg-window ends. For consumed message one of
	 * the callback is mandatory while for produced callback is optional.
	 */
	if (!cfg->mstrfip_cons_msg_handler) { // no global callback registered
		// check if every cons msg have registered a callback
		//TODO : to be completed.
	}
	return 0;
}

/**
 * Check consistency between ba macro cycle instructions and periodic variables
 * requested services
 * Returns 0 in case of inconsistency otherwhise 1
 */
int mstrfip_ba_consistency_chk(struct mstrfip_desc *mstrfip)
{
//	int req_services = 0, i, j;
//
//	/* iterate through all periodic var ORing the requested services */
//	for (i = 0; i < mstrfip->ba.var_count; ++i) {
//		req_services |= mstrfip->ba.varlist[i]->hdr->flags;
//	}
//	/*
//	 * All the services are collected, check if the macro cycle instruction
//	 * is coherent regarding the services:
//	 */
//	/*
//	 * Aperiodic messages: at least one periodic variable has the
//	 * capability to request to send apeiodic message
//	 * TODO !!! to be confirmed with Eva | Julien !!!!!!
//	 */
//	if (req_services & MSTRFIP_VAR_FLAGS_APER_MSG) {
//		/* */
//		for (i = 0; i < mstrfip->ba.instr_count; ++i)
//			if (mstrfip->ba.instrlist[i].instr == MSTRFIP_BA_APER_MSG_WIND)
//				break;
//		if (i == mstrfip->ba.instr_count)
//			return 0;
//	}
//	/*
//	 * SMMPS variable like "test presence" requires to schedule an
//	 * aperiodic window in the macro cycle.
//	 */
//	if (req_services & MSTRFIP_VAR_FLAGS_APER_MPS) {
//		/* */
//		for (i = 0; i < mstrfip->ba.instr_count; ++i)
//			if (mstrfip->ba.instrlist[i].instr == MSTRFIP_BA_APER_VAR_WIND)
//				break;
//		if (i == mstrfip->ba.instr_count)
//			return 0;
//	}
	return 1;
}

/**
 * this routine computes the theoritical duration using the
 * theoritical turnaround (tr) and silence time (ts) according to the bus speed
 *
 */
static uint32_t compute_list_var_length(struct mstrfip_ba *ba,
		struct mstrfip_ba_macrocycle *mc, int start_idx, int stop_idx)
{
	int i;
	uint32_t ustime = 0;
	double vald;
	uint32_t tr_ns, ts_ns, bit_ns;
	/*
	 * TODO:
	 */

//	tr_ns = mstrfip_tr_nstime[ba->bitrate];
	tr_ns = ba->hw_cfg.turn_around_ustime * 1000;
	ts_ns = mstrfip_ts_nstime[ba->bitrate];
	bit_ns = mstrfip_bit_nstime[ba->bitrate];

	for (i = start_idx; i < stop_idx; ++i) {
		struct mstrfip_data_priv *pvar_priv = (struct mstrfip_data_priv *)mc->per_vars.sorted_varlist[i]->priv;
		uint64_t rpdat_time, iddat_time;

		/*
		 * rpdat frame is made of nbytes of payload + 9 extra bytes:
		 * header, footer, crc ...
		 */
		rpdat_time = ((pvar_priv->hdr.max_bsz + 9) * 8 * bit_ns) +
								(2 * tr_ns);
		/*
		 * iddat frame is made of 2 payload bytes
		 */
		iddat_time = (2 + 6) * 8 * bit_ns;
		if (pvar_priv->hdr.flags & MSTRFIP_DATA_FLAGS_CONS) { /* consumed var */
			vald = (ts_ns < rpdat_time) ? rpdat_time : ts_ns;
			vald += iddat_time;
		}
		else { /*produced var */
			vald = iddat_time + rpdat_time;
		}
		ustime += ceil(vald/1000);
	}
	return ustime;
}

/**
 * this routine computes the theoritical macro cycle duration using the
 * theoritical turnaround and silence time according to the bus speed.
 * In case of error, error messages are built on fly because they contain
 * dynamic data as, computed cycle length.
 */

int mstrfip_ba_cyclelength_chk(struct mstrfip_ba *ba,
				struct mstrfip_ba_macrocycle *mc)
{
	uint32_t cpu_hz = trtl_config_get(mc->dev->trtl)->clock_freq;
	int i, start_idx, stop_idx, margin;
	uint32_t cycle_uslength, ticks, us_endtime;
	char *instr;

	cycle_uslength = 0;
	for (i = 0; i < mc->instr_count; ++i) {
		switch (mc->instrlist[i].code) {
		case MSTRFIP_BA_PER_VAR_WIND:
			start_idx = mc->instrlist[i].per_var_wind.start_var_idx;
			stop_idx = mc->instrlist[i].per_var_wind.stop_var_idx;
			cycle_uslength += compute_list_var_length(ba, mc,
							start_idx, stop_idx);
			continue; /* skip the rest and treat next instruction */
			break;
		case MSTRFIP_BA_APER_MSG_WIND:
			/*
			 * TODO: check if enough time to schedule at least one
			 * message
			 */
			instr = "APER_MSG_WIND";
			ticks = mc->instrlist[i].aper_msg_wind.ticks_end_time;
			break;
		case MSTRFIP_BA_APER_VAR_WIND:
			instr = "APER_VAR_WIND";
			ticks = mc->instrlist[i].aper_var_wind.ticks_end_time;
			break;
		case MSTRFIP_BA_WAIT_WIND:
			instr = "WAIT_WIND";
			ticks = mc->instrlist[i].wait_wind.ticks_end_time;
			break;
		default:
			continue; /* should never happen */
			break;
		}
		/*
		 * the current cycle duration should be less than the
		 * end time of the aperiodic window or the wait window
		 */
		us_endtime = ticks_to_us(ticks, cpu_hz);
		/* Last wait macro cycle wait window should last a min of 20us  */
		margin = (i == (mc->instr_count - 1)) ? MSTRFIP_LAST_WAIT_WIND_US_DURATION : 0;
		if (cycle_uslength > (us_endtime - margin) ) {
			/*
			 * inconsistent BA: the current cycle length
			 * exceeds the end of ba-wait or aperiodic
			 * window
			 */
			snprintf(mstrfip_errors[MSTRFIP_BA_INVALID_MACROCYCLE
						- MSTRFIP_ERROR_CODE_OFFSET],
				MSTRFIP_DYNAMIC_ERR_MSG_SZ,
				 "Wrong macro cycle: periodic traffic overrun the %s window: periodic ends at %dus and %s ends at %dus\n",
				instr, cycle_uslength + margin, instr,
				us_endtime);
			errno = MSTRFIP_BA_INVALID_MACROCYCLE;
			return -1;
		}
		cycle_uslength = us_endtime;
	}
	mc->comp_cycle_ustime = cycle_uslength;
	if (mc->cycle_ustime < cycle_uslength) {
		snprintf(mstrfip_errors[MSTRFIP_BA_INVALID_MACROCYCLE
					- MSTRFIP_ERROR_CODE_OFFSET],
			MSTRFIP_DYNAMIC_ERR_MSG_SZ,
			 "Mismatch between requested macro cycle length %dus and computed length: %dus\n",
			mc->cycle_ustime, cycle_uslength);
		errno = MSTRFIP_BA_INVALID_MACROCYCLE;
		return -1;
	}
	return 0;
}

int mstrfip_ba_tr_chk(struct mstrfip_desc *mstrfip, uint32_t tr_nstime)
{
	uint32_t tr_min_ns = mstrfip_tr_min_nstime[mstrfip->ba.bitrate];
	uint32_t tr_max_ns = mstrfip_tr_max_nstime[mstrfip->ba.bitrate];

	if (tr_nstime < tr_min_ns || tr_nstime > tr_max_ns)
		return 1;
	return 0;
}
