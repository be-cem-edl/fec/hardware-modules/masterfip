/*
 * Copyright (C) 2016-2017 CERN (www.cern.ch)
 * Author: Michel Arruat <michel.arruat@cern.ch>
 *
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#include <errno.h>

#include <mockturtle/libmockturtle.h>

#include "libmasterfip-priv.h"
#include "libmasterfip.h"

/**
 * It validates the answer of a synchronous message
 */
int mstrfip_validate_acknowledge(struct trtl_msg *msg)
{
	if (msg->hdr.len < 2) {
		errno = MSTRFIP_INVALID_ANSWER_ACK;
		return -1;
	}
	switch (msg->data[0]) {
	case MSTRFIP_REP_ACK:
		return 0;
	case MSTRFIP_REP_NACK:
		//TODO: to be reviewed
		//data[2] is supposed to contain a clear error code
		return (msg->hdr.len >= 3) ? msg->data[2] : -1;
	default:
		errno = MSTRFIP_INVALID_ANSWER_ACK;
		return -1;
	}
}


/**
 * masterfip wrapper to trtl_msg_async_send.
 * It sends only to MSTRFIP_CPU1 - MSTRFIP_HMQ_IO_CPU1_CMD
 * @param[in] mstrfip masterfip device
 * @param[in] mockturtle message to be sent
 * @return 0 on success, -1 on error and errno is set appropriately
 */
int mstrfip_trtl_msg_async_send(struct mstrfip_desc *mstrfip, struct trtl_msg *msg)
{
	const int msg_count = 1;
	int res;

	res = trtl_msg_async_send(mstrfip->trtl,
				  MSTRFIP_CPU1, MSTRFIP_HMQ_IO_CPU1_CMD,
				  msg, msg_count);

	if (res < 0)
		return res;
	if (res == 0 || res > msg_count) {
		errno = EIO;
		return -1;
	}

	return 0;
}
