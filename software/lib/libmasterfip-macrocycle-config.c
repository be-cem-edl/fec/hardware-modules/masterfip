/*
 * Copyright (C) 2016-2017 CERN (www.cern.ch)
 * Author: Michel Arruat <michel.arruat@cern.ch>
 *
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#include <stdint.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <poll.h>
#include <pthread.h>

#include <mockturtle/libmockturtle.h>

#include "libmasterfip-priv.h"
#include "masterfip-common-priv.h"
#include "libmasterfip.h"


#define MSTRFIP_VARLIST_INC_SZ 50 /* varlist increment size */

/**
 * It appends a given data to a given buffer
 * @param[in|out] buf buffer to use
 * @param[in] data data to append
 *
 * The function take care of re-allocating the buffer memory when necessary
 *
 * @return 0 on success, -1 on error and errno is appropriately set
 */
static int mstrfip_macrocycle_data_append(struct mstrfip_data_buf *buf,
					  struct mstrfip_data *data)
{
	struct mstrfip_data **tmp_msglist;

	/* store the pointer to the new fip-var into varlist */
	if (buf->count == buf->size) { /* no more space */
		/*
		 * to mimimize the number of realloc, varlist array is allocated
		 * by batchs of MSTRFIP_VARLIST_INC_SZ. (Even if we allocate
		 * more than necessary, msglist it's just a collection of
		 * pointers to aperiodic msg).
		 */
		tmp_msglist = realloc(buf->list,
				(buf->size + MSTRFIP_VARLIST_INC_SZ) *
				sizeof(struct mstrfip_data *));
		if (tmp_msglist == NULL)
			return -1;
		/* no error, keep the new pointer update varlist size */
		buf->list = tmp_msglist;
		buf->size += MSTRFIP_VARLIST_INC_SZ;
	}
	buf->list[buf->count] = data;
	++buf->count;

	return 0;
}

/**
 * Reset macrocycle by deleting all objects dynamically allocated.
 * If the macrocycle is the one in used stop the macrocycle execution in
 * mockturtle and chnage is state to INITIAL saying that it waits for a new
 * configuration.
 * @param[in] dev device token
 * @param[in] mcycle pointer to the macrocycle object.
 * @return 0 on success, -1 on error and errno is set appropriately
 */
int mstrfip_macrocycle_reset(struct mstrfip_dev *dev,
			struct mstrfip_macrocycle *mcycle)
{
	struct mstrfip_desc* mstrfip = (struct mstrfip_desc *)dev;
	struct mstrfip_ba_macrocycle *mc = (struct mstrfip_ba_macrocycle *)mcycle;
	int i, res = 0;

	if (mstrfip->ba.mcycle == mc &&
		mstrfip->ba.state == MSTRFIP_FSM_RUNNING)
		//TODO return correct error code
		return -1;
	free(mc->instrlist);
	free(mc->per_vars.cons_varlist);
	free(mc->per_vars.prod_varlist);
	free(mc->per_vars.sorted_varlist);
	free(mc->per_vars.trtl.msglist);
	/* free periodic variables */
	for (i = 0; i < mc->per_vars.var.count; ++i) {
		free(mc->per_vars.var.list[i]->priv);
		free(mc->per_vars.var.list[i]->buffer);
		free(mc->per_vars.var.list[i]);
	}
	free(mc->per_vars.var.list);
	mc->state = MSTRFIP_MACROCYCLE_INITIAL;
	mstrfip->ba.mcycle = NULL;

	/* last but least check if it was the macro cycle currently loaded */
	if (mstrfip->ba.mcycle == mc && mstrfip->ba.state == MSTRFIP_FSM_READY) {
		res = mstrfip_ba_reset(dev); // reset ba in mturtle
		mstrfip->ba.state = MSTRFIP_FSM_INITIAL;
	}
	return res;
}

/**
 * Delete macrocycle: calls macrocycle_reset and delete the object itself from
 * the macrocycle table.
 * @param[in] dev device token
 * @param[in] mcycle pointer to the macrocycle object.
 * @return 0 on success, -1 on error and errno is set appropriately
 */
int mstrfip_macrocycle_delete(struct mstrfip_dev *dev,
			struct mstrfip_macrocycle *mcycle)
{
	struct mstrfip_desc* mstrfip = (struct mstrfip_desc *)dev;
	struct mstrfip_ba_macrocycle *mc = (struct mstrfip_ba_macrocycle *)mcycle;
	int i, j, res;

	res = mstrfip_macrocycle_reset(dev, mcycle);
	if (res)
		return res;

	/* remove and pack remaining cycles in mcycle_list */
	for (i = 0, j = 0; i < mstrfip->mcycle_count; ++i) {
		if (mstrfip->mcycle_list[i] != mc)
			mstrfip->mcycle_list[j++] = mstrfip->mcycle_list[i];
	}
	--mstrfip->mcycle_count;
	/*clean last entry in the list */
	mstrfip->mcycle_list[mstrfip->mcycle_count] = NULL;
	free(mc);
	return 0;
}

/**
 * Frre all resources
 * @param[in] mstrfip device token
 */
void mstrfip_ba_free(struct mstrfip_desc *mstrfip)
{
	int i;

	for (i = 0; i < mstrfip->mcycle_count; ++i)
		mstrfip_macrocycle_delete((struct mstrfip_dev *)mstrfip,
			(struct mstrfip_macrocycle *)mstrfip->mcycle_list[i]);
	free(mstrfip->mcycle_list);
}

/**
 * Check if the given macrocycle is valid.
 * @param[in] dev device token
 * @param[in] mcycle pointer to the macrocycle object.
 * @return 1 if it is valid, 0 for an invalid macrocycle
 */
int mstrfip_macrocycle_isvalid( struct mstrfip_dev *dev,
					struct mstrfip_macrocycle *mcycle)
{
	struct mstrfip_desc* mstrfip = (struct mstrfip_desc *)dev;
	struct mstrfip_ba_macrocycle *mc = (struct mstrfip_ba_macrocycle *)mcycle;

	return mstrfip_mcycle_check_and_prepare(mstrfip, mc);
}

/**
 * Create macrocycle object.
 * @param[in] dev device token
 * @return a pointer to the new object on success, NULL on error and errno
 * 	is set appropriately
 */
struct mstrfip_macrocycle *mstrfip_macrocycle_create(struct mstrfip_dev *dev)
{
	struct mstrfip_desc* mstrfip = (struct mstrfip_desc *)dev;
	struct mstrfip_ba_macrocycle **tmplist;
	struct mstrfip_ba_macrocycle *mcycle;

	if (mstrfip->mcycle_list_size == mstrfip->mcycle_count) {
		/* Request to append a new macrocycle */
		tmplist = realloc(mstrfip->mcycle_list, (mstrfip->mcycle_list_size + 1) *
					sizeof(struct mstrfip_ba_macrocycle *));
		if (tmplist == NULL)
			return NULL;
		mstrfip->mcycle_list = tmplist;
		++mstrfip->mcycle_list_size;
	}
	/* allocate a new macrocycle */
	mcycle = calloc(1, sizeof(struct mstrfip_ba_macrocycle));
	if (!mcycle)
		return NULL;
	mcycle->dev = mstrfip;
	mstrfip->mcycle_list[mstrfip->mcycle_count] = mcycle;
	++mstrfip->mcycle_count;
	return (struct mstrfip_macrocycle *)mcycle;
}

/**
 * mstrfip_data factory method.
 */
static struct mstrfip_data *mstrfip_data_create(
			struct mstrfip_data_cfg *var_cfg, int domain)
{
	struct mstrfip_data *pvar;
	struct mstrfip_data_priv *pvar_priv;

	pvar = calloc(1, sizeof(struct mstrfip_data));
	if (!pvar)
		return NULL;
	/* allocate the private object and link it to the mstrfip data object */
	pvar_priv = calloc(1, sizeof(struct mstrfip_data_priv));
	if (!pvar_priv)
		goto fail1;
	pvar->priv = pvar_priv;

	pvar_priv->hdr.id = (uint16_t)var_cfg->id;
	pvar_priv->hdr.flags = (uint8_t)var_cfg->flags;
	pvar_priv->hdr.max_bsz = var_cfg->max_bsz;
	/* assign mq slot depending of the variable's domain */
	pvar_priv->hdr.mq_slot = (domain == MSTRFIP_APP_DOMAIN) ?
		MSTRFIP_HMQ_O_APP_PER_VAR : MSTRFIP_HMQ_O_DIAG_PER_VAR;
	pvar_priv->mstrfip_data_handler = var_cfg->mstrfip_data_handler;
	/* append IRQ_FLAG if an handler has been registered */
	if (pvar_priv->mstrfip_data_handler != NULL)
		pvar_priv->hdr.flags |= MSTRFIP_DATA_FLAGS_IRQ;
	/*
	 * Public part: nbytes gives the current payload size
	 * Periodic variable have a fix payload size, so by default nbytes is
	 * set with max_bsz
	 */
	pvar->bsz = pvar_priv->hdr.max_bsz;
	pvar->id = var_cfg->id;
	/* allocate the buffer */
	pvar->buffer = calloc(1, pvar_priv->hdr.max_bsz);
	if (!pvar->buffer)
		goto fail2;

	return pvar;

fail2:
	free(pvar->priv);
fail1:
	free(pvar);
	return NULL;
}

/**
 * Create a periodic variable using the given variable configuration and
 * assigned it to the given macrocycle.
 * @param[in] macrocycle pointer
 * @param[in] variable configuration
 * @return a pointer to the new object on success, NULL on error
 * 		and errno is set appropriately.
 */
struct mstrfip_data *mstrfip_var_create(struct mstrfip_macrocycle *macrocycle,
				struct mstrfip_data_cfg *var_cfg) {
	struct mstrfip_ba_macrocycle * mc =
				(struct mstrfip_ba_macrocycle *)macrocycle;
	struct mstrfip_ba_per_var_set *per_vars = &mc->per_vars;
	struct mstrfip_data *pvar;
	int err;

	/*TODO: calls mstrfip_is_action_allowed with action = CREATE_VAR*/
//	if (mc->state != MSTRFIP_MACROCYCLE_INITIAL)
//		// TODO return appropriate error code
//		// errno = MSTRFIP_BA_NOT_STOP_STATE
//		return NULL;
	/* TODO: check the periodic variable length */
//	if (var_cfg->nbyte > MSTRFIP_VAR_MAX_SZ) {
//		errno = MSTRFIP_BA_NOT_STOP_STATE;
//		return NULL;
//	}

	pvar = mstrfip_data_create(var_cfg, MSTRFIP_APP_DOMAIN);
	if (pvar == NULL)
		return NULL;

	err = mstrfip_macrocycle_data_append(&per_vars->var, pvar);
	if (err)
		goto fail;

	if (var_cfg->mstrfip_data_handler != NULL) {
		/* callback is registered: raise flag used later to start irq_thread */
		mc->has_registered_cb = 1;
	}
	return pvar;
fail:
	free(pvar->priv);
	free(pvar);
	return NULL;
}

/**
 * Create an aperiodic identification variable using the given
 * variable configuration and assigned it to the given macrocycle.
 * @param[in] macrocycle pointer
 * @param[in] variable configuration
 * @return a pointer to the new object on success, NULL on error
 * 		and errno is set appropriately.
 */
struct mstrfip_data *mstrfip_ident_var_create(
		struct mstrfip_macrocycle *macrocycle, uint32_t agent_addr)
{
	struct mstrfip_ba_macrocycle * mc =
				(struct mstrfip_ba_macrocycle *)macrocycle;
	struct mstrfip_ba_aper_var_set *aper_vars = &mc->aper_vars;
	struct mstrfip_data *p_ident_var;
	struct mstrfip_data_cfg ident_cfg;
	struct mstrfip_data_priv *pvar_priv;
	int err;

	/*TODO: calls mstrfip_is_action_allowed with action = CREATE_VAR*/

	/* create identification variable */
	ident_cfg.max_bsz = MSTRFIP_IDENT_VAR_DATA_BSZ;
	ident_cfg.id = MSTRFIP_AGENT_IDENT_VAR_ID | (0xFF & agent_addr);
	ident_cfg.flags = MSTRFIP_DATA_FLAGS_CONS;
	ident_cfg.mstrfip_data_handler = NULL;
	p_ident_var = mstrfip_data_create(&ident_cfg, MSTRFIP_APP_DOMAIN);
	if (p_ident_var == NULL)
		return NULL;
	/* set key field: for ident var, key is the agent address */
	pvar_priv = (struct mstrfip_data_priv *)p_ident_var->priv;
	pvar_priv->hdr.key = agent_addr;

	err = mstrfip_macrocycle_data_append(&aper_vars->ident, p_ident_var);
	if (err)
		goto fail;

	return p_ident_var;
fail:
	free(p_ident_var->priv);
	free(p_ident_var);
	return NULL;
}

/**
 * Create an aperiodic message using the given
 * configuration and assigned it to the given macrocycle.
 * @param[in] macrocycle pointer
 * @param[in] message configuration
 * @return a pointer to the new object on success, NULL on error
 * 		and errno is set appropriately.
 */
struct mstrfip_data *mstrfip_msg_create(struct mstrfip_macrocycle *macrocycle,
					struct mstrfip_data_cfg *msg_cfg)
{
	struct mstrfip_ba_macrocycle * mc =
				(struct mstrfip_ba_macrocycle *)macrocycle;
	struct mstrfip_ba_aper_msg_set *aper_msgs = &mc->aper_msgs;
	struct mstrfip_data *pmsg;
	struct mstrfip_data_priv *pmsg_priv;
	int err;

	/*TODO: calls mstrfip_is_action_allowed with action = CREATE_MSG*/
//	if (ba->state != MSTRFIP_FSM_INITIAL) {
//		errno = MSTRFIP_BA_NOT_STOP_STATE;
//		return NULL; /* not allowed */
//	}
      	/* TODO: check the aperiodic message length */
//	if (msg_cfg->nbyte > MSTRFIP_MSG_MAX_SZ) {
//		errno = MSTRFIP_BA_NOT_STOP_STATE;
//		return NULL;
//	}

	pmsg = calloc(1, sizeof(struct mstrfip_data));
	if (!pmsg)
		return NULL;
	/* allocate the private object and link it to the mstrfip data object */
	pmsg_priv = calloc(1, sizeof(struct mstrfip_data_priv));
	if (!pmsg_priv)
		goto fail1;
	pmsg->priv = pmsg_priv;

	pmsg_priv->hdr.id = (uint16_t)msg_cfg->id;
	/* for fip msg key is the msg id */
	pmsg_priv->hdr.key = pmsg_priv->hdr.id;
	pmsg_priv->hdr.flags = (uint8_t)msg_cfg->flags;
	pmsg_priv->hdr.max_bsz = msg_cfg->max_bsz;
	/* assign mq slot */
	pmsg_priv->hdr.mq_slot = MSTRFIP_HMQ_O_APP_APER_MSG;
	pmsg_priv->mstrfip_data_handler = msg_cfg->mstrfip_data_handler;
	/* append IRQ_FLAG if an handler has been registered */
	if (pmsg_priv->mstrfip_data_handler != NULL) {
		pmsg_priv->hdr.flags |= MSTRFIP_DATA_FLAGS_IRQ;
		/* callback is registered: raise flag used later to start irq_thread */
		mc->has_registered_cb = 1;
	}

	/* Public part: nbytes gives the current payload size */
	pmsg->bsz = 0;
	pmsg->id = msg_cfg->id;
	/* allocate the buffer */
	pmsg->buffer = calloc(1, pmsg_priv->hdr.max_bsz);
	if (!pmsg->buffer)
		goto fail2;

	if (pmsg_priv->hdr.flags & MSTRFIP_DATA_FLAGS_CONS)
		err = mstrfip_macrocycle_data_append(&aper_msgs->cons, pmsg);
	else
		err = mstrfip_macrocycle_data_append(&aper_msgs->prod, pmsg);

	if (err)
		goto fail2;


	return pmsg;

fail2:
	free(pmsg->priv);
fail1:
	free(pmsg);
	return NULL;
}

#define MSTRFIP_INSTRLIST_INC_SZ 5 /* instrlist increment size */
static int mstrfip_inc_instrlist_sz(struct mstrfip_ba_macrocycle *mcycle,
								int instr_count)
{
	struct mstrfip_ba_instr *tmp_instrlist;

	if (mcycle->instr_count + instr_count > mcycle->instrlist_size) { /* no more space */
		/*
		 * to mimimize the number of realloc instrlist array is allocated
		 * by increments of MSTRFIP_INSTRLIST_INC_SZ. (Even if we allocate
		 * more than necessary, instrlist is just a collection of
		 * instructions which is a few bytes size).
		 */
		tmp_instrlist = realloc(mcycle->instrlist,
			(mcycle->instrlist_size + MSTRFIP_INSTRLIST_INC_SZ) *
			sizeof(struct mstrfip_ba_instr));
		if (tmp_instrlist == NULL)
			return -1;
		/* no error, update instruction list size */
		mcycle->instrlist = tmp_instrlist;
		mcycle->instrlist_size += MSTRFIP_INSTRLIST_INC_SZ;
	}
	return 0;
}

/**
 * Append a periodic variable window in the macro cycle
 * using the given configuration
 * @param[in] macrocycle pointer to the given macrocyle
 * @param[in] cfg periodic window configuration
 * @return 0 on success, -1 on error and errno is set appropriately
 */
int mstrfip_per_var_wind_append(struct mstrfip_macrocycle *macrocycle,
		struct mstrfip_per_var_wind_cfg * cfg)
{
	struct mstrfip_ba_macrocycle *mcycle =
			(struct mstrfip_ba_macrocycle *)macrocycle;
	struct mstrfip_ba_per_var_set *per_vars = &mcycle->per_vars;
	struct mstrfip_ba_instr* instr;
	struct mstrfip_data **tmp_varlist;
	int i, res;

	/* sanity check before accepting the config */
	/* macrocycle state */
	if (mcycle->state != MSTRFIP_MACROCYCLE_INITIAL) {
		errno = MSTRFIP_BA_NOT_STOP_STATE;
		return -1; /* not allowed */
	}

	/* let's try first to allocate space */
	/* Increase the instruction list array if necessary */
	res = mstrfip_inc_instrlist_sz(mcycle, 1);
	if (res)
		return res;
	tmp_varlist = realloc(per_vars->sorted_varlist,
				(per_vars->sorted_var_count + cfg->var_count) *
				sizeof(struct mstrfip_data *));
	if (tmp_varlist == NULL)
		/*
		 * even if the instrlist has been reallocated we keep it ready
		 * to receive new instruction if the system recovers
		 */
		return -1;
	per_vars->sorted_varlist = tmp_varlist;

	/* all allocation has been done successfuly */
	/* append the new list of periodic var into varlist */
	memcpy(&(per_vars->sorted_varlist[per_vars->sorted_var_count]),
		cfg->varlist, cfg->var_count * sizeof(struct mstrfip_data *));
	/* keep up-to-date consumed and produced var count*/
	for (i = per_vars->sorted_var_count;
	     i < per_vars->sorted_var_count + cfg->var_count; ++i) {
		struct mstrfip_data_priv *pvar_priv = (struct mstrfip_data_priv *)per_vars->sorted_varlist[i]->priv;

		if (pvar_priv->hdr.flags & MSTRFIP_DATA_FLAGS_CONS)
			++(per_vars->cons_var_count);
		else
			++(per_vars->prod_var_count);
	}
	/*
	 * append the new ba instruction in the instrlist: BA_SEND_LIST
	 * arg1, arg2 are start and stop index in sorted varlist
	 */
	instr = &mcycle->instrlist[mcycle->instr_count];
	instr->code = MSTRFIP_BA_PER_VAR_WIND;
	/* start index in sorted var list */
	instr->per_var_wind.start_var_idx = per_vars->sorted_var_count;
	/* update sorted_var_count */
	per_vars->sorted_var_count += cfg->var_count;
	/* stop index in sorted varlist */
	instr->per_var_wind.stop_var_idx = per_vars->sorted_var_count;

	++(mcycle->instr_count); /* increment instruction count */

	return 0;
}

/*
 * Requesting diagnostic implies some extra traffic on the bus:
 * 1) Periodic traffic:
 * 	a periodic window scheduling the two variables of the fip diag
 * 	is inserted in the macro-cycle to ensure a good survey of the
 * 	electrical quality of the bus.
 * 2) Aperiodic traffic:
 * 	- Presence variables are scheduled during the aperiodic window to
 *	  keep uptodate the list of presence.
 * 	- Identification of the FIP diag is also schedule during the
 * 	  aperiodic window at a rate defined by diag_period in sw config.
 */
static int mstrfip_diag_per_wind_append(struct mstrfip_ba_macrocycle *mcycle)
{
	struct mstrfip_data_cfg var_cfg;
	struct mstrfip_data *pvar;
	struct mstrfip_per_var_wind_cfg pwind_cfg;

	/* Create periodic diag variables */
	var_cfg.max_bsz = MSTRFIP_DIAG_VAR_DATA_BSZ;
	var_cfg.id = MSTRFIP_DIAG_CONS_VAR_ID; /* consumed FIP Diag var */
	var_cfg.flags = MSTRFIP_DATA_FLAGS_CONS;
	var_cfg.mstrfip_data_handler = NULL;
	pvar = mstrfip_data_create(&var_cfg, MSTRFIP_DIAG_DOMAIN);
	if (pvar == NULL)
		return -1;
	mcycle->diag.varlist[0] = pvar;
	var_cfg.id = MSTRFIP_DIAG_PROD_VAR_ID; /* produced FIP Diag var */
	var_cfg.flags = MSTRFIP_DATA_FLAGS_PROD; /* IRQ flag*/
	var_cfg.mstrfip_data_handler = mstrfip_diag_var_handler;
	pvar = mstrfip_data_create(&var_cfg, MSTRFIP_DIAG_DOMAIN);
	if (pvar == NULL)
		return -1;
	mcycle->diag.varlist[1] = pvar;

	/* callback is registered: raise flag used later to start irq_thread */
	mcycle->has_registered_cb = 1;

	/* insert diag periodic window in the ba instruction set and register
	 * the diag var handler
	 */
	pwind_cfg.varlist = mcycle->diag.varlist;
	pwind_cfg.var_count = 2;
	/*
	 * we should check that the aperiodic window last at least 1ms to be
	 * sure that the periodic diag window can be scheduled like it it was
	 * SEND_LIST diag_var and SEND_APER time-time of send list diag
	 */
	return mstrfip_per_var_wind_append(
			(struct mstrfip_macrocycle *)mcycle, &pwind_cfg);
}

/**
 * Append an aperiodic variable window in the macro cycle
 * using the given configuration
 * @param[in] macrocycle pointer to the given macrocyle
 * @param[in] cfg aperiodic variable window configuration
 * @return 0 on success, -1 on error and errno is set appropriately
 */
int mstrfip_aper_var_wind_append(struct mstrfip_macrocycle *macrocycle,
		struct mstrfip_aper_var_wind_cfg * cfg)
{
	struct mstrfip_ba_macrocycle *mcycle =
			(struct mstrfip_ba_macrocycle *)macrocycle;
	struct mstrfip_ba_instr* instr;
	struct mstrfip_data_cfg ident_cfg;
	struct mstrfip_data *p_ident_var;
	uint32_t cpu_hz = trtl_config_get(mcycle->dev->trtl)->clock_freq;
	int res;

	/* sanity check before accepting the config */
	/* ba state */
	if (mcycle->state != MSTRFIP_MACROCYCLE_INITIAL) {
		errno = MSTRFIP_BA_NOT_STOP_STATE;
		return -1; /* not allowed */
	}

	/* let's try first to allocate space in the instr list*/
	/*
	 * Check if diagnostic is requested for the first time
	 */
	if (cfg->enable_diag && mcycle->diag.varlist[0] == NULL) {
		/*
		 * Scheduling diagnostic implies two ba instructions to be
		 * inserted(see comments in mstrfip_diag_per_wind_append() above).
		 */
		res = mstrfip_diag_per_wind_append(mcycle);
		if (res < 0)
			return res;
		/* create identification fip-diag variable */
		ident_cfg.max_bsz = MSTRFIP_IDENT_VAR_DATA_BSZ;
		ident_cfg.id = MSTRFIP_DIAG_IDENT_VAR_ID;
		ident_cfg.flags = MSTRFIP_DATA_FLAGS_CONS;
		ident_cfg.mstrfip_data_handler = NULL;
		p_ident_var = mstrfip_data_create(&ident_cfg,
						  MSTRFIP_DIAG_DOMAIN);
		if (p_ident_var == NULL)
			return -1;
		mcycle->diag.ident_var = p_ident_var;
		((struct mstrfip_data_priv *)p_ident_var->priv)->hdr.key =
							MSTRFIP_DIAG_ADDR;
		/* Allocate trtl msg buffer: diag requires a single message */
		mcycle->diag.trtl.msglist = calloc(1, sizeof(struct trtl_msg));
		mcycle->diag.trtl.msglist_size = 1;
		mcycle->diag.trtl.msg_count = 0;
		if (!mcycle->diag.trtl.msglist)
			return -1;
	}

	/* Increase the instruction list array if necessary */
	res = mstrfip_inc_instrlist_sz(mcycle, 1);
	if (res)
		return res;

	/* if app has set an identlist allocate trtl message buffer */
	if (cfg->ident_var_count) {
		/*
		 * TODO: optimize the max number of trtl message.
		 * Currently, max number of trtl messages equal to the number of
		 * agents. It's oversized because several ident variables are
		 * packed into a single trtl message, but knowing that a trtl
		 * message size is half Kb, * max number of agent (256) gives
		 * 128Kb of memory
		 */
		mcycle->aper_vars.trtl.msglist_size = cfg->ident_var_count;
		/* Allocate trtl msg buffer */
		mcycle->aper_vars.trtl.msglist =
				calloc(mcycle->aper_vars.trtl.msglist_size,
					sizeof(struct trtl_msg));
		if (!mcycle->aper_vars.trtl.msglist)
			return -1;
		mcycle->aper_vars.mstrfip_ident_var_handler =
						cfg->mstrfip_ident_var_handler;
		/* callback is registered: raise flag used later to start irq_thread */
		if (cfg->mstrfip_ident_var_handler != NULL)
			mcycle->has_registered_cb = 1;
	}

	/*
	 * append the new ba instruction in the instrlist: BA_SEND_APER
	 * arg2 is the end time in us relative to the start of the ba cycle.
	 */
	instr = &mcycle->instrlist[mcycle->instr_count];
	instr->code = MSTRFIP_BA_APER_VAR_WIND;
	instr->aper_var_wind.ticks_end_time = us_to_ticks(cfg->end_ustime,
							  cpu_hz);
	/* time to update instr count fields*/
	++(mcycle->instr_count); /* number of instruction */
	return 0;
}

/**
 * Append an aperiodic message window in the macro cycle
 * using the given configuration
 * @param[in] macrocycle pointer to the given macrocyle
 * @param[in] cfg aperiodic message window configuration
 * @return 0 on success, -1 on error and errno is set appropriately
 */
int mstrfip_aper_msg_wind_append(struct mstrfip_macrocycle *macrocycle,
		struct mstrfip_aper_msg_wind_cfg *cfg)
{
	struct mstrfip_ba_macrocycle *mcycle =
			(struct mstrfip_ba_macrocycle *)macrocycle;
	struct mstrfip_ba_instr* instr;
	uint32_t cpu_hz = trtl_config_get(mcycle->dev->trtl)->clock_freq;
	int res, max_sz, i ;

	/* sanity check before accepting the config */
	/* ba state */
	if (mcycle->state != MSTRFIP_MACROCYCLE_INITIAL) {
		errno = MSTRFIP_BA_NOT_STOP_STATE;
		return -1; /* not allowed */
	}

	/* let's try first to allocate space */
	/* Increase the instruction list array if necessary */
	res = mstrfip_inc_instrlist_sz(mcycle, 1);
	if (res)
		return res;
	/*
	 * append the new ba instruction in the instrlist: BA_APER_MSG_WIND
	 */
	instr = &mcycle->instrlist[mcycle->instr_count];
	instr->code = MSTRFIP_BA_APER_MSG_WIND;
	/* convert us time into cpu ticks given in ns */
	instr->aper_msg_wind.ticks_end_time = us_to_ticks(cfg->end_ustime,
							  cpu_hz);
	instr->aper_msg_wind.cons_msg_fifo_sz = cfg->cons_msg_fifo_size;
	instr->aper_msg_wind.prod_msg_fifo_sz = cfg->prod_msg_fifo_size;
	instr->aper_msg_wind.cons_msg_global_irq =
		(cfg->mstrfip_cons_msg_handler == NULL) ? 0 : 1;
	instr->aper_msg_wind.prod_msg_global_irq =
		(cfg->mstrfip_prod_msg_handler == NULL) ? 0 : 1;

	/* callback is registered: raise flag used later to start irq_thread */
	if (cfg->mstrfip_cons_msg_handler != NULL ||
		cfg->mstrfip_prod_msg_handler != NULL)
		mcycle->has_registered_cb = 1;

	/* produced message max size */
	for (i = 0, max_sz = 0; i < mcycle->aper_msgs.prod.count; ++i) {
		struct mstrfip_data_priv *pmsg_priv = (struct mstrfip_data_priv *)mcycle->aper_msgs.prod.list[i]->priv;

		max_sz = (max_sz < pmsg_priv->hdr.max_bsz) ?
					pmsg_priv->hdr.max_bsz : max_sz;
	}
	instr->aper_msg_wind.prod_msg_max_bsz = max_sz;

	++(mcycle->instr_count); /* increment instruction count */

	return 0;
}

/**
 * Append a wait window in the macro cycle using the given configuration.
 * Note: as the other windows, a wait window can be inserted at any place in the
 * macrocycle, but a macrocycle should always end with a wait window, in order
 * to know the macrocycle duration.
 * @param[in] macrocycle pointer to the given macrocyle
 * @param[in] cfg wait window configuration
 * @return 0 on success, -1 on error and errno is set appropriately
 */
int mstrfip_wait_wind_append(struct mstrfip_macrocycle *macrocycle,
		uint32_t silent_wait, uint32_t us_cycle_end)
{
	struct mstrfip_ba_macrocycle *mcycle =
			(struct mstrfip_ba_macrocycle *)macrocycle;
	struct mstrfip_ba_instr* instr;
	uint32_t cpu_hz = trtl_config_get(mcycle->dev->trtl)->clock_freq;
	int res;

	if (mcycle->state != MSTRFIP_MACROCYCLE_INITIAL) {
		errno = MSTRFIP_BA_NOT_STOP_STATE;
		return -1; /* not allowed */
	}
	/* let's try first to allocate space */
	/* Increase first the instruction list array if necessary */
	res = mstrfip_inc_instrlist_sz(mcycle, 1);
	if (res)
		return res;

	/* append the new ba instruction in the instrlist */
	instr = &mcycle->instrlist[mcycle->instr_count];
	instr->code = MSTRFIP_BA_WAIT_WIND;
	/* convert us time into cpu ticks given in ns */
	instr->wait_wind.ticks_end_time = us_to_ticks(us_cycle_end, cpu_hz);
	instr->wait_wind.is_silent = (silent_wait) ? 1 : 0;

	/* time to update instr count fields*/
	++(mcycle->instr_count); /* number of instruction */
	return 0;
}
