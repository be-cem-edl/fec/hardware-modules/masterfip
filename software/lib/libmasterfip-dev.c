/*
 * Copyright (C) 2016-2017 CERN (www.cern.ch)
 * Author: Michel Arruat <michel.arruat@cern.ch>
 *
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <pthread.h>
#include <libgen.h>
#include <glob.h>

#include <mockturtle/libmockturtle.h>

#include "masterfip-common-priv.h"
#include "libmasterfip-priv.h"
#include "libmasterfip.h"

/**
 * It initializes the mstrfip library. It must be called before doing
 * anything else. If you are going to load/unload mstrfip devices, then
 * you have to un-load (mstrfip_exit()) e reload (mstrfip_init()) the library.
 *
 * This library is based on the libmockturtle, so internally, this function also
 * run trtl_init() in order to initialize the trtl library.
 * @return 0 on success, otherwise -1 and errno is appropriately set
 */
int mstrfip_init()
{
	int err;

	mstrfip_error_init();

	err = trtl_init();
	if (err)
		return err;

	return 0;
}


/**
 * It releases the resources allocated by mstrfip_init(). It must be called when
 * you stop to use this library. Then, you cannot use functions from this
 * library.
 */
void mstrfip_exit()
{
	trtl_exit();
	mstrfip_error_free();
}


#define MSTRFIP_GLOB_PATTER "/sys/bus/platform/drivers/*fip-s[pv]ec/id:*.auto/mock-turtle.*.auto/mockturtle/trtl-*/"
/**
 * Return the number of available masterfips.
 *
 * @return the number of masterfip available
 */
unsigned int mstrfip_count(void)
{
	glob_t g;
	int err, count;

	err = glob(MSTRFIP_GLOB_PATTER, GLOB_NOSORT, NULL, &g);
	if (err)
		return 0;
	count = g.gl_pathc;
	globfree(&g);

	return count;
}

#define TRTL_MAX_NAME 16  // the device name is trtl-XXXX
/**
 * Allocate and return the list of available masterfip devices. The user is
 * in charge to free the allocated memory wit mstrfip_list_free(). The list
 * contains mstrfip_count() + 1 elements. The last element is a NULL pointer.
 * @return a list of TRTL device's names. NULL on error
 */
char **mstrfip_list(void)
{
	char **list = NULL;
	glob_t g;
	int err, i, count;

	err = glob(MSTRFIP_GLOB_PATTER, GLOB_NOSORT, NULL, &g);
	if (err)
		return NULL;
	count = g.gl_pathc;

	list = calloc((count + 1), sizeof(char *));
	if (!list)
		return NULL;
	for (i = 0; i < count; ++i)
		list[i] = strndup(basename(g.gl_pathv[i]),
				  TRTL_MAX_NAME);
	list[i] = NULL;


	globfree(&g);

	return list;
}

/**
 * Release the list allocated memory
 * @param[in] list device list to release
 */
void mstrfip_list_free(char **list)
{
	int i;

	for (i = 0; list[i]; i++)
		free(list[i]);
	free(list);
}

/**
 * Return the masterfip LUN from its ID
 *
 * @param[in] a masterfip (mockturtle) ID
 *
 * @return the masterfip LUN associated with the given ID on success, -1 on
 * error and errno is set appropriately.
 */
int mstrfip_id_to_lun(uint32_t devid, unsigned int *lun)
{
	glob_t g;
	int i, ret;

	ret = glob("/dev/masterfip.*", GLOB_NOSORT, NULL, &g);
	if (ret) {
		errno = ENODEV;
		return -1;
	}

	*lun = -1;
	for (i = 0; i < g.gl_pathc && *lun == -1; ++i) {
		char trtl_path[32];
		char trtl_dev[32];

		ret = readlink(g.gl_pathv[i], trtl_path, sizeof(trtl_path));
		if (ret < 0)
			goto out;
		ret = snprintf(trtl_dev, sizeof(trtl_dev), "trtl-%04x", devid);
		if (ret < 0)
			goto out;
		ret = strncmp(basename(trtl_path), trtl_dev, strlen(trtl_dev));
		if (ret)
			continue;

		ret = sscanf(g.gl_pathv[i], "/dev/masterfip.%u", lun);
		if (ret == 1) {
			ret = 0;
			goto out;
		}
	}

out:
	globfree(&g);

	return ret;
}


int mstrfip_lun_from_fmc_devid(uint32_t device_id, int *lun)
{
	char path[32], dev_id_str[4];
	uint32_t dev_id;
	int i;

	for (i = 0; i < 10; ++i) {
		int ret = snprintf(path, sizeof(path), "/dev/trtl.%d", i);
		if (ret < 0 || ret >= sizeof(path)) {
			errno = EINVAL;
			return -1;
		}
		ret = readlink(path, dev_id_str, sizeof(dev_id_str));
		if (ret < 0)
			continue;
		if (sscanf(dev_id_str, "%4x", &dev_id) != 1) {
			errno = ENODEV;
			return -1;
		}
		if (dev_id == device_id) {
			*lun = i;
			return 0;
		}
	}
	return -1;
}

/**
 * Open a mstrfip device node using the mockturtle ID
 * @param[in] device_id mockturtle device identificator
 * @return It returns an anonymous mstrfip_dev structure on success.
 *         On error, NULL is returned, and errno is set appropriately.
 */
struct mstrfip_dev *mstrfip_open_by_id(uint32_t device_id)
{
	struct mstrfip_desc *mstrfip;
	int res;

	mstrfip = malloc(sizeof(struct mstrfip_desc));
	if (!mstrfip)
		return NULL;
	memset(mstrfip, 0, sizeof(*mstrfip));

	/*
	 * Constraint imposed by existing diagnostic tools: FIP diagnostic
	 * tools are based on SHM associated with a key. To distinguish between
	 * several masterFIP boards the key is computed in accordance with the
	 * lun, that is to say: key = fix-number + lun.
	 * Therefore when teh device is open using its device id whe have to
	 * retrieve the corresponding lun.
	 */
	res = mstrfip_id_to_lun(device_id, &mstrfip->lun);
	if (res)
		goto err_id_to_lun;

	/*
	 * Reference version for rt app. and fpga are injected at compile time
	 * and compred to ones get from HW to check any mismatch
	 * In case of mismatch the device is not opened.
	 */
	mstrfip->ref_rt_version = MFIP_RT_VERSION;
	mstrfip->ref_fpga_version = MFIP_FPGA_VERSION;

	mstrfip->ba.bitrate = MSTRFIP_BITRATE_UNDEFINED;


	/* open trtl device */
	mstrfip->trtl = trtl_open_by_id(device_id);
	if (!mstrfip->trtl)
		goto err_trtl;

	return (struct mstrfip_dev *)mstrfip;

err_trtl:
err_id_to_lun:
	free(mstrfip);

	return NULL;
}

/**
 * Open a mstrfip device node using LUN
 * @param[in] lun an integer argument to select the device or
 *            negative number to take the first one found.
 * @return It returns an anonymous mstrfip_dev structure on success.
 *         On error, NULL is returned, and errno is set appropriately.
 */
struct mstrfip_dev *mstrfip_open_by_lun(uint32_t lun)
{
	char path[32], dev_id_str[32];
	uint32_t dev_id;
	int ret;

	ret = snprintf(path, sizeof(path), "/dev/masterfip.%u", lun);
	if (ret < 0 || ret >= sizeof(path)) {
		errno = EINVAL;
		return NULL;
	}
	ret = readlink(path, dev_id_str, sizeof(dev_id_str));
	if (ret < 0)
		return NULL;
	dev_id_str[ret]='\0';
	if (sscanf(dev_id_str, "/dev/mockturtle/trtl-%4x", &dev_id) != 1) {
		errno = ENODEV;
		return NULL;
	}

	return mstrfip_open_by_id(dev_id);
}

/**
 * It closes a mstrfip device opened with one of the following function:
 * mstrfip_open_by_lun(), mstrfip_open_by_id()
 * @param[in] dev device token
 */
void mstrfip_close(struct mstrfip_dev *dev)
{
	struct mstrfip_desc *mstrfip = (struct mstrfip_desc *)dev;

	/* first stop MFIP bus and reset CPUs*/
	mstrfip_ba_stop(dev);
	mstrfip_ba_reset(dev);
	mstrfip_diag_stop(dev);

	mstrfip_rtapp_reset(dev);

	/* free all internal buffers */
	mstrfip_ba_free(mstrfip);


	/* close mockturtle device */
	trtl_close(mstrfip->trtl);

	free(mstrfip);
}


/**
 * It returns the trtl token in order to allows users to run
 * functions from the trtl library
 * @param[in] dev device token
 * @return the trtl token
 */
struct trtl_dev *mstrfip_get_trtl_dev(struct mstrfip_dev *dev)
{
	struct mstrfip_desc *mstrfip = (struct mstrfip_desc *)dev;

	return (struct trtl_dev *)mstrfip->trtl;
}
