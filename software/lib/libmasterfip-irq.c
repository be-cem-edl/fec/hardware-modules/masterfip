/*
 * Copyright (C) 2016-2017 CERN (www.cern.ch)
 * Author: Michel Arruat <michel.arruat@cern.ch>
 *
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#include <stdint.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <pthread.h>
#include <sched.h>
#include <signal.h>
#include <sys/signalfd.h>
#include <poll.h>
#include <sys/types.h>

#include <mockturtle/libmockturtle.h>

#include "libmasterfip-priv.h"
#include "libmasterfip.h"

/**
 * masterFIP IRQ handler type
 */
typedef int (*mstrfip_irq_handler_t)(struct mstrfip_desc *mstrfip,
				     struct mstrfip_irq_entry_desc *irq_entry,
				     struct mstrfip_irq *irq);

/**
 * It returns the number of messages in a masterFIP Mock Turtle buffer
 * @param[in] trtl masterFIP Mock turtle buffer
 * @return The total number of messages in the buffer
 */
static unsigned int mstrfip_trtl_buf_msg_counter(struct mstrfip_trtl_buf *trtl)
{
	unsigned int count = 0;
	int i;

	for (i = 0; i < trtl->msg_count; ++i) {
		struct mstrfip_acq_trtlmsg_desc *acq_trtlmsg = (struct mstrfip_acq_trtlmsg_desc *)trtl->msglist[i].data;

		count += acq_trtlmsg->nentries;
	}

	return count;
}

static void mstrfip_handle_error(struct mstrfip_desc *mstrfip, int errcode)
{
	if (mstrfip->ba.sw_cfg.mstrfip_error_handler == NULL)
		return; /* client didn't specify an error handler */
	mstrfip->ba.sw_cfg.mstrfip_error_handler((struct mstrfip_dev *)mstrfip,
						errcode);
}

static void mstrfip_get_irq_entry(struct mstrfip_desc *mstrfip,
				  struct mstrfip_trtl_buf *trtl,
				  struct mstrfip_irq_entry_desc **irq_entry,
				  struct mstrfip_irq *irq)
{
	uint32_t cpu_hz = trtl_config_get(mstrfip->trtl)->clock_freq;
	struct mstrfip_acq_trtlmsg_desc *acq_trtlmsg;
	struct trtl_msg *trtlmsg;

	trtlmsg = &trtl->msglist[trtl->msg_count - 1];
	acq_trtlmsg = (struct mstrfip_acq_trtlmsg_desc *)trtlmsg->data;
	/* Check if this message carries an irq */
	if (acq_trtlmsg->irq_woffset == 0) {
		*irq_entry = NULL; // No irq entry found: set it to NULL
		return; // keep the message till an irq is raised
	}
	// the message carries an irq: points to the irq entry
	*irq_entry = (struct mstrfip_irq_entry_desc *)
			((uint32_t *)acq_trtlmsg + acq_trtlmsg->irq_woffset);
	if (trtl->msg_count != (*irq_entry)->trtlmsg_count) {
		snprintf(mstrfip_errors[MSTRFIP_TRTL_MSG_COUNT_INCONSISTENCY
					- MSTRFIP_ERROR_CODE_OFFSET],
			 MSTRFIP_DYNAMIC_ERR_MSG_SZ,
			 "%s(): read %d mockturtle msg but %u was expected.",
			 __func__, trtl->msg_count, (*irq_entry)->trtlmsg_count);
		mstrfip_handle_error(mstrfip,
				     MSTRFIP_TRTL_MSG_COUNT_INCONSISTENCY);
	}
	irq->hw_sec = (*irq_entry)->hw_sec;
	/* HW CPU ticks are translated into ns */
	irq->hw_ns = ticks_to_ns((*irq_entry)->hw_ticks, cpu_hz);
	irq->irq_count = (*irq_entry)->irq_count;
}

static void  mstrfip_purge_trtl_hmq(struct mstrfip_desc *mstrfip)
{
	struct trtl_msg trtlmsg;
	int i, n;

	/*
	 * try to read a maximum of trtl messages in once: we don't know how
	 * many messages are accumulated in the driver, but we can't read more
	 * than the remaining space in the msglist
	 */
	for (i = 0; i < MSTRFIP_HMQ_ACQ_SLOT_COUNT; ++i) {
		do {
			n = trtl_msg_async_recv(mstrfip->trtl, 0 /*CPU_ID*/, i, &trtlmsg, 1);
		} while(n > 0);
	}
}

static int mstrfip_read_trtlmsg(struct mstrfip_desc *mstrfip, int mq_slot,
			struct mstrfip_trtl_buf *trtl)
{
	struct trtl_msg *trtlmsg;
	int res;

	if (trtl->msg_count == trtl->msglist_size) {
		snprintf(mstrfip_errors[MSTRFIP_TRTL_MSG_COUNT_OVERFLOW
					- MSTRFIP_ERROR_CODE_OFFSET],
			 MSTRFIP_DYNAMIC_ERR_MSG_SZ,
			 "%s(): Too much mockturtle msg accumulated due "
			 "probably to an absence of irq from mockturtle rt-app.",
			 __func__);
		mstrfip_handle_error(mstrfip,
				     MSTRFIP_TRTL_MSG_COUNT_OVERFLOW);
		return -1;
	}

	trtlmsg = &trtl->msglist[trtl->msg_count];
	/*
	 * try to read a maximum of trtl messages in once: we don't know how
	 * many messages are accumulated in the driver, but we can't read more
	 * than the remaining space in the msglist
	 * FIX introduces in LIB version 1.1.3
	 * Read one message at once to avoid packing of two messages containing
	 * both an irq entry. When interrupt rate is very high (interrupt on
	 * each variable for instance), mockturtle drive starts to accumulate
	 * messages and receive_n function can return several messages.
	 * To avoid this problem messages are get one by one.
	 */
	res = trtl_msg_async_recv(mstrfip->trtl, MSTRFIP_CPU0, mq_slot, trtlmsg, 1);

	if (res == -1) {
		snprintf(mstrfip_errors[MSTRFIP_TRTL_MSG_READ_ERR
					- MSTRFIP_ERROR_CODE_OFFSET],
			 MSTRFIP_DYNAMIC_ERR_MSG_SZ,
			 "%s(), trtl_hmq_receive_n() failed with: %s.",
			 __func__, strerror(errno));
		mstrfip_handle_error(mstrfip,
				     MSTRFIP_TRTL_MSG_READ_ERR);
		return -1;
	}
	else if (res == 0) {
		snprintf(mstrfip_errors[MSTRFIP_TRTL_MSG_READ_NULL
					- MSTRFIP_ERROR_CODE_OFFSET],
			 MSTRFIP_DYNAMIC_ERR_MSG_SZ,
			 "%s(), trtl_hmq_receive_n() returned no msg.",
			 __func__);
		mstrfip_handle_error(mstrfip,
				     MSTRFIP_TRTL_MSG_READ_NULL);
		return -1;
	}
	/* no error, res is the number of trtl messages read */
	trtl->msg_count += res; // increment pending trtlmsg counter
	return 0;
}

static int mstrfip_handle_diag_per_var_irq(struct mstrfip_desc *mstrfip,
					   struct mstrfip_irq_entry_desc *irq_entry,
					   struct mstrfip_irq *irq)
{
	struct mstrfip_ba_diag *diag = &mstrfip->ba.mcycle->diag;
	struct mstrfip_data *pvar;

	irq->acq_count = 1; // diag var single acquisition
	/* locate the periodic var which raises the irq */
	pvar = (irq_entry->flags & MSTRFIP_DATA_FLAGS_PROD) ?
		diag->varlist[1] : diag->varlist[0];
	/* calls application's var handler */
	mstrfip_diag_var_handler((struct mstrfip_dev *)mstrfip, pvar, irq);
	/* trtlmsg have been consumed, reset trtlmsg count */
	diag->trtl.msg_count = 0;
	return 0;
}

static int mstrfip_handle_diag_aper_var_irq(struct mstrfip_desc *mstrfip,
					    struct mstrfip_irq_entry_desc *irq_entry,
					    struct mstrfip_irq *irq)
{
	struct mstrfip_ba_diag *diag = &mstrfip->ba.mcycle->diag;
	struct mstrfip_data *pvar = diag->ident_var;

	irq->acq_count = 1;// diag ident single acquisition
	/* calls application's var handler */
	mstrfip_diag_ident_var_handler((struct mstrfip_dev *)mstrfip,
				       pvar, irq);
	/* trtlmsg have been consumed, reset msg count */
	diag->trtl.msg_count = 0;

	return 0;
}

static int mstrfip_handle_app_aper_var_irq(struct mstrfip_desc *mstrfip,
				     struct mstrfip_irq_entry_desc *irq_entry,
				     struct mstrfip_irq *irq)
{
	struct mstrfip_ba_aper_var_set *aper_vars =
					&mstrfip->ba.mcycle->aper_vars;
	/* -1 to remove the IRQ message itself */
	irq->acq_count = mstrfip_trtl_buf_msg_counter(&aper_vars->trtl) - 1;

	/* calls application's var handler */
	if (aper_vars->mstrfip_ident_var_handler != NULL) {
		aper_vars->mstrfip_ident_var_handler((struct mstrfip_dev *)mstrfip,
						     irq);
	}
	else { // no callback registered
		snprintf(mstrfip_errors[MSTRFIP_BA_APER_VAR_NO_CB
					- MSTRFIP_ERROR_CODE_OFFSET],
			 MSTRFIP_DYNAMIC_ERR_MSG_SZ,
			 "%s(), no user callback registered to consume "
			 "aperiodic variable FIP data.", __func__);
		mstrfip_handle_error(mstrfip, MSTRFIP_BA_APER_VAR_NO_CB);
	}
	/* trtlmsg have been consumed in the callback, reset wrnmsg count */
	aper_vars->trtl.msg_count = 0;
	return 0;
}

static int mstrfip_handle_app_per_var_irq(struct mstrfip_desc *mstrfip,
					  struct mstrfip_irq_entry_desc *irq_entry,
					  struct mstrfip_irq *irq)
{
	struct mstrfip_ba_per_var_set *per_vars = &mstrfip->ba.mcycle->per_vars;
	struct mstrfip_data *pvar;
	struct mstrfip_data_priv *pvar_priv;

	/* locate the periodic var which raises the irq */
	pvar = (irq_entry->flags & MSTRFIP_DATA_FLAGS_PROD) ?
		per_vars->prod_varlist[irq_entry->key] :
		per_vars->cons_varlist[irq_entry->key];
	pvar_priv = (struct mstrfip_data_priv *)pvar->priv;

	/* -1 to remove the IRQ message itself */
	irq->acq_count = mstrfip_trtl_buf_msg_counter(&per_vars->trtl) - 1;

	/* calls application's var handler */
	if (pvar_priv->mstrfip_data_handler != NULL) {
		pvar_priv->mstrfip_data_handler((struct mstrfip_dev *)mstrfip,
						pvar, irq);
	}
	else { // no callback registered
		snprintf(mstrfip_errors[MSTRFIP_BA_PER_VAR_NO_CB
					- MSTRFIP_ERROR_CODE_OFFSET],
			 MSTRFIP_DYNAMIC_ERR_MSG_SZ,
			 "%s(), no user callback registered to process "
			 "periodic variable FIP data.", __func__);
		mstrfip_handle_error(mstrfip, MSTRFIP_BA_PER_VAR_NO_CB);
	}
	/* trtlmsg have been consumed in the callback, reset wrnmsg count */
	per_vars->trtl.msg_count = 0;
	return 0;
}

static int mstrfip_handle_app_aper_msg_irq(struct mstrfip_desc *mstrfip,
					   struct mstrfip_irq_entry_desc *irq_entry,
					   struct mstrfip_irq *irq)
{
	struct mstrfip_ba_aper_msg_set *aper_msgs =
					&mstrfip->ba.mcycle->aper_msgs;
	int i;
	struct mstrfip_data *pmsg;
	struct mstrfip_data_priv *pmsg_priv;
	struct mstrfip_data_buf *buf;

	/* locate the periodic var which raises the irq */
	if (irq_entry->flags & MSTRFIP_DATA_FLAGS_PROD)
		buf = &aper_msgs->prod;
	else
		buf = &aper_msgs->cons;

	pmsg = NULL;
	for (i = 0; i < buf->count; ++i) {
		if (irq_entry->key == buf->list[i]->id) {
			pmsg = buf->list[i];
			break;
		}
	}
	if (pmsg == NULL) {
		return -1;
	}
	pmsg_priv = (struct mstrfip_data_priv *)pmsg->priv;

	/* -1 to remove the IRQ message itself */
	irq->acq_count = mstrfip_trtl_buf_msg_counter(&aper_msgs->trtl) - 1;

	/* calls application's var handler */
	if (pmsg_priv->mstrfip_data_handler != NULL) {
		pmsg_priv->mstrfip_data_handler((struct mstrfip_dev *)mstrfip,
						pmsg, irq);
	}
	else { // no callback registered
		snprintf(mstrfip_errors[MSTRFIP_BA_APER_MSG_NO_CB
					- MSTRFIP_ERROR_CODE_OFFSET],
			 MSTRFIP_DYNAMIC_ERR_MSG_SZ,
			 "%s(), no user callback registered to process "
			 "aperiodic message FIP data.", __func__);
		mstrfip_handle_error(mstrfip, MSTRFIP_BA_APER_MSG_NO_CB);
	}
	/* trtlmsg have been consumed in the callback, reset wrnmsg count */
	aper_msgs->trtl.msg_count = 0;
	return 0;
}


/**
 * Lookup table for IRQ handlers (one for each slot)
 */
static const mstrfip_irq_handler_t mstrfip_irq_handler[] ={
	[MSTRFIP_HMQ_O_APP_PER_VAR] =  mstrfip_handle_app_per_var_irq,
	[MSTRFIP_HMQ_O_APP_APER_VAR] = mstrfip_handle_app_aper_var_irq,
	[MSTRFIP_HMQ_O_APP_APER_MSG] = mstrfip_handle_app_aper_msg_irq,
	[MSTRFIP_HMQ_O_DIAG_PER_VAR]= mstrfip_handle_diag_per_var_irq,
	[MSTRFIP_HMQ_O_DIAG_APER_VAR] = mstrfip_handle_diag_aper_var_irq,
};


static void* mstrfip_irq_run(void *arg)
{
	struct mstrfip_desc *mstrfip = (struct mstrfip_desc *)arg;
	int nevt, evt, mq_slot, res, timeout, start_flg;
	struct mstrfip_irq irq = {0};
	struct mstrfip_irq_entry_desc *irq_entry = NULL;
	struct mstrfip_trtl_buf *trtl;
	struct mstrfip_trtl_buf *mfip_irq_trtlbuf[MSTRFIP_HMQ_ACQ_SLOT_COUNT];

	struct polltrtl trtlp[MSTRFIP_HMQ_ACQ_SLOT_COUNT];

	for (mq_slot = 0; mq_slot < MSTRFIP_HMQ_ACQ_SLOT_COUNT; ++mq_slot) {
		trtlp[mq_slot].events = POLLIN | POLLERR;
		trtlp[mq_slot].revents = 0;
		trtlp[mq_slot].idx_hmq = mq_slot;
		trtlp[mq_slot].idx_cpu = MSTRFIP_CPU0;
		trtlp[mq_slot].trtl = mstrfip->trtl;
	}

	// init mstrfip_trtl_buf lookup table
	mfip_irq_trtlbuf[MSTRFIP_HMQ_O_APP_PER_VAR] =
				&mstrfip->ba.mcycle->per_vars.trtl;
	mfip_irq_trtlbuf[MSTRFIP_HMQ_O_APP_APER_VAR] =
				&mstrfip->ba.mcycle->aper_vars.trtl;
	mfip_irq_trtlbuf[MSTRFIP_HMQ_O_APP_APER_MSG] =
				&mstrfip->ba.mcycle->aper_msgs.trtl;
	mfip_irq_trtlbuf[MSTRFIP_HMQ_O_DIAG_PER_VAR] =
				&mstrfip->ba.mcycle->diag.trtl;
	mfip_irq_trtlbuf[MSTRFIP_HMQ_O_DIAG_APER_VAR] =
				&mstrfip->ba.mcycle->diag.trtl;

	mstrfip->ba.irq_run = 1;
	/*
	 * purge mock-turtle queue in case of remaining messages from previous
	 * macro-cycle. This may happen when application switch dynamically
	 * macro-cycle.
	 */
	mstrfip_purge_trtl_hmq(mstrfip);

	/*
	 * poll timeout = 10 % more than macrocycle. Add + 1ms for very short
	 * macrocycle less than 10ms to be sure to geta number bigger than
	 * macrocycle length. (not neccessary to use round for so simple
	 * computation)
	 */
	timeout = (int)(1.1 * (mstrfip->ba.mcycle->cycle_ustime / 1000)) + 1;
	start_flg = 1;
	while(mstrfip->ba.irq_run) {
		/*
		 * man 7 pthreads: according to POSIX.1-2001 and/or POSIX.1-2008
		 * poll is a cancelation point, so no point to call
		 * pthread_testcancel to create a cancellation point.
		 */
		/* timeout is equivalent to 1.1 macrocycle duration.
	 	 * In addition, to avoid a first timeout when the macrocycle starts,
	 	 * because it waits for external event for instance, the first timeout
	 	 * is mulitplied by 2
		 */
		if (start_flg) {
			nevt = trtl_msg_poll(trtlp, MSTRFIP_HMQ_ACQ_SLOT_COUNT, 2 * timeout);
			start_flg = 0;
		}
		else
			nevt = trtl_msg_poll(trtlp, MSTRFIP_HMQ_ACQ_SLOT_COUNT, timeout);
		if (nevt == 0 ) { //nevt=-1: error, nevt=0: time_out
			if (mstrfip->ba.state != MSTRFIP_FSM_RUNNING)
				continue; // not report time out
			snprintf(mstrfip_errors[MSTRFIP_POLL_TIMEOUT - MSTRFIP_ERROR_CODE_OFFSET],
				 MSTRFIP_DYNAMIC_ERR_MSG_SZ,
			 	"%s(), poll timeout (current timeout:%dms).",
			 	__func__, timeout);
			mstrfip_handle_error(mstrfip, MSTRFIP_POLL_TIMEOUT);
			continue;
		}
		else if (nevt < 0) {
			snprintf(mstrfip_errors[MSTRFIP_POLL_TIMEOUT - MSTRFIP_ERROR_CODE_OFFSET],
				 MSTRFIP_DYNAMIC_ERR_MSG_SZ,
			 	"%s(), cannot poll acquisition mq slot:  %s.",
			 	__func__, strerror(errno));
			mstrfip_handle_error(mstrfip, MSTRFIP_POLL_ERROR);
			continue;
		}
		// no error: loop throught all slots till nevt
		for (evt = 0, mq_slot = 0;
		     evt < nevt || mq_slot < MSTRFIP_HMQ_ACQ_SLOT_COUNT;
		     ++mq_slot) {
			if (!(trtlp[mq_slot].revents & POLLIN))
				continue; // no event reported by this slot
			/* event reported: increment evt and process the event */
			++evt;

			trtl = mfip_irq_trtlbuf[mq_slot];
			/* get the turtle message */
			res = mstrfip_read_trtlmsg(mstrfip, mq_slot, trtl);
			if (res)
				continue;
			/* extract from the message the irq entry if any */
			mstrfip_get_irq_entry(mstrfip, trtl, &irq_entry, &irq);
			if (!irq_entry) //message doesn't carry an irq
				continue; // no need to wake_up the client

			mstrfip_irq_handler[mq_slot](mstrfip, irq_entry, &irq);
		}
	}

	return NULL;
}

int mstrfip_irq_start(struct mstrfip_desc *mstrfip)
{
	pthread_attr_t      thread_attr;    // Thread attributes
	struct sched_param  thread_param;   // Thread scheduling parameters

	// Check whether thread should already be running
	if(mstrfip->ba.irq_run) return 1;

	// Configure thread attributes
	if(pthread_attr_init(&thread_attr) == -1) {
		perror("pthread_attr_init");
		return -1;
	}

	pthread_attr_setinheritsched(   &thread_attr, PTHREAD_EXPLICIT_SCHED);
	pthread_attr_setschedpolicy(    &thread_attr, SCHED_FIFO);
// 	pthread_attr_setstacksize(      &thread_attr, PTHREAD_STACK_SIZE);

	if (sched_getparam(getpid(), &thread_param) < -1 )
		return -1;
	thread_param.sched_priority = mstrfip->ba.sw_cfg.irq_thread_prio;
	pthread_attr_setschedparam(&thread_attr, &thread_param);

	// Start mstrfip irq thread
	if (pthread_create(&mstrfip->ba.irq_thread_id, &thread_attr,
	                  mstrfip_irq_run, mstrfip) != 0) {
		perror("pthread_create");
		mstrfip->ba.irq_thread_id = 0;
		pthread_attr_destroy(&thread_attr);
		return -1;
	}
	pthread_attr_destroy(&thread_attr);

	return 0;
}

int mstrfip_irq_stop(struct mstrfip_desc *mstrfip)
{
	if (mstrfip->ba.irq_run == 0)
		return 0;

	/* synchronize properly the thread */
	mstrfip->ba.irq_run = 0;
	if (mstrfip->ba.irq_thread_id) {
		pthread_cancel(mstrfip->ba.irq_thread_id);
		pthread_join(mstrfip->ba.irq_thread_id, NULL);
//		pthread_detach(mstrfip->ba.irq_thread_id);
		mstrfip->ba.irq_thread_id = 0;
	}

	/* irq thread is stopped, discard any pending message */
	mstrfip->ba.mcycle->per_vars.trtl.msg_count = 0;
	mstrfip->ba.mcycle->aper_vars.trtl.msg_count = 0;
	mstrfip->ba.mcycle->aper_msgs.trtl.msg_count = 0;
	mstrfip->ba.mcycle->diag.trtl.msg_count = 0;
	mstrfip->ba.mcycle->diag.trtl.msg_count = 0;;

//	mstrfip_irq_trtlbuf[MSTRFIP_HMQ_O_APP_PER_VAR]->msg_count = 0;
//	mstrfip_irq_trtlbuf[MSTRFIP_HMQ_O_APP_APER_VAR]->msg_count = 0;
//	mstrfip_irq_trtlbuf[MSTRFIP_HMQ_O_APP_APER_MSG]->msg_count = 0;
//	mstrfip_irq_trtlbuf[MSTRFIP_HMQ_O_DIAG_PER_VAR]->msg_count = 0;
//	mstrfip_irq_trtlbuf[MSTRFIP_HMQ_O_DIAG_APER_VAR]->msg_count = 0;

	return 0;
}
