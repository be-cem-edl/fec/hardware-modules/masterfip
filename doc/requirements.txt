# SPDX-FileCopyrightText: 2024 CERN (home.cern)
#
# SPDX-License-Identifier: CC-BY-SA-4.0+

breathe
docutils
sphinx
sphinx-rtd-theme
sphinx-autodoc-typehints
