.. SPDX-FileCopyrightText: 2023 CERN (home.cern)
..
.. SPDX-License-Identifier: CC-BY-SA-4.0+

.. _sw_arch:

=====================
Software Architecture
=====================


Project structure
=================

.. list-table:: 
   :header-rows: 1

   * - Dir
     - Description
   * - doc/
     - complete project documentation
   * - doc/profip-um/
     - User manual for ProFip
   * - doc/profip-utd/
     - Technical documentation for ProFip
   * - doc/ertec/
     - Documentations for Ertec
   * - software/
     - all software
   * - software/rt/
     - firmware for Mockturtle
   * - software/rt/ba/
     - firmware for core0
   * - software/rt/cmd/
     - firmware for core1
   * - software/lib/
     - library for masterFIP
   * - software/ertec/
     - firmware for Ertec 200-P
   * - software/ertec/gsdml/
     - files describing the ProFip device for TIA Portal
   * - software/ertec/pn-utils/
     - ECOS & libraries with Profinet stack
   * - software/ertec/setup/ 
     - scripts that download the necessary tools to 
       build a project on windows
   * - software/ertec/sources/
     - Source Files for ProFIP Application
   * - software/ertec/sources/appl/
     - ProFip application
   * - software/tools/
     - additional tools, e.g. for flashing
   * - tests/
     - scripts for installing ProFIP on FEC
   * - LICENCES/
     - all licences used in MasterFIP

Static architecture
===================

This chapter describes all the modules in the project and their dependencies.

Sofware C Modules
-----------------

    - PN API, ProFip Events and ProFip Main - are modules that initialize
      activity with Profinet. Through these modules, Profinet-related events
      are sent to the application ('connected', 'disconnected',
      'new parameters', 'new data', etc),
    - :ref:`profip` is the main translator module. Based on the information
      about the connection status, it controls the main state machine which
      programs/starts/stops/resets the MasterFip. In this module, variables
      are received from the Profinet network and transferred to MasterFip
      and vice versa,
    - :ref:`pf_diag` is responsible for sending additional information about
      the device to the PLC,
    - :ref:`cas` receives control commands from the PLC. These are then
      processed by the main state machine in the "ProFip" module.
      CAS also collects information about the state of the entire device 
      and sends it to the PLC.
    - :ref:`ertec-libmasterfip` - MasterFip control library,
    - :ref:`mf_handler` - simplifies functions to control the libMasterFip,
    - :ref:`mfh_helper` - auxiliary functions for calculating more
      complicated parameters for MasterFip, 
    - :ref:`fr_timer` - timer for precise time measurements, 
    - :ref:`mqospi` - protocol for communication with RMQ via SPI,
    - :ref:`mstrfip_ctrl` - enables/disables/resets MasterFip via GPIOs,
    - :ref:`nf_node` -  nanoFip nodes management class - initializes, sending,
      receiving and storing variables,
    - :ref:`pf_alarm` - sets/clears Profinet alarms,
    - :ref:`pf_err_m` - saves all faults/problems, 
    - :ref:`pf_utils` - utils, 
    - :ref:`profip_bsp` - starts/stops led blinking for profinet, 
    - :ref:`profip_gpio` - controlls GPIOs, 
    - :ref:`profip_led` - controlls LEDs, 
    - :ref:`profip_wdg` - watchdog, 
    - :ref:`spi_dma` - sending/receiving messages over SPI using DMA.

Dependencies between software modules
-------------------------------------

.. graphviz::

   digraph {

      LIBMASTERFIP [shape="rectangle", label="Lib MasterFip", href="../modules/libmasterfip.html", target="_top"];

      MFHANDLER [shape="rectangle", label="MasterFip handler", href="../modules/mf_handler.html", target="_top"];
      MFHELPER [shape="rectangle", label="MFH helper", href="../modules/mfh_helper.html", target="_top"];

      FRTIMER [shape="rectangle", label="FR timer", href="../modules/fr_timer.html", target="_top"];
      MQOSPI [shape="rectangle", label="MQOSPI", href="../modules/mqospi.html", target="_top"];
      SPIDMA [shape="rectangle", label="SPI DMA", href="../modules/spi_dmadma.html", target="_top"];

      MFCONTROL [shape="rectangle", label="MasterFip control", href="../modules/mstrfip_ctrl.html", target="_top"];

      PFERR [shape="rectangle", label="Error Mgr", href="../modules/pf_err.html", target="_top"];
      PFUTILS [shape="rectangle", label="Utils", href="../modules/pf_utils.html", target="_top"];
      PFLOG [shape="rectangle", label="Logging", href="../modules/profip_log.html", target="_top"];

      CAS [shape="rectangle", label="Control & Status", href="../modules/profip.html", target="_top"];
      NF [shape="rectangle", label="nanoFip node", href="../modules/profip.html", target="_top"];
      PFALARM [shape="rectangle", label="Alarms", href="../modules/pf_alarm.html", target="_top"]; 
      PFDIAG [shape="rectangle", label="Diagnostics", href="../modules/pf_diag.html", target="_top"];
      PFBSP [shape="rectangle", label="BSP", href="../modules/profip_bsp.html", target="_top"];
      PF [shape="rectangle", label="ProFIP", href="../modules/profip.html", target="_top"];

      PFEVENT [shape="rectangle", label="ProFIP Events", href="../modules/profip.html", target="_top"];
      PFMAIN [shape="rectangle", label="ProFIP Main", href="../modules/profip.html", target="_top"];

      PFGPIO [shape="rectangle", label="GPIO", href="../modules/profip_gpio.html", target="_top"];
      PFLED [shape="rectangle", label="LEDs", href="../modules/profip_led.html", target="_top"];

      PFWDG [shape="rectangle", label="WDG", href="../modules/profip_wdg.html", target="_top"];

      PNAPI [shape="rectangle", label="PN API", href="../modules/profip.html", target="_top"];

      PFEVENT-> PNAPI [dir=back];
      PFMAIN-> PNAPI [dir=back];
      PF-> PFEVENT [dir=back];
      CAS-> PFEVENT [dir=back];
      PF-> PFMAIN [dir=back];
      PFDIAG -> PFEVENT [dir=back];
      PFBSP -> PFEVENT [dir=back];
      
      MFHANDLER-> PF [dir=back];
      PFALARM-> PF [dir=back];
      CAS -> PF [dir=back];
      CAS -> MFHANDLER [dir=back];

      MFHELPER -> MFHANDLER [dir=back];
      LIBMASTERFIP -> MFHANDLER [dir=back];
      NF-> MFHANDLER [dir=back];
      LIBMASTERFIP -> NF [dir=back];

      MFCONTROL -> LIBMASTERFIP [dir=back];
      MQOSPI -> LIBMASTERFIP [dir=back];

      SPIDMA -> MQOSPI [dir=back];
      FRTIMER -> MQOSPI [dir=back];

      PFWDG -> PF [dir=back];
      PFWDG -> LIBMASTERFIP [dir=back];

      PFLED -> PF [dir=back];
      PFLED -> PFALARM [dir=back];

      PFGPIO -> PFLED [dir=back];
      PFGPIO -> MQOSPI [dir=back];
      PFGPIO -> MFCONTROL [dir=back];

      PFERR -> PFDIAG [dir=back];
      LIBMASTERFIP -> PFDIAG [dir=back];

      PFLED -> PFBSP [dir=back];

   }

Dynamic architecture
====================

This chapter describes the dependencies between modules over time
for different scenarios.

ProFip startup
--------------

When starting ProFip, it:
   - initializes the Profinet stack,
   - initializes the translator module: starts the thread where main
     state machine is executed.

.. figure:: img/diag5.svg
    :align: center
    :width: 600

Main state machine
------------------

The main state machine has 6 states.
State transitions are based on:
   
   - profinet network information (flags),
   - user commands (cmd).

.. figure:: img/diag0.svg
    :align: center
    :width: 600

Connection to Profinet
----------------------

After recognizing the name of the device (by default it is ProFip), the ProFip:
   - accepts all "NanoFip node" modules,
   - receives parameters for each "NanoFip node" module and saves its
     configuration to temporaty config.
   - after receiving the "All parameters received" signal, it issues
     the "config done" flag to the state machine.

.. figure:: img/diag1.svg
    :align: center
    :width: 600

Disconnection from Profinet
---------------------------

When the ProFip receives a "disconnected" signal, it sends a "disconnected"
flag to the state machine.

.. figure:: img/diag2.svg
    :align: center
    :width: 600

Programming MasterFip
---------------------

MasterFip programming is triggered by a state machine. 
The ProFip translator creates a configuration based on:

   - the received parameters of the NanoFip nodes and
   - the parameters of the device received by the "CAS" module.

The configuration is processed in the MF_Handler module and MasterFip library
and sent via SPI to MasterFip (MockTurtle).

.. figure:: img/diag8.svg
    :align: center
    :width: 600

Starting MasterFip
------------------

MasterFip startup is triggered by a state machine.
A thread is started for MasterFip (for receiving/sending messages)
A "Start" command is sent via SPI to MasterFip.


.. figure:: img/diag9.svg
    :align: center
    :width: 600

Data exchange with PLC (Profinet)
---------------------------------

When ProFip is connected to the Profinet network, data exchange takes place
all the time - when the previous cycle (send/receive) is finished, a new
one begins. Data is written/read from buffers in "NanoFip node" modules.
If the data in the node is not valid, an alarm is raised.

.. figure:: img/diag6.svg
    :align: center
    :width: 600

Data exchange with MasterFip
----------------------------

Data exchange with MasterFip is synchronized with macrocycles.
The library for MasterFip checks the GPIO (if there is a new message in
any RMQ). If it is, the message is read. If there is an IRQ in the message,
a callback to MF_Handler is called. In Callback, messages are read and saved
to NanoFip nodes, and variables are read from NanoFip nodes and sent
to MasterFip.

.. figure:: img/diag7.svg
    :align: center
    :width: 600


Threads
=======

The ProFip application is running on ECOS and runs on several threads.

A lower number is higher priority.

.. list-table:: 
   :header-rows: 1

   * - Name
     - Priority
     - 
     - Description
   * - Watchdog
     - 2 
     - TASK_PRIO_WDG  
     - Verifies that other tasks are execurted correctly
   * - LibMasterFip IRQ 
     - 3
     - TASK_PRIO_LIBMFIP_IRQ_LOW 
     - Reads MasterFip messages and triggers callbacks
   * - ProFip Main
     - 4
     - TASK_PRIO_PROFIP_MAIN_LOW 
     - Executes the main state machine
   * - Alarm
     - 4
     - TASK_PRIO_PROFIP_ALARM_LOW 
     - Processing (sends/clears) alarms for Profinet
   * - LibMasterFip Diag
     - 5
     - TASK_PRIO_LIBMFIP_DIAG_LOW 
     - It periodically sends diagnostic messages to nanoFip nodes


