.. SPDX-FileCopyrightText: 2023 CERN (home.cern)
..
.. SPDX-License-Identifier: CC-BY-SA-4.0+

.. _pf_diag:

===========
Diagnostics
===========

.. doxygenfile:: pf_diag.h