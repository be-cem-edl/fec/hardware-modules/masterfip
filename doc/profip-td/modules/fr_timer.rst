.. SPDX-FileCopyrightText: 2023 CERN (home.cern)
..
.. SPDX-License-Identifier: CC-BY-SA-4.0+

.. _fr_timer:

==================
Free Running Timer
==================

.. doxygenfile:: fr_timer.h