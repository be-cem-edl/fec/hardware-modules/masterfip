.. SPDX-FileCopyrightText: 2023 CERN (home.cern)
..
.. SPDX-License-Identifier: CC-BY-SA-4.0+


.. _introduction:glossary:

============
Glossary
============

.. list-table:: 
   :widths: 25 75
   :header-rows: 0
   
   * - SBC
     - Single Board Computer
   * - PLC
     - Programmable logic controller
   * - RMQ
     - Remote Message Queue
   * - HMQ
     - Host Message Queue
