.. SPDX-FileCopyrightText: 2023 CERN (home.cern)
..
.. SPDX-License-Identifier: CC-BY-SA-4.0+

============
Introduction
============

What is ProFip?
===============

ProFip is part of the
`DI/OT project <https://ohwr.org/project/diot/wikis/home>`_ .

ProFip is a translator between
`PROFINET <https://www.profibus.com/technology/profinet>`_  and
`WorldFIP <https://ohwr.org/projects/cern-fip/wiki/WorldFIP>`_ that allows
for a deterministic low-latency exchange of the cyclical process data between
the two networks. Equipment in radiation communicates only over WorldFIP,
industrial PLCs cannot use WorldFIP directly, so ProFip fills this gap. ProFip
is based on `SVEC <https://ohwr.org/project/svec/wikis/home>`_, with two
mezzanines:

    - `FMC-PROFINET <https://ohwr.org/project/fmc-profinet/wikis/home>`_ , 
    - `FMC-MasterFip <https://ohwr.org/project/fmc-masterfip/wikis/home>`_ .

FMC-PROFINET is a PROFINET IO device (“slave”) card based on the Siemens
ERTEC200-P chip.

**In software/ertec you will find code only for Ertec 200-P.**
For ProFip to work properly, you also need a correctly programmed
bitstream on SVEC and firmware on MockTurtle.

| A brief description of the entire system can be found here: :ref:`sys_arch`.
| A description of the software architecture for Ertec is described here
  :ref:`sw_arch`.

In this document you will find information on how to build a project
and how to program ProFIP.

Setup in the 774 lab
====================

cfv-774-celmarack3

PROFIP-PC.cern.ch (usefull files in C:/Shared)


Building
========

The software for ProFip consists of two parts built separately:
     - ECOS (operating system),
     - Application.
    
ProFIP can be build both on Linux on Windows.

Building on Linux
-----------------

Required programs:
    - gcc-arm-none-eabi
    - make
    - libncurses5-dev
    - libstdc++6
    - build-essential
    - libstdc++-9-dev
    - libstdc++-arm-none-eabi-newlib
    - python3-pip
    - python3-sphinx
    - doxygen 
    - graphviz 
    - git 
    - libstdc++5:i386
    - tclsh

.. code-block:: sh

    # Clone project 
    git clone ssh://git@gitlab.cern.ch:7999/be-cem-edl/fec/hardware-modules/masterfip.git
    git checkout master
    git submodules update --init

    # Run docker
    DOCKER=gitlab-registry.cern.ch/be-cem-edl/evergreen/gitlab-ci/ertec-fw-build:latest
    docker run \
    -v ${HOME}:${HOME} \
    -v /etc/passwd:/etc/passwd:ro \
    -v /etc/shadow:/etc/shadow:ro \
    -v /etc/group:/etc/group:ro \
    -v /tmp/.X11-unix:/tmp/.X11-unix \
    -v /tmp:/tmp \
    -e DISPLAY=$DISPLAY \
    --privileged \
    --net=host \
    -i -w "$PWD" -u $(id -u):$(id -g) -t --rm \
    ${DOCKER} bash

    # Build ECOS
    cd masterfip/software/ertec/pn-utils/ecos
    make -s -j12

    # Build Application
    cd ../../sources
    make -j12

    # Build documentation (user manual and technical documentation)
    cd ../../../doc
    make html 

| The output files are in the directory: 
| ``masterfip/software/ertec/build``.

Building on Windows (Eclipse)
-----------------------------

| Instructions on how to build the ECOS are available in the manual:
| ``Guideline_EvalKit_ERTEC2000P_V4.7.0.pdf`` in chapter ``4.1.6.2``.
| This document is available on an ISO image on PROFIP-PC.cern.ch
  in C:/Shared.

#. Clone project

#. Run script ``masterfip\software\ertec\setup\install.bat`` for installing
   all needed programs.

#. Open Eclipse
   ``masterfip\software\ertec\installed_tools\Eclipse\eclipse\eclipse.exe``. 

#. Import projects: 
    #. ``File -> Open Projects from File system```
    #. Set "Import source" to "profip/sw"
    #. check sw/appl and sw/ecos
    #. Click Next

    .. figure:: img/img_1.png
        :align: center
        :width: 400

#. Build ECOS ``ECOS -> Build Targets -> build ecos-native EB200p SDRAM
   1x32bit NOR-FLASH 32bit``

    .. figure:: img/img_0.png
        :align: center
        :width: 400

#. Build Application ``Projetc -> Build Project``



Programming
===========

Prerequirements:
   - FMC-Profinet V2 shall be placed in SVEC slot 2,
   - programmed FPGA bitstream for ProFIP 
     (otherwise the CPU will be kept in RESET),

The new board can only be programmed using JTAG:
    - program flash using Olimex programmer (:ref:`introduction_program_flash_`)
or
    - program RAM via JTAG (:ref:`introduction_program_ram_`) 
      and then reprogram flash using 'pf_ertec_reprog' tool
      (``sudo /usr/local/3.10.0-957.1.3.rt56.913.el7.x86_64/svec-profip/program-ertec.sh``)
      - ProFIP and WBUART drivers shall be installed - described in User Manual.

**! IMPORTANT !** After programming ProFIP for the first time, the user must set the device name:

.. code-block:: sh

    # open console: 
    screen /dev/ttywbu-pf-0
    name set
    profip # (or other - matching the configuration in TIA Portal)

The programmed board can be reprogrammed (e.g. for updates) over UART
using the program: 'pf_ertec_reprog'
(``sudo /usr/local/3.10.0-957.1.3.rt56.913.el7.x86_64/svec-profip/program-ertec.sh``).

.. _introduction_program_ram_:

Proggramming and Debugging using Eclipse and Segger J-Link
----------------------------------------------------------

**Only RAM can be programmed with whis method!**

Needed device: 
    - `Segger J-Link Pro
      <https://www.segger.com/products/debug-probes/j-link/models/j-link-pro/>`_ .

Needed software:
    - `J-Link GDB server
      <https://www.segger.com/products/debug-probes/j-link/tools/j-link-gdb-server/about-j-link-gdb-server/>`_  ,
    - Eclipse - Instructions on how to install and use are described in this document:
      ``doc\ertec\Guideline_EvalKit_ERTEC2000P_V4.7.0.pdf``.
      The document can be found in the ISO image available
      on PROFIP-PC.cern.ch in the C:/Shared.

Instruction: 
    - ``doc\ertec\Guideline_EvalKit_ERTEC2000P_V4.7.0.pdf chapter 8.4``

The file with "debug configuration for eclipse" can be found here:
``masterfip\software\ertec\sources\Profip2 GDB.launch``. 


.. _introduction_program_flash_:

Programming Flash using OpenOCD and Olimex ARM-USB-TINY-H
---------------------------------------------------------

**Only FLASH memory can be programmed with this method!**

Needed device: 
    - `Olimex ARM-USB-TINY-H
      <https://www.olimex.com/Products/ARM/JTAG/ARM-USB-TINY-H/>`_

Needed SW: 
    - Open OCD ``software/ertec/tools/ocd_v1``, 

Instruction: 
    - `Olimex Manual 3.3.3 <https://olimex.com/Products/ARM/JTAG/_resources/ARM-USB-TINY_and_TINY_H_manual.pdf>`_
