.. SPDX-FileCopyrightText: 2023 CERN (home.cern)
..
.. SPDX-License-Identifier: CC-BY-SA-4.0+

================
Debug interfaces
================

ProFIP provides access to serial ports for ERTEC (FMC-Profinet) and
MockTurtle cores (MasterFip). 

MasterFip UART
==============

| The MasterFip serial port logs information about MasterFip operation.
  To open this console, use the Mock Turtle tty:
| ``sudo cat /dev/ttytrtl--<trtl_dev_id>-0`` (for CPU 0 (ba))
| ``sudo cat /dev/ttytrtl--<trtl_dev_id>-1`` (for CPU 1 (cmd))

.. _ertec_terminal_:

ProFIP UART
===========

The configuration and current status of ProFIP can be checked using the
TTY terminal ``/dev/ttywbu-pf-0``. To connect to the terminal, you can
use, e.g, the screen program: ``screen /dev/ttywbu-pf-0``. To display
all possible commands in the console, enter the command ``?``.

More about ProFIP UART is described here: :ref:`tools-tty`.

.. _ertec_reset_:
