.. SPDX-FileCopyrightText: 2023 CERN (home.cern)
..
.. SPDX-License-Identifier: CC-BY-SA-4.0+


.. _installation:

============
Installation
============

ProFIP is a device based on :
  - 1U VME crate (or bigger),
  - SVEC with two mezzanine cards: FMC-Profinet and FMC-MasterFip and
  - SBC.

| FMC-MasterFip shall be placed in slot 1. 
| FMC-Profinet should be placed in slot 2.

.. figure:: img/installation.png
   :align: center

| 

The ProFIP consists of:
   - hardware (SVEC, FMC-PROFINET, FMC-MasterFip)
   - gateware for SVEC FPGA (``hdl``),
   - ProFIP's drivers (``software/drivers``), 
   - firmware for MasterFIP (``software/rt``),
   - library for MasterFIP (``software/lib``),
   - firmware for Ertec (``software/ertec``),    
   - GSDLM for TIA portal (``software/ertec/gsdml``).

The next subsections describe how to program all ProFIP elements.

.. _configuration_ccde_:

Configuration in CCDE
=====================

After each power reset, the FPGA is cleared. The appropriate configuration
should be entered in CCDE so that the FPGA can be programmed with
the appropriate bitstream after each reboot. Startup script will also install
all necessary linux drivers. It will not program ERTEC 200-P (FMC-Profinet), 
because it's not needed - after reset ERTEC 200-P is still programmed. 

If the ERTEC 200-P (FMC-PROFINET) is unprogrammed, it must be programmed using
JTAG (described in the Technical Documentation or :ref:`installation-profip`).

ERTEC 200-P firmware can be updated over UART (:ref:`pf_ertec_reprog_`). 

The FMC-SVEC-A24 module should be placed in the selected slot with 2
submodules:

   - FMC-MASTERFIP-(31.25k/1M/2.5M) (depending on the target bus speed) - in slot 1
   - FMC-PROFINET V2 - in slot 2.

.. figure:: img/installation-3.png
   :align: center
   :width: 600


An exception must be defined in the FMC-SVEC-A24 module to use
svec-profip drivers.

.. figure:: img/installation-4.png
   :align: center
   :width: 500

After restarting the computer, the scripts will install all drivers
and program the firmware needed for ProFIP.

.. _configuration_mode_masterfip_:

MasterFIP Mode
--------------

If it is necessary to use ProFIP in MasterFIP mode, scripts 
`svec-mode-masterfip` and `svec-mode-profip` can be used to
switch between modes.

.. code-block:: sh

   # switch to MasterFIP mode:
   cd /usr/local/3.10.0-957.1.3.rt56.913.el7.x86_64/svec-profip/
   sh ./svec-mode-masterfip.sh

   # return to ProFIP mode:
   sh ./svec-mode-profip.sh

The following sections describe how to manually program each part of ProFIP
separately (if you are not using a script or want to program specific versions).

.. _installation-masterfip:

Programming MasterFIP 
=====================
The MasterFIP part consists of: gateware, drivers, firmware, library.

- FMC-MasterFip shall be placed in SVEC slot 1.

Depending on whether you want to control WorldFIP via Profinet (translation)
or via SBC (MasterFIP library), you need to program different
:ref:`installation-masterfip-firmware`.

.. _installation-masterfip-drivers:

Linux Drivers
-------------

Drivers to be installed on SBC:
   -	SVEC,
   -	Mock Turtle v. 4. 1. 0,
   -	ProFIP,
   -  Wbuart.

Drivers for MasterFip can be build or taken from MasterFip package registry:
https://gitlab.cern.ch/be-cem-edl/fec/hardware-modules/masterfip/-/releases.

.. code-block:: sh

    # ProFIP driver
    # Download repository
    git clone ssh://git@gitlab.cern.ch:7999/be-cem-edl/fec/hardware-modules/masterfip.git
    git checkout master

    # Build driver
    cd masterfip/software/drivers
    make

    # Install driver
    modprobe -d /usr/local/ mockturtle
    insmod profip_svec_drv.ko

    # WB Uart driver
    # Download repository
    git clone ssh://git@ohwr.org:7999/project/general-cores.git
    git checkout master

    # Build driver
    cd general-cores/software/wb_uart/drivers
    make

    # Install driver
    insmod wb_uart.ko

Gateware
--------

Bitstream for ProFIP can be downloaded from MasterFip artifactory: 
https://gitlab.cern.ch/be-cem-edl/fec/hardware-modules/masterfip/-/releases.

The gateware can be programmed via SBC. The detailed process of programming
the bitstream is described in SVEC documentation.

.. code-block:: sh

    # Program SVEC
    sudo su -
    echo -n PATH_TO_YOUR_BITSTREAMS > /sys/module/firmware_class/parameters/path
    echo -n BITSTREAM_NAME > /sys/bus/vme/devices/slot.02/vme.2/svec-vme.2/firmware_name
    echo $?
    SLOT=2
    /usr/local/bin/vme-scan --slot ${SLOT}
    echo vme.${SLOT} > /sys/bus/vme/drivers/svec-fmc-carrier/unbind
    /usr/local/bin/vme-module --slot ${SLOT} --disable
    ADDR=$(awk -vslot=${SLOT} '$6 == "FMC-SVEC-A24" && $20 == slot {print "0x"$16}' /etc/transfer.ref)
    /usr/local/bin/vme-module --slot ${SLOT} --ader 1,${ADDR},0x39
    /usr/local/bin/vme-module --slot ${SLOT} --enable
    echo vme.${SLOT} > /sys/bus/vme/drivers/svec-fmc-carrier/bind

.. _installation-masterfip-firmware:

Firmware
--------

ProFIP uses the ProFIP version of MasterFip firmware. The firmware consists of
binaries for two CPU’s: “core 0 ba” and “core 1 cmd” (profip-rt-ba.bin,
profip-rt-cmd.bin).

Binaries can be taken from MasterFIP package registry: 
https://gitlab.cern.ch/be-cem-edl/fec/hardware-modules/masterfip/-/releases.

Binaries can be programmed via SBC (Mock Turtle driver shall be installed
correctly):

This firmware version allows to control WorldFIP only via Profinet
(FMC-Profinet-FMC, ERTEC 200-P processor). If you need to control WorldFIP
via SBC (MasterFIP lib), you need to program a different firmware:
masterfip-rt-ba.bin, masterfip-rt-cmd.bin.

.. code-block:: sh

    # Program mockturtle
    sudo dd if=<your_workspace>/masterfip/rt/ba/profip-rt-ba.bin of=/dev/mockturtle/trtl-0004-00
    sudo dd if=<your_workspace>/masterfip/rt/ba/profip-rt-cmd.bin of=/dev/mockturtle/trtl-0004-01

    # Start the CPU's
    echo 0 > /sys/class/mockturtle/trtl-0004-00/reset
    echo 0 > /sys/class/mockturtle/trtl-0004-01/reset

    # Open the console (optional)
    sudo cat /dev/ttytrtl-0004-0
    sudo cat /dev/ttytrtl-0004-1

.. _installation-library:

MasterFip library
-----------------

To control WorldFIP via SBC (using the MasterFIP library) you need to program
another firmware! See :ref:`installation-masterfip-firmware`.

For controlling the MasterFip from SBC (e.g., for testing the WorldFip network)
the MasterFip library shall be build and linked to user program. The MasterFip
library can be downloaded from GitLab:
https://gitlab.cern.ch/be-cem-edl/fec/hardware-modules/masterfip/-/tree/master/software/lib.

.. _installation-profip:

Programming the FMC-Profinet (ERTEC 200-P)
==========================================

Prerequirements:
   - FMC-Profinet V2 shall be placed in SVEC slot 2,
   - programmed FPGA bitstream for ProFIP 
     (otherwise the CPU will be kept in RESET),

|    

The new board can only be programmed using JTAG:
    - program flash using Olimex programmer (:ref:`jtag_reprogram_`)
or
    - program RAM via JTAG (described in the Technical Documentation)
      and then reprogram flash using pf_ertec_reprog tool
      (:ref:`pf_ertec_reprog_`).


**!!! IMPORTANT !!!** After programming ProFIP for the first time, the user must set the device name:

.. code-block:: sh

    # open console: 
    screen /dev/ttywbu-pf-0
    name set
    profip # (or other - matching the configuration in TIA Portal)

If ERTEC 200-P is programmed correctly then it can be reprogrammed
over the UART (:ref:`pf_ertec_reprog_`).


.. _pf_ertec_reprog_:

Programming via UART
--------------------

#. Prerequisites: 
   
   - Ertec must be running and programmed with version 3.0.0 or higher - 
     if it is not, it should be programmed via JTAG (:ref:`jtag_reprogram_`),

   - ProFIP and WBUART drivers shall be installed - (:ref:`configuration_ccde_`).

#. | The easiest way is to execute the script available on FEC:
   | ``sudo ./usr/local/3.10.0-957.1.3.rt56.913.el7.x86_64/svec-profip/program-ertec.sh``
   | It will update to the current ProFIP version.

#. Another way is to build the pf_ertec_prog program manually and install
   the version of the user choice:

   #. Tool for programming over UART is available here: ``software/tools/pf_ertec_reprog``.

   #. To build ertec reprogram tool: ``make``.

   #. Run ``./pf_ertec_reprog -h`` for checking program parameters.

   #. Run ``./ertec_reprogram -f <path to ertec binaries>``

ProFip binaries for Ertec can be downloaded from Package Registry: 
https://gitlab.cern.ch/be-cem-edl/fec/hardware-modules/masterfip/-/releases.

.. _tcp_reprogram_:

Programming via Ethernet (TCP)
------------------------------

Possible to use, but not recommended,

Tool and instruction for programming over TCP are available here: 
https://gitlab.cern.ch/be-cem-edl/fec/hardware-modules/masterfip/-/tree/master/software/ertec/tools/tcpfwloader_linux.

ProFip binaries for Ertec can be downloaded from Package Registry: 
https://gitlab.cern.ch/be-cem-edl/fec/hardware-modules/masterfip/-/releases.

.. _jtag_reprogram_:

Programming via JTAG
--------------------

ERTEC 200-P (processor on FMC-Profinet) flash can be programmed using the Olimex
ARM-USB-TINY-H programmer. The installation process for Olimex drivers
is described in Olimex Manual 3.3.3: 
https://www.olimex.com/Products/ARM/JTAG/_resources/ARM-USB-TINY_and_TINY_H_manual.pdf.  

ProFip binaries for Ertec can be downloaded from Package Registry: 
https://gitlab.cern.ch/be-cem-edl/fec/hardware-modules/masterfip/-/releases.

Script and tools for programming the Ertec are available in ProFip GitLab: 
https://gitlab.cern.ch/be-cem-edl/fec/hardware-modules/masterfip/-/tree/master/software/ertec/tools/ocd_v1

Instructions on how to program RAM can be found in the Technical Documentation for PROFIP.

**!!! IMPORTANT !!!** After programming ProFIP for the first time, the user must set the device name:

.. code-block:: sh

    # open console (on FEC): 
    screen /dev/ttywbu-pf-0
    name set
    profip # (or other - matching the configuration in TIA Portal)
