.. SPDX-FileCopyrightText: 2023 CERN (home.cern)
..
.. SPDX-License-Identifier: CC-BY-SA-4.0+


.. _introduction:description:

============
Introduction
============

ProFIP is a translator between PROFINET and WorldFIP that allows for
a deterministic low-latency exchange of the cyclical process data between
the two networks. ProFIP is part of the DI/OT project. Equipment in radiation
communicates only over WorldFIP, industrial PLCs cannot use WorldFIP directly,
so ProFIP fills this gap.

This manual covers the configuration, installation and diagnostics of the ProFIP. 

.. figure:: img/img8.PNG
    :align: center
    :width: 200

.. _introduction:features:

Features
========

-	WorldFIP configuration and control via PLC (TIA portal), 
-	up to 30 nanoFip nodes,
-	deterministic latency: 

   - 5ms + 2 * WorldFip macrocycle duration for transmitted data,
   - 5ms + 2 * WorldFip macrocycle duration for received data,
   
-	MasterFip diagnostics (via SBC or PLC),
-	ProFIP diagnostics,
-	testing WorldFIP through FEC.

