.. SPDX-FileCopyrightText: 2023 CERN (home.cern)
..
.. SPDX-License-Identifier: CC-BY-SA-4.0+


===============
Troubleshooting
===============

In case of any problems:

#. Check if the console ``/dev/ttywby-pf-0`` exists.

	If not, make sure the ProFIP and WBUART drivers are installed correctly.

#. Launch the console

	``screen /dev/ttywbu-pf-0``

#. Enter the command ``?``. 

	If nothing appears on the screen then:

	- Switch to MasterFIP mode (:ref:`configuration_mode_masterfip_`)
	  and check if WorldFIP can be controlled by the library
	  (:ref:`installation-library`). If so, only FMC-PROFINET is not working
	  properly. If MasterFIP is also not working, both FMCs are not working
	  properly.
	- reprogram the entire ProFIP (reboot FEC) and check
	  if the console ``/dev/ttywby-pf-0`` works properly,
	- if this did not help, try reset the ERTEC-200P device using
	  the :ref:`tools-ertec-reset` and test again,
	- check if the entire setup is assembled correctly (e.g. FMCs are correctly
	  placed in the sockets) (:ref:`installation`) and if it is powered?
	  - does any diode light up? (:ref:`diagnostics-leds`),
	- check if the WBUART driver has been changed. If so, install an older
	  version,
	- try to reprogram the ERTEC-200P using JTAG
	  (detailed description in Technical Documentation).

#. If the TTY console works then:

	- enter the ``name`` command to verify if device name matches the one
	  set in TIA portal,
	- enter the ``ip`` command and verify if device has correct IP address,
	- enter the ``status`` command to see if the device or any node is in
	  an error state,
	- if ProFIP reports an error, try to reset the macrocycle with
	  the ``reset mcycle`` or ``reboot`` command,
	- In case of a ProFIP error, enter the ``alarms`` command to see the error
	  code. It is possible that the configuration (in TIA Portal) is incorrect.
	- In case of a node error, check whether it is connected correctly and
	  whether the size of the consumed variable is set correctly.
	- enter the ``mcycle`` command to verify that the macrocycle is configured
	  as expected.
