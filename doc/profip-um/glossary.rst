.. SPDX-FileCopyrightText: 2023 CERN (home.cern)
..
.. SPDX-License-Identifier: CC-BY-SA-4.0+


.. _introduction:glossary:

============
Glossary
============

.. list-table:: 
   :widths: 25 75
   :header-rows: 0
   
   * - SBC
     - Single Board Computer
   * - PLC
     - Programmable logic controller
   * - SVEC
     - Simple VME FMC Carrier
   * - FMC
     - FPGA Mezzanine Card
   * - ERTEC 200-P
     - processor on FMC-Profinet, for communication with the PROFINET network
