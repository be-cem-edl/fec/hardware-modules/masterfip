.. SPDX-FileCopyrightText: 2023 CERN (home.cern)
..
.. SPDX-License-Identifier: CC-BY-SA-4.0+


.. _config_and_runtime:description:

=======
Modules
=======

.. _module-control-status:

Module "Control & Status"
=========================

"Control & Status" is a module that represents the ProFIP translator.
After connecting the device to the PLC, the PLC sends basic information
about the macrocycle (configurable in module parameters) to ProFIP.
Then - during operation - this module allows to read the current status
of the device and the basic parameters of the macrocycle (via input cyclic
data), and allows to send commands (throught output cyclic data) e.g. "reboot",
"reset macrocycle".

Parameters
----------

List of parameters that are sent from the PLC (configurable via TIA Portal)
to ProFIP at the beginning of the program.

.. list-table:: 
   :header-rows: 1

   * - Name	
     - Acceptable values	
     - Description
   * - turnaround time (us)
     - 0 - 65536	
     - | WorldFip specific parameter. If 0, it is calculated automatically
         based on the bus speed. The default value is 0.
       | This is the time between the end of one WorldFIP frame and the
         start of transmission of the next frame.
   * - wait window time (us)	
     - 0 - 4294967296	
     - | Total length of the entire macrocycle. 
       | If 0, the value is calculated automatically based on bus speed,
         number of nanoFip nodes and its sizes. The default value is 0.
   * - enable masterfip diagnostics	
     - 0, 1	
     - | Enables the MasterFIP diagnostics.
       | If enabled, then it adds a diag node to the macrocycle and sends
         masterfip diagnostic information (similar to SHM in MasterFIP).
         The default value is 0.
   * - aperiodic window time (us)	
     - 0 - 65536	
     - | Duration of the aperiodic window time for diagnostic. 
       | If 0, it is calculated automatically based on the bus speed.
         The default value is 0.

Output data (control)
---------------------

Cyclic data sent from the PLC to ProFIP, used to control ProFIP.

.. figure:: img/modules_cas_2.PNG
    :align: center
    :width: 600

.. list-table:: 
   :widths: 25 25 25 25
   :header-rows: 1

   * - Name	
     - Type	
     - Position	
     - Description
   * - reset	
     - byte	
     - 0
     - On rising edge, ProFIP is reset
   * - reset masterfip	
     - byte	
     - 1	
     - On rising edge, MasterFip is reprogrammed
   * - stop macrocycle
     - byte	
     - 2	
     - If 1, the macrocycle is stopped, If 0, the macrocycle is started
   * - clear all errors	
     - byte	
     - 4	
     - On rising edge, it clears all saved ProFip errors
   * - clear node error counters
     - byte	
     - 5
     - On rising edge, resets error counters for all NanoFIP nodes
   * - start tcp reprogramming
     - byte	
     - 6	
     - On rising edge, starts the tcp reprogramming

Input data (status)
-------------------

Cyclic data received by the PLC cyclically from ProFIP with information
about the device status.

.. figure:: img/modules_cas_1.PNG
    :align: center
    :width: 500

.. list-table:: 
   :widths: 25 25 25 25
   :header-rows: 1

   * - Name	
     - Type	
     - Position
     - Description
   * - ProFip status	
     - byte	
     - 0	
     - | Status of the ProFip
       | 0 - INIT 
       | 1 – CONNECTING (to PROFINET)
       | 2 – PROGRAMMING (macrocycle)
       | 3 – RUNNING
       | 4 - STOPPED
       | 5 - ERROR
       | 6 - RESET
   * - MasterFip status	
     - byte
     - 1	
     - | Status of the MasterFip
       | 0 – INIT
       | 1 – MACROCYCLE NOT PROGRAMMED
       | 2 – MACROCYCLE PROGRAMMING
       | 3 – MACROCYCLE PROGRAMMED & RUNNING
       | 4 – MACROCYCLE PROGRAMMED & STOPPED
       | 5 – MACROCYCLE STOPPING
       | 6 – ERROR
   * - nbr of produced variables	
     - byte
     - 2
     - Number of produced variables in the macrocycle
   * - nbr of consumed variables
     - byte
     - 3
     - Number of consumed variables in the macrocycle
   * - FIP cycle counter
     - word
     - 4
     - Increments by 1 after every MasterFip cycle.
   * - turnaround time (us)
     - word
     - 6
     - | WorldFip specific parameter. If parameter "turnaround time"
       | is set to zero, here you can check what the actual
       | calculated "turnaround time" is.
   * - wait window time (us)
     - dword
     - 8
     - | Total length of the entire macrocycle. If parameter "wait
       | window time" is set to zero, here you can check what 
       |  the actual calculated "wait wind time" is.
   * - | apereriodic variable
       | window time (us)
     - dword
     - 12
     - | Lenght of aperiodic variable window. If parameter "apereriodic
       | variable window time" is set to zero, here you can check
       | what the actual calculated "apereriodic variable window
       | time" is.
   * - irq thread tick
     - byte
     - 16	
     - | Counter to verify if the device is alive. 
       | It increments only during "ProFIP RUNNING" state.
   * - pf thread tick
     - byte
     - 17
     - Counter to verify if the device is alive.
   * - bus speed
     - byte
     - 18
     - | WorldFip bus speed: 
       | 0 – 32.25 kbps
       | 1 – 1 Mbps
       | 2 – 2 Mbps
   * - tcp reprogramming
     - byte
     - 19	
     - '1' if the TCP Flash reprogramming mode is enabled.
   * - Major version
     - byte
     - 21	
     - ProFip Major Version
   * - Minor version
     - byte
     - 22	
     - ProFip Minor Version
   * - Patch version
     - byte
     - 23	
     - ProFip Patch Version
   * - Git version
     - dword
     - 24	
     - Git SHA
   * - MasterFIP Git version
     - dword
     - 24	
     - Git SHA of running RT application.

.. _module-control-status-alarms:

Alarms
------

List of potential alarms (with description) that "Control & Status" module
can send. These alarms are related to the inability to start translation
due to incorrect configuration or device error.

.. list-table:: 
   :widths: 20 40 40 
   :header-rows: 1

   * - Name	
     - Description	
     - Handling
   * - Software Problem
     - Cannot start macrocycles due to a bug in the software.
     - Please contact ProFIP developers.
   * - Wrong FPGA bitstream
     - The FPGA on SVEC and/or MasterFip (on Mockturtle) is not programmed
       correctly.
     - Check that SVEC and/or MasterFIP are programmed correctly.
   * - Wrong MasterFip version
     - The FPGAs in SVEC and MasterFip are programmed correctly, but
       the wrong version was used. 
     - Reprogram SVEC and MasterFip with correct versions.
   * - Wrong configuration - empty macrocycle
     - No NanoFIP nodes are added to the configuration (nor MasterFip
       diagnostics is  enabled), so the macrocycle cannot be created.
     - Insert more NanoFIP modules into slots or enable MasterFIP diagnostics.
   * - Wrong configuration - incorrect macrocycle
     - Wait window time is shorter than the minimum required.
     - Extend wait window, or use parameter 0 (automatically calculated).
   * - Wrong configuration - duplicated agents ID
     - Two NanoFIP nodes has the same agt ID.
     - Change the agt id of one node.

.. _module-diagnostics:

Module "Diagnostics"
====================

| The MasterFIP Diagnostics Module is an optional module.
| The module can be placed in slot 2.
| To use it, "MasterFip diagnostics" must be enabled in
  the :ref:`module-control-status`. Otherwise all values in the module
  will be 0.
| This module contains a list of parameters for MasterFip diagnostics
  and it has the same structure as the shared memory (SHM) in the MasterFip
  library for FEC.
| The same data can also be read from the :ref:`module-control-status`
  using acyclic communication (record id: 90, record lenght: 180).

Input data
----------

.. list-table:: 
   :widths:  25 25 25 25
   :header-rows: 1

   * - Name	
     - Type	
     - Position
     - Description
   * - building_id	
     - byte
     - 8	
     - retrieved from ident diag var
   * - system_id
     - byte
     - 9	
     - retrieved from ident diag var
   * - segment_id
     - byte
     - 10	
     - retrieved from ident diag var	
   * - com_ok_count	
     - dword
     - 12 	
     - count successful consumed diag var
   * - com_fault_count
     - dword
     - 16 	
     - count failed consumed diag var
   * - com_recover_count	
     - dword
     - 20	
     - count transition between BAD to OK
   * - bad_data_count
     - dword
     - 24	
     - count data mismatch between read/write
   * - cycle_count
     - dword
     - 28	
     - cycle counter
   * - ctrl 0
     - byte
     - 32	
     - byte send on the current cycle prod diag var
   * - ctrl 1
     - byte	
     - 33	
     - byte sent during the previous cycle prod diag var
   * - acq 0
     - byte	
     - 34	
     - data acquired using the consumed diag var
   * - acq 1	
     - byte	
     - 35	
     - data acquired during the previous cycle consumed diag var
   * - var_status	
     - byte	
     - 36
     - | status of the consumed diag var, 
       | bitfield, 
       | bit 0 is set when refresh bit is set in variable, 
       | bit 2 is set when significant bit is set in variable
   * - com_first_ok_date
     - dword	
     - 40
     - last transition time from FAULT to OK
   * - com_first_fault_date
     - dword
     - 44
     - last transition time from OK to FAULT
   * - last_cycle_date
     - dword
     - 48
     - last cycle time
   * - start_time	
     - dword	
     - 52	
     - application start time
   * - | present_list 
       | for nodes N*32 to N*32 + 7,
       | 0 <= N <= 31	
     - array
     - 56 + N
     - | list of agents presents for nodes N*32 to N*32 + 7, 
       | bitfield, 
       | bit M is set when agent with address N*32 + M is present
   * - | present_theoric_list 
       | for nodes N*32 to N*32 + 7, 
       | 0 <= N <= 31
     - array	
     - 88 + N
     - | theoretic list of agents presents for nodes N*32 to N*32 + 7, 
       | bitfield, 
       | bit M is set when agent with address N*32 + M is present
   * - | absent_list 
       | for nodes N*32 to N*32 + 7, 
       | 0 <= N <= 31
     - array
     - 120 + N
     - | list of agents absents for nodes N*32 to N*32 + 7, 
       | bitfield, 
       | bit M is set when agent with address N*32 + M is absent
   * - com_status
     - word	
     - 152
     - | summary giving an overall of the status of the bus, 
       | 0 - no error at all with diag var, 
       | 1 - absent agent are detected, 
       | 2 - mismatch between read/write diag data, 
       | 3 - diag var status is faulty: hw error, 
       | 4 - diag var is faulty + agent is missing, 
       | 5 - no irq received poll leaves by timeout
   * - tx_ok_count
     - word
     - 154	
     - number of tx ok received frames between two consecutives get report
   * - tx_bad_count
     - word	
     - 156	
     - number of tx bad received frames between two consecutives get report
   * - rx_bad_count	
     - word	
     - 158	
     - number of rx bad received frames between two consecutives get report
   * - rx_ok_count
     - word
     - 160	
     - number of rx ok received frames between two consecutives get report
   * - ch_status
     - word	
     - 162	
     - idk
   * - comp_cycle_uslength	
     - dword	
     - 164
     - a
   * - lib_vers	
     - dword
     - 168	
     - a

.. _module-nanofip-node:

Module "NanoFip node"
=====================

The module represents a single NanoFip agent, the module comes in 12 different
types:

    -	NanoFip node, 124 bytes, input,
    -	NanoFip node, 124 bytes, output,
    - NanoFip node, 124 bytes, IO,
    -	NanoFip node, 64 bytes, input,
    -	NanoFip node, 64 bytes, output,
    -	NanoFip node, 64 bytes, IO,
    -	NanoFip node, 32 bytes, input,
    -	NanoFip node, 32 bytes, output,
    -	NanoFip node, 32 bytes, IO,
    -	NanoFip node, 16 bytes, input,
    -	NanoFip node, 16 bytes, output,
    -	NanoFip node, 16 bytes, IO.

One NanoFip agent can be represented by one module (which contains produced
and consumed variables) or two modules (one for produced variables and one for
consumed variables). The NanoFip node shall be configured with a proper agent
ID. NanoFip nodes could be placed in slots 3 – 32.

The amount of cyclic data transferred in PROFINET in one cycle is limited
to 1440 bytes produced and 1440 bytes consumed. Therefore, the maximum number
of modules is limited depending on the module size:

- NanoFIP 16 I/O - maximum 30 modules,
- NanoFIP 32 I/O - maximum 30 modules,
- NanoFIP 64 I/O - maximum 20 modules,
- NanoFIP 124 I/O - maximum 10 modules.

If the :ref:`module-diagnostics` is used, the maximum number of "NanoFip node"
modules is lower.

This module also allows you to read the identification variable for the agent.
The "Enable NanoFip identification" parameter should be enabled. More here:
:ref:`module-nanofip-records`.

Parameters
----------

Before programming the PLC, correct parameters for each "NanoFIP node" module
shall be set. Each "NanoFIP node" module must have a unique AGT ID. There
cannot be two nodes with the same ID connected to the bus. The size of
a variable consumed or produced can be equal to or less than the maximum size
for the module. If the size of the "consumed" variable does not match the size
of the variable sent by the NanoFIP node, the module will report an error
(see :ref:`module-nanofip-alarms`).

.. list-table:: 
   :widths: 25 25 50
   :header-rows: 1

   * - Name	
     - Acceptable values
     - Description
   * - WorldFip node agent ID
     - 0 - 255	
     - WorldFip node agent ID
   * - Size of produced variable (bytes)
     - 1 - (submodule size)
     - Description
   * - Size of consumed variable (bytes)
     - 1 - (submodule size)
     - Description
   * - Enable NanoFip identification
     - 0, 1
     - | Enables the possibility of reading
       | the identification variable from the NanoFIP
       | node by reading the record (record ID: 90).
   * - Enable NanoFip reset
     - 0, 1
     - | Enables the possibility of writting
       | the reset variable to the NanoFIP node
       | by writtig the record (record ID: 95).
       | This is a reset that resets the DIOT Systemboard.

Input Data
----------

The input variables of the "NanFIP node" module consist of the consumed
variable received from the node and 6 bytes of status, which indicates
whether the variable was received correctly.

    .. figure:: img/modules_nfnode_2.PNG
        :align: center
        :width: 700

.. list-table:: 
   :widths: 25 25 25 25
   :header-rows: 1

   * - Name	
     - Type	
     - Position
     - Description
   * - consumed variable
     - array
     - | 0 : (m - 1)
       | where m is 
       | maximum size 
       | of NanoFip
       | consumed
       | variable
     - Variable consumed from NanoFIP node
   * - cons_status
     - byte
     - m
     - | Status of the consumed variable
       | 0 - DATA_OK
       | 1 - FRAME ERROR
       | 2 - PAYLOAD ERROR
       | 3 - FRAME NOT RECEIVED
       | 4 - NODE NOT EXISTS (e.g. macrocycle is not programmed)
       | 5 - DATA_EXPIRED (e.g. profip device error)
       | 6 - WRONG_SIZE
       | In the case of a status other than 0, if translation is running,
       | an appropriate Alarm is reported (See :ref:`module-nanofip-alarms`)
   * - prod_status
     - byte
     - m + 1
     - | Status of the produced variable
       | 0 - OK
       | 4 - NODE NOT EXISTS (e.g. macrocycle is not programmed)
       | 5 - DATA_EXPIRED (e.g. profip device error)
       | 6 - WRONG_SIZE
       | In the case of a status other than 0, if translation is running,
       | an appropriate Alarm is reported (See :ref:`module-nanofip-alarms`)
   * - cons_var_cnt
     - byte
     - m + 2
     - Increases by 1 each time new data is received
   * - prod_var_cnt
     - byte
     - m + 3
     - Increases by 1 each time data has been sent
   * - cons_prev_status
     - byte
     - m + 4
     - | Status of the prevoius consumed variable
       | 0 - DATA_OK
       | 1 - FRAME ERROR
       | 2 - PAYLOAD ERROR
       | 3 - FRAME NOT RECEIVED
   * - cons_err_cnt
     - byte
     - m + 5
     - | Counter of incorrectly received frames. 
       | Increments only if the current number of errors is less than 255,
       | and if the previous status was different.
  
Output Data
-----------

Cyclic data from this module is sent to the corresponding NanoFIP agent.

.. list-table:: 
   :header-rows: 1

   * - Name	
     - Type	
     - Position
     - Description
   * - produced variable
     - array
     - | 0 : (m - 1)
       | where m is maximum size of
       | NanoFip produced variable
     - Variable produced for the NanoFIP node

.. _module-nanofip-records:

Records
-------

90 - read identification variable
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The NanoFIP module allows to read the identification variable through
the record (acyclic communication). To make the identification variable
available, select the "Enable NanoFip identification" option in
the module parameters.

| Record ID: 90,
| Record size: 8,
| The record can be read using a "RDREC" block.

    .. figure:: img/modules_nfnode_1.PNG
        :align: center
        :width: 400

.. _module-nanofip-alarms:

95 - write reset variable
^^^^^^^^^^^^^^^^^^^^^^^^^

Writing to this record resets the nanoFIP node. To make
the reset variable available, select the "Enable NanoFip
reset" option in the module parameters. Selecting this
parameter will add an additional variable to the macrocycle.

| Record ID: 95,
| Record size: any,
| The record can be written using a "WRREC" block.

    .. figure:: img/modules_nfnode_3.PNG
        :align: center
        :width: 400

Alarms
------

Alarms are reported if ProFIP translation is running and the status of
the consumed or produced variable is different than 0. Different alarms
are reported for different variable statuses.

.. list-table:: 
   :widths: 20 40 40 
   :header-rows: 1

   * - Name	
     - Description	
     - Handling
   * - Data frame error
     - | Variable received from a node, but frame fault occurred 
       | (CRC or bad n bytes) or variable not received from a node (timeout).
     - | Check that the node is properly connected and working properly. 
       | You can use masterFip diagnostics to check if the node is visible
       | on the bus (presence). Check whether the size of the consumed
       | variable is set correctly.
   * - Data payload error
     - | Variable received from a node, but payload fault occurred: 
       | Bits 'freshed' or 'significant' was not set.
     - | Check that the node is properly connected and working properly. 
       | You can use masterFip diagnostics to check if the node is visible
       | on the bus (presence). Check whether the size of the consumed
       | variable is set correctly.
   * - Data not received
     - Variable not received from a masterFip in last macrocycle.
     - Please contact ProFIP developers.
   * - Produced data not sent
     - MasterFip's macrocycle stopped sending variables.
     - Try reset MasterFip or contact ProFIP developers.
   * - Consumed data not reveived
     - MasterFip's macrocycle stopped receiving variables.
     - Try reset MasterFip or contact ProFIP developers.



