<!---
  SPDX-License-Identifier: CC-BY-SA-4.0+
  SPDX-FileCopyrightText: 2019 CERN
-->

# MasterFIP

MasterFIP is the electronics board that along with its associated gateware, drivers and
software libraries acts as a master for WorldFIP communication. The masterFIP
implements only those WorldFIP services that are essential for the CERN applications:
exchange of periodic variables, aperiodic unacknowledged messages and aperiodic
SMMPS variables and reception of aperiodic acknowledged messages. The project is
developed as part of the WorldFIP insourcing project, driven mainly by the obsolescence
of the Alstom WorldFIP components.

## Documentation

A description of the MasterFIP project can be found here: https://ohwr.org/project/masterfip/wikis/home.
There are also links to the Functional Specification and Design Guide.

## How to use MasterFIP?

1. Program SVEC. Gateware (.bin) for MasterFIP can be taken from GitLab package registry (https://gitlab.cern.ch/cohtdrivers/masterfip/-/packages).
2. Program firmware and start Mockturtle (both cores). Firmware (.bin) for MasterFIP can be taken from GitLab package registry (https://gitlab.cern.ch/cohtdrivers/masterfip/-/packages).
3. Link your program to MasterFIP and Mockturtle library

<code>MFIP ?= $(TOP_DIR)/software
TRTL ?= $(TOP_DIR)/dependencies/mockturtle/software
CFLAGS += -I$(MFIP) -I$(MFIP)/include -I$(MFIP)/lib
LDLIBS += -Wl,-Bstatic -L$(MFIP)/lib -L$(TRTL)/lib
LDLIBS += -lmasterfip -lmockturtle
LDLIBS += -Wl,-Bdynamic -lpthread -lm -lrt</code>

## ProFIP

ProFIP is a translator between PROFINET and MasterFIP networks. It allows to control the MasterFIP network (and NanoFIP nodes) via PROFINET.
ProFIP uses the same firmware and libraries as MasterFIP, but has different gateware (the current version of MasterFIP runs on SPEC, ProFIP requires SVEC),
and additional firmware for the ERTEC processor.
ProFIP is developed on the /profip_master branch.
Detailed information for PROFIP can be found in the /software/ertec directory and documentation /doc.
